# plot by response wrt. sextupole displacement

reset

# dy
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key center top #set position of the titles of points of lines

set output "MPIP_DY_SF.eps"
set xlabel "{/Symbol D}y [mm]"
set ylabel "DX [um]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/SF6FF_DY.dat' u ($1*1e3):($17*1e6) pt 2 ps 1.5 lc rgb 'black' lw 3 w lp title "SF6" ,\
      './data/SF5FF_DY.dat' u ($1*1e3):($17*1e6) pt 2 ps 1.5 lc rgb 'black' lw 3 w lp title "SF5" ,\
      
# dy
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key center top #set position of the titles of points of lines

set output "MPIP_DX_SK.eps"
set xlabel "{/Symbol D}y [mm]"
set ylabel "DY [um]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/SK1FF_DY.dat' u ($1*1e3):($17*1e6) pt 2 ps 1.5 lc rgb 'black' lw 3 w lp title "SK1"