# plot by response wrt. sextupole displacement

reset

# dx = 0
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key center top #set position of the titles of points of lines

set output "IPbety_SF6FF.eps"
set xlabel "{/Symbol D}y [mm]"
set ylabel "{/Symbol b}_y^{1/2}  [mm]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/SF6FF_Y.dat' u ($1*1e3):(sqrt($2*1e3)) pt 2 ps 1.5 lc rgb 'black' title "SF6" ,\
      './data/SF5FF_Y.dat' u ($1*1e3):(sqrt($2*1e3)) pt 5 ps 1.5 lc rgb 'blue' title "SF5" ,\
      './data/SD4FF_Y.dat' u ($1*1e3):(sqrt($2*1e3)) pt 7 ps 1.5 lc rgb 'red' title "SD4" ,\
      './data/SF1FF_Y.dat' u ($1*1e3):(sqrt($2*1e3)) pt 9 ps 1.5 lc rgb 'orange' title "SF1" ,\
      './data/SD0FF_Y.dat' u ($1*1e3):(sqrt($2*1e3)) pt 13 ps 1.5 lc rgb 'cyan' title "SD0"

# dx = 1e-3
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "IPbety_SF6FF_Y_DX=1e-3.eps"
set xlabel "{/Symbol D}x [mm]"
set ylabel "{/Symbol b}_y^{1/2}  [mm]"

# set grid off
set xrange[-2:2]
# set format x "%.0e"
set yrange[0:10]

plot  './data/SF6FF_X.dat' u ($1*1e3):(sqrt($2*1e3)) pt 2 ps 1.5 lc rgb 'black' lw 3 w lp title "SF6" ,\
      './data/SF5FF_X.dat' u ($1*1e3):(sqrt($2*1e3)) pt 5 ps 1.5 lc rgb 'blue' lw 3 w lp title "SF5" ,\
      './data/SD4FF_X.dat' u ($1*1e3):(sqrt($2*1e3)) pt 7 ps 1.5 lc rgb 'red' lw 3 w lp title "SD4" ,\
      './data/SF1FF_X.dat' u ($1*1e3):(sqrt($2*1e3)) pt 9 ps 1.5 lc rgb 'orange' lw 3 w lp title "SF1" ,\
      './data/SD0FF_X.dat' u ($1*1e3):(sqrt($2*1e3)) pt 13 ps 1.5 lc rgb 'cyan' lw 3 w lp title "SD0"

