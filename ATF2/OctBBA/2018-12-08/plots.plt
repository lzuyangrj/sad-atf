# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_DY.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Ver. orbit IPB [um]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370.dat' u ($1*1e3):($3*1e6) pt 5 ps 1.5 lc rgb 'black' lw 3 w lp t "#1" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370.dat' u ($1*1e3):($3*1e6) pt 7 ps 1.5 lc rgb 'red' lw 3 w lp t "#2"


##################################
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "figures/BBA_OCT1_DY=1e-3_DX.eps"
set xlabel "{/Symbol D}{/Italic x} [mm]"
set ylabel "Ver. orbit IPB [um]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DY=1e-3_DX_K3=-370.dat' u ($1*1e3):($3*1e6) pt 5 ps 1.5 lc rgb 'black' lw 3 w lp t "#1" ,\
      './data/BBA_OCT1_DY=1e-3_DX_K3=370.dat' u ($1*1e3):($3*1e6) pt 7 ps 1.5 lc rgb 'red' lw 3 w lp t "#2"

