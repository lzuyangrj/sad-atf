# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set border lw 2
set key left top #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_IPC_hor_wJitter.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Hor. orbit IPC [um]"

# set grid off
set yrange[-20:40]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370_IPC.dat' u ($1*1e3):($2*1e6) pt 0 ps 0 lc rgb 'red' lw 5 w lines t "",\
      './data/OCT1_DX=1e-3_K3=-370_IPBPMC_mshots.dat' u ($1*1e3):($2*1e6):($3*1e6/sqrt(200)) pt 5 ps 1.5 lc rgb 'red' lw 5 w yerr t "+50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370_IPC.dat' u ($1*1e3):($2*1e6) pt 0 ps 0 lc rgb 'blue' lw 5 w lines t "" ,\
      './data/OCT1_DX=1e-3_K3=370_IPBPMC_mshots.dat' u ($1*1e3):($2*1e6):($3*1e6/sqrt(200)) pt 5 ps 1.5 lc rgb 'blue' lw 5 w yerr t "-50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=-370_IPC.dat' u ($1*1e3):($3*1e6) lt 6 lc rgb 'black' lw 5 w lines t "SIGX"


############################################

reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left center #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_MPIP_hor_wJitter.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Hor. MPIP [um]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
set yrange[-20:82]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370_MPIP.dat' u ($1*1e3):($2*1e6) lc rgb 'red' lw 5 w lines t "" ,\
      './data/OCT1_DX=1e-3_K3=-370_MPIP_mshots.dat' u ($1*1e3):($2*1e6):($3*1e6/sqrt(200)) pt 5 ps 1.5 lc rgb 'red' lw 5 w yerr t "+50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370_MPIP.dat' u ($1*1e3):($2*1e6) pt 13 ps 1.5 lc rgb 'blue' lw 5 w lines t "" ,\
      './data/OCT1_DX=1e-3_K3=370_MPIP_mshots.dat' u ($1*1e3):($2*1e6):($3*1e6/sqrt(200)) pt 5 ps 1.5 lc rgb 'blue' lw 5 w yerr t "-50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=-370_MPIP.dat' u ($1*1e3):($3*1e6) lc rgb 'black' lw 5 w lines t "SIGX"

