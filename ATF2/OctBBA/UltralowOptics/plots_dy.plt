# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set border lw 2
set key left top #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_IPC_hor.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Ver. orbit IPC [um]"

# set grid off
set yrange[-20:40]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370_IPC.dat' u ($1*1e3):($2*1e6) pt 5 ps 1.5 lc rgb 'red' lw 5 w lp t "+50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370_IPC.dat' u ($1*1e3):($2*1e6) pt 13 ps 1.5 lc rgb 'blue' lw 5 w lp t "-50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=-370_IPC.dat' u ($1*1e3):($3*1e6) pt 7 ps 1.5 lc rgb 'black' lw 5 w lines t "SIGX" ,\


##############
reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set border lw 2
set key left center #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_IPC_ver.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Ver. orbit IPC [um]"

# set grid off
set yrange[-20:80]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370_IPC.dat' u ($1*1e3):($4*1e6) pt 5 ps 1.5 lc rgb 'red' lw 5 w lp t "+50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370_IPC.dat' u ($1*1e3):($4*1e6) pt 13 ps 1.5 lc rgb 'blue' lw 5 w lp t "-50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=-370_IPC.dat' u ($1*1e3):($5*1e6) pt 7 ps 1.5 lc rgb 'black' lw 5 w lines t "SIGY"


############################################

reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set border lw 2
set key left center #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_MPIP_hor.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Hor. MPIP [um]"

# set grid off
set yrange[-20:100]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370_MPIP.dat' u ($1*1e3):($2*1e6) pt 5 ps 1.5 lc rgb 'red' lw 5 w lp t "+50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370_MPIP.dat' u ($1*1e3):($2*1e6) pt 13 ps 1.5 lc rgb 'blue' lw 5 w lp t "-50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=-370_MPIP.dat' u ($1*1e3):($3*1e6) pt 7 ps 1.5 lc rgb 'black' lw 5 w lines t "SIGX"


##############
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set border lw 2
set key left center #set position of the titles of points of lines

set output "./figures/BBA_OCT1_DX=1e-3_MPIP_ver.eps"
set xlabel "{/Symbol D}{/Italic y} [mm]"
set ylabel "Ver. MPIP [um]"

# set grid off
set yrange[-50:500]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/BBA_OCT1_DX=1e-3_DY_K3=-370_MPIP.dat' u ($1*1e3):($4*1e6) pt 5 ps 1.5 lc rgb 'red' lw 5 w lp t "+50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=370_MPIP.dat' u ($1*1e3):($4*1e6) pt 13 ps 1.5 lc rgb 'blue' lw 5 w lp t "-50 A" ,\
      './data/BBA_OCT1_DX=1e-3_DY_K3=-370_MPIP.dat' u ($1*1e3):($5*1e6) pt 7 ps 1.5 lc rgb 'black' lw 5 w lines t "SIGY"

