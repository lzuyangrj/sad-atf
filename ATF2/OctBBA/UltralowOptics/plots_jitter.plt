# plot jitter vs. beam size
# 2019.01.10

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set border lw 2
set key left top #set position of the titles of points of lines

set output "./figures/Testjitter_hor.eps"
set xlabel "{/Italic s} [m]"
set ylabel "Hor. Jitter [um]"

# set grid off
# set yrange[-20:30]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/Test_jitter.dat' u 2:($5*1e6):($6*1e6) pt 5 ps 1.5 lc rgb 'black' lw 5 w yerr t "Jitter" ,\
      './data/Test_jitter.dat' u 2:($3*1e6) lc rgb 'red' lw 5 w lines t "SIGX"


set output "./figures/Testjitter_ver.eps"
set xlabel "{/Italic s} [m]"
set ylabel "Ver. Jitter [um]"

# set grid off
# set yrange[-20:30]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/Test_jitter.dat' u 2:($7*1e6):($8*1e6) pt 5 ps 1.5 lc rgb 'black' lw 5 w yerr t "Jitter" ,\
      './data/Test_jitter.dat' u 2:($4*1e6) lc rgb 'red' lw 5 w lines t "SIGY"


set output "./figures/Testjitter_averagePos.eps"
set xlabel "{/Italic s} [m]"
set ylabel "Orbit [um]"

# set grid off
set yrange[-30:30]
# set format x "%.0e"
# set yrange[0:35]

plot  './data/Test_jitter.dat' u 2:($5*1e6) pt 5 ps 1.5 lc rgb 'blue' lw 5 w lp t "<x>" ,\
      './data/Test_jitter.dat' u 2:($7*1e6) pt 7 ps 1.5 lc rgb 'orange' lw 5 w lp t "<y>"