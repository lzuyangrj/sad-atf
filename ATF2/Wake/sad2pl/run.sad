! title: ATF2 ultra-low bety optics matching
! 2018.11.16
! modified from Okugi-san's script for nominal optics matching
! path of lattice and .n files have to updated wrt. machine

 ON ECHO; OFF CTIME;

 read "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)

 MARK   IEX     =(AX =-.7155662723894454 BX =1.1626355159010389   AY =-3.678958491678835   BY =4.534050395633643  EX =.00949075154435475   EPX =.004192920073472923 DP =.0008 EMITX =1.2e-09 EMITY =1.2e-11 )
 ;

 FFS USE=ATF2;

 SeedRandom[100];
 
 FFS["NPARA = 8"];
 path0 = "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/";
!Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 
 TRPT;
 INS;
 FFS["cal"];

! LoadATF2Optics["ATF2_Ultralow_Jun17_0"];
 LoadATF2Optics["ATF2_June21_20bx0p3by_df"];
 FFS["cal"];

 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 FFS["cal"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

 twissIP = Twiss[All, IP];
 {axIP, bxIP, ayIP, byIP} = {#1, #2, #4, #5}&@@twissIP;

 FREE AXI BXI AYI BYI;
 FIT OTR0X BX 5.349; 
 FIT OTR0X AX -2.5613;
 FIT OTR0X BY 9.418; 
 FIT OTR0X AY 4.70;
 Do[FFS["go"],{II,100}];
 FFS["cal"]; 
 FIT REJECT TOTAL;
 FIX AXI BXI AYI BYI;

! FREE AXI BXI;
! FIT IP AX 0;
! FIT IP BX 120e-3;
! Do[FFS["go"],{II,100}];
! FFS["cal"]; 
! FIT REJECT TOTAL;
! FIX AXI BXI AYI BYI;
 
 (* beam waist scanning *)
 FREE QD0FF QF1FF;
 FIT IP AXM 10;
 FIT IP AYM 10;
 FFS["go"];
 FIT IP AX  0;
 FIT IP AY  0;
 Do[FFS["go"],{II,100}]; FFS["cal"];

! FREE AXI BXI AYI BYI;
! FIT IP BX 90; 
! FIT IP AX 0;
! FIT IP BY 30e-6; 
! FIT IP AY 0;
! Do[FFS["go"],{II,100}];
! FFS["cal"]; 
! FIT REJECT TOTAL;
! FIX AXI BXI AYI BYI;
1 Do[FFS["go"],{II,100}]; FFS["cal"];

 (* matching initial horizonal dispersion *)
 FREE EXI EPXI;
 FIT OTR0X EX 0;
 FIT OTR0X EPX 0;
 Do[FFS["go"],{II,100}];
 FIT REJECT TOTAL;
 FIX EXI EPXI;
 Do[FFS["go"],{II,100}]; FFS["cal"];

! --> export placet input (ultra-low)
 tag = "21jun19_20bx0p3by"; 
 fn1 = "./data/Quad_Sext_"//tag//".dat";
 fn2 = "./data/Twiss_"//tag//".dat";
 
 quadK1L = sextK2L = {};
 quadName = Flatten@{"QF6X", "QF19X", "QD20X", "QF21X", Element["NAME", "QM*FF"], "QF1FF", "QD0FF"};
 sextName = Element["NAME", "S{FD}*FF"];
 Scan[AppendTo[quadK1L, Element["K1", #]]&, quadName];
 Scan[AppendTo[sextK2L, Element["K2", #]]&, sextName];

 $FORM="S18.7";
 fw = OpenWrite[fn1];
 WriteString[fw, "Mag.\t Type\t K1L/K2L\n"]
 Scan[WriteString[fw, ToString[#], '\t', 'K1L\t', Element["K1", #], '\n']&, quadName];
 Scan[WriteString[fw, ToString[#], '\t', 'K2L\t', Element["K2", #], '\n']&, sextName];
 Close[fw];

 fw2 = OpenWrite[fn2];
 WriteString[fw2, "S [m]\t Betx [m]\t Alfx [m]\t Bety [m]\t Alfy [m]\t EX [m]\t EY [m]\n"];
 Do[
    tw = Flatten[{LINE["S", i], Twiss[{"BX", "AX", "BY", "AY", "PEX", "PEY"}, i]}];
    Scan[WriteString[fw2, tw[#], "\t"]&, Range[Length@tw]];
    WriteString[fw2, "\n"]
    , {i, LINE["POSITION", IP]}]
 Close[fw2];

end;

! --> below to rematch optics
 k1qm0 = Element["K1", {QD20X, QF21X, QM16FF, QM15FF, QM14FF, QM13FF, QM12FF, QM11FF}];
 
 (* Matching FFS optics *)
 bxIP = 60e-3;
 byIP = 16e-6;
 BetaMatching[bxIP, byIP];
 dr;

 (* Matching FFS optics *)
 FREE QF21X QD20X QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)
! QM* MIN -1.3 MAX 1.3;
 FIT ZHFB1FF IP NX 2.000;
 FIT ZVFB1FF IP NY 2.000; 
 FIT IP AX 0;
 FIT IP AY 0;
 FIT IP BX bxIP;
 FIT IP BY byIP;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 k1qm2 = Element["K1", {QD20X, QF21X, QM16FF, QM15FF, QM14FF, QM13FF, QM12FF, QM11FF}];

 Print[Element["K1","QF1FF"]];
 FREE QF1FF;
! FIT IP AX 0;
 FIT IP EX 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 Print[Element["K1","QF1FF"]]; 
 end;
 
 SaveATF2Optics["ATF2_June18_20bx0p16by"];
 end;
 
 !FREE QF21X QM15FF QM14FF QM13FF QM12FF QM11FF;
 FREE QF21X QD20X QM16FF QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)
 FIT IP AX 0;
 FIT IP AY 0;
 FIT IP BX bxIP;
 FIT IP BY byIP;
 FIT ZHFB1FF IP NX 2.000;
 FIT ZVFB1FF IP NY 2.000;   
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 FIT REJECT TOTAL;
 FIX *;
 dr;

 (* particle tracking for QF1 scan *)
 kk = 0.0610291/10; ! current (A) to K1
 $FORM = "S10.4";
 
 emitx0 = 1.2e-9;
 emity0 = 12e-12;
 sigz0 = 7e-3;
 sigp0 = 6e-4;
 npars = 1e5;
 beam0 = GenParticleGauss[emitx0, emity0, sigz0, sigp0, npars];
 K1QF10 = LINE["K1", QF1FF];
 
 Table[
   SetElement["QF1FF",,{"K1"->K1QF10+kk*dI/2}];
   FFS["cal"];
   beam2 = TrackParticles[beam0, "IP"];
   WriteDistributionPos[beam2, "20bx0p125by_QF1Curr="//ToString[dI]];
   System["mv "//"*20bx0p125by_QF1Curr="//ToString[dI]//".dat"//" ./data/"]
   ,{dI, -3, 3}];

end;
abort;

