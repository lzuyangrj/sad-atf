! Title Ultra bety optics matching
! 2018.11.16
! modified from Okugi-san's script for nominal optics matching
! path of lattice and .n files have to updated wrt. machine

 ON ECHO; OFF CTIME;

 read "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)

 MARK   IEX     =(AX =-.7155662723894454 BX =1.1626355159010389   AY =-3.678958491678835   BY =4.534050395633643  EX =.00949075154435475   EPX =.004192920073472923 DP =.0008 EMITX =1.2e-09 EMITY =1.2e-11 ) ;

 FFS USE=ATF2;

 SeedRandom[17];
 
 FFS["NPARA = 8"];
 path0 = "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/";
!Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 
 TRPT;
 INS;
 FFS["cal"];

 LoadATF2Optics["ATF2_201812051656"];
 FFS["cal"];

 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 FFS["cal"];
 
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

!K-value of SF6-SD0, QF19-QM11, QF1 and QD0
 KQF19 = 0.6009161/2;
 KQD20 = -0.2011774/2;
 KQF21 = 0.3923976/2;
 KQM15 = 1.12465/2;
 KQM14 = -1.7862195/2;
 KQM13 = 1.052469/2;
 KQM12 = -0.2721159/2;
 KQM11 = 0.1648456/2;
 KQF1 = 0.7383851/2;
 KQD0 = -1.3368907/2;
 KSF6 = 22.1746227/2;
 KSF5 = 1.8147234/2;
 KSD4 = -18.484251/2;
 KSF1 = 1.6519815/2;
 KSD0 = -3.0514438/2;

 SetElement["QF19X",,{"K1"->KQF19}];
 SetElement["QD20X",,{"K1"->KQD20}];
 SetElement["QF21X",,{"K1"->KQF21}];
 SetElement["QM15FF",,{"K1"->KQM15}];
 SetElement["QM14FF",,{"K1"->KQM14}];
 SetElement["QM13FF",,{"K1"->KQM13}];
 SetElement["QM12FF",,{"K1"->KQM12}];
 SetElement["QM11FF",,{"K1"->KQM11}];
 SetElement["QF1FF",,{"K1"->KQF1}];
 SetElement["QD0FF",,{"K1"->KQD0}];
 SetElement["SF6FF",,{"K2"->KSF6}];
 SetElement["SF5FF",,{"K2"->KSF5}];
 SetElement["SD4FF",,{"K2"->KSD4}];
 SetElement["SF1FF",,{"K2"->KSF1}];
 SetElement["SD0FF",,{"K2"->KSD0}];
 
 FFS["cal"];

 twissIP = Twiss[All, IP];
 {axIP, bxIP, ayIP, byIP} = {#1, #2, #4, #5}&@@twissIP;
 
 (* Matching FFS optics *)
 bxIP = 80e-3;
 byIP = 12.5e-6;

 (* matching initial horizonal dispersion *)
 FREE EXI EPXI;
 FIT OTR0X EX 0;
 FIT OTR1X EX 0;
 Do[FFS["go"],{II,100}];
 FIT REJECT TOTAL;
 FIX EXI EPXI; 
 FFS["cal"];

 (* matching initial beta-function *)
 FREE BXI BYI AXI AYI;
 FIT OTR0X BX 15.7438;
 FIT OTR0X AX  -8.5831;
 FIT OTR0X BY 8.6011; 
 FIT OTR0X AY 3.8286;
 Do[FFS["go"],{II,100}];
 FIT REJECT TOTAL;
 FIX BXI BYI AXI AYI;
 FFS["cal"];  

 (* beam waist scan *)
 FREE QF1FF;
 FIT IP AX 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"];
 
 FREE QD0FF;
 FIT IP AY 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 FREE QF1FF;
 FIT IP AX 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"];
 
 FREE QD0FF;
 FIT IP AY 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 (* optics matching *)
 FREE QD20X QF21X QM11FF QM12FF QM13FF QM14FF QM15FF QM16FF;
 !FIT ZHFB1FF IP NX 2.000;
 !FIT ZVFB1FF IP NY 2.000;
 !FIT IP EX 0;
 FIT IP BX bxIP;
 FIT IP BY byIP;
 FIT IP AX 0;
 FIT IP AY 0;
 Do[FFS["go"],{II, 200}];
 FIT REJECT TOTAL;
 FIX * ;
 FFS["cal"];

 phiFBIPx = #2-#1&@@(Twiss["NX",{"ZHFB1FF", "IP"}])/2/PI
 phiFBIPy = #2-#1&@@(Twiss["NY",{"ZVFB1FF", "IP"}])/2/PI
 
 type IP;

 FREE QF1FF;
 FIT IP EX 0; 
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 FREE QD20X QF21X QM11FF QM12FF QM13FF QM14FF QM15FF QM16FF;
 FIT ZHFB1FF IP NX 2.000;
 FIT ZVFB1FF IP NY 2.000;
 !FIT IP EX 0;
 FIT IP BX bxIP;
 FIT IP BY byIP;
 FIT IP AX 0;
 FIT IP AY 0;
 Do[FFS["go"],{II, 200}];
 FIT REJECT TOTAL;
 FIX * ;
 FFS["cal"];

 phiFBIPx = #2-#1&@@(Twiss["NX",{"ZHFB1FF", "IP"}])/2/PI
 phiFBIPy = #2-#1&@@(Twiss["NY",{"ZVFB1FF", "IP"}])/2/PI
end;

 (* save optimized magnet settings *)
 SaveATF2Optics["ATF2_20181205_20bx0p125by"];
 
end;

 (* particle tracking for QF1 scan *)
 emitx0 = 1.2e-9;
 emity0 = 12e-12;
 sigz0 = 7e-3;
 sigp0 = 6e-4;
 npars = 1e5;
 beam0 = GenParticleGauss[emitx0, emity0, sigz0, sigp0, npars];
 
 beam2 = TrackParticles[beam0, "IP"];
 WriteDistributionPos[beam2, "2018Feb22_AYscan"];
 System["mv *2018Feb22_AYscan.dat ./data/"];

end;
abort;

