#!/usr/bin/env python3
# To generate the amplitude of the wave-like GM (0.1-50 Hz)
# for ATF2 tuning simulation in SAD
# by R. Yang (30/08/2019)
import os
import sys
from numpy import pi
import numpy as np
import scipy.integrate as integrate
import scipy.special as special

# L relative distance between two element
# f1, f2 are the lower and upper frequency for integration
# PSD parameters taken from B. Bolzon PAC09 paper
# updated 4/10/2019
def amp_fast(L, fmin, fmax):
    f1 = 0.2; f2 = 2.9; f3 = 10.4; # Hz
    w1 = 2*pi*f1; w2 = 2*pi*f2; w3 = 2*pi*f3;
    a1 = 1e-13; a2 = 6e-15; a3 = 2.6e-17; # m^2/Hz
    d1 = 1.1; d2 = 3.6; d3 = 2.0
    v1 = 1000; v2 = 300; v3 = 250; # m/s
    def func(f):
        return 2/pi*np.sqrt(2*(a1/(1+(d1*(2*pi*f-w1)/w1)**4))*(1-special.jv(0,2*pi*f*float(L)/v1))+2*(a2/(1+(d2*(2*pi*f-w2)/w2)**4))*(1-special.jv(0,2*pi*f*float(L)/v2))+2*(a3/(1+(d3*(2*pi*f-w3)/w3)**4))*(1-special.jv(0,2*pi*f*float(L)/v3)))
    res1 = integrate.quad(lambda f: func(f), fmin, fmax)
    return res1[0]   

# export an array of wave-like GM amplitude
# data structure:
# 0, f1, f2, f3, ... (1st line gives the centre frequency)
# ds, a1, a2, a3, ...
# ds, ...
def amp_fast_array(L, fn):
    exist = os.path.isfile(fn)
    with open(fn, 'a+') as fw:
        f=np.logspace(-1, np.log10(50), 50)
        if not exist:
            for i in range(len(f)-1):
                fw.write('%18.7e\t'%np.mean([f[i], f[i+1]]))
            fw.write('\n')                
        #else:
        #    fw.write('%18.7e\t'%float(L))
            
        for i in range(len(f)-1):
            amp_i = amp_fast(L, f[i], f[i+1])
            fw.write('%18.7e\t'%amp_i)                
        fw.write('\n')
        fw.close()
    return 1

if __name__ == '__main__':
    args = sys.argv
    narg = len(args[1:])
    if narg!=2:
        print("Type in distances (array) or output file name was missed!\n")
        sys.exit(1)
    else:
        amp_fast_array(args[1], args[2])
