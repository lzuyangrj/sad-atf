! title: ATF2 June 2019 IP bandwidth simulation
! since 2019.07
! 2019-12-12   Renjun
! operational IP beta-functions, final beam size around 55 nm
! measured IEX twiss (not yet...)
! chromatic IEX parameters (not yet...)
!
! 2020-01-07   Renjun
! newSext+oldknobs
! measured IEX Twiss parameters, dispersions (extrapolated from ATF2 dispersion) and emity
! assuming the initial emitx and IEX dispersions/xy-coupling are const.
! mimic realistic orbit in the FFS (ATF2 BPMs) --> disabled due to the significant shrink of bandwidth
! 2020-03-11   Renjun
! modify for chromatic sigx estimations
! 
 ON ECHO; OFF CTIME;

 read "/afs/cern.ch/work/r/reyang/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605_oct.sad";  
 MARK   IEX     =(AX =1.1612156083066414 BX =2.76294045372163     AY =-2.156855359536961
           BY =4.11262450425779     EX =.016059886446523758  EPX =-.005260998086075944     BZ =1
           DP =.0007 EMITX =1.1e-09 EMITY =1.2e-11 );
  
 FFS USE=ATF2;
  
 GCUT = 3;
 NPARA = 2;
 SeedRandom[NEWSEED];

 path0 = "/afs/cern.ch/work/r/reyang/SAD/ATF2/lattice/";
 wdir = "/afs/cern.ch/work/r/reyang/SAD/ATF2/ultralow/tuning/htcondor_df/";
 ! Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 Get[path0//"atf2correction.n"]; 
 Get[path0//"knobs.n"];
 
 TRPT;
 INS;
 FFS["cal noexp;"];

! LoadATF2Optics["ATF2_20feb26_25bx0p25by_1_4"];  FFS["cal"];
! LoadATF2Optics["ATF2_ultralow_sext"];  FFS["cal"];

 LoadATF2Optics["ATF2_20mar06_25bx0p25by_df"]; FFS["cal noexp;"];

 Z* K0 0;
 QS* K1 0;
 !SK*F K2 0;
 FFS["cal noexp;"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

 k2sext = SextOff[]; ! Sext. OFF for optics matching

 (* match optics/dispersion wrt. OTR measurements *)
 mt = Matching[];
 !mt@IPBeta[100e-3, 25e-6];
 !mt@IPBeta[100e-3, 25e-6];

 FREE QD0FF QF1FF;
 FIT IP AXM 10;
 FIT IP AYM 10;
 FFS["go"];
 FIT IP AX  0;
 FIT IP AY  0;
 Do[FFS["go"],{II,100}]; FFS["cal"];
 FIT REJECT TOTAL;
 FIX QD0FF QF1FF; FFS["CAL"];

 FREE QF21X QD20X QM16FF QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)
 FFS["QM* MIN -1.3 MAX 1.3;"];
 FFS["QM14FF MIN -1. MAX 1.;"]; 
 !FIT ZHFB1FF IP NX 2.000;
 FIT ZVFB1FF IP NY 2.000;
 FIT ZX2X IP NX 4.5;
 FIT IP AX 0;
 FIT IP AY 0;
 FIT IP BX 110e-3;
 !!FIT IP BY 20e-6;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 FREE QF21X QD20X QM16FF QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)
 FFS["QM* MIN -1.3 MAX 1.3;"];
 FFS["QM14FF MIN -1. MAX 1.;"]; 
 FIT ZVFB1FF IP NY 2.000;
 FIT ZX2X IP NX 4.5; 
 FIT IP AX 0;
 FIT IP AY 0;
 FIT IP BX 110e-3;
 !FIT IP BY 20e-6;
 Do[FFS["go"],{II,100}];
 FFS["cal"];
 FIT REJECT TOTAL;
 FIX QF21X QD20X QM16FF QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)

! set mag. strength/rolling error
 SetBenderr[200e-6, 1e-3];
 SetQuaderr[80e-6, 80e-6, 200e-6, 1e-3, Correction->True];
 SetQuaderr[2e-6, 2e-6, 200e-6, 1e-5, Correction->True, FFSmd->True, Weight->"FFS", SVD->False]; 
 SetSexterr[100e-6, 100e-6, 200e-6, 1e-3];
 SetMultiPoleError;

 FFS["CALC NOEXP"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

! go to tuning 
 OC = OrbitCorrection[];
 BBA = BeamBasedAlignment[];

 mt@Dispersion[{0, 0, -0.569E-3, 1.584E-3}];
 (* operational orbit *)
 ! fn = wdir//"orbit/atf2bpm19jun18_171439_20bx0p25by_2_FBon.dat";
 ! fw = ""; !"./orbit/orbFit_test_w_xReduction.dat";
 ! OC@RealOrbit[fn, fw, Reduction->{0.2,1}];

 BBA@BPM[100e-6];
 BBA@Sextupole[100e-6]; ! accuracy 100 for all sext. BBA
 BBA@SkewSextupole[100e-6];
 BBA@Octupole[100e-6];  
 SextOn[k2sext]; ! Sext. ON for tuning 
 ! orbit correction 
 Do[OC@Correction[Tolerance->300e-6, Weight->"FFS", SVD->True], {3}];
 ! Do[OC@RealOrbit[fn, fw, Reduction->{0.2, 1.}], {3}];
 ! dispersion correction
 Do[TK@SumKnobFit[i*1e-3], {i, 15, 3, -3}]; ! Max. EY 5 mm in FFS

! ---------------------------------------------------------------
 (*           Beam size tuning            *)
 sigp = 7e-4;
 b0 = GaussBeam[EMITX, EMITY, SIGZ, sigp, np=5000];
 GMmag = Element["NAME", "{QS}*{F}"]; !{QS}*F
 !paraGMFast = FastGMIntialization[wdir//"input/FastGM_gen.py", GMmag, "FastGMData.dat"];

 TK = TuneKnobs[MoverError$->False, Jit$->False, Vibration$->False, GM$fast->False, GM$slow->False, nshot$->1, MagGM$->GMmag];
 TK@file$ = "LinearKnob20191007";
 TK@mknob$ = "multiknob_init_param_191007"; !"160201";
 TK@beam0$ = b0;
 !TK@FastGMInit$ = paraGMFast;
 
 res = {};
 
 !TK@FDScanFit[]; 
 AppendTo[res, Prepend[TK@FDScan[], "FD"]];
 !TK@EX[-0.2, 0.2, 0.05];
 AppendTo[res, Prepend[TK@EX2[], "EX2"]];
 AppendTo[res, Prepend[TK@FDScan[], "FD"]];
 
 Do[TK@SumKnobFit[i*1e-3], {i, 15, 3, -3}]; ! Max. EY 5 mm in FFS
 ! pre-EY correction
 TK@EY[-0.4, 0.4, .08];
 TK@EY[-0.2, 0.2, .04];
 TK@IPSizeTrack[b0];

 res = ScanLinearKnob[-0.5, 0.5, 0.15, res];
 res = ScanLinearKnob[-0.5, 0.5, 0.15, res]; 
 res = ScanLinearKnob[-0.3, 0.3, 0.1, res];

 ! res: {knob, tag, {x, sigx, y, sigy}} or {knob, tag, {x, sigx, y, sigy, {std_x, std_sigx, std_y, std_sigy}, sigxy}}
 resave = Thread@{res[;;,1], res[;;,3,2], res[;;, 3, 4]};
 Export["res_20Mar06_constIEX_"//NEWSEED//".dat", resave]

 sigxydf0 = ScanSigydE[b0, -10e3, 10e3, 0.5e3, Mode->"Tracking"];
 tw0 = ScanSigydE[b0, -10e3, 10e3, 0.5e3, Mode->"Twiss"];
 
 Export["sigdf_20Mar06_constIEX_"//NEWSEED//".dat", sigxydf0];
 ! Twiss at IP
 Export["twiss_20Mar06_constIEX_"//NEWSEED//".dat", tw0];

abort;
