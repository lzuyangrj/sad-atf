! title: ATF2 ultra-low bety tuning simulation (w/ random machine errors)
! since 2019.07
! path of lattice and .n files have to updated wrt. machine
! Including multipole errors..
! Do not re-match IEX Beta wrt. mOTR measurements (not need);
! Assuming IEX EY=0; EPY=0; again, not needed to match OTR0 EY/EPY (need mOTR)
! small misalignment/roll error to FFS quads., decided by mover accuray.
! Orbit correction w/ SVD but not 1to1 steering
! Pre-EY knob to reduce IP EY before knob scan

 ON ECHO; OFF CTIME;

 read "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605_oct.sad";   ! updated by RJ (20170530)
 MARK   IEX     =(AX =-.7155662723894454 BX =1.1626355159010389   AY =-3.678958491678835   BY =4.534050395633643  EX =.00949075154435475   EPX =.004192920073472923 DP =.0006 EMITX =1.2e-09 EMITY =1.2e-11 SIGZ = 6e-3);
 FFS USE=ATF2;
  
 GCUT = 3;
 NPARA = 2;
 SeedRandom[9];
 
 path0 = "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/";
 !path0 = "/eos/user/r/reyang/SAD/ATF2/lattice/";
 !Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 Get[path0//"atf2correction.n"]; 
 Get[path0//"knobs.n"];
 
 TRPT;
 INS;
 FFS["CALC NOEXP"];

 LoadATF2Optics["ATF2_June21_20bx0p3by_df"];
 FFS["CALC NOEXP"];
  
 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 SK*FF K2 0;
 FFS["CALC NOEXP;"];
 
 mt = Matching[];
 
 FFS["RESET QM*FF;"];
 k2sext = SextOff[]; ! Sext. OFF for optics matching
 mt@IPBeta[100e-3, 25e-6]; !, {5.349, -2.5613, 9.418, 4.70}];

! set mag. strength/rolling error
! SetBenderr[200e-6, 1e-3];
! SetQuaderr[100e-6, 100e-6, 200e-6, 1e-3, Correction->True];
! SetQuaderr[2e-6, 2e-6, 10e-6, 1e-5, Correction->True, FFSmd->True, Weight->"FFS", SVD->False]; 
! SetSexterr[100e-6, 100e-6, 200e-6, 1e-3];
 SetMultiPoleError;

 FFS["CALC NOEXP"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

! go to tuning 
 OC = OrbitCorrection[];
 BBA = BeamBasedAlignment[];
 
 sigp = 7e-4;
 b0 = GaussBeam[EMITX, EMITY, SIGZ, sigp, np=3000];
 GMmag = Element["NAME", "{QS}*{F}"]; !{QS}*F
 !paraGMFast = FastGMIntialization["FastGM_gen.py", GMmag, "FastGMData.dat"];
 
 TK = TuneKnobs[MoverError$->False, Jit$->False, Vibration$->False, GM$fast->False, GM$slow->False, nshot$->1, MagGM$->GMmag];
 TK@file$ = "LinearKnob20191007";
 TK@mknob$ = "multiknob_init_param_191007"; !"160201";
 TK@beam0$ = b0;
 TK@FastGMInit$ = paraGMFast;
 TK@MoverError$Set={3e-6, 10e-6}; ! sigx/sigy and sigroll
 
 mt@IPBeta[100e-3, 25e-6];
 mt@IPBeta[100e-3, 25e-6];
 mt@Dispersion[{0, 0}]; 
 SextOn[k2sext]; ! Sext. ON for tuning
 
 ! orbit correction 
 Do[OC@Correction[Tolerance->300e-6, Weight->"FFS", SVD->True], {5}];
 ! dispersion correction
 Do[TK@SumKnobFit[i*1e-3], {i, 15, 3, -3}]; ! Max. EY 5 mm in FFS
 TK@SumKnob2[]; ! real sum-knob
 
 (* Beam size tuning *)
 res = {};
 res = ScanLinearKnob[-0.5, 0.5, 0.15, res]; 
 res = ScanLinearKnob[-0.3, 0.3, 0.1, res];
 res = ScanKnobAllRough[res]; 
 res = ScanKnobAllFine[res];
 res = ScanKnobAllFine[res]; 
end;

 ! res: {knob, tag, {x, sigx, y, sigy}} or {knob, tag, {x, sigx, y, sigy, {std_x, std_sigx, std_y, std_sigy}, sigxy}}
 resave = Thread@{res[;;,1], res[;;,3,2], res[;;, 3, 4]};
 Export["./data/res_randerr_newsext_oldknob_w_SKBBA.dat", resave]
 CreatePlainDeck["ATF2_randerr_newsext_oldknob_w_SKBBA_17.sad", "ATF2"]; 
 ! Export res

end;
abort;

