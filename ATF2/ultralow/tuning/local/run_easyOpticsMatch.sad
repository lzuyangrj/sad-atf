! title: ATF2 ultra-low bety optics matching
! 2018.11.16
! modified from Okugi-san's script for nominal optics matching
! path of lattice and .n files have to updated wrt. machine

 ON ECHO; OFF CTIME;

 read "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)

 MARK   IEX     =(AX =-.7155662723894454 BX =1.1626355159010389   AY =-3.678958491678835   BY =4.534050395633643  EX =.00949075154435475   EPX =.004192920073472923 DP =.0006 EMITX =1.2e-09 EMITY =1.2e-11 SIGZ = 6e-3)
 ;

 FFS USE=ATF2;

 SeedRandom[100];
 
 FFS["NPARA = 8"];
 path0 = "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/";
!Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 Get[path0//"knobs.n"];
 
 TRPT;
 INS;
 FFS["cal"];

! LoadATF2Optics["ATF2_Ultralow_Jun17_0"];
 LoadATF2Optics["ATF2_June21_20bx0p3by_df"];
 FFS["cal"];

 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 FFS["cal"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

 twissIP = Twiss[All, IP];
 {axIP, bxIP, ayIP, byIP} = {#1, #2, #4, #5}&@@twissIP;

 FREE AXI BXI AYI BYI;
 FIT OTR0X BX 5.349; 
 FIT OTR0X AX -2.5613;
 FIT OTR0X BY 9.418; 
 FIT OTR0X AY 4.70;
 Do[FFS["go"],{II,100}];
 FFS["cal"]; 
 FIT REJECT TOTAL;
 FIX AXI BXI AYI BYI;

! FREE AXI BXI;
! FIT IP AX 0;
! FIT IP BX 120e-3;
! Do[FFS["go"],{II,100}];
! FFS["cal"]; 
! FIT REJECT TOTAL;
! FIX AXI BXI AYI BYI;
 
 (* beam waist scanning *)
 FIX *;
 FREE QD0FF QF1FF;
 FIT IP AXM 10;
 FIT IP AYM 10;
 FFS["go"];
 FIT IP AX  0;
 FIT IP AY  0;
 Do[FFS["go"],{II,100}]; FFS["cal"];

 FIX *;
 FREE AXI BXI AYI BYI;
 FIT IP BX 90e-3; 
 FIT IP AX 0;
 FIT IP BY 30e-6; 
 FIT IP AY 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"]; 
 FIT REJECT TOTAL;
 FIX AXI BXI AYI BYI;

 (* matching initial horizonal dispersion *)
 FIX *;
 FREE EXI EPXI;
 FIT OTR0X EX 0;
 FIT OTR0X EPX 0;
 Do[FFS["go"],{II,100}];
 FIT REJECT TOTAL;
 
 (* match the vertical dispersion in the EXT *)
 FIX *;
 FREE EYI EPYI;
 FIT OTR0X EY -0.569E-3;
 FIT OTR0X EPY 1.584E-3;
 GO;
 FFS["cal"]; 
 
! SetQuaderr[10e-6, 10e-6, 50e-6, 0.1e-3];
! SetSexterr[100e-6, 100e-6, 200e-6, 0.1e-3];

 OC = OribtCorrection[];
 BBA = BeamBasedAlignment[];
 
 file$ = "LinearKnob20151111";
 sigp = 7e-4;
 b0 = GaussBeam[EMITX, EMITY, SIGZ, sigp, np=5000];
 
 TK = TuneKnobs[];
 TK@file$="LinearKnob20151111";
 TK@beam0$=b0;
 
!Real Orbit/dispersion approximation and correction
 fn = "./orbit/atf2bpm19jun18_171439_20bx0p25by_2_FBon.dat";
 OC@RealOrbit[fn];


end;

 BBA@Sextupole[20e-6]; ! alignment accuracy 20 um (rms)
 Do[TK@SumKnobFit[i*1e-3], {i, 16, 2, -2}]; ! Max. EY 5 mm in FFS

! Beam size tuning
 TK@FDScan[]; 

 res = TK@EX[-0.2, 0.2, 0.05];
 If[~(res[1]), TK@EX2[]];


 TK@AY[-0.5, 0.5, 0.1, Track->False];
 TK@EY[-.5, .5, 0.1, Track->False];
 TK@Coup2[-0.5, 0.5, 0.1, Track->False];
 
 TK@AY[-0.3, 0.3, 0.05, Track->False];
 TK@EY[-.3, .3, 0.05, Track->False];
 TK@Coup2[-0.3, 0.3, 0.05, Track->False];

end;   
! --> below to rematch optics
 k1qm0 = Element["K1", {QD20X, QF21X, QM16FF, QM15FF, QM14FF, QM13FF, QM12FF, QM11FF}];
 
 (* Matching FFS optics *)
 bxIP = 60e-3;
 byIP = 16e-6;
 BetaMatching[bxIP, byIP];
 dr;

 (* Matching FFS optics *)
 FIX *;
 FREE QF21X QD20X QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)
! QM* MIN -1.3 MAX 1.3;
 FIT ZHFB1FF IP NX 2.000;
 FIT ZVFB1FF IP NY 2.000; 
 FIT IP AX 0;
 FIT IP AY 0;
 FIT IP BX bxIP;
 FIT IP BY byIP;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 k1qm2 = Element["K1", {QD20X, QF21X, QM16FF, QM15FF, QM14FF, QM13FF, QM12FF, QM11FF}];

 Print[Element["K1","QF1FF"]];
 FREE QF1FF;
! FIT IP AX 0;
 FIT IP EX 0;
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 Print[Element["K1","QF1FF"]]; 
 end;
 
 SaveATF2Optics["ATF2_June18_20bx0p16by"];
 end;
 
 !FREE QF21X QM15FF QM14FF QM13FF QM12FF QM11FF;
 FREE QF21X QD20X QM16FF QM15FF QM14FF QM13FF QM12FF QM11FF; (* why not qm15 *)
 FIT IP AX 0;
 FIT IP AY 0;
 FIT IP BX bxIP;
 FIT IP BY byIP;
 FIT ZHFB1FF IP NX 2.000;
 FIT ZVFB1FF IP NY 2.000;   
 Do[FFS["go"],{II,100}];
 FFS["cal"];

 FIT REJECT TOTAL;
 FIX *;
 dr;

 (* particle tracking for QF1 scan *)
 kk = 0.0610291/10; ! current (A) to K1
 $FORM = "S10.4";
 
 emitx0 = 1.2e-9;
 emity0 = 12e-12;
 sigz0 = 7e-3;
 sigp0 = 6e-4;
 npars = 1e5;
 beam0 = GenParticleGauss[emitx0, emity0, sigz0, sigp0, npars];
 K1QF10 = LINE["K1", QF1FF];
 
 Table[
   SetElement["QF1FF",,{"K1"->K1QF10+kk*dI/2}];
   FFS["cal"];
   beam2 = TrackParticles[beam0, "IP"];
   WriteDistributionPos[beam2, "20bx0p125by_QF1Curr="//ToString[dI]];
   System["mv "//"*20bx0p125by_QF1Curr="//ToString[dI]//".dat"//" ./data/"]
   ,{dI, -3, 3}];

end;
abort;
