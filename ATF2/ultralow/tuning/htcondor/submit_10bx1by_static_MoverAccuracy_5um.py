#!/usr/bin/env python3
import os
import sys
from subprocess import Popen, PIPE

# generate joblist
npara = 102 # 102

with open('joblist', 'w') as fw:
    for i in range(npara): 
        fw.write(str(i+1))
        fw.write('\n')

# write .sub file
wdir = '/afs/cern.ch/work/r/reyang/SAD/ATF2/ultralow/tuning/htcondor/'
fn_input = '_run_afs_10bx1by_static_MoverAccuracy_5um.sad'
fn_res1 = 'res_10bx1by_static_MoverAccuracy_5um_'
#fn_res2 = 'ATF2_oldsext_oldknob_static_'
fn_sub = 'run_10bx1by_static_MoverAccuracy_5um.sub'
flavour = "microcentury"

with open(fn_sub, 'w') as fw:
    fw.write('# htcondor sub file for LSF-like jobs\n'
    'universe = vanilla \n'
    'executable = '+ wdir +'run.py \n'
    'arguments = $(wu_id) '+wdir+'input/'+fn_input+'\n'
    'initialdir = '+wdir+'output/ \n'
    'output = '+wdir+'output/htcondor.$(ClusterId).$(ProcId).out \n'
    'error = '+wdir+'output/htcondor.$(ClusterId).$(ProcId).err \n'
    'log = '+wdir+'output/htcondor.$(ClusterId).$(ProcId).log \n'
    'getenv = true \n'
    '# do not ask htcondor to take care of output files, otherwise empty ones will be created while the simulation proceeds \n'
    'RequestCpus = 2 \n'
    'transfer_input_files = "" \n'
    'transfer_output_files = '+fn_res1+'$(wu_id).dat, '+fn_res1+'$(wu_id)_all.dat \n'
    'ShouldTransferFiles = YES \n'
    'WhenToTransferOutput = ON_EXIT_OR_EVICT \n'
    '+JobFlavour = "'+flavour+'" \n'
    'queue wu_id from joblist')

process = Popen(['condor_submit', fn_sub], stdout=PIPE, stderr=PIPE,
        universal_newlines=True)
stdout, stderr = process.communicate()
if stderr:
    print(stdout)
    print(stderr)
else:
    print(stdout)

