! title: ATF2 ultra-low bety tuning simulation
! since 2019.07
! path of lattice and .n files have to updated wrt. machine
! Including multipole errors..

 ON ECHO; OFF CTIME;

 read "/afs/cern.ch/work/r/reyang/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605_oct.sad";  
 MARK   IEX     =(AX =-.7155662723894454 BX =1.1626355159010389   AY =-3.678958491678835   BY =4.534050395633643  EX =.00949075154435475   EPX =.004192920073472923 DP =.0006 EMITX =1.2e-09 EMITY =1.2e-11 SIGZ = 6e-3);
 FFS USE=ATF2;
  
 GCUT = 3;
 NPARA = 2;
 SeedRandom[NEWSEED];

 path0 = "/afs/cern.ch/work/r/reyang/SAD/ATF2/lattice/";
 wdir = "/afs/cern.ch/work/r/reyang/SAD/ATF2/ultralow/tuning/htcondor/";
 ! Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 Get[path0//"atf2correction.n"]; 
 Get[path0//"knobs.n"];
 
 TRPT;
 INS;
 FFS["CALC NOEXP"];

! LoadATF2Optics["ATF2_Ultralow_Jun17_0"];
 LoadATF2Optics["ATF2_June21_20bx0p3by_df"];
 FFS["CALC NOEXP"];

 (* nominal sext. setting *)
 RESET S{FD}*FF;
 
 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 SK*FF K2 0;
 FFS["CALC NOEXP;"];

 k2sext = SextOff[]; ! Sext. OFF for optics matching
 
 (* match optics/dispersion wrt. OTR measurements *)
 mt = Matching[];
 mt@IPBeta[100e-3, 25e-6];
 mt@IPBeta[100e-3, 25e-6];

! set mag. strength/rolling error
 SetBenderr[200e-6, 1e-3];
 SetQuaderr[100e-6, 100e-6, 200e-6, 1e-3, Correction->True];
 SetQuaderr[2e-6, 2e-6, 10e-6, 1e-5, Correction->True, FFSmd->True, Weight->"FFS", SVD->False]; 
 SetSexterr[100e-6, 100e-6, 200e-6, 1e-3];
 SetMultiPoleError;

 FFS["CALC NOEXP"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);
 
! go to tuning 
 OC = OrbitCorrection[];
 BBA = BeamBasedAlignment[];
 
 sigp = 7e-4;
 b0 = GaussBeam[EMITX, EMITY, SIGZ, sigp, np=5000];
 GMmag = Element["NAME", "{QS}*{F}"]; !{QS}*F
 paraGMFast = FastGMIntialization[wdir//"input/FastGM_gen.py", GMmag, "FastGMData.dat"];

 TK = TuneKnobs[MoverError$->False, Jit$->False, Vibration$->False, GM$fast->False, GM$slow->False, nshot$->1, MagGM$->GMmag];
 TK@file$ = "LinearKnob20151111";
 TK@mknob$ = "multiknob_init_param_160201"; !"160201";
 TK@beam0$ = b0;
 TK@FastGMInit$ = paraGMFast;
 TK@MoverError$Set={1.3e-6, 0}; ! sigx/sigy and sigroll
 
 mt@IPBeta[100e-3, 25e-6];
 mt@IPBeta[100e-3, 25e-6];
 mt@Dispersion[{0, 0}];
 
 BBA@BPM[100e-6];
 BBA@Sextupole[100e-6]; ! accuracy 100 for all sext. BBA
 BBA@SkewSextupole[100e-6];
 BBA@Octupole[100e-6];  
 SextOn[k2sext]; ! Sext. ON for tuning 
 ! orbit correction 
 Do[OC@Correction[Tolerance->300e-6, Weight->"FFS", SVD->True], {5}];
 ! dispersion correction
 Do[TK@SumKnobFit[i*1e-3], {i, 15, 3, -3}]; ! Max. EY 5 mm in FFS
 
 (* Beam size tuning *)
 res = {};
 
 !TK@FDScanFit[]; 
 AppendTo[res, Prepend[TK@FDScan[], "FD"]];
 !TK@EX[-0.2, 0.2, 0.05];
 AppendTo[res, Prepend[TK@EX2[], "EX2"]];
 AppendTo[res, Prepend[TK@FDScan[], "FD"]];
 ! pre-EY correction
 TK@EY[-0.4, 0.4, .08];
 TK@EY[-0.2, 0.2, .04];

 res = ScanLinearKnob[-0.5, 0.5, 0.15, res];
 res = ScanLinearKnob[-0.5, 0.5, 0.15, res]; 
 res = ScanLinearKnob[-0.3, 0.3, 0.1, res];

 res = ScanKnobAllRough[res]; 
 res = ScanKnobAllFine[res];
 res = ScanKnobAllFine[res];
 res = ScanKnobAllFine[res];

 ! res: {knob, tag, {x, sigx, y, sigy}} or {knob, tag, {x, sigx, y, sigy, {std_x, std_sigx, std_y, std_sigy}, sigxy}}
 resave = Thread@{res[;;,1], res[;;,3,2], res[;;, 3, 4]};
 Export["res_25bx0p25by_static_MoverAccuracy_1300nm_"//NEWSEED//".dat", resave];
 fw=OpenWrite["res_25bx0p25by_static_MoverAccuracy_1300nm_"//NEWSEED//"_all.dat"];
 Write[fw, res];
 Close[fw];
 
abort;
