! title: ATF2 ultra-low bety tuning simulation
! since 2019.07
! path of lattice and .n files have to updated wrt. machine
! Including multipole errors..

 ON ECHO; OFF CTIME;

 read "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)

 MARK   IEX     =(AX =-.7155662723894454 BX =1.1626355159010389   AY =-3.678958491678835   BY =4.534050395633643  EX =.00949075154435475   EPX =.004192920073472923 DP =.0006 EMITX =1.2e-09 EMITY =1.2e-11 SIGZ = 6e-3)
 ;

 FFS USE=ATF2;
 NPARA = 4;
 SeedRandom[100];
 
 FFS["NPARA = 8"];
 path0 = "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/";
!Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 Get[path0//"knobs.n"];
 
 TRPT;
 INS;
 FFS["cal"];

! LoadATF2Optics["ATF2_Ultralow_Jun17_0"];
 LoadATF2Optics["ATF2_June21_20bx0p3by_df"];
 FFS["cal"];

 (* nominal sext. setting *)
 RESET S{FD}*FF;

 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 SK*FF K2 0;

 SetMultiPoleError;
! set mag. strength/rolling error
 SetBenderr[200e-6, 1e-3];
 SetQuaderr[0, 0, 200e-6, 1e-3];
 SetSexterr[0, 0, 200e-6, 1e-3];

 FFS["cal"];
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);
 
 (* match optics/dispersion wrt. OTR measurements *)
 mt = Matching[];
 
 k2sext = SextOff[]; ! Sext. OFF for optics matching
 mt@IPBeta[90e-3, 30e-6, {5.349, -2.5613, 9.418, 4.70}];

 SextOn[k2sext]; ! Sext. ON for tuning
 
 mt@Dispersion[{0, 0, -0.569E-3, 1.584E-3}]; 
! mt@Dispersion[{0, 0, 0, 0}];

! go to tuning
! OC = OribtCorrection[];
! BBA = BeamBasedAlignment[];
 ! sk-sext offset
 ! >>>>>

 file$ = "LinearKnob20151111";
 sigp = 7e-4;
 b0 = GaussBeam[EMITX, EMITY, SIGZ, sigp, np=5000];
 
 TK = TuneKnobs[];
 TK@file$ = "LinearKnob20151111";
 TK@mknob$ = "multiknob_init_param_160201";
 TK@beam0$ = b0;
 
! Real Orbit
 fn = "./orbit/atf2bpm19jun18_171439_20bx0p25by_2_FBon.dat";
 fw = "";
 OC@RealOrbit[fn, fw, Reduction->{1., 1.}];
end;
! BBA@Sextupole[100e-6]; ! accuracy 50 um ?? 
! BBA@SkewSextupole[100e-6];

 Do[TK@SumKnobFit[i*1e-3]; dr, {i, 15, 3, -2}]; ! Max. EY 5 mm in FFS

! Beam size tuning
 res = {};
 TK@FDScanFit[];
 AppendTo[res, Prepend[TK@FDScan[], "FD"]];
!TK@EX[-0.2, 0.2, 0.05];
 AppendTo[res, Prepend[TK@EX2[Track->True], "EX2"]];
 AppendTo[res, Prepend[TK@FDScan[Track->True], "FD"]];

 res = ScanLinearKnob[-0.5, 0.5, 0.15, res];
 res = ScanLinearKnob[-0.5, 0.5, 0.15, res]; 
 res = ScanLinearKnob[-0.3, 0.3, 0.1, res];

 res = ScanKnobAllRough[res];
 res = ScanKnobAllFine[res];
 res = ScanKnobAllFine[res]; 

 resave = Thread@{res[;;,1], res[;;,3,1], res[;;, 3, 2]};
 Export["./data/res_newsext_oldknob_2.dat", resave];
 
end;
abort;