Get["/userhome/okugi/SAD/lib/tracking.n"];
Get["/atf/sad/operation/lib/drawgraph.n"];

BPMnames={
{MQF1X,      2,      7.2134,      0.5641,      0.0000},
{MQD2X,      3,      9.2120,      0.1755,      0.0000},
{MQF3X,      4,     10.1951,      0.1969,      0.0000},
{MQF4X,      5,     14.5006,     -0.2067,      0.0000},
{MQD5X,      6,     15.2197,     -0.1758,      0.0000},
{MQF6X,      7,     17.2303,     -0.5483,      0.0000},
{MQF7X,      8,     20.6702,      0.0256,      0.0000},
{MQD8X,      9,     22.9287,      0.0060,      0.0000},
{MQF9X,     10,     24.8076,      0.0000,      0.0000},
{MQD10X,    11,     27.7454,      0.0000,      0.0000},
{MQF11X,    12,     29.1954,      0.0000,      0.0000},
{MQD12X,    13,     30.6454,      0.0000,      0.0000},
{MQF13X,    14,     31.7138,      0.0000,      0.0000},
{MQD14X,    15,     32.7493,      0.0000,      0.0000},
{MQF15X,    16,     33.7848,      0.0000,      0.0000},
{MQD16X,    17,     34.7832,      0.0000,      0.0000},
{MQF17X,    18,     36.2332,      0.0000,      0.0000},
{MQD18X,    19,     38.6157,      0.0000,      0.0000},
{MQF19X,    20,     40.0604,      0.0000,      0.0000},
{MQD20X,    21,     43.8217,      0.0000,      0.0000},
{MQF21X,    22,     47.8184,      0.0000,      0.0000},
{MQM16FF,   28,     51.5797,      0.0000,      0.0000},
{MQM15FF,   29,     53.3177,      0.0000,      0.0000},
{MQM14FF,   30,     54.8177,      0.0000,      0.0000},
{MQM13FF,   32,     56.3178,      0.0000,      0.0000},
{MQM12FF,   33,     57.8178,      0.0000,      0.0000},
{MQM11FF,   35,     59.4178,      0.0000,      0.0000},
{MQD10BFF,  36,     60.9178,      0.0000,      0.0000},
{MQD10AFF,  37,     61.8178,      0.0000,      0.0000},
{MQF9BFF,   38,     63.1178,      0.0000,      0.0000},
{MQF9AFF,   40,     64.2378,      0.0000,      0.0000},
{MQD8FF,    41,     66.0378,      0.0000,      0.0000},
{MQF7FF,    42,     67.9378,      0.0000,      0.0000},
{MQD6FF,    43,     69.8379,     -0.0619,      0.0000},
{MQF5BFF,   44,     71.6379,     -0.1991,      0.0000},
{MQF5AFF,   46,     72.7579,     -0.1976,      0.0000},
{MQD4BFF,   47,     74.0579,     -0.1100,      0.0000},
{MQD4AFF,   49,     75.1779,     -0.0720,      0.0000},
{MQF3FF,    50,     77.9779,     -0.0579,      0.0000},
{MQD2BFF,   51,     79.6780,     -0.0839,      0.0000},
{MQD2AFF,   52,     81.3780,     -0.1813,      0.0000}
};


GetInitialParam:=Module[{},
  Do[Print[BPMnames[II,1]," ",CaGet["Okugi:DISP:"//BPMnames[II,1]//":X:sw"][1]
                         ," ",CaGet["Okugi:DISP:"//BPMnames[II,1]//":Y:sw"][1]]
    ,{II,Length[BPMnames]}];];

GetBPMposition:=Module[{BPMread,BeamIntensity,BPMposi},
  BPMread=CaGet["ATF2:monitors"][1];
  BeamIntensity=BPMread[604];
  BPMposi=Table[{BPMnames[II,1],
         BPMread[BPMnames[II,2]*10+1],
         BPMread[BPMnames[II,2]*10+2],
         BPMread[BPMnames[II,2]*10+3]},{II,Length[BPMnames]}];
  {BeamIntensity,BPMposi}];

DispersionMeas:=Module[{NN,Icut,BPMsig,BPMX,BPMY,BPMave,BPMave1,BPMave2},
  NN=Length[BPMnames];
  CaPut["atf:rfRamp:sw",1];TkSense[1];
  CaPut["atf:rfRamp:freq:set",+2];TkSense[1];
  Print[" RF ramp = +2kHz "];
  CaPut["Okugi:DISP:main:message"," RF ramp = +2kHz "];
  BPMave1=Table[0,{II,NN},{JJ,4}];
  BPMX=Table[0,{II,NN},{JJ,10}];
  BPMY=Table[0,{II,NN},{JJ,10}];
  Do[TkSense[1];BPMsig=GetBPMposition;
     Icut=CaGet["Okugi:DISP:IntensityCut"][1];
     While[BPMsig[1]<Icut,
           Icut=CaGet["Okugi:DISP:IntensityCut"][1];
           TkSense[2];BPMsig=GetBPMposition;
           Print[KK," Icut:",Icut,"   I:",BPMsig[1]];
           CaPut["Okugi:DISP:main:message"," Icut:"//Icut//"   I:"//BPMsig[1]];
     ];
     Print[" N =  "//KK//"/10"];
     CaPut["Okugi:DISP:main:message"," N =  "//KK//"/10"];
     Do[Do[BPMX[II,JJ]=BPMX[II,JJ-1];
           BPMY[II,JJ]=BPMY[II,JJ-1],{JJ,10,2,-1}];
     BPMX[II,1]=BPMsig[2,II,3];
     BPMY[II,1]=BPMsig[2,II,4],{II,NN}],{KK,10}];
  Do[BPMave1[II,1]=Statistics[BPMX[II]][1];
     BPMave1[II,2]=Statistics[BPMX[II]][2];
     BPMave1[II,3]=Statistics[BPMY[II]][1];
     BPMave1[II,4]=Statistics[BPMY[II]][2],{II,NN}];
  CaPut["atf:rfRamp:freq:set",-2];TkSense[1];
  Print[" RF ramp = -2kHz "];
  CaPut["Okugi:DISP:main:message"," RF ramp = -2kHz "];
  BPMave2=Table[0,{II,NN},{JJ,4}];
  BPMX=Table[0,{II,NN},{JJ,10}];
  BPMY=Table[0,{II,NN},{JJ,10}];
  Do[TkSense[1];BPMsig=GetBPMposition;
     Icut=CaGet["Okugi:DISP:IntensityCut"][1];
     While[BPMsig[1]<Icut,
           Icut=CaGet["Okugi:DISP:IntensityCut"][1];
           TkSense[2];BPMsig=GetBPMposition;
           Print[KK," Icut:",Icut,"   I:",BPMsig[1]];
           CaPut["Okugi:DISP:main:message"," Icut:"//Icut//"   I:"//BPMsig[1]];
     ];
     Print[" N =  "//KK//"/10"];
     CaPut["Okugi:DISP:main:message"," N =  "//KK//"/10"];
     Do[Do[BPMX[II,JJ]=BPMX[II,JJ-1];
           BPMY[II,JJ]=BPMY[II,JJ-1],{JJ,10,2,-1}];
     BPMX[II,1]=BPMsig[2,II,3];
     BPMY[II,1]=BPMsig[2,II,4],{II,NN}],{KK,10}];
  Do[BPMave2[II,1]=Statistics[BPMX[II]][1];
     BPMave2[II,2]=Statistics[BPMX[II]][2]/3;
     BPMave2[II,3]=Statistics[BPMY[II]][1];
     BPMave2[II,4]=Statistics[BPMY[II]][2]/3,{II,NN}];
  CaPut["atf:rfRamp:freq:set",0];TkSense[1];
  CaPut["atf:rfRamp:sw",0];TkSense[1];
  Print[" RF ramp OFF "];
  BPMave=Table[0,{II,NN},{JJ,4}];
  Do[BPMave[II,1]=2.14*0.714*(BPMave1[II,1]-BPMave2[II,1])/4;
     BPMave[II,3]=2.14*0.714*(BPMave1[II,3]-BPMave2[II,3])/4;
     BPMave[II,2]=2.14*0.714*Sqrt[BPMave1[II,2]^2+BPMave2[II,2]^2]/4;
     BPMave[II,4]=2.14*0.714*Sqrt[BPMave1[II,4]^2+BPMave2[II,4]^2]/4;
    ,{II,NN}];
  BPMave];

