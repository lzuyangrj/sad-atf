
! --- Function to Make Input Parameters for 
!       the function "TrackParticles"
!
!  Input Parameters: Dev ; Device Name or Number at the Begining of Tracking 
!                    NNN ; Number of Particles for Trackin
!
!                    written by T.Okugi (12/08/2004)
!
MapBeamParameters[Dev_,NNN_]:=Module[
 {POS,TwsX,TwsY,TwsZ,EMX,EMY,XXX,XXP,YYY,YYP,ZZZ,EEE,FFF,ENE,
  Rnd1,Rnd2,Rnd3,Rnd4,Rnd5,Rnd6,ANS1,ANS2,II},
  FFS["CALC"]; POS=LINE["Position",Dev]; ANS1=POS;
  TwsX=Twiss[{"AX","BX","EX","EPX","DX","DPX"},POS];
  TwsY=Twiss[{"AY","BY","EY","EPY","DY","DPY"},POS];
  TwsZ={DZ,DP,DP0};
  EMX=EMITX; EMY=EMITY;
  XXX=Table[0,{II,NNN}];
  XXP=Table[0,{II,NNN}];
  YYY=Table[0,{II,NNN}];
  YYP=Table[0,{II,NNN}];
  ZZZ=Table[0,{II,NNN}];
  EEE=Table[0,{II,NNN}];
  FFF=Table[0,{II,NNN}];
  Do[Rnd1=GaussRandom[];Rnd2=GaussRandom[];Rnd3=GaussRandom[];
     Rnd4=GaussRandom[];Rnd5=GaussRandom[];Rnd6=GaussRandom[];
     ENE=TwsZ[2]*Rnd6+TwsZ[3];
     XXX[II]=TwsX[5]+Sqrt[EMX*TwsX[2]]*Rnd1+TwsX[3]*ENE;
     XXP[II]=TwsX[6]+(Rnd2-TwsX[1]*Rnd1)*Sqrt[EMX/TwsX[2]]+TwsX[4]*ENE;
     YYY[II]=TwsY[5]+Sqrt[EMY*TwsY[2]]*Rnd3+TwsY[3]*ENE;
     YYP[II]=TwsY[6]+(Rnd4-TwsY[1]*Rnd3)*Sqrt[EMY/TwsY[2]]+TwsY[4]*ENE;
     ZZZ[II]=TwsZ[1]*Rnd5;
     EEE[II]=ENE;
     FFF[II]=1;
  ,{II,NNN}];
   ANS2={XXX,XXP,YYY,YYP,ZZZ,EEE,FFF};{ANS1,ANS2}];

MapBeamParametersJitter[Dev_,NNN_,Xjitter_,Yjitter_,DPjitter_]:=Module[
 {POS,TwsX,TwsY,TwsZ,EMX,EMY,XXX,XXP,YYY,YYP,ZZZ,EEE,FFF,ENE,
  XXJ,XPJ,YYJ,YPJ,DPJ,
  Rnd1,Rnd2,Rnd3,Rnd4,Rnd5,Rnd6,ANS1,ANS2,II},
  FFS["CALC"]; POS=LINE["Position",Dev]; ANS1=POS;
  TwsX=Twiss[{"AX","BX","EX","EPX","DX","DPX"},POS];
  TwsY=Twiss[{"AY","BY","EY","EPY","DY","DPY"},POS];
  TwsZ={DZ,DP,DP0};
  EMX=EMITX;EMY=EMITY;
  XXX=Table[0,{II,NNN}];
  XXP=Table[0,{II,NNN}];
  YYY=Table[0,{II,NNN}];
  YYP=Table[0,{II,NNN}];
  ZZZ=Table[0,{II,NNN}];
  EEE=Table[0,{II,NNN}];
  FFF=Table[0,{II,NNN}];
  Rnd1=GaussRandom[];Rnd2=GaussRandom[];Rnd3=GaussRandom[];
  Rnd4=GaussRandom[];Rnd5=GaussRandom[];Rnd6=GaussRandom[];
  DPJ=DPjitter*Rnd6;
  XXJ=Xjitter*Sqrt[EMX*TwsX[2]]*Rnd1+TwsX[3]*DPJ;
  XPJ=Xjitter*(Rnd2-TwsX[1]*Rnd1)*Sqrt[EMX/TwsX[2]]+TwsX[4]*DPJ;
  YYJ=Yjitter*Sqrt[EMY*TwsY[2]]*Rnd3+TwsY[3]*DPJ;
  YPJ=Yjitter*(Rnd4-TwsY[1]*Rnd3)*Sqrt[EMY/TwsY[2]]+TwsY[4]*DPJ;
  Do[Rnd1=GaussRandom[];Rnd2=GaussRandom[];Rnd3=GaussRandom[];
     Rnd4=GaussRandom[];Rnd5=GaussRandom[];Rnd6=GaussRandom[];
     ENE=TwsZ[2]*Rnd6+TwsZ[3]+DPJ;
     XXX[II]=TwsX[5]+Sqrt[EMX*TwsX[2]]*Rnd1+TwsX[3]*ENE+XXJ;
     XXP[II]=TwsX[6]+(Rnd2-TwsX[1]*Rnd1)*Sqrt[EMX/TwsX[2]]+TwsX[4]*ENE+XPJ;
     YYY[II]=TwsY[5]+Sqrt[EMY*TwsY[2]]*Rnd3+TwsY[3]*ENE+YYJ;
     YYP[II]=TwsY[6]+(Rnd4-TwsY[1]*Rnd3)*Sqrt[EMY/TwsY[2]]+TwsY[4]*ENE+YPJ;
     ZZZ[II]=TwsZ[1]*Rnd5;
     EEE[II]=ENE;
     FFF[II]=1;
   ,{II,NNN}];
   ANS2={XXX,XXP,YYY,YYP,ZZZ,EEE,FFF};{ANS1,ANS2}];


! --- Function to Make Input Parameters for 
!       the function "TrackParticles"
!
!  Input Parameters: Dev ; Device Name or Number at the Begining of Tracking 
!                    NNN ; Number of Particles for Trackin
!
!                    written by T.Okugi (12/08/2004)
!
MapBeamParameters2[Dev_,Wx_]:=Module[
 {POS,TwsX,TwsY,TwsZ,EMX,EMY,XXX,XXP,YYY,YYP,ZZZ,EEE,FFF,
  Sin10,Cos10,PI2,ANS1,ANS2,II},
  PI2=2*3.1415926535;
  FFS["CALC"]; POS=LINE["Position",Dev]; ANS1=POS;
  TwsX=Twiss[{"AX","BX","EX","EPX","DX","DPX"},POS];
  Sin10=Sin[PI2/10];Cos10=Cos[PI2/10];
  XXX={TwsX[5]+Sqrt[Wx*TwsX[2]]*Sin10};
  XXP={TwsX[6]+(Cos10-TwsX[1]*Sin10)*Sqrt[Wx/TwsX[2]]};
  YYY={0.};YYP={0.};ZZZ={0.};EEE={0.};FFF={1};
  Do[Sin10=Sin[PI2*II/10];Cos10=Cos[PI2*II/10];
     XXX=Join[XXX,{TwsX[5]+Sqrt[Wx*TwsX[2]]*Sin10}];
     XXP=Join[XXP,{TwsX[6]+(Cos10-TwsX[1]*Sin10)*Sqrt[Wx/TwsX[2]]}];
     YYY=Join[YYY,{0}];
     YYP=Join[YYP,{0}];
     ZZZ=Join[ZZZ,{0}];
     EEE=Join[EEE,{0}];
     FFF=Join[FFF,{1}],{II,2,10}];
   ANS2={XXX,XXP,YYY,YYP,ZZZ,EEE,FFF};{ANS1,ANS2}];


MapBeamParameters3[Dev_,DevX_,ApX_,DevY_,ApY_,NNN_]:=Module[
 {POS,TwsX,TwsY,TwsZ,EMX,EMY,XXX,XXP,YYY,YYP,ZZZ,EEE,FFF,ENE,
  Rnd1,Rnd2,Rnd3,Rnd4,Rnd5,Rnd6,ANS1,ANS2,BetaX,BetaY,POSX,POSY,II},
  FFS["CALC"];  
  ANS1=POS;
  POS=LINE["Position",Dev];
  POSX=LINE["Position",DevX];
  POSY=LINE["Position",DevY];
  TwsX=Twiss[{"AX","BX","EX","EPX","DX","DPX"},POS];
  TwsY=Twiss[{"AY","BY","EY","EPY","DY","DPY"},POS];
  TwsZ={DZ,DP,DP0};
  BetaX=Twiss["BX",POSX];
  BetaY=Twiss["BY",POSY];
  EMX=ApX*ApX/BetaX; 
  EMY=ApY*ApY/BetaY;
  Rnd1=1-2*Random[];
  Rnd2=1-2*Random[];
  Rnd3=1-2*Random[];
  Rnd4=1-2*Random[];
  Rnd5=1-2*Random[];
  Rnd6=1-2*Random[];
  ZZZ={TwsZ[1]*Rnd5};
  ENE=TwsZ[2]*Rnd6+TwsZ[3];
  EEE={ENE};
  XXX={TwsX[5]+Sqrt[EMX*TwsX[2]]*Rnd1+TwsX[3]*ENE};
  XXP={TwsX[6]+(Rnd2-TwsX[1]*Rnd1)*Sqrt[EMX/TwsX[2]]+TwsX[4]*ENE};
  YYY={TwsY[5]+Sqrt[EMY*TwsY[2]]*Rnd3+TwsY[3]*ENE};
  YYP={TwsY[6]+(Rnd4-TwsY[1]*Rnd3)*Sqrt[EMY/TwsY[2]]+TwsY[4]*ENE};
  FFF={1};
  Do[Rnd1=GaussRandom[];Rnd2=GaussRandom[];Rnd3=GaussRandom[];
     Rnd4=GaussRandom[];Rnd5=GaussRandom[];Rnd6=GaussRandom[];
     ZZZ=Join[ZZZ,{TwsZ[1]*Rnd5}];
     ENE=TwsZ[2]*Rnd6+TwsZ[3];
     EEE=Join[EEE,{ENE}];
     XXX=Join[XXX,{TwsX[5]+Sqrt[EMX*TwsX[2]]*Rnd1+TwsX[3]*ENE}];
     XXP=Join[XXP,{TwsX[6]+(Rnd2-TwsX[1]*Rnd1)*Sqrt[EMX/TwsX[2]]+TwsX[4]*ENE}];
     YYY=Join[YYY,{TwsY[5]+Sqrt[EMY*TwsY[2]]*Rnd3+TwsY[3]*ENE}];
     YYP=Join[YYP,{TwsY[6]+(Rnd4-TwsY[1]*Rnd3)*Sqrt[EMY/TwsY[2]]+TwsY[4]*ENE}];
    FFF=Join[FFF,{1}],{II,2,NNN}];
   ANS2={XXX,XXP,YYY,YYP,ZZZ,EEE,FFF};{ANS1,ANS2}];



! --- Function to Make Input Parameters for 
!       the function "TrackParticles"
!
!  Input Parameters: Dev ; Device Name or Number at the Begining of Tracking 
!                    NNN ; Number of Particles for Trackin
!
!                    written by T.Okugi (12/08/2004)
!
MapBeamParameters3[Dev_,Sigma_,NNN_]:=Module[
 {POS,TwsX,TwsY,TwsZ,EMX,EMY,XXX,XXP,YYY,YYP,ZZZ,EEE,FFF,
  WX1,WX2,WY1,WY2,Rnd1,Rnd2,Rnd3,Rnd4,Rnd5,Rnd6,ANS1,ANS2,II},
  FFS["CALC"]; POS=LINE["Position",Dev]; ANS1=POS;
  TwsX=Twiss[{"AX","BX","EX","EPX","DX","DPX"},POS];
  TwsY=Twiss[{"AY","BY","EY","EPY","DY","DPY"},POS];
  TwsZ={DZ,DP,DP0};
  EMX=EMITX; EMY=EMITY;
  Rnd1=Sigma*Random[];
  Rnd2=2*3.1415926535*Random[];
  Rnd3=Sigma*Random[];
  Rnd4=2*3.1415926535*Random[];
  WX1=Rnd1*Cos[Rnd2]; WX2=Rnd1*Sin[Rnd2];
  WY1=Rnd3*Cos[Rnd4]; WY2=Rnd3*Sin[Rnd4];
  Rnd5=GaussRandom[];Rnd6=GaussRandom[];
  XXX={TwsX[5]+Sqrt[EMX*TwsX[2]]*WX1};
  XXP={TwsX[6]+(WX2-TwsX[1]*WX1)*Sqrt[EMX/TwsX[2]]};
  YYY={TwsY[5]+Sqrt[EMY*TwsY[2]]*WY1};
  YYP={TwsY[6]+(WY2-TwsY[1]*WY1)*Sqrt[EMY/TwsY[2]]};
  ZZZ={TwsZ[1]*Rnd5};
  EEE={TwsZ[2]*Rnd6+TwsZ[3]};
  FFF={1};
  Do[Rnd1=Sigma*Random[];
     Rnd2=2*3.1415926535*Random[];
     Rnd3=Sigma*Random[];
     Rnd4=2*3.1415926535*Random[];
     WX1=Rnd1*Cos[Rnd2]; WX2=Rnd1*Sin[Rnd2];
     WY1=Rnd3*Cos[Rnd4]; WY2=Rnd3*Sin[Rnd4];
     Rnd5=GaussRandom[];Rnd6=GaussRandom[];
     XXX=Join[XXX,{TwsX[5]+Sqrt[EMX*TwsX[2]]*WX1}];
     XXP=Join[XXP,{TwsX[6]+(WX2-TwsX[1]*WX1)*Sqrt[EMX/TwsX[2]]}];
     YYY=Join[YYY,{TwsY[5]+Sqrt[EMY*TwsY[2]]*WY1}];
     YYP=Join[YYP,{TwsY[6]+(WY2-TwsY[1]*WY1)*Sqrt[EMY/TwsY[2]]}];
     ZZZ=Join[ZZZ,{TwsZ[1]*Rnd5}];
     EEE=Join[EEE,{TwsZ[2]*Rnd6+TwsZ[3]}];
     FFF=Join[FFF,{1}],{II,2,NNN}];
   ANS2={XXX,XXP,YYY,YYP,ZZZ,EEE,FFF};{ANS1,ANS2}];






! --- Function to Calculate RMS and MEAN for the List 
!
!  Input Parameters: InputList ; Input List
!  Output Format   : {MeanValue, RMSvalue}
!
!                    written by T.Okugi (12/08/2004)
!
Statistics[InputList_]:=Module[{LLL,BBB,MEAN,DEV,RMS},
  LLL=Length[InputList];
  MEAN=Apply[Plus,InputList]/LLL;
  BBB=(InputList-MEAN)^2;
  DEV=Apply[Plus,BBB]/LLL;
  RMS=Sqrt[DEV];
  {MEAN,RMS}];


Statistics2[InputList_,CENTER_,SD_]:=Module[{II,NN,LL,AA,MEAN,RMS},
  LL=Length[InputList];
  NN=0;AA=0;
  Do[If[Abs[InputList[II]-CENTER]<3*SD,
        NN=NN+1;AA=AA+InputList[II]],{II,LL}];
  If[NN>0,MEAN=AA/NN,MEAN=0];
  NN=0;AA=0;
  Do[If[Abs[InputList[II]-CENTER]<3*SD,
        NN=NN+1;AA=AA+(InputList[II]-MEAN)^2],{II,LL}];
  If[NN>0,RMS=Sqrt[AA/NN],RMS=0];
  {MEAN,RMS}];


Statistics3[InputList_]:=Module[{II,LLL,N1,N2,AA1,AA2,MEAN,RMS1,RMS2},
  MEAN=Statistics[InputList][1];
  LLL=Length[InputList];
  N1=0;N2=0;AA1=0;AA2=0;
  Do[If[InputList[II]>MEAN,
        N1=N1+1;AA1=AA1+(InputList[II]-MEAN)^2,
        N2=N2+1;AA2=AA2+(InputList[II]-MEAN)^2],{II,LLL}];
  If[N1>0,RMS1=Sqrt[AA1/N1],RMS1=0];
  If[N2>0,RMS2=Sqrt[AA2/N2],RMS2=0];
  {MEAN,RMS1,RMS2}];


!GaussFUNC[XXX_,PAR01_,PAR02_,PAR03_]:=(PAR01*Exp[-0.5*(XXX-PAR02)^2/PAR03]);!
!
!GaussChi2[XXXX_,YYYY_,PAR1_,PAR2_,PAR3_]:=Module[{LL,II,ChiSq},
!  LL=Length[XXXX];ChiSq=0
!  Do[ChiSq=ChiSq+(YYYY[II]-GaussFUNC[XXXX[II],PAR1,PAR2,PAR3])^2,{II,LL}];
!  ChiSq];





! --- Function to Show the Beam Size etc.
!
!  Input Parameters: Alist ; Same Format to "TrackParticle"
!
!                    written by T.Okugi (12/08/2004)
!
StatList[Alist_]:=Module[{BBB,XXX,XXP,YYY,YYP,ZZZ,EEE},
  $FORM="E15.5";
  BBB=Alist[2,1];
  XXX=Statistics[BBB];
  XXP=Statistics[Alist[2,2]];
  YYY=Statistics[Alist[2,3]];
  YYP=Statistics[Alist[2,4]];
  BBB=Alist[2,5];
  ZZZ=Statistics[BBB];
  EEE=Statistics[Alist[2,6]];
  Print[" "];
  Print[" SizeX   : "//XXX[2]];
  Print[" SizeXP  : "//XXP[2]];
  Print[" SizeY   : "//YYY[2]];
  Print[" SizeYP  : "//YYP[2]];
  Print[" SizeZ   : "//ZZZ[2]];
  Print[" SizeP   : "//EEE[2]];
  Print[" "];
  Print[" DeltaX  : "//XXX[1]];
  Print[" DeltaXP : "//XXP[1]];
  Print[" DeltaY  : "//YYY[1]];
  Print[" DeltaYP : "//YYP[1]];
  Print[" DeltaZ  : "//ZZZ[1]];
  Print[" DeltaP  : "//EEE[1]];
  Print[" "];
  EEEEX=(XXX[2]^2-Twiss["EX",$$$]^2*EEE[2]^2)/Twiss["BX",$$$];
  EEEEY=(YYY[2]^2)/Twiss["BY",$$$];
  Print[" EmitX   : "//EEEEX];
  Print[" EmitY   : "//EEEEY];
  Print[" "];];


! --- Function to Show the Beam Size etc.
!
!  Input Parameters: Alist ; Same Format to "TrackParticle"
!
!                    written by T.Okugi (12/08/2004)
!
StatList2[Alist_]:=Module[{BBB,YYY},
  YYY=Statistics[Alist[2,3]];
  {YYY[1]*1e9,YYY[2]*1e9}];


! --- Function to Show the Beam Size etc.
!
!  Input Parameters: Alist ; Same Format to "TrackParticle"
!
!                    written by T.Okugi (12/08/2004)
!
StatList3[Alist_]:=Module[{BBB,XXX,YYY},
  XXX=Statistics[Alist[2,1]];
  YYY=Statistics[Alist[2,3]];
  {XXX[2]*1e6,YYY[2]*1e9,XXX[1]*1e6,YYY[1]*1e9}];


StatList5[Alist_]:=Module[{BBB,XXX,YYY,XMEAN1,XMEAN2,XRMS1,XRMS2,YMEAN1,YMEAN2,YRMS1,YRMS2},
  XXX=Statistics[Alist[2,1]];
  YYY=Statistics[Alist[2,3]];
  XMEAN1=XXX[1];XRMS1=XXX[2];
  YMEAN1=YYY[1];YRMS1=YYY[2];
  XXX=Statistics2[Alist[2,1],XMEAN1,XRMS1];
  YYY=Statistics2[Alist[2,3],YMEAN1,YRMS1];
  XMEAN2=XXX[1];XRMS2=XXX[2];
  YMEAN2=YYY[1];YRMS2=YYY[2];
  {XRMS1*1e6,YRMS1*1e9,XRMS2*1e6,YRMS2*1e9}];








StatList4[Alist_]:=Module[{BBB,XXX,YYY},
  XXX=Statistics[Alist[2,1]];
  YYY=Statistics[Alist[2,3]];
  {XXX[2],YYY[2],XXX[1],YYY[1]}];



BeamSizeSta[Alist_]:=Module[{EEX,EEY,BBX,BBY,XXX,YYY,EXX,EYY},
  EEX=Twiss["PEX",Alist[1]]*Alist[2,6];
  EEY=Twiss["PEY",Alist[1]]*Alist[2,6];
!  EEX=0;EEY=0;
  BBX=Alist[2,1];
  BBY=Alist[2,3];
  XXX=Statistics[BBX];
  YYY=Statistics[BBY];
  EXX=Statistics[EEX];
  EYY=Statistics[EEY];
  {XXX[2],YYY[2],EXX[2],EYY[2]}];





! --- Function to Write the Results to File
!
!  Input Parameters: Fname ; Output File name
!                    Alist ; Input List, Same Format to "TrackParticle"
!
!                    written by T.Okugi (12/08/2004)
!
PrintOutput[Fname_,Alist_]:=Module[{LLL,Blist,File,II},
  $FORM="F15.6";
  LLL=Length[Alist[2,1]];
  File=OpenWrite[Fname];
  Blist=Alist*1e3; 
  Do[Write[File,Blist[2,1,II]//",  "//Blist[2,2,II]//",  "//Blist[2,3,II]//
        ",  "// Blist[2,4,II]//",  "//Blist[2,5,II]//",  "//Blist[2,6,II]],
  {II,LLL}];Close[File];];


! --- Function to Write the Results to File
!
!  Input Parameters: Fname ; Output File name
!                    Alist ; Input List, Same Format to "TrackParticle"
!
!                    written by T.Okugi (12/08/2004)
!
PrintOutput2[Fname_,Alist_]:=Module[{LLL,NNN,Blist,File,II},
  LLL=Length[Alist[2,1]];
  File=OpenWrite[Fname];
  Blist=Alist*1e3;
  NNN=Count[Alist[2,7],1];
  $FORM="F15.0";
  Write[File,NNN];
  $FORM="F15.6";
  Do[If[Alist[2,7,II]==1,
     Write[File,Blist[2,1,II],Blist[2,2,II],Blist[2,3,II],Blist[2,4,II],
                Blist[2,5,II],Blist[2,6,II]]],{II,LLL}];Close[File];];






LinearParamIP[Alist_,BX0_,BY0_]:=Module[
 {X00,XP0,Y00,YP0,D00,AX00,BX00,CX00,EX00,EPX0,AY00,BY00,CY00,EY00,EPY0,R310,R320,R410,R420},
  X00=Statistics[Alist[2,1]][1];
  XP0=Statistics[Alist[2,2]][1];
  Y00=Statistics[Alist[2,3]][1];
  YP0=Statistics[Alist[2,4]][1];
  D00=Statistics[Alist[2,6]][1];
  AX00=-Statistics[(Alist[2,1]-X00)*(Alist[2,2]-XP0)][1]/EMITX;
!  BX00=Twiss["BX","IP"];
  BX00=BX0;;
  CX00=(1+AX00*AX00)/BX00;
  EX00=+Statistics[(Alist[2,1]-X00)*(Alist[2,6]-D00)][1]/DP/DP;
  EPX0=+Statistics[(Alist[2,2]-XP0)*(Alist[2,6]-D00)][1]/DP/DP;
  AY00=-Statistics[(Alist[2,3]-Y00)*(Alist[2,4]-YP0)][1]/EMITY;
!  BY00=Twiss["BY","IP"];
  BY00=BY0;
  CY00=(1+AY00*AY00)/BY00;
  EY00=+Statistics[(Alist[2,3]-Y00)*(Alist[2,6]-D00)][1]/DP/DP;
  EPY0=+Statistics[(Alist[2,4]-YP0)*(Alist[2,6]-D00)][1]/DP/DP;
  R310=+Statistics[(Alist[2,3]-Y00)*(Alist[2,1]-X00)][1]/EMITX/BX00;
  R320=+Statistics[(Alist[2,3]-Y00)*(Alist[2,2]-XP0)][1]/EMITX/CX00;
  R410=+Statistics[(Alist[2,4]-YP0)*(Alist[2,1]-X00)][1]/EMITY/CY00;
  R420=+Statistics[(Alist[2,4]-YP0)*(Alist[2,2]-XP0)][1]/EMITY/CY00;
!  R310=+Statistics[(Alist[2,3]-Y00)*(Alist[2,1]-X00)][1];
!  R320=+Statistics[(Alist[2,3]-Y00)*(Alist[2,2]-XP0)][1];
!  R410=+Statistics[(Alist[2,4]-YP0)*(Alist[2,1]-X00)][1];
!  R420=+Statistics[(Alist[2,4]-YP0)*(Alist[2,2]-XP0)][1];
  {{AX00,EX00,EPX0},{AY00,EY00,EPY0},{R310,R320,R410,R420}}];




NonlinearParamIP[Alist_]:=Module[
 {X00,XP0,Y00,YP0,D00,T122,T124,T126,T144,T146,T166,T322,T324,T326,T344,T346,T366},
  X00=Statistics[Alist[2,1]][1];
  XP0=Statistics[Alist[2,2]][1];
  Y00=Statistics[Alist[2,3]][1];
  YP0=Statistics[Alist[2,4]][1];
  D00=Statistics[Alist[2,6]][1];
  T122=Statistics[(Alist[2,1]-Y00)*(Alist[2,2]-XP0)*(Alist[2,2]-XP0)][1]
      /Statistics[Alist[2,2]][2]/Statistics[Alist[2,2]][2];
  T124=Statistics[(Alist[2,1]-Y00)*(Alist[2,2]-XP0)*(Alist[2,4]-YP0)][1]
      /Statistics[Alist[2,2]][2]/Statistics[Alist[2,4]][2];
  T126=Statistics[(Alist[2,1]-Y00)*(Alist[2,2]-XP0)*(Alist[2,6]-D00)][1]
      /Statistics[Alist[2,2]][2]/Statistics[Alist[2,6]][2];
  T144=Statistics[(Alist[2,1]-Y00)*(Alist[2,4]-YP0)*(Alist[2,4]-YP0)][1]
      /Statistics[Alist[2,4]][2]/Statistics[Alist[2,4]][2];
  T146=Statistics[(Alist[2,1]-Y00)*(Alist[2,4]-YP0)*(Alist[2,6]-D00)][1]
      /Statistics[Alist[2,4]][2]/Statistics[Alist[2,6]][2];
  T166=Statistics[(Alist[2,1]-Y00)*(Alist[2,6]-D00)*(Alist[2,6]-D00)][1]
      /Statistics[Alist[2,6]][2]/Statistics[Alist[2,6]][2];
  T322=Statistics[(Alist[2,3]-Y00)*(Alist[2,2]-XP0)*(Alist[2,2]-XP0)][1]
      /Statistics[Alist[2,2]][2]/Statistics[Alist[2,2]][2];
  T324=Statistics[(Alist[2,3]-Y00)*(Alist[2,2]-XP0)*(Alist[2,4]-YP0)][1]
      /Statistics[Alist[2,2]][2]/Statistics[Alist[2,4]][2];
  T326=Statistics[(Alist[2,3]-Y00)*(Alist[2,2]-XP0)*(Alist[2,6]-D00)][1]
      /Statistics[Alist[2,2]][2]/Statistics[Alist[2,6]][2];
  T344=Statistics[(Alist[2,3]-Y00)*(Alist[2,4]-YP0)*(Alist[2,4]-YP0)][1]
      /Statistics[Alist[2,4]][2]/Statistics[Alist[2,4]][2];
  T346=Statistics[(Alist[2,3]-Y00)*(Alist[2,4]-YP0)*(Alist[2,6]-D00)][1]
      /Statistics[Alist[2,4]][2]/Statistics[Alist[2,6]][2];
  T366=Statistics[(Alist[2,3]-Y00)*(Alist[2,6]-D00)*(Alist[2,6]-D00)][1]
      /Statistics[Alist[2,6]][2]/Statistics[Alist[2,6]][2];
  {{T122,T124,T126,T144,T146,T166},{T322,T324,T326,T344,T346,T366}}];









