
! --- Function to Make Input Parameters for 
!       the function "TrackParticles"
!
!  Input Parameters: Dev ; Device Name or Number at the Begining of Tracking 
!                    NNN ; Number of Particles for Trackin
!
!                    written by T.Okugi (12/08/2004)
!
MapBeamParameters[Dev_,NNN_]:=Module[
 {POS,TwsX,TwsY,TwsZ,EMX,EMY,XXX,XXP,YYY,YYP,ZZZ,EEE,FFF,ENE,
  Rnd1,Rnd2,Rnd3,Rnd4,Rnd5,Rnd6,ANS1,ANS2,II},
  FFS["CALC"]; POS=LINE["Position",Dev]; ANS1=POS;
  TwsX=Twiss[{"AX","BX","EX","EPX","DX","DPX"},POS];
  TwsY=Twiss[{"AY","BY","EY","EPY","DY","DPY"},POS];
  TwsZ={DZ,DP,DP0};
  EMX=EMITX; EMY=EMITY;
  XXX=Table[0,{II,NNN}];
  XXP=Table[0,{II,NNN}];
  YYY=Table[0,{II,NNN}];
  YYP=Table[0,{II,NNN}];
  ZZZ=Table[0,{II,NNN}];
  EEE=Table[0,{II,NNN}];
  FFF=Table[0,{II,NNN}];
  Do[Rnd1=GaussRandom[];Rnd2=GaussRandom[];Rnd3=GaussRandom[];
     Rnd4=GaussRandom[];Rnd5=GaussRandom[];Rnd6=GaussRandom[];
     ENE=TwsZ[2]*Rnd6+TwsZ[3];
     XXX[II]=TwsX[5]+Sqrt[EMX*TwsX[2]]*Rnd1+TwsX[3]*ENE;
     XXP[II]=TwsX[6]+(Rnd2-TwsX[1]*Rnd1)*Sqrt[EMX/TwsX[2]]+TwsX[4]*ENE;
     YYY[II]=TwsY[5]+Sqrt[EMY*TwsY[2]]*Rnd3+TwsY[3]*ENE;
     YYP[II]=TwsY[6]+(Rnd4-TwsY[1]*Rnd3)*Sqrt[EMY/TwsY[2]]+TwsY[4]*ENE;
     ZZZ[II]=TwsZ[1]*Rnd5;
     EEE[II]=ENE;
     FFF[II]=1;
  ,{II,NNN}];
   ANS2={XXX,XXP,YYY,YYP,ZZZ,EEE,FFF};{ANS1,ANS2}];

! --- Function to Calculate RMS and MEAN for the List 
!
!  Input Parameters: InputList ; Input List
!  Output Format   : {MeanValue, RMSvalue}
!
!                    written by T.Okugi (12/08/2004)
!
Statistics[InputList_]:=Module[{LLL,BBB,MEAN,DEV,RMS},
  LLL=Length[InputList];
  MEAN=Apply[Plus,InputList]/LLL;
  BBB=(InputList-MEAN)^2;
  DEV=Apply[Plus,BBB]/LLL;
  RMS=Sqrt[DEV];
  {MEAN,RMS}];

! ---- USAGE ----



Get["temp_tracking.n"];
EMITX= 2e-09;
EMITY=12e-12;
DP=8e-4;
DP0=0;
DZ=10e-3;
PIP=LINE["Position","IP"];
AAA=MapBeamParameters[1,20000];
BBB=TrackParticles[AAA,PIP];
Statistics[BBB[2,5]] ! -- bunch length

end;