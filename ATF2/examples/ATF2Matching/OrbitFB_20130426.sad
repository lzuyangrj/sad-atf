2OFF ECHO CTIME;ON COD RFSW RADCOD; ON ECHO;

LINE DUMMY = (P0, L0);

DRIFT L0 = ( L=1 );
MARK  P0 = (AX = 1  BX = 3  AY = -2    BY = 3 );

!
! ---  How to use EPICS IOC -----
! /atf/op/epics/apptop/atfApp/Db/dbOkugi.db
! make - @atfopr(atfatf2)
!
! atfioc ioc_okugi start @atfopr
! atfioc ioc_okugi stop  @atfopr
!

FFS USE=DUMMY;
Get["/userhome/okugi/SAD/lib/tracking.n"];
TRPT;
cal;


! ----   Initial Setup  ------
!
ZHFB1name="ZH4X";
ZHFB1amp=0.500;
!
ZHFB2name="ZH9X";
ZHFB2amp=0.200;
!
ZVFB1name="ZV2X";
ZVFB1amp=0.040;
!
ZVFB2name="ZV9X";
ZVFB2amp=0.040;


BPMnames={{"MQF1X",    2},
          {"MQD2X",    3},
          {"MQF3X",    4},
          {"MQF4X",    5},
          {"MQD5X",    6},
          {"MQF6X",    7},
          {"MQF7X",    8},
          {"MQD8X",    9},
          {"MQF9X",   10},
          {"MQD10X",  11},
          {"MQF11X",  12},
          {"MQD12X",  13},
          {"MQF13X",  14},
          {"MQD14X",  15},
          {"MQF15X",  16},
          {"MQD16X",  17},
          {"MQF17X",  18},
          {"MQD18X",  19},
          {"MQF19X",  20},
          {"MQD20X",  21},
          {"MQF21X",  22},
          {"MQM16FF", 28},
          {"MQM15FF", 29},
          {"MQM14FF", 30},
          {"MQM13FF", 32},
          {"MQM12FF", 33},
          {"MQM11FF", 35},
          {"MQD10BFF",36},
          {"MQD10AFF",37},
          {"MQF9BFF", 38},
          {"MQF9AFF", 40},
          {"MQD8FF",  41},
          {"MQF7FF",  42},
          {"MQD6FF",  43},
          {"MQF5BFF", 44},
          {"MQF5AFF", 46},
          {"MQD4BFF", 47},
          {"MQD4AFF", 49},
          {"MQF3FF",  50},
          {"MQD2BFF", 51},
          {"MQD2AFF", 52}
};

GetBPMposition:=Module[{BPMread,BeamIntensity,BPMposi},
  BPMread=CaGet["ATF2:monitors"][1];
  BeamIntensity=BPMread[604];
  BPMposi=Table[{BPMnames[II,1],
         BPMread[BPMnames[II,2]*10+1],
         BPMread[BPMnames[II,2]*10+2],
         BPMread[BPMnames[II,2]*10+3]},{II,Length[BPMnames]}];
  {BeamIntensity,BPMposi}];

GetInitialParam:=Module[{},
  Print[" "];
  Print[" FB MAIN SWITCH     :",CaGet["Okugi:FB:main:sw"][1]];
  Print[" FB STATUS          :",CaGet["Okugi:FB:status"][1]];
  Print[" Intensity Cut [e9] :",CaGet["Okugi:FB:IntensityCut"][1]];
  Print[" "];
  Do[Print[BPMnames[II,1]," ",CaGet["Okugi:FB:"//BPMnames[II,1]//":X:sw"][1]
                         ," ",CaGet["Okugi:FB:"//BPMnames[II,1]//":Y:sw"][1]]
    ,{II,Length[BPMnames]}];];

BPM10shot:=Module[{NN,Icut,BPMsig,BPMX,BPMY,BPMave},
  NN=Length[BPMnames];
  BPMX=Table[0,{II,NN},{JJ,10}];
  BPMY=Table[0,{II,NN},{JJ,10}];
  BPMave=Table[0,{II,NN},{JJ,4}];
  Do[TkSense[0.5];BPMsig=GetBPMposition;
     Icut=CaGet["Okugi:FB:IntensityCut"][1];
     While[BPMsig[1]<Icut,
           Icut=CaGet["Okugi:FB:IntensityCut"][1];
           TkSense[2];BPMsig=GetBPMposition;
           Print[KK," Icut:",Icut,"   I:",BPMsig[1]];
           CaPut["Okugi:FB:main:message"," Icut:"//Icut//"   I:"//BPMsig[1]];
     ];
     CaPut["Okugi:FB:main:message"," N =  "//KK//"/10"];
     Do[Do[BPMX[II,JJ]=BPMX[II,JJ-1];
           BPMY[II,JJ]=BPMY[II,JJ-1],{JJ,10,2,-1}];
     BPMX[II,1]=BPMsig[2,II,3];
     BPMY[II,1]=BPMsig[2,II,4],{II,NN}],{KK,10}];
  Do[BPMave[II,1]=Statistics[BPMX[II]][1];
     BPMave[II,2]=Statistics[BPMX[II]][2];
     BPMave[II,3]=Statistics[BPMY[II]][1];
     BPMave[II,4]=Statistics[BPMY[II]][2],{II,NN}];
BPMave];

ResponseRead:=Module[{NN,ZH1orig,ZV1orig,ZH2orig,ZV2orig,RR,
  XY0,X011,X012,X021,X022,Y011,Y012,Y021,Y022,AAA,BBB,CCC,fn},
  AAA=Date[];
  BBB=AAA[1]*10000+AAA[2]*100+AAA[3];
  CCC=AAA[4]*100+AAA[5];
  fn=OpenWrite["/atf/sad/operation/OrbitFB/FBcalib_"//BBB//"_"//CCC//".txt"];
  CaPut[ZHFB1name//":currentRead.PROC",1];
  CaPut[ZHFB2name//":currentRead.PROC",1];
  CaPut[ZVFB1name//":currentRead.PROC",1];
  CaPut[ZVFB2name//":currentRead.PROC",1];
  ZH1orig=CaGet[ZHFB1name//":currentRead"][1];
  ZH2orig=CaGet[ZHFB2name//":currentRead"][1];
  ZV1orig=CaGet[ZVFB1name//":currentRead"][1];
  ZV2orig=CaGet[ZVFB2name//":currentRead"][1];
  CaPut["Okugi:FB:calib:message"," Normal Orbit Data Taking ... "];
  XY0=BPM10shot;
  CaPut["Okugi:FB:calib:message",ZHFB1name//" Data Taking ... "];
  CaPut[ZHFB1name//":currentWrite",ZH1orig+ZHFB1amp];TkSense[5];
  X011=BPM10shot;
  CaPut[ZHFB1name//":currentWrite",ZH1orig-ZHFB1amp];TkSense[5];
  X012=BPM10shot;
  CaPut[ZHFB1name//":currentWrite",ZH1orig];
  CaPut["Okugi:FB:calib:message",ZHFB2name//" Data Taking ... "];
  CaPut[ZHFB2name//":currentWrite",ZH2orig+ZHFB2amp];TkSense[5];
  X021=BPM10shot;
  CaPut[ZHFB2name//":currentWrite",ZH2orig-ZHFB2amp];TkSense[5];
  X022=BPM10shot;
  CaPut[ZHFB2name//":currentWrite",ZH2orig];
  CaPut["Okugi:FB:calib:message",ZVFB1name//" Data Taking ... "];
  CaPut[ZVFB1name//":currentWrite",ZV1orig+ZVFB1amp];TkSense[5];
  Y011=BPM10shot;
  CaPut[ZVFB1name//":currentWrite",ZV1orig-ZVFB1amp];TkSense[5];
  Y012=BPM10shot;
  CaPut[ZVFB1name//":currentWrite",ZV1orig];
  CaPut["Okugi:FB:calib:message",ZVFB2name//" Data Taking ... "];
  CaPut[ZVFB2name//":currentWrite",ZV2orig+ZVFB2amp];TkSense[5];
  Y021=BPM10shot;
  CaPut[ZVFB2name//":currentWrite",ZV2orig-ZVFB2amp];TkSense[5];
  Y022=BPM10shot;
  CaPut[ZVFB2name//":currentWrite",ZV2orig];
  NN=Length[BPMnames];
  RR=Table[{0,{0,0,0,0,0,0},{0,0,0,0,0,0}},{II,NN}];
  Do[RR[II,1]=BPMnames[II,1];
     RR[II,2,1]=XY0[II,1];
     RR[II,2,2]=XY0[II,2];
     RR[II,2,3]=(X011[II,1]-X012[II,1])/2;
     RR[II,2,4]=Sqrt[X011[II,2]^2+X012[II,2]^2]/2;
     RR[II,2,5]=(X021[II,1]-X022[II,1])/2;
     RR[II,2,6]=Sqrt[X021[II,2]^2+X022[II,2]^2]/2;
     RR[II,3,1]=XY0[II,3];
     RR[II,3,2]=XY0[II,4];
     RR[II,3,3]=(Y011[II,3]-Y012[II,3])/2;
     RR[II,3,4]=Sqrt[Y011[II,4]^2+Y012[II,4]^2]/2;
     RR[II,3,5]=(Y021[II,3]-Y022[II,3])/2;
     RR[II,3,6]=Sqrt[Y021[II,4]^2+Y022[II,4]^2]/2,{II,NN}];
     Write[fn," CalibData = ",RR,";"];Close[fn];RR];

ShowX:=Module[{AAA,NN},
  AAA=Reference;
  NN=Length[AAA];
  Do[Print[AAA[II,1],
     "   ",AAA[II,2,1]," ",AAA[II,2,2],
     "   ",AAA[II,2,3]," ",AAA[II,2,4],
     "   ",AAA[II,2,5]," ",AAA[II,2,6]],{II,NN}];];

ShowY:=Module[{AAA,NN},
  AAA=Reference;
  NN=Length[AAA];
  Do[Print[AAA[II,1],
     "   ",AAA[II,3,1]," ",AAA[II,3,2],
     "   ",AAA[II,3,3]," ",AAA[II,3,4],
     "   ",AAA[II,3,5]," ",AAA[II,3,6]],{II,NN}];];

StartFB:=Module[
 {NN,MM,BPMsig,BPMX,BPMY,Icut,ZH1,ZH2,ZV1,ZV2,ZH1orig,ZH2orig,ZV1orig,ZV2orig,
  MX,MY,MATX1,MATX2,MATY1,MATY2,VECX1,VECX2,VECY1,VECY2,ZH1name,ZH2name,ZV1name,ZV2name},
  NN=Length[BPMnames];KK=0;
  BPMX=Table[0,{II,NN},{JJ,10}];
  BPMY=Table[0,{II,NN},{JJ,10}];
  CaPut["Okugi:FB:main:sw","ON"];
  While[CaGet["Okugi:FB:main:sw"][1]=="ON",
        CaPut["Okugi:FB:status","ON"];
        KK=KK+1;TkSense[0.5];BPMsig=GetBPMposition;
        CaPut["Okugi:FB:main:message"," FB loop : "//KK];
        Icut=CaGet["Okugi:FB:IntensityCut"][1];
        While[BPMsig[1]<Icut,
              Icut=CaGet["Okugi:FB:IntensityCut"][1];
              TkSense[2];BPMsig=GetBPMposition;
              CaPut["Okugi:FB:main:message"," Icut:"//Icut//"   I:"//BPMsig[1]];
        ];
	While[CaGet["atf:rfRamp"][1]=="ON",
              CaPut["Okugi:FB:main:message"," RF Ramp ON "];TkSense[2];
        ];
        Do[Do[BPMX[II,JJ]=BPMX[II,JJ-1];
              BPMY[II,JJ]=BPMY[II,JJ-1],{JJ,10,2,-1}];
           BPMX[II,1]=BPMsig[2,II,3];
           BPMY[II,1]=BPMsig[2,II,4],{II,NN}];
        If[KK>10,
           MX=0;MY=0;
           Do[If[CaGet["Okugi:FB:"//BPMnames[II,1]//":X:sw"][1]=="ON",MX=MX+1],{II,NN}];
           If[MX>2,
              MATX1=Table[{0,0},{II,MX}];
              VECX1=Table[0,{II,MX}];
              MM=0;
              Do[If[CaGet["Okugi:FB:"//BPMnames[II,1]//":X:sw"][1]=="ON",MM=MM+1;
                 MATX1[MM]={Reference[II,2,3],Reference[II,2,5]};
	         VECX1[MM]=Statistics[BPMX[II]][1]-Reference[II,2,1]],{II,NN}];
              MATX2=Inverse[MATX1];
              VECX2=MATX2.VECX1;
	      CaPut[ZHFB1name//":currentRead.PROC",1];
	      CaPut[ZHFB2name//":currentRead.PROC",1];
	      ZH1orig=CaGet[ZHFB1name//":currentRead"][1];
	      ZH2orig=CaGet[ZHFB2name//":currentRead"][1];
	      ZH1=ZH1orig-0.08*ZHFB1amp*VECX2[1];
	      ZH2=ZH2orig-0.08*ZHFB2amp*VECX2[2];
	      CaPut[ZHFB1name//":currentWrite",ZH1];
  	      CaPut[ZHFB2name//":currentWrite",ZH2];
           ];
           Do[If[CaGet["Okugi:FB:"//BPMnames[II,1]//":Y:sw"][1]=="ON",MY=MY+1],{II,NN}];
           If[MY>2,
              MATY1=Table[{0,0},{II,MY}];
              VECY1=Table[0,{II,MY}];
              MM=0;
              Do[If[CaGet["Okugi:FB:"//BPMnames[II,1]//":Y:sw"][1]=="ON",MM=MM+1;
                 MATY1[MM]={Reference[II,3,3],Reference[II,3,5]};
	         VECY1[MM]=Statistics[BPMY[II]][1]-Reference[II,3,1]],{II,NN}];
              MATY2=Inverse[MATY1];
              VECY2=MATY2.VECY1;
	      CaPut[ZVFB1name//":currentRead.PROC",1];
	      CaPut[ZVFB2name//":currentRead.PROC",1];
	      ZV1orig=CaGet[ZVFB1name//":currentRead"][1];
	      ZV2orig=CaGet[ZVFB2name//":currentRead"][1];
	      ZV1=ZV1orig-0.08*ZVFB1amp*VECY2[1];
	      ZV2=ZV2orig-0.08*ZVFB2amp*VECY2[2];
	      CaPut[ZVFB1name//":currentWrite",ZV1];
  	      CaPut[ZVFB2name//":currentWrite",ZV2];
           ];
           $FORM="F9.5";
             CaPut["Okugi:FB:main:message",ZH1//" "//ZH2//" "//ZV1//" "//ZV2];
           $FORM="";
        ];
  ];
  CaPut["Okugi:FB:status","OFF"];
];



CaPut["Okugi:FB:server:stop","START"];
CaPut["Okugi:FB:status","OFF"];
CaPut["Okugi:FB:calib:sw","OFF"];
CaPut["Okugi:FB:calib:status","OFF"];
CaPut["Okugi:FB:calib:message","Calibration is not yet performed!"];

GetInitialParam

!BPM10shot
!end;

While[CaGet["Okugi:FB:server:stop"][1]=="START",
      CaPut["Okugi:FB:server:message","Orbit FB server is running ..."];
      While[CaGet["Okugi:FB:main:sw"][1]=="OFF",TkSense[5];
            If[CaGet["Okugi:FB:calib:sw"][1]=="ON",
               CaPut["Okugi:FB:calib:message","Calibrating ... "];
               Reference=ResponseRead;
               CaPut["Okugi:FB:calib:sw","OFF"];
               CaPut["Okugi:FB:calib:message","Calibration was done!"];
               CaPut["Okugi:FB:calib:status","ON"];
            ];
            If[CaGet["Okugi:FB:server:stop"][1]=="STOP",
               CaPut["Okugi:FB:server:message","Orbit FB server is stopped!"];
               CaPut["Okugi:FB:calib:sw","OFF"];
               CaPut["Okugi:FB:calib:message"," "];
               FFS["abort"];
            ];
      ];
      If[CaGet["Okugi:FB:calib:status"][1]=="OFF",
         CaPut["Okugi:FB:calib:message","Calibrating ... "];
         Reference=ResponseRead;
         CaPut["Okugi:FB:calib:sw","OFF"];
         CaPut["Okugi:FB:calib:message","Calibration was done!"];
         CaPut["Okugi:FB:calib:status","ON"];
      ];
      StartFB;
];

CaPut["Okugi:FB:calib:sw","OFF"];
CaPut["Okugi:FB:calib:message"," "];
CaPut["Okugi:FB:server:stop","STOP"];
CaPut["Okugi:FB:server:message","Orbit FB server is stopped!"];

FFS["abort"];
end;








