
CalcBTBumpData[fn_]:=Module[{allq=allQ, allzh=xcorr, allzv=ycorr,
                              allb=LINE["Name","B*"], allbpm=LINE["Name","{IM}*"] },
 
 allzvpos=Map[LINE["Position",#]&,allzv];
 allzhpos=Map[LINE["Position",#]&,allzh];
 allzvset=Map[(spos=LINE["Position",#];name=#;
               pup=Select[allzvpos,(#<spos)&];
               pdown=Select[allzvpos,(#>spos)&];
             If[(Length[pup]=>2), 
              If[(Length[pdown]=>2),
                  zpos2=pup[Length[pup]];
                  zpos1=pup[Length[pup]-1];
                  zpos3=pdown[1];
                  zpos4=pdown[2];
              ,
                  zpos2=pup[Length[pup]];
                  zpos1=pup[Length[pup]-1];
                  zpos3=1;
                  zpos4=1;
              ];
               {spos,zpos1,zpos2,zpos3,zpos4}])&,Join[allq,allb,allbpm]];

 allzhset=Map[(spos=LINE["Position",#];name=#;
               pup=Select[allzhpos,(#<spos)&];
               pdown=Select[allzhpos,(#>spos)&];
             If[(Length[pup]=>2), 
              If[(Length[pdown]=>2),
                  zpos2=pup[Length[pup]];
                  zpos1=pup[Length[pup]-1];
                  zpos3=pdown[1];
                  zpos4=pdown[2];
              ,
                  zpos2=pup[Length[pup]];
                  zpos1=pup[Length[pup]-1];
                  zpos3=1;
                  zpos4=1;
              ];
               {spos,zpos1,zpos2,zpos3,zpos4}])&,Join[allq,allb,allbpm]];

 allzvset=Select[allzvset,(Length[#]>1)&];
 allzhset=Select[allzhset,(Length[#]>1)&];

 FFS["z* k0 0"];
 FFS["cal"];

 bumplisty=Map[(spos=#[1];
   zpos1=#[2];zpos2=#[3];zpos3=#[4];zpos4=#[5];
   r112=TransferMatrix[zpos1+0.5,spos+0.5][3,4];
   r122=TransferMatrix[zpos1+0.5,spos+0.5][4,4];
   r212=TransferMatrix[zpos2+0.5,spos+0.5][3,4];
   r222=TransferMatrix[zpos2+0.5,spos+0.5][4,4];

   AAr={{r112,r212},{r122,r222}};
   bumpu=Inverse[AAr];
   bumpd={{0,0},{0,0}};

  If[zpos4>1,
   s112=TransferMatrix[zpos1+0.5,zpos4+1][3,4];
   s122=TransferMatrix[zpos1+0.5,zpos4+1][4,4];
   s212=TransferMatrix[zpos2+0.5,zpos4+1][3,4];
   s222=TransferMatrix[zpos2+0.5,zpos4+1][4,4];
   s312=TransferMatrix[zpos3+0.5,zpos4+1][3,4];
   s322=TransferMatrix[zpos3+0.5,zpos4+1][4,4];
   s412=TransferMatrix[zpos4+0.5,zpos4+1][3,4];
   s422=TransferMatrix[zpos4+0.5,zpos4+1][4,4];
   
   AAs={{s312,s412},{s322,s422}};
   BBs=Inverse[AAs];
   bumpd=BBs.{{-s112,-s212},{-s122,-s222}}.bumpu;
  ];
   {spos,{zpos1,zpos2,zpos3,zpos4},Join[bumpu,bumpd]})&,allzvset];


 bumplistx=Map[(spos=#[1];
   zpos1=#[2];zpos2=#[3];zpos3=#[4];zpos4=#[5];
   r112=TransferMatrix[zpos1+0.5,spos+0.5][1,2];
   r122=TransferMatrix[zpos1+0.5,spos+0.5][2,2];
   r212=TransferMatrix[zpos2+0.5,spos+0.5][1,2];
   r222=TransferMatrix[zpos2+0.5,spos+0.5][2,2];

   AAr={{r112,r212},{r122,r222}};
   bumpu=Inverse[AAr];
   bumpd={{0,0},{0,0}};

  If[zpos4>1,
   s112=TransferMatrix[zpos1+0.5,zpos4+1][1,2];
   s122=TransferMatrix[zpos1+0.5,zpos4+1][2,2];

   s212=TransferMatrix[zpos2+0.5,zpos4+1][1,2];
   s222=TransferMatrix[zpos2+0.5,zpos4+1][2,2];

   s312=TransferMatrix[zpos3+0.5,zpos4+1][1,2];
   s322=TransferMatrix[zpos3+0.5,zpos4+1][2,2];

   s412=TransferMatrix[zpos4+0.5,zpos4+1][1,2];
   s422=TransferMatrix[zpos4+0.5,zpos4+1][2,2];
   
   AAs={{s312,s412},{s322,s422}};
   BBs=Inverse[AAs];
   bumpd=BBs.{{-s112,-s212},{-s122,-s222}}.bumpu;
  ];

   {spos,{zpos1,zpos2,zpos3,zpos4},Join[bumpu,bumpd]})&,allzhset];

  
  $FORM="8.5";
  Print[file];

  Scan[(
   Write[fn,"'"//LINE["Name",#[1]]//"','y','"//
            LINE["Name",#[2,1]]//"','"//LINE["Name",#[2,2]]//"','"//
            LINE["Name",#[2,3]]//"','"//LINE["Name",#[2,4]]//"',",
            STCur[LINE["Name",#[2,1]],#[3,1,1]],",",STCur[LINE["Name",#[2,1]],#[3,1,2]],",",
            STCur[LINE["Name",#[2,2]],#[3,2,1]],",",STCur[LINE["Name",#[2,2]],#[3,2,2]],",",
            STCur[LINE["Name",#[2,3]],#[3,3,1]],",",STCur[LINE["Name",#[2,3]],#[3,3,2]],",",
            STCur[LINE["Name",#[2,4]],#[3,4,1]],",",STCur[LINE["Name",#[2,4]],#[3,4,2]]];
  )&,bumplisty];

  Scan[(
   Write[fn,"'"//LINE["Name",#[1]]//"','x','"//
            LINE["Name",#[2,1]]//"','"//LINE["Name",#[2,2]]//"','"//
            LINE["Name",#[2,3]]//"','"//LINE["Name",#[2,4]]//"',",
            STCur[LINE["Name",#[2,1]],#[3,1,1]],",",STCur[LINE["Name",#[2,1]],#[3,1,2]],",",
            STCur[LINE["Name",#[2,2]],#[3,2,1]],",",STCur[LINE["Name",#[2,2]],#[3,2,2]],",",
            STCur[LINE["Name",#[2,3]],#[3,3,1]],",",STCur[LINE["Name",#[2,3]],#[3,3,2]],",",
            STCur[LINE["Name",#[2,4]],#[3,4,1]],",",STCur[LINE["Name",#[2,4]],#[3,4,2]]];
  )&,bumplistx];

 
! System["chmod 664 "//file];

];




  setbumpy[ns_,y0_,py0_]:=(
      LINE["K0",bumplisty[ns][2,1]]=LINE["K0",bumplisty[ns][2,1]]+bumplisty[ns][3,1,1]*y0+bumplisty[ns][3,1,2]*py0;
      LINE["K0",bumplisty[ns][2,2]]=LINE["K0",bumplisty[ns][2,2]]+bumplisty[ns][3,2,1]*y0+bumplisty[ns][3,2,2]*py0;
      LINE["K0",bumplisty[ns][2,3]]=LINE["K0",bumplisty[ns][2,3]]+bumplisty[ns][3,3,1]*y0+bumplisty[ns][3,3,2]*py0;
      LINE["K0",bumplisty[ns][2,4]]=LINE["K0",bumplisty[ns][2,4]]+bumplisty[ns][3,4,1]*y0+bumplisty[ns][3,4,2]*py0;
  );

  resetbumpy[ns_,y0_,py0_]:=(
      LINE["K0",bumplisty[ns][2,1]]=LINE["K0",bumplisty[ns][2,1]]-bumplisty[ns][3,1,1]*y0-bumplisty[ns][3,1,2]*py0;
      LINE["K0",bumplisty[ns][2,2]]=LINE["K0",bumplisty[ns][2,2]]-bumplisty[ns][3,2,1]*y0-bumplisty[ns][3,2,2]*py0;
      LINE["K0",bumplisty[ns][2,3]]=LINE["K0",bumplisty[ns][2,3]]-bumplisty[ns][3,3,1]*y0-bumplisty[ns][3,3,2]*py0;
      LINE["K0",bumplisty[ns][2,4]]=LINE["K0",bumplisty[ns][2,4]]-bumplisty[ns][3,4,1]*y0-bumplisty[ns][3,4,2]*py0;
  );

  setbumpx[ns_,x0_,px0_]:=(
      LINE["K0",bumplistx[ns][2,1]]=LINE["K0",bumplistx[ns][2,1]]+bumplistx[ns][3,1,1]*x0+bumplistx[ns][3,1,2]*px0;
      LINE["K0",bumplistx[ns][2,2]]=LINE["K0",bumplistx[ns][2,2]]+bumplistx[ns][3,2,1]*x0+bumplistx[ns][3,2,2]*px0;
      LINE["K0",bumplistx[ns][2,3]]=LINE["K0",bumplistx[ns][2,3]]+bumplistx[ns][3,3,1]*x0+bumplistx[ns][3,3,2]*px0;
      LINE["K0",bumplistx[ns][2,4]]=LINE["K0",bumplistx[ns][2,4]]+bumplistx[ns][3,4,1]*x0+bumplistx[ns][3,4,2]*px0;
  );

  resetbumpx[ns_,x0_,px0_]:=(
      LINE["K0",bumplistx[ns][2,1]]=LINE["K0",bumplistx[ns][2,1]]-bumplistx[ns][3,1,1]*x0-bumplistx[ns][3,1,2]*px0;
      LINE["K0",bumplistx[ns][2,2]]=LINE["K0",bumplistx[ns][2,2]]-bumplistx[ns][3,2,1]*x0-bumplistx[ns][3,2,2]*px0;
      LINE["K0",bumplistx[ns][2,3]]=LINE["K0",bumplistx[ns][2,3]]-bumplistx[ns][3,3,1]*x0-bumplistx[ns][3,3,2]*px0;
      LINE["K0",bumplistx[ns][2,4]]=LINE["K0",bumplistx[ns][2,4]]-bumplistx[ns][3,4,1]*x0-bumplistx[ns][3,4,2]*px0;
  );

