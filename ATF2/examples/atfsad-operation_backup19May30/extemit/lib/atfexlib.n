 
!!  Get["~atfopr/sad/operation/lib/atf2lib.n"];


  SetExtQuad[n_:1]:=Module[{
    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{}},
! --- Modified by T.Okugi (2001/05/09) ---
!    k1list=Select[k1list,MemberQ[quadex,#[1]]&];
!    Scan[Check[Element["K1",#[1]]=#[2],]&,k1list]];
    k1list=Map[{qexname[#[[1]]],#[[2]]}&,k1list];
    k1list=Select[k1list,(MemberQ[quadex,#[1]] || MemberQ[qdrex,#[1]])&];Print[k1list];
    Scan[Check[LINE["K1",#[1]]=#[2],]&,k1list]];
! ----------------------------------------
  SetExtSteer[n_:1]:=Module[{
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{}},
    k0list=Map[{stname[#[[1]]],#[[2]]}&,k0list];
    k0list=Select[k0list,(MemberQ[sthex,#[1]] || MemberQ[stvex,#[1]] || MemberQ[stxex,#[1]])&];
    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0list]];
!  SetExtBend[n_:1]:=Module[{
!    k0blist=ATFBend/.ATFData[n]/.ATFBend->{}},
!    k0blist=Select[k0list,MemberQ[bendex,#[1]]&];
!    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0blist]];

!!!!!!!
  SaveExtOptics[f_]:=Module[
        {fn=OpenWrite["./ringoptics/"//f]},
!----- Modified by T.Okugi (2001/05/09) -----
!    qv=Thread[{quadex,Element["K1",quadex]}];
    qexv=Thread[{quadex,Element["K1",quadex]}];
    qdrv=Thread[{qdrex,Element["K1",qdrex]}];
!-----------------------------------------
    sthv=Thread[{sthex,Element["K0",sthex]}];
    stvv=Thread[{stvex,Element["K0",stvex]}];
    stxv=Thread[{stxex,Element["K0",stxex]}];
!    bendv=Thread[{bendex,Element["K0",bendex]}];
    $FORM="";
    Write[fn,
!----- Modified by T.Okugi (2001/05/09) -----
!      "qvex=",qv,
      "qexvex=",qexv,
      ";\n\nqdrvex=",qdrv,
!-----------------------------------------
      ";\n\nsthvex=",sthv,
      ";\n\nstvvex=",stvv,
      ";\n\nstxvex=",stxv,
!      ";\n\nbendvex=",bendv,
    ";"];
    Close[fn];
    System["chmod 664 "//f]];

  Bmag[a1_,b1_,a2_,b2_]:=((b2/b1+b1/b2+b1*b2*(a1/b1-a2/b2)^2)/2);
