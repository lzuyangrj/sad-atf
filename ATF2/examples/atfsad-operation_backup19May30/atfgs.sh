#!/bin/csh -f
#===================================================================================
#
# SAD Executable Script on the ATF Linux/Unix System
#
#	11-apr-2003 N.Terunuma
#
#	parameter $1 is a SAD FFS control filename with a correct directory.
#	parameter $2 is a client IP address with a display number ':0.0'.
#
#===================================================================================
#
# change to lower case characters
#
	setenv SADHost `echo $HOST | tr '[A-Z]' '[a-z]'`
	setenv SADfile `echo $1 | tr '[A-Z]' '[a-z]'`

# for ATF environment

	setenv SADFFSFile  /atf/op/atfsad/sad/operation/$1

# for SAD environment
	limit stacksize 200000

# Show gs Parameters

	echo " "
	echo " "
	echo "<< Start the ATFSAD Shell Script >>"
	echo " "
	echo "   SAD Hostname = "$SADHost 
	echo "   SAD FFSfile  = "$SADFFSFile 
	echo " "
	echo "   DISPLAY = "$DISPLAY
	echo " "
#	echo "   SAD_ROOT = "$SADROOT 
	echo " "

# execute SAD

	OldDir=`pwd`
	cd /atf/op/atfsad/sad/operation
	/usr/local/SAD-2958/arch/i386-Linux2/bin/sad1.exe < $SADFFSFile
	cd $OldDir

	exit

	/usr/local/SAD/arch/i386-Linux2/bin/sad1.exe < $SADFFSFile

