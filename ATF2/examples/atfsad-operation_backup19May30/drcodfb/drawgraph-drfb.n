
! input arguments are [datalist,f]
!datalist={{data,range,frame,text},{data,range,frame,text},{data,range,frame,text}, , , ,}
!
! data={data[1],data[2],data[3], , ,,data[n]}  (n>=1) 
!  data[i]={option1,option2,xlist,ylist,dylist}
! xlist is list of horizontal points
! ylist is list of vertical points
! dylist is list of vertical error of points. If there is no dylist, no error bar.
!
! option1 decide style of line
!     0: no line
!     1: solid
!     2: dashed
!
! option2 decide style of point
!     0: none
!     1: cross
!     2: star 
! 
! range={{xlow,xup},{ylow,yup}} decide limits of h and v values
! 
! frame={{xbottom,xtop},{ybottom,ytop}} decide size of frame
!   positions of bottoms and tops are (may be) relative to the screen size
!
! text={text[1],text[2],,,} is list of text data
!  text[i]={string, xposition, yposition,angle}
!    positions are (may be) relative to the screen size
!
! f is output file name, may not be used depending tools.

!!!! This should be changed depending on graphic tool
!!   DrawGraph[data_,range_,frame_,text_,f_:"dummyfileforfigure"]:=TopDrawGraph[data,range,frame,text,f];
!   DrawGraph[datalist_,f_]:=TopDrawGraph[datalist,f];
!!!!!!!!!!!!!!!!
   DrawGraph[datalist_,f_]:=CanvasDrawGraph[datalist,f];
!!!!!!!!!!!!!!!!

! This is for Canvas.
CanvasDrawGraph[datalist_,f_]:=Module[
! {},
{data,range,frame,text,xlist,ylist,dylist,opt1,opt2,scalex,scaley,offsetx,offsety,hight,width,c,w,txt,gr,plot,plotcolor},

 w=Window[];
 b=Button[w, Text -> "Next", Command :> TkReturn["ReturnToSAD"]];
 hight=600; width=800;
 c=Canvas[w, Height -> hight, Width -> width];

 $DisplayFunction = CanvasDrawer;
 Canvas$Widget = c;


! scalex=1/0.74; scaley=1/0.6;
! offsetx=0.185; offsety=0.22;
 scalex=1/0.76; scaley=1/0.64;
 offsetx=0.185; offsety=0.18;

 fig=
 Map[(
  data=#[1];range=#[2];frame=#[3];text=#[4];
!  Print[frame]; Print[range];Print[text];
  
  titley="";

   Scan[(
     If[#[4]>80 && #[4]<100, titley=#[1]];
    )&,text];

  gr=Map[(
   xlist=#[3];
   ylist=#[4];	
   If[Length[#]=>5,dylist=#[5]; datai=Thread[{xlist,ylist,dylist}],datai=Thread[{xlist,ylist}]];
   opt1=#[1];
   opt2=#[2];
   If[Length[#]=>5,dylist=#[5]];

   plot=0; pointcolor="green";
   If[opt2==1, pointcolor="black";];
   If[opt2==2, pointcolor="red";];
   If[opt2>=1, plot=1;];
   plotjoined=0; plotcolor="black";
   If[opt1==1, plotcolor="black";];
   If[opt1==2, plotcolor="red";];
   If[opt1>=1, plotjoined=1;];

!   Print[opt1," ",opt2];
!   Print[plot];
!   Print[plotjoined];

     gplot=ListPlot[datai,  
!       PlotRegion -> {{(frame[1,1]-offsetx)*scalex,(frame[1,2]-offsetx)*scalex},{(frame[2,1]-offsety)*scaley,(frame[2,2]-offsety)*scaley}},
!       AspectRatio -> width*(frame[1,2]-frame[1,1])/hight/(frame[2,2]-frame[2,1]),
       FrameLabel -> {"",titley,"",""},
       PlotRange -> {{range[1,1],range[1,2]},{range[2,1],range[2,2]}},
       Plot -> plot, 
       PlotJoined -> plotjoined, 
       PointColor -> pointcolor, 
       PlotColor -> plotcolor,
       DisplayFunction -> Identity
     ];

   Rectangle[{(frame[1,1]-offsetx)*scalex,(frame[2,1]-offsety)*scaley},{(frame[1,2]-offsetx)*scalex,(frame[2,2]-offsety)*scaley},gplot]
   )&,data];

   text1=Select[text,(#[4]<80)&];
   txt=Map[(
!      Text[{#[1],{#[2]*13,#[3]*9.6}}, TextSize -> 2.0]
      Text[{#[1],{#[2]*13,#[3]*9.6}}, TextSize -> 1.0]
    )&,text1];

  {gr,txt})&,datalist];  

     Show[Graphics[Flatten[fig]]];

!   Update[];
!   Pause[10];
   TkWait[];
   
   DeleteWidget[w];
!!   DeleteAllWidgets[];
 ];
 
!!!!!!!!!!!!!!!!
 
! This is for topdrawer.
TopDrawGraph[datalist_,f_]:=Module[
! {},
{data,range,frame,text,fn,xlist,ylist,dylist,opt1,opt2,scalex=13,scaley=10},
!! Print[datalist[1,5]];
! Print[f];
  fn=OpenWrite[f];
 Scan[(
  data=#[1];range=#[2];frame=#[3];text=#[4];
 ! Print[frame]; Print[range];Print[text];

  round=100;

  $FORM="6.2";
  Write[fn," set window x ",frame[1,1]*scalex," ",frame[1,2]*scalex];
  Write[fn," set window y ",frame[2,1]*scaley," ",frame[2,2]*scaley];
  If[Length[text[1]]>=1,
   Scan[(
    Write[fn," titl ",#[2]*scalex," ",#[3]*scaley," ANGLE ",#[4]," '"//#[1]//"'"];
   )&,text];
  ]; 

!  Print[frame]; Print[range];Print[text];

  Write[fn," set limit x ",range[1,1]," ",range[1,2]];
  Write[fn," set limit y ",range[2,1]," ",range[2,2]];
  
  
  $FORM="";
  Scan[(
   xlist=#[3];
   ylist=#[4];
   opt1=#[1];
   opt2=#[2];
   If[Length[#]=>5,dylist=#[5]];
   If[opt2==1,Write[fn,' set symbol 1O']];
   If[opt2==2,Write[fn,' set symbol 4O']];
   If[opt1==2, Write[fn," set pattern 0.2 0.08"]];

   If[Length[#]=>5,
    Write[fn," set order x y dy"];
    Scan[(Write[fn,#[1]," ",#[2]," ",#[3]])&,Thread[{xlist,ylist,dylist}]];
   , 
    Write[fn," set order x y "];
    Scan[(Write[fn,#[1]," ",#[2]])&,Thread[{xlist,ylist}]];
   ]; 

      
   If[opt2>=1, Write[fn," plot"]];
   If[opt1==1, Write[fn," join 1"]];
   If[opt1==2, Write[fn," join 1 patterned"]];
 
  )&,data];
 )&,datalist];  

 Close[fn];
 System[" chmod 664 "//f];
 System[" tdr -v X "//f];

 ];

!!!!!!!! set limits and window size aughtomatically !!!!
!
! data={data[1],data[2],data[3], , ,,data[n]}  (n>=1) 
!  data[i]={option1,option2,xlist,ylist,dylist}
! xlist is list of horizontal points
! ylist is list of vertical points
! dylist is list of vertical error of points. If there is no dylist, no error bar.
!
   ADrawGraph[data_,titl_,f_]:=Module[{range,frame,text,datalist,xlow,ylow,xup,yup,xlow0,ylow0,xup0,yup0},
      xlow0=Min[Map[(Min[#[3]])&,data]];
      xup0 =Max[Map[(Max[#[3]])&,data]];
      ylow0=Min[Map[(Min[#[4]])&,data]];
      yup0 =Max[Map[(Max[#[4]])&,data]];
      xlow=xlow0-(xup0-xlow0)*0.05;
      xup =xup0 +(xup0-xlow0)*0.05;
      ylow=ylow0-(yup0-ylow0)*0.05;
      yup =yup0 +(yup0-ylow0)*0.05;

      range={{xlow,xup},{ylow,yup}};
      frame={{.1,.9},{.1,.9}};
      text={{titl,0.2,1,0}};
      datalist={{data,range,frame,text}};

      ACanvasDrawGraph[datalist,f];
  ];


! This is for Canvas. 
! Automatically disapear
 BBCanvasDrawGraph[datalist_,time_,f_]:=Module[
! {},
{data,range,frame,text,xlist,ylist,dylist,opt1,opt2,scalex,scaley,offsetx,offsety,hight,width,c,w,txt,gr,plot,plotcolor},

! w=Window[];
! b=Button[w, Text -> "Next", Command :> TkReturn["ReturnToSAD"]];
! hight=600; width=800;
! c=Canvas[w, Height -> hight, Width -> width];

! $DisplayFunction = CanvasDrawer;
! Canvas$Widget = c;

!Print["aaa"];

! scalex=1/0.74; scaley=1/0.6;
! offsetx=0.185; offsety=0.22;
 scalex=1/0.76; scaley=1/0.64;
 offsetx=0.185; offsety=0.18;

 fig=
 Map[(
  data=#[1];range=#[2];frame=#[3];text=#[4];
!  Print[frame]; Print[range];Print[text];
  
  titley="";

   Scan[(
     If[#[4]>80 && #[4]<100, titley=#[1]];
    )&,text];

  gr=Map[(
   xlist=#[3];
   ylist=#[4];	
   If[Length[#]=>5,dylist=#[5]; datai=Thread[{xlist,ylist,dylist}],datai=Thread[{xlist,ylist}]];
   opt1=#[1];
   opt2=#[2];
   If[Length[#]=>5,dylist=#[5]];

   plot=0; pointcolor="green";
   If[opt2==1, pointcolor="black";];
   If[opt2==2, pointcolor="red";];
   If[opt2>=1, plot=1;];
   plotjoined=0; plotcolor="black";
   If[opt1==1, plotcolor="black";];
   If[opt1==2, plotcolor="red";];
   If[opt1>=1, plotjoined=1;];

!   Print[opt1," ",opt2];
!   Print[plot];
!   Print[plotjoined];

     gplot=ListPlot[datai,  
!       PlotRegion -> {{(frame[1,1]-offsetx)*scalex,(frame[1,2]-offsetx)*scalex},{(frame[2,1]-offsety)*scaley,(frame[2,2]-offsety)*scaley}},
!       AspectRatio -> width*(frame[1,2]-frame[1,1])/hight/(frame[2,2]-frame[2,1]),
       FrameLabel -> {"",titley,"",""},
       PlotRange -> {{range[1,1],range[1,2]},{range[2,1],range[2,2]}},
       Plot -> plot, 
       PlotJoined -> plotjoined, 
       PointColor -> pointcolor, 
       PlotColor -> plotcolor,
       DisplayFunction -> Identity
     ];

   Rectangle[{(frame[1,1]-offsetx)*scalex,(frame[2,1]-offsety)*scaley},{(frame[1,2]-offsetx)*scalex,(frame[2,2]-offsety)*scaley},gplot]
   )&,data];

   text1=Select[text,(#[4]<80)&];
   txt=Map[(
!      Text[{#[1],{#[2]*13,#[3]*9.6}}, TextSize -> 2.0]
      Text[{#[1],{#[2]*13,#[3]*9.6}}, TextSize -> 1.0]
    )&,text1];

  {gr,txt})&,datalist];  

     Show[Graphics[Flatten[fig]]];

   Update[];
   Pause[time];
!!   TkWait[];
   
!!   DeleteWidget[w];
!!   DeleteAllWidgets[];
 ];

!!!!!!!! set limits and window size aughtomatically, two graphs !!!!!!
!
! data={data[1],data[2],data[3], , ,,data[n]}  (n>=1) 
!  data[i]={option1,option2,xlist,ylist,dylist}
! xlist is list of horizontal points
! ylist is list of vertical points
! dylist is list of vertical error of points. If there is no dylist, no error bar.
!
   BBAutoDrawGraph[data_,titl_,time_,f_]:=Module[
   {range,frame,text,data1,data2,xlow,ylow,xup,yup,xlow0,ylow0,xup0,yup0},
  !!!{},
 !!     Print[data]; Print[f];

      xlow0=Min[Map[(Min[#[3]])&,data[1]]];
      xup0 =Max[Map[(Max[#[3]])&,data[1]]];
      ylow0=Min[Map[(Min[#[4]])&,data[1]]];
      yup0 =Max[Map[(Max[#[4]])&,data[1]]];
      xlow=xlow0-(xup0-xlow0)*0.05;
      xup =xup0 +(xup0-xlow0)*0.05;
      ylow=ylow0-(yup0-ylow0)*0.05;
      yup =yup0 +(yup0-ylow0)*0.05;

      range={{xlow,xup},{ylow,yup}};
      frame={{.1,.9},{.1,.5}};
      text={{titl,0.2,0.95,0}};

      data1={data[1],range,frame,text};

      xlow0=Min[Map[(Min[#[3]])&,data[2]]];
      xup0 =Max[Map[(Max[#[3]])&,data[2]]];
      ylow0=Min[Map[(Min[#[4]])&,data[2]]];
      yup0 =Max[Map[(Max[#[4]])&,data[2]]];
      xlow=xlow0-(xup0-xlow0)*0.05;
      xup =xup0 +(xup0-xlow0)*0.05;
      ylow=ylow0-(yup0-ylow0)*0.05;
      yup =yup0 +(yup0-ylow0)*0.05;

      range={{xlow,xup},{ylow,yup}};
      frame={{.1,.9},{.5,.9}};
      text={{"",0.2,0.85,0}};

      data2={data[2],range,frame,text};

!      ACanvasDrawGraph[datalist,f];

!!   Print[data2];
      BBCanvasDrawGraph[{data1,data2},time,f]
  ];

