
fitgoy:=[

!  VariableRange["IEX","AY",v_]:=v>-1000 && v<1000;
!  VariableRange["IEX","BY",v_]:=v>0.001 && v<1000;
  VariableRange["IEX","AY",v_]:=(v>minalphy && v<maxalphy);
  VariableRange["IEX","BY",v_]:=(v>minbetay && v<maxbetay);
  FitFunction:=kaiyi;

  FFS["FIT MW0X BYM 150;"];
  FFS["FIT MW1X BYM 150;"];
  FFS["FIT MW2X BYM 150;"];
  FFS["FIT MW3X BYM 150;"];
  FFS["FIT MW4X BYM 200;"];

  FFS["FREE AYI BYI  ;GO"];
  
 Print[Twiss["BY",1]];
 Print[Twiss["AY",1]];

  mresy=MatchingResidual;

  ib=0;ia=11;
  While[mresy>50 && ib<=40, 
    If[ia==11,ia=-10; ib=ib+1; , ia=ia+1];
      FFS["AYI "//ia];
      FFS["BYI "//ib*0.5];
      FFS["CAL"];
      FFS[" GO;"];
      mresy=MatchingResidual;
  ];

  Print[chi2y];
  emitY=sqrtemiy^2;Print[" emity ",emitY];
  chi2yfitted=chi2y;
  alphyfitted=alphY;
  betayfitted=betaY;
  alphyiex=Twiss["AY","IEX"];
  betayiex=Twiss["BY","IEX"];
  demit=Max[emitY*0.9,1e-12];
  emi0=emitY;
  minchi2=0;
  While[Abs[demit]>emitY*1e-2, 
   While[minchi2<1+chi2yfitted,
    emi0=emi0+demit;
    minchi2=minchi2yfixe[emi0];
!    Print["minchi2 ",minchi2," ",demit," ",emi0];
   ];
   emi0=emi0-demit;
   minchi2=minchi2yfixe[emi0];
   demit=demit*0.5;
  ];
   Print["minchi2",minchi2," ",chi2y];
  erremitYp=emi0-emitY;
  demit=Max[emitY*0.9,1e-12];
  emi0=emitY;
  minchi2=0;
  While[Abs[demit]>emitY*1e-2,
   While[minchi2<1+chi2yfitted,
    If[emi0<=demit, demit=emi0*0.4];
    emi0=emi0-demit;
    minchi2=minchi2yfixe[emi0];
!    Print["minchi2 ",minchi2," ",demit," ",emi0];
   ];
   emi0=emi0+demit;
   minchi2=minchi2yfixe[emi0];
   demit=demit*0.5;
  ];
   Print["minchi2 ",minchi2," ",chi2y];
  erremitYm=-emi0+emitY;
  erremitY=(erremitYp+erremitYm)/2;
     pydesignp[y_]:=(-alphY0/betaY0*y+Sqrt[betaY0*emitY-y^2]/betaY0);
     pydesignm[y_]:=(-alphY0/betaY0*y-Sqrt[betaY0*emitY-y^2]/betaY0);
     ymax0=Sqrt[betaY0*emitY];
    If[ymax0>0,
     ylim10=Table[t,{t,-ymax0*0.99,ymax0*0.99,ymax0*1.e-2}];
     ylim20=Table[t,{t,ymax0*0.99,-ymax0*0.99,-ymax0*1.e-2}];
    ];
     bmagY=Bmag[alphY0,betaY0,alphY,betaY];


    FFS["IEX OFFSET "//(LINE["POSITION",ent]-1)];
    FFS[" ayi "//alphyfitted];
    FFS[" byi "//betayfitted];
    If[emitY>0,
     EMITY=emitY;
    ];
    FFS["CALC"]; 

    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    f1="./output/plotdata_wsemitt_y1";
    frame1={{0.154,0.615},{0.2,0.8}};

     $FORM="7.3";
    text11={"y(m)",0.35,0.1,0};
    text12={"dy/ds",0.05,0.45,90};
    text13={DateString[]//" K.Kubo",0.631,0.89,0};
    text14={"emittanceY "//emitY*1.e9//" +"//erremitYp*1.e9//" -"//erremitYm*1.e9//" (nm)",0.3,0.84,0};
    text15={"alphaY at "//ent//" "//alphyfitted//" ",0.631,0.75,0};
    text16={"betaY  at "//ent//" "//betayfitted//"(m)",0.631,0.70,0};
    text17=Flatten[Map[(
      {{"alphaY at "//moniy[#]//" "//Twiss["AY",moniy[#]]//" ",0.631,0.75-#*0.1,0},
      {"betaY  at "//moniy[#]//" "//Twiss["BY",moniy[#]]//"(m)",0.631,0.70-#*0.1,0}}
    )&,Table[i,{i,1,Length[moniy]}]],1];
    text18={"BmagY "//bmagY//" ",0.631,0.65-Length[moniy]*0.1,0};
    text1=Join[{text11,text12,text13,text14,text15,text16},text17,{text18}];

    $FORM="";

     errsizey=sigy;
     cy=Map[#[3,3]&,maty];
     sy=Map[#[3,4]&,maty];

   If[emitY<=0 || emitY>0,
     pyp[y_]:=(-alphY/betaY*y+Sqrt[betaY*emitY-y^2]/betaY);
     pym[y_]:=(-alphY/betaY*y-Sqrt[betaY*emitY-y^2]/betaY);
     ymax=Sqrt[betaY*emitY];
    If[ymax>0,
     ylim1=Table[t,{t,-ymax*0.99,ymax*0.99,ymax*1.e-2}];
     ylim2=Table[t,{t,ymax*0.99,-ymax*0.99,-ymax*1.e-2}];
    ];

     data1={};

     dymax=0;
    If[ymax>0,
     x11=ylim1;
     y11=Map[(pyp[#])&,ylim1]; 
     x12=ylim2;
     y12=Map[(pym[#])&,ylim2]; 
     dymax=Max[Abs[y11]];
     range1={{-ymax*2,ymax*2},{-dymax*2,dymax*2}};
     data1=Join[data1,{{1,0,Join[x11,x12],Join[y11,y12]}}];
     ,
     range1={{-1,1},{-1,1}};
    ];
    If[ymax0>0,
     x13=ylim10;
     y13=Map[(pydesignp[#])&,ylim10]; 
     x14=ylim20;
     y14=Map[(pydesignm[#])&,ylim20]; 
     data1=Join[data1,{{2,0,Join[x13,x14],Join[y13,y14]}}];
    ];

     nmplotx=8;nmploty=1-nmplotx;

     data15=Flatten[Map[(
          {{1,0,{nmplotx*#[1]/#[2],nmploty*#[1]/#[2]},{nmploty*#[1]/#[3],nmplotx*#[1]/#[3]}},
           {2,0,{nmplotx*(#[1]+#[4])/#[2],nmploty*(#[1]+#[4])/#[2]},{nmploty*(#[1]+#[4])/#[3],nmplotx*(#[1]+#[4])/#[3]}},
           {2,0,{nmplotx*(#[1]-#[4])/#[2],nmploty*(#[1]-#[4])/#[2]},{nmploty*(#[1]-#[4])/#[3],nmplotx*(#[1]-#[4])/#[3]}}}
     )&,Thread[{sizey,cy,sy,errsizey}]],1];

    data1=Join[data1,data15,{{0,1,{0},{0}}}];


   ];

    $FORM="";
   DrawGraph[{{data1,range1,frame1,text1}},f1];

     data2={};     

   If[emitY>0,
    f2="./output/plotdata_wsemitt_y2";
    frame2={{0.154,0.615},{0.2,0.8}};
    range2={{-2,2},{-2,2}};

     xnorm[x_,y_]:=(x/Sqrt[emitY*betayfitted]);
     ynorm[x_,y_]:=(x*alphyfitted/Sqrt[emitY*betayfitted]+y*Sqrt[betayfitted]/Sqrt[emitY]);
     xnmax=Sqrt[betaY*emitY];
     xnlim1=Table[t,{t,-xnmax*0.99,xnmax*0.99,xnmax*1.e-2}];
     xnlim2=Table[t,{t,xnmax*0.99,-xnmax*0.99,-xnmax*1.e-2}];

     xnmax0=2*xnmax;
     xnlim10=Table[t,{t,-xnmax0*0.99,xnmax0*0.99, xnmax0*1.e-2}];
     xnlim20=Table[t,{t,xnmax0*0.99,-xnmax0*0.99,-xnmax0*1.e-2}];

    $FORM="7.4";

     x21=Map[(xnorm[#,pyp[#]])&,xnlim1];
     y21=Map[(ynorm[#,pyp[#]])&,xnlim1];
     x22=Map[(xnorm[#,pym[#]])&,xnlim2];
     y22=Map[(ynorm[#,pym[#]])&,xnlim2];
     data2=Join[data2,{{1,0,Join[x21,x22],Join[y21,y22]}}];
    
     x23=Map[(yy=ynorm[#,pydesignp[#]];
         If[Im[yy]==0, xnorm[#,pydesignp[#]],-1e10])&,xnlim10];
     y23=Map[(yy=ynorm[#,pydesignp[#]];
         If[Im[yy]==0, yy,-1e10])&,xnlim10];
     x24=Map[(yy=ynorm[#,pydesignm[#]];
         If[Im[yy]==0, xnorm[#,pydesignm[#]],-1e10])&,xnlim20];
     y24=Map[(yy=ynorm[#,pydesignm[#]];
         If[Im[yy]==0, yy,-1e10])&,xnlim20];
     x234=Select[Join[x23,x24],(#>-9e9)&];
     y234=Select[Join[y23,y24],(#>-9e9)&];
     data2=Join[data2,{{2,0,x234,y234}}];

     data22=Flatten[Map[(
           x10=xnorm[#[1]/#[2],0];y10=ynorm[#[1]/#[2],0];
           x20=xnorm[0,#[1]/#[3]];y20=ynorm[0,#[1]/#[3]];
           x1a=xnorm[(#[1]+#[4])/#[2],0];
           y1a=ynorm[(#[1]+#[4])/#[2],0];
           x2a=xnorm[0,(#[1]+#[4])/#[3]];
           y2a=ynorm[0,(#[1]+#[4])/#[3]];
           x1b=xnorm[(#[1]-#[4])/#[2],0];
           y1b=ynorm[(#[1]-#[4])/#[2],0];
           x2b=xnorm[0,(#[1]-#[4])/#[3]];
           y2b=ynorm[0,(#[1]-#[4])/#[3]];

           x00=(y10*x20-y20*x10)/(y10-y20);
           y00=(x10*y20-x20*y10)/(x10-x20);
           x0a=(y1a*x2a-y2a*x1a)/(y1a-y2a);
           y0a=(x1a*y2a-x2a*y1a)/(x1a-x2a);
           x0b=(y1b*x2b-y2b*x1b)/(y1b-y2b);
           y0b=(x1b*y2b-x2b*y1b)/(x1b-x2b);

           nmplotx=8;nmploty=1-nmplotx;

           {{1,0,{nmploty*x00,nmplotx*x00},{nmplotx*y00,nmploty*y00}},
            {2,0,{nmploty*x0a,nmplotx*x0a},{nmplotx*y0a,nmploty*y0a}},
            {2,0,{nmploty*x0b,nmplotx*x0b},{nmplotx*y0b,nmploty*y0b}}}
    )&,Thread[{sizey,cy,sy,errsizey}]],1];


    data2=Join[data2,data22,{{0,1,{0},{0}}}];

   
    $FORM="";
   DrawGraph[{{data2,range2,frame2,{{" ",.1,.1,0}}}},f2];

   ];

   
    f3="./output/plotdata_wsemitt_y3";
    frame3={{0.154,0.846},{0.2,0.8}};

    $FORM="F11.5";
    text3={{"chi2="//chi2y,0.154,0.85,0},{DateString[],0.538,0.85,0},{"s(m)",0.45,0.1,0},{"sizeY(m)",0.05,0.45,90}};

    If[LINE["Position",endplot]>1,
     allelement=Map[LINE["Name",#]&,Table[i,{i,LINE["Position",ent],LINE["Position",endplot]}]],
     allelement=Map[LINE["Name",#]&,Table[i,{i,LINE["Position",ent],LINE["Position",$$$]}]];
    ];
     x31=Map[LINE["S",#]&,allelement];
     y31=Map[Sqrt[EMITY*Twiss["BY",#]]&,allelement];
     
     x32=Map[(LINE["S",moniy[#]])&,Table[i,{i,1,Length[moniy]}]];
     y32=Map[(sizey[#])&,Table[i,{i,1,Length[moniy]}]];
     dy32=Map[(sigy[#])&,Table[i,{i,1,Length[moniy]}]];

     data3={{1,0,x31,y31},{0,1,x32,y32,dy32}};

     range3={{Min[x31],Max[x31]},{0,Max[Join[y31,y32]]*1.2}};

    $FORM="";
   DrawGraph[{{data3,range3,frame3,text3}},f3];

 FFS[" FIX *;"];
];

