
 CorrectSkewCoupling[]:=(
!!  f="RING_ORBIT_skewcortest01.SAD";
!!  fn=OpenWrite[f];
  tunelist=ATFTune/.ATFData[1]/.ATFTune->{};
    NXm=tunelist[1];NYm=tunelist[2];
    FFS["FIT NX NXm NY NYm"];
    Print[NXm," ",NYm];
    FFS["FREE QF* go; cal"];
    FFS["Reject Total;"]; 
    FFS["FIX Q* "];
  parameter=SkewCorrect/.Option;
  steer1=steer1/.parameter;
  steer2=steer2/.parameter;
  stk01=270e-6*steerchange1/.parameter;
  stk02=270e-6*steerchange2/.parameter;
  sdcalib=sdcalib/.parameter;
  sfcalib=sfcalib/.parameter;
!  sdmax=Abs[8.97*sdcalib];
!  sfmax=Abs[8.97*sfcalib];
  sdmax=Abs[8.5*sdcalib];
  sfmax=Abs[8.5*sfcalib];
  bpmdata1=ATFBPM/.ATFData[1];
  bpmdata2=ATFBPM/.ATFData[2];
  allbpm=LINE["Name","M.*"];

    bpmdata1=Select[bpmdata1, ((#[2]<0 || #[2]>=0)&&(#[3]<0 || #[3]>=0)&&(#[4]<0 || #[4]>=0))&];
    bpmdata2=Select[bpmdata2, ((#[2]<0 || #[2]>=0)&&(#[3]<0 || #[3]>=0)&&(#[4]<0 || #[4]>=0))&];

  bpmdata1=Select[bpmdata1,MemberQ[allbpm,#[1]]&];
  bpmdata2=Select[bpmdata2,MemberQ[allbpm,#[1]]&];
  useskew={};
  If[(SD1R/.parameter)=="ON",useskew=Join[useskew,LINE["Name","SD1R.*"]]];
  If[(SF1R/.parameter)=="ON",useskew=Join[useskew,LINE["Name","SF1R.*"]]];
  skewdata=ATFSext/.ATFData[1];
  skewdata0=Select[skewdata,(MemberQ[useskew,#[1]])&];
!  skewdata0=Map[({#[1],#[2]*skfactor})&,skewdata0];
  skewdata1=Map[(If[#[1][2]=="D",max=sdmax-#[2];min=-sdmax-#[2];skfactor=skfactorsd,
                                 max=sfmax-#[2];min=-sfmax-#[2];skfactor=skfactorsf];
                {#[1],#[2]*skfactor,max,min})&,skewdata0];
  useskew0=Map[#[1]&,skewdata0];
  bpmname1=Map[#[1]&,bpmdata1];
  bpmname2=Map[#[1]&,bpmdata2];

  bpmdata1=Select[bpmdata1,MemberQ[bpmname2,#[1]]&];
  bpmdata2=Select[bpmdata2,MemberQ[bpmname1,#[1]]&];
  bpmname=Map[#[1]&,bpmdata1];

  B=Join[Map[#[3]&,bpmdata1],Map[#[3]&,bpmdata2]];
  B0=B;

  FFS["Z* K0 0;"];
 
 iflimit=1;
 useskew1=Map[#[1]&,skewdata1];
  limitskewdata={};
 While[iflimit==1 && Length[useskew1]>=1,

! respzh1=resp12factors[{steer1},useskew1];
! respzh2=resp12factors[{steer2},useskew1];
 respzh=resp12factors[{steer1,steer2},useskew1];
 respsky=resp34factors[useskew1,bpmname];

!  A=Partition[Flatten[Thread[{Transpose[(stk01*respzh1[1]*respsky)],
!          Transpose[(stk02*respzh2[1]*respsky)]}]],Length[useskew1]];
  A=Partition[Flatten[{stk01,stk02}*Map[Transpose[(#*respsky)]&,respzh]],Length[useskew1]];
  
    Sx=SingularValues[A,Tolerance->0.4];
    invx=Transpose[Sx[3]].DiagonalMatrix[Sx[2]].Sx[1];
    dsk=invx.B;
 
  iflimit=0;
  isk=0;
  Scan[(isk=isk+1;
       If[(#[1]>#[2,3]),
           If[#[2,1][2]=="D",max=sdmax,max=sfmax];
           limitskewdata=Append[limitskewdata,{#[2,1],max}];
           B=B-Map[#[isk]&,A]*#[2,3];
           iflimit=1];
       If[(#[1]<#[2,4]),
           If[#[2,1][2]=="D",max=sdmax,max=sfmax];
           limitskewdata=Append[limitskewdata,{#[2,1],-max}];
           B=B-Map[#[isk]&,A]*#[2,4];
           iflimit=1];
  )&,Thread[{dsk,skewdata1}]];
  If[iflimit==1,
    skewdata1=Select[skewdata1,(Not[MemberQ[Map[#[1]&,limitskewdata],#[1]]])&];
    dsk={};
    useskew1=Map[#[1]&,skewdata1];
  ];
  If[iflimit==0,B=B-A.dsk];
 ];
  B1=B0-B;

  skewdata2=
  If[Length[skewdata1]>=1,
  Map[(
    {#[2,1],#[2,2]+#[1]}
  )&,Thread[{dsk,skewdata1}]],
  {}];
  skewdatafinal=Join[limitskewdata,skewdata2];

!  f="./output/plot_skewcor_01.top";
!  fn=OpenWrite[f];
!   Write[fn,"titl x 's(m)';titl 4 8.5 '"//steer1//"'"];
!  Scan[(Write[fn,LINE["S",#[1,1]]," ",#[2]])&,Thread[{bpmdata1,Take[B0,{1,Length[bpmdata1]}]}]];
!  Write[fn,"plot"];
!  Scan[(Write[fn,LINE["S",#[1,1]]," ",#[2]])&,Thread[{bpmdata1,Take[B1,{1,Length[bpmdata1]}]}]];

!  Write[fn,"join 1;new"];
!  Write[fn,"titl x 's(m)';titl 4 8.5 '"//steer2//"'"];
!  Scan[(Write[fn,LINE["S",#[1,1]]," ",#[2]])&,Thread[{bpmdata1,Take[B0,{Length[bpmdata1]+1,Length[B0]}]}]];
!  Write[fn,"plot"];
!  Scan[(Write[fn,LINE["S",#[1,1]]," ",#[2]])&,Thread[{bpmdata1,Take[B1,{Length[bpmdata1]+1,Length[B1]}]}]];
!  Write[fn,"join 1"];
!!2003.04.09  System["tpx "//f//"&"];
!  System["tdr -v X "//f//"&"];
!  System["chmod 664 "//f];

   f1="./output/plotdata_skewcor1";
    frame1={{0.15,0.85},{0.2,0.9}};
    text1={{"s (m)",0.45,0.13,0},{"DY (m)",0.045,0.45,90},{steer1,0.35,0.85,0}};
    x1=Map[(LINE["S",#[1]])&,bpmdata1];
    y11=Take[B0,{1,Length[bpmdata1]}];
    y12=Take[B1,{1,Length[bpmdata1]}];

    ymin=Min[Join[y11,y12]];
    ymax=Max[Join[y11,y12]];
    range1={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
    data1={{{0,2,x1,y11},{1,0,x1,y12}},range1,frame1,text1};

   f2="./output/plotdata_skewcor2";
    frame2={{0.15,0.85},{0.2,0.9}};
    text2={{"s (m)",0.45,0.13,0},{"DY (m)",0.045,0.45,90},{steer2,0.35,0.85,0}};
    x2=Map[(LINE["S",#[1]])&,bpmdata1];
    y21=Take[B0,{Length[bpmdata1]+1,Length[B0]}];
    y22=Take[B1,{Length[bpmdata1]+1,Length[B0]}];

    ymin=Min[Join[y21,y22]];
    ymax=Max[Join[y21,y22]];
    range2={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
    data2={{{0,2,x2,y21},{1,0,x1,y22}},range2,frame2,text2};
   
    DrawGraph[{data1},f1];
    DrawGraph[{data2},f2];

 );



 resp12factors[list1_,list2_]:=Module[{
   plist1=LINE["Position",list1]+0.5,
   plist2=LINE["Position",list2]+0.5,
   denom=Sin[Twiss["NX",$$$]*0.5],
   b1list,b2list,
   p1,p2,b1,b2,ph},

   b1list=Twiss["BX",plist1];
   b2list=Twiss["BX",plist2];
  If[Abs[denom]>1e-10,
   Map[(p1=#[1];b1=#[2];Map[(p2=#[1];b2=#[2];
    ph=Twiss["NX",p2]-Twiss["NX",p1];
    If[ph<0,ph=ph+Twiss["NX",$$$]];
   Sqrt[b1*b2]*Cos[ph-Twiss["NX",$$$]*0.5]/denom/2
  )&,Thread[{plist2,b2list}]])&,Thread[{plist1,b1list}]]
 ]];

 resp34factors[list1_,list2_]:=Module[{
   plist1=LINE["Position",list1]+0.5,
   plist2=LINE["Position",list2]+0.5,
   denom=Sin[Twiss["NY",$$$]*0.5],
   b1list,b2list,
   p1,p2,b1,b2,ph}, 

   b1list=Twiss["BY",plist1];
   b2list=Twiss["BY",plist2];
  If[Abs[denom]>1e-10,
   Map[(p1=#[1];b1=#[2];Map[(p2=#[1];b2=#[2];
    ph=Twiss["NY",p2]-Twiss["NY",p1];
    If[ph<0,ph=ph+Twiss["NY",$$$]];
   Sqrt[b1*b2]*Cos[ph-Twiss["NY",$$$]*0.5]/denom/2
  )&,Thread[{plist2,b2list}]])&,Thread[{plist1,b1list}]]
 ]];
