!datalist={{data,range,frame,text},{data,range,frame,text},{data,range,frame,text}, , , ,}
!
! data={data[1],data[2],data[3], , ,,data[n]}  (n>=1) 
!  data[i]={option1,option2,xlist,ylist,dylist}
! xlist is list of horizontal points
! ylist is list of vertical points
! dylist is list of vertical error of points. If there is no dylist, no error bar.
!
! option1 decide style of line
!     0: no line
!     1: solid
!     2: dashed
!
! option2 decide style of point
!     0: none
!     1: cross
!     2: star 
! 
! range={{xlow,xup},{ylow,yup}} decide limits of h and v values
! 
! frame={{xbottom,xtop},{ybottom,ytop}} decide size of frame
!   positions of bottoms and tops are (may be) relative to the screen size
!
! text={text[1],text[2],,,} is list of text data
!  text[i]={string, xposition, yposition,angle}
!    positions are (may be) relative to the screen size
!
! f is output file name, may not be used depending tools.

   DrawGraph[datalist_,f_]:=CanvasDrawGraph[datalist,f];
!!!!!!!!!!!!!!!!

! This is for canvas.
CanvasDrawGraph[datalist_,f_]:=Module[
! {},
{data,range,frame,text,xlist,ylist,dylist,opt1,opt2,scalex,scaley,offsetx,offsety,hight,width,c,w},

 w=Window[];
 b=Button[w, Text -> "Next", Command :> TkReturn["ReturnToSAD"]];
 hight=600; width=800;
 c=Canvas[w, Height -> hight, Width -> width];

 $DisplayFunction = CanvasDrawer;
 Canvas$Widget = c;


! scalex=1/0.74; scaley=1/0.6;
! offsetx=0.185; offsety=0.22;
 scalex=1/0.76; scaley=1/0.64;
 offsetx=0.185; offsety=0.18;

 fig=
 Map[(
  data=#[1];range=#[2];frame=#[3];text=#[4];
!  Print[frame]; Print[range];Print[text];
  
  titley="";

   Scan[(
     If[#[4]>80 && #[4]<100, titley=#[1]];
    )&,text];

  gr=Map[(
   xlist=#[3];
   ylist=#[4];	
   If[Length[#]=>5,dylist=#[5]; datai=Thread[{xlist,ylist,dylist}],datai=Thread[{xlist,ylist}]];
   opt1=#[1];
   opt2=#[2];
   If[Length[#]=>5,dylist=#[5]];

   plot=0; pointcolor="green";
   If[opt2==1, pointcolor="black";];
   If[opt2==2, pointcolor="red";];
   If[opt2>=1, plot=1;];
   plotjoined=0; plotcolor="black";
   If[opt1==1, plotcolor="black";];
   If[opt1==2, plotcolor="red";];
   If[opt1>=1, plotjoined=1;];

!   Print[opt1," ",opt2];
!   Print[plot];
!   Print[plotjoined];

     gplot=ListPlot[datai,  
!       PlotRegion -> {{(frame[1,1]-offsetx)*scalex,(frame[1,2]-offsetx)*scalex},{(frame[2,1]-offsety)*scaley,(frame[2,2]-offsety)*scaley}},
!       AspectRatio -> width*(frame[1,2]-frame[1,1])/hight/(frame[2,2]-frame[2,1]),
       FrameLabel -> {"",titley,"",""},
       PlotRange -> {{range[1,1],range[1,2]},{range[2,1],range[2,2]}},
       Plot -> plot, 
       PlotJoined -> plotjoined, 
       PointColor -> pointcolor, 
       PlotColor -> plotcolor,
       DisplayFunction -> Identity
     ];

   Rectangle[{(frame[1,1]-offsetx)*scalex,(frame[2,1]-offsety)*scaley},{(frame[1,2]-offsetx)*scalex,(frame[2,2]-offsety)*scaley},gplot]
   )&,data];

   text1=Select[text,(#[4]<80)&];
   txt=Map[(
      Text[{#[1],{#[2]*13,#[3]*9.6}}, TextSize -> 1.4]
    )&,text1];

  {gr,txt})&,datalist];  

     Show[Graphics[Flatten[fig]]];
   TkWait[];
!   Update[];
   
   DeleteWidget[w];
   DeleteAllWidgets[];
 ];
 

