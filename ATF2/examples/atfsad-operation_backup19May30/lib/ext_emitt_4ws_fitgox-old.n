
fitgox:=[
!  VariableRange["IEX","AX",v_]:=v>-1000 && v<1000;
!  VariableRange["IEX","BX",v_]:=v>0.001 && v<1000;
  VariableRange["IEX","AX",v_]:=v>minalphx && v<maxalphx;
  VariableRange["IEX","BX",v_]:=v>minbetax && v<maxbetax;
  FitFunction:=kaixi;
  FFS["FREE AXI BXI  ;MAXI 3;GO"];
  Print["BA"];Print[Twiss["BX","IEX"]," ",FFS["VAR"]];
  Print[kai2x];
  emitX=sqrtemix^2;Print[" emitx ",emitX];
  kai2xfitted=kai2x;
  alphxfitted=alphX;
  betaxfitted=betaX;
  alphxiex=Twiss["AX","IEX"];
  betaxiex=Twiss["BX","IEX"];

  demit=Max[emitX*0.9,1e-12];
  emi0=emitX;
  minkai2=0;
  While[Abs[demit]>emitX*1e-2,
   While[minkai2<1+kai2xfitted,
    emi0=emi0+demit;
    minkai2=minkai2xfixe[emi0];
   ];
   emi0=emi0-demit;
   minkai2=minkai2xfixe[emi0];
   demit=demit*0.5;
  ];
   Print[minkai2," ",kai2x];
  erremitXp=emi0-emitX;
  demit=emitX*0.9;
  emi0=emitX;
  minkai2=0;
  While[Abs[demit]>emitX*1e-2,
   While[minkai2<1+kai2xfitted,
    If[emi0<=demit, demit=emi0*0.4];
    emi0=emi0-demit;
    minkai2=minkai2xfixe[emi0];
   ];
   emi0=emi0+demit;
   minkai2=minkai2xfixe[emi0];
   demit=demit*0.5;
  ];
   Print[minkai2," ",kai2x];
  erremitXm=-emi0+emitX;
  erremitX=(erremitXp+erremitXm)/2;


     pxdesignp[x_]:=(-alphX0/betaX0*x+Sqrt[betaX0*emitX-x^2]/betaX0);
     pxdesignm[x_]:=(-alphX0/betaX0*x-Sqrt[betaX0*emitX-x^2]/betaX0);
     xmax0=Sqrt[betaX0*emitX];
    If[xmax0>0,
     xlim10=Table[t,{t,-xmax0*0.99,xmax0*0.99,xmax0*1.e-2}];
     xlim20=Table[t,{t,xmax0*0.99,-xmax0*0.99,-xmax0*1.e-2}];
    ];
     bmagX=Bmag[alphX0,betaX0,alphX,betaX];


    FFS["IEX OFFSET "//(LINE["POSITION",ent]-1)];
    FFS[" axi "//alphxfitted];
    FFS[" bxi "//betaxfitted];
    If[emitX>0,
     EMITX=emitX;
    ];
    FFS["CALC"]; 

     drawoutx='@drawoutxxxxx';
     fn=OpenWrite[drawoutx];

     Write[fn," set window  x 2 8 y 2 8"];
     Write[fn," label size 1.5"];
     Write[fn," set titl size 1.5"];
     Write[fn," titl x  'x(m)'"];
     Write[fn," titl y  'dx/ds'"];
     Write[fn," set pattern 0.2 0.08"];

     $FORM="7.4";
     Write[fn," titl 8.2 8.5 '"//DateString[]//"'"];
!     Write[fn,"titl 8.2 8 'emittanceX ",emitX*1.e9," +-",erremitX*1.e9," (nm)'"];
     Write[fn,"titl 8.2 8 'emittanceX ",emitX*1.e9,
           " +",erremitXp*1.e9," -",erremitXm*1.e9," (nm)'"];
     Write[fn,"titl 8.2 7.5 'alphaX at "//ent//" ",alphX," '"];
     Write[fn,"titl 8.2 7.  'betaX  at "//ent//" ",betaX," (m)'"];
     For[i=1,i<=Length[monix],i=i+1,
      Write[fn,"titl 8.2 ",7.5-i," 'alphaX at "//monix[i]//" ",Twiss["AX",monix[i]]," '"];
      Write[fn,"titl 8.2 ",7.-i," 'betaX  at "//monix[i]//" ",Twiss["BX",monix[i]]," (m)'"];
     ];
    Write[fn,"titl 8.2 ",7.5-i," 'BmagX ",bmagX,"'"];
     $FORM="";

     errsizex=sigx;
     cx=Map[#[1,1]&,matx];
     sx=Map[#[1,2]&,matx];

   If[emitX<=0 || emitX>0,
     pxp[x_]:=(-alphX/betaX*x+Sqrt[betaX*emitX-x^2]/betaX);
     pxm[x_]:=(-alphX/betaX*x-Sqrt[betaX*emitX-x^2]/betaX);
     xmax=Sqrt[betaX*emitX];
    If[xmax>0,
     xlim1=Table[t,{t,-xmax*0.99,xmax*0.99,xmax*1.e-2}];
     xlim2=Table[t,{t,xmax*0.99,-xmax*0.99,-xmax*1.e-2}];
    ];
 
!     Scan[(Print[#," ",pxp[#]])&,xlim1];
     dxmax=0;
    If[xmax>0,
     Scan[(Write[fn,#," ",pxp[#]];dxmax=Max[dxmax,Abs[pxp[#]]])&,xlim1];
     Scan[(Write[fn,#," ",pxm[#]])&,xlim2];
     Write[fn," set limit x ",-xmax*2," ",xmax*2];
     Write[fn," set limit y ",-dxmax*2," ",dxmax*2];
     Write[fn," join 1"];
    ];
    If[xmax0>0,
     Scan[(Write[fn,#," ",pxdesignp[#]])&,xlim10];
     Scan[(Write[fn,#," ",pxdesignm[#]])&,xlim20];
     Write[fn," join 1 patterned"];
    ];
!     Scan[(Write[fn,2*#[1]/#[2]," ",-#[1]/#[3]];
!          Write[fn,-#[1]/#[2]," ",2*#[1]/#[3]," ;join 1 "])
!     &,Thread[{sizex,cx,sx}]];

   nmplotx=8;nmploty=1-nmplotx;
     Write[fn," set pattern 0.1 0.05"];
     Scan[(Write[fn,nmplotx*#[1]/#[2]," ",nmploty*#[1]/#[3]];
           Write[fn,nmploty*#[1]/#[2]," ",nmplotx*#[1]/#[3]," ;join 1 "];
           Write[fn,nmplotx*(#[1]+#[4])/#[2]," ",nmploty*(#[1]+#[4])/#[3]];
           Write[fn,nmploty*(#[1]+#[4])/#[2]," ",nmplotx*(#[1]+#[4])/#[3]," ;join 1 patterned"];
           Write[fn,nmplotx*(#[1]-#[4])/#[2]," ",nmploty*(#[1]-#[4])/#[3]];
           Write[fn,nmploty*(#[1]-#[4])/#[2]," ",nmplotx*(#[1]-#[4])/#[3]," ;join 1 patterned"];
     )&,Thread[{sizex,cx,sx,errsizex}]];



     Write[fn," 0 0 ; plot"];
    
   ];



   If[emitX>0,
     Write[fn," new frame"];
     Write[fn," set window  x 2 8 y 2 8"];
     xnorm[x_,y_]:=(x/Sqrt[emitX*betaxfitted]);
     ynorm[x_,y_]:=(x*alphxfitted/Sqrt[emitX*betaxfitted]+y*Sqrt[betaxfitted]/Sqrt[emitX]);
     xnmax=Sqrt[betaX*emitX];
     xnlim1=Table[t,{t,-xnmax*0.99,xnmax*0.99,xnmax*1.e-2}];
     xnlim2=Table[t,{t,xnmax*0.99,-xnmax*0.99,-xnmax*1.e-2}];

!     xnmax0=Sqrt[betaX0*emitX];
     xnmax0=2*xnmax;
     xnlim10=Table[t,{t,-xnmax0*0.99,xnmax0*0.99, xnmax0*1.e-2}];
     xnlim20=Table[t,{t,xnmax0*0.99,-xnmax0*0.99,-xnmax0*1.e-2}];
 
     Write[fn," set limit x ",-2," ",2];
     Write[fn," set limit y ",-2," ",2];
     Scan[(Write[fn,xnorm[#,pxp[#]]," ",ynorm[#,pxp[#]]])&,xnlim1];
     Scan[(Write[fn,xnorm[#,pxm[#]]," ",ynorm[#,pxm[#]]])&,xnlim2];
     Write[fn," join 1"];
     Scan[(yy=ynorm[#,pxdesignp[#]];
      If[(yy=>0 || yy<0), Write[fn,xnorm[#,pxdesignp[#]]," ", yy]])&,xnlim10];
     Scan[(yy=ynorm[#,pxdesignm[#]];
      If[(yy=>0 || yy<0), Write[fn,xnorm[#,pxdesignm[#]]," ", yy]])&,xnlim20];
     Write[fn," join 1 patterned"];

     Write[fn," set pattern 0.1 0.05"];

     Scan[(
           x10=xnorm[#[1]/#[2],0];y10=ynorm[#[1]/#[2],0];
           x20=xnorm[0,#[1]/#[3]];y20=ynorm[0,#[1]/#[3]];
           x1a=xnorm[(#[1]+#[4])/#[2],0]; y1a=ynorm[(#[1]+#[4])/#[2],0];
           x2a=xnorm[0,(#[1]+#[4])/#[3]]; y2a=ynorm[0,(#[1]+#[4])/#[3]];
           x1b=xnorm[(#[1]-#[4])/#[2],0]; y1b=ynorm[(#[1]-#[4])/#[2],0];
           x2b=xnorm[0,(#[1]-#[4])/#[3]];
           y2b=ynorm[0,(#[1]-#[4])/#[3]];
           x00=(y10*x20-y20*x10)/(y10-y20);
           y00=(x10*y20-x20*y10)/(x10-x20);
           x0a=(y1a*x2a-y2a*x1a)/(y1a-y2a);
           y0a=(x1a*y2a-x2a*y1a)/(x1a-x2a);
           x0b=(y1b*x2b-y2b*x1b)/(y1b-y2b);
           y0b=(x1b*y2b-x2b*y1b)/(x1b-x2b);

           nmplotx=8;nmploty=1-nmplotx;
	   Write[fn,nmploty*x00," ",nmplotx*y00];
	   Write[fn,nmplotx*x00," ",nmploty*y00," ; join 1"];
	   Write[fn,nmploty*x0a," ",nmplotx*y0a];
	   Write[fn,nmplotx*x0a," ",nmploty*y0a," ; join 1 patterned"];
	   Write[fn,nmploty*x0b," ",nmplotx*y0b];
	   Write[fn,nmplotx*x0b," ",nmploty*y0b," ; join 1 patterned"];

!           Write[fn,"0 ",(x10*y20-x20*y10)/(x10-x20)];
!           Write[fn,(y10*x20-y20*x10)/(y10-y20)," 0; join 1"];
!           Write[fn,"0 ",(x1a*y2a-x2a*y1a)/(x1a-x2a)];
!           Write[fn,(y1a*x2a-y2a*x1a)/(y1a-y2a)," 0; join 1 patterned"];
!           Write[fn,"0 ",(x1b*y2b-x2b*y1b)/(x1b-x2b)];
!           Write[fn,(y1b*x2b-y2b*x1b)/(y1b-y2b)," 0; join 1 patterned"];
           
     )&,Thread[{sizex,cx,sx,errsizex}]];
     Write[fn," 0 0 ; plot"];

   ];


     Write[fn," new frame"];
     Write[fn," set window x 2 11 "];
     Write[fn," set window y 2 8 "];
     Write[fn," titl 2 8.5 'kai2=",kai2x,"'"];
     Write[fn," titl 7 8.5 '"//DateString[]//"'"];

     allelement=Map[LINE["Name",#]&,Table[i,{i,LINE["Position",ent],LINE["Position",$$$]}]];
     Scan[Write[fn,LINE["S",#],"  ",Sqrt[EMITX*Twiss["BX",#]]]&,allelement];
     Write[fn," join 1"];
     Write[fn," set symbol 1O"];
     Write[fn," set order x y dy"];
     Write[fn," titl x 's(m)'"];
     Write[fn," titl y 'sizeX(m)'"];
     For[i=1,i<=Length[monix],i=i+1,
      Write[fn,LINE["S",monix[i]],"  ",sizex[i]," ",sigx[i]];
     ];
     Write[fn," plot"];
     
    
    System["tdr -v X "//drawoutx//" &"];
];
