
 setBH1RK1[]:=(
   
  slaveB["BH1R.1"]={"BH1R.1","BH1R.2","BH1R.3","BH1R.4","BH1R.5","BH1R.6"};
  slaveB["BH1R.7"]={"BH1R.7","BH1R.8","BH1R.9","BH1R.10",
         "BH1R.11","BH1R.12","BH1R.13","BH1R.14","BH1R.15",
         "BH1R.16","BH1R.17","BH1R.18","BH1R.19","BH1R.20",
         "BH1R.21","BH1R.22","BH1R.23","BH1R.24","BH1R.25",
         "BH1R.26","BH1R.27","BH1R.28","BH1R.29","BH1R.30",
         "BH1R.31","BH1R.32","BH1R.33","BH1R.34","BH1R.35",
         "BH1R.36"};
   Scan[(LINE["K1",#]=-1.17442)&,slaveB["BH1R.1"]];
   Scan[(LINE["K1",#]=-1.14780)&,slaveB["BH1R.7"]];
  
 );

 correctQK1[mode_]:=(
  fudgefactor={

  {"QF1R.1", 1},
  {"QF1R.10", 1},
  {"QF1R.11", 1},
  {"QF1R.12", 1},
  {"QF1R.13", 1},
  {"QF1R.14", 1},
  {"QF1R.15", 1},
  {"QF1R.16", 1},
  {"QF1R.17", 1},
  {"QF1R.18", 1},
  {"QF1R.19", 1},
  {"QF1R.2", 1},
  {"QF1R.20", 1},
  {"QF1R.21", 1},
  {"QF1R.22", 1},
  {"QF1R.23", 1},
  {"QF1R.24", 1},
  {"QF1R.25", 1},
  {"QF1R.26", 1},
  {"QF1R.27", 1},
  {"QF1R.28", 1},
  {"QF1R.3", 1},
  {"QF1R.4", 1},
  {"QF1R.5", 1},
  {"QF1R.6", 1},
  {"QF1R.7", 1},
  {"QF1R.8", 1},
  {"QF1R.9", 1},
  {"QF2R.1", 1.0051441655463682},
  {"QF2R.10", 1.0051441655463682},
  {"QF2R.11", 1.0051441655463682},
  {"QF2R.12", 1.0051441655463682},
  {"QF2R.13", 1.0051441655463682},
  {"QF2R.14", 1.0051441655463682},
  {"QF2R.15", 1.0051441655463682},
  {"QF2R.16", 1.0051441655463682},
  {"QF2R.17", 1.0051441655463682},
  {"QF2R.18", 1.0051441655463682},
  {"QF2R.19", 1.0051441655463682},
  {"QF2R.2", 1.0051441655463682},
  {"QF2R.20", 1.0051441655463682},
  {"QF2R.21", 1.0051441655463682},
  {"QF2R.22", 1.0051441655463682},
  {"QF2R.23", 1.0051441655463682},
  {"QF2R.24", 1.0051441655463682},
  {"QF2R.25", 1.0051441655463682},
  {"QF2R.26", 1.0051441655463682},
  {"QF2R.3", 1.0051441655463682},
  {"QF2R.4", 1.0051441655463682},
  {"QF2R.5", 1.0051441655463682},
  {"QF2R.6", 1.0051441655463682},
  {"QF2R.7", 1.0051441655463682},
  {"QF2R.8", 1.0051441655463682},
  {"QF2R.9", 1.0051441655463682},
  {"QM10R.1", 1.0128793817896742},
  {"QM10R.2", 1.0128793817896742},
  {"QM11R.1", 1.0047129325935689},
  {"QM11R.2", 1.0047129325935689},
  {"QM12R.1", 1.015727307741687},
  {"QM12R.2", 1.015727307741687},
  {"QM13R.1", 1.0191774308582298},
  {"QM13R.2", 1.0191774308582298},
  {"QM14R.1", 1.0023064118248732},
  {"QM14R.2", 1.0023064118248732},
  {"QM15R.1", .9971866810544164},
  {"QM15R.2", .9971866810544164},
  {"QM16R.1", 1.0078207588804615},
  {"QM16R.2", 1.0078207588804615},
  {"QM17R.1", 1.0054286383589828},
  {"QM17R.2", 1.0054286383589828},
  {"QM18R.1", .9999845165286056},
  {"QM18R.2", .9999845165286056},
  {"QM19R.1", 1.0004546517367696},
  {"QM19R.2", 1.0004546517367696},
  {"QM1R.1", 1.0121439827054057},
  {"QM1R.2", 1.0121424295270571},
  {"QM20R.1", 1.0168559278470957},
  {"QM20R.2", 1.01685247601763},
  {"QM21R.1", .9893250521616769},
  {"QM21R.2", .9893271236598262},
  {"QM22R.1", .998351278600269},
  {"QM22R.2", .9983396426911071},
  {"QM23R.1", 1.017934325671723},
  {"QM23R.2", 1.0179329202251381},
  {"QM3R.1", 1.0175289793610405},
  {"QM3R.2", 1.0175132060138157},
  {"QM4R.1", 1.0068406858900734},
  {"QM4R.2", 1.0068380004605388},
  {"QM5R.1", .9961531962267527},
  {"QM5R.2", .9961531962267527},
  {"QM6R.1", .9947161668695362},
  {"QM6R.2", .9947161668695362},
  {"QM7AR", .9827674537748592},
  {"QM7R", .9759682516351876},
  {"QM8R.1", .9683762232693006},
  {"QM8R.2", .9683762232693006},
  {"QM9R.1", .9982196610328309},
  {"QM9R.2", .9982196610328309}
 };

  If[mode==1,
    Scan[(LINE["K1",#[1]]=LINE["K1",#[1]]*#[2])&,fudgefactor];
  ,
    Scan[(LINE["K1",#[1]]=LINE["K1",#[1]]/#[2])&,fudgefactor];
  ];
 );
