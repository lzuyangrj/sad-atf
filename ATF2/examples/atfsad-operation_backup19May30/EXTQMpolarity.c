#include <stdio.h>
#include <cadef.h>

/*
gcc -o EXTQMpolarity EXTQMpolarity.c -I/usr/local/epics/base/include -I/usr/local/epics/base/include/os/Linux -L/usr/local/epics/base/lib/linux-x86 -lca

*/

#define atferr(msg) fprintf(stderr,"ERR: %s, %s(%s:%d)\n", \
	(char*)__FUNCTION__,msg,__FILE__,__LINE__ )

int main( int argc, char *argv[] )
{
	#define QM_NMAG 6
	#define SAVE_FILE "/atfnfs/linux/sad/EXTQMpolarity.dat"
	FILE *fp;
	char qm_name[QM_NMAG][16] = {"QM11FF","QM12FF","QM13FF","QM14FF","QM15FF","QM16FF"};
	chid pvs[QM_NMAG];
	int i;
	long polarity[QM_NMAG];
	char pvname[60];
/*
 [メモ]
	 EPICS_CA_ADDR_LISTに　atf-lxs2.atf-local:5066　がないとダメ

*/
	setenv( "EPICS_CA_ADDR_LIST", "atf-lxs2.atf-local:5066", 1 );

	/* EPICS Initialize */
	if( ca_task_initialize() != ECA_NORMAL ){
		atferr("ca_task_initialize");
		return(1);
	}

	/* Search PV*/
	for( i=0; i<QM_NMAG; i++ ){
		sprintf(pvname,"%s:polarity", qm_name[i] );
		ca_search( pvname, &pvs[i] );
	}
	if( ca_pend_io(1.0) != ECA_NORMAL ){
		atferr("ca_search");
		ca_task_exit();
		return(1);
	}

	/* Read polarity from PV */
	for( i=0; i<QM_NMAG; i++ ){
		ca_get( DBR_LONG, pvs[i], &polarity[i] );
	}
	if( ca_pend_io(1.0) != ECA_NORMAL ){
		atferr("ca_get");
		ca_task_exit();
		return(1);
	}

	/* EPICS Exit */
	ca_task_exit();

	/* Save to file */
	fp = fopen( SAVE_FILE, "w");
	if( fp == NULL ){
		atferr("fopen");
		return(1);
	}

	fprintf(fp,"QMpolarity={\n");		
	for( i=0; i<QM_NMAG; i++ ){
		if( i != (QM_NMAG-1) ) fprintf(fp," {\"%s\",%d},\n", qm_name[i], (int)polarity[i] );		
		else fprintf(fp," {\"%s\",%d}\n", qm_name[i], (int)polarity[i] );		
	}
	fprintf(fp,"};\n");		

	fclose(fp);

	return(0);
}
