!
! ATF-DR EXTRACTION LINE
!                  original version                    07/05/1994   by S.Kuroda
!         Redesign from present magnet arrangement
!            and with effective length of Quad.        11/08/1999   by T.Okugi
!
!  change names of MARK: IEX0 --> IEX0, IEX2 --> IP2   11/10/1999  K.Kubo 
!  change Effective Length of BH1X, BH2X               11/11/1999  T.Okugi
!  Added the beamline "EXR", which included "QM5R"     12/02/1999  T.Okugi 
!  change ROTATE of horizontal steers, Z{HXS}*X, 0 --->180 DEG 
!                                                12/08/1999 K.KUBO T.OKUGI 
!
!----For Cavity BPM (unofficial)                        05/25/2000  T.Imai
!		    (re-install,separate sensor cavity) 11/19/2000  T.Imai
!		    (re-install,separate sensor cavity) 04/05/2001  T.Imai
!
! ---- Skew quadrupole magnet QS1X, QS2X were added.    04/24/2001  T.Okugi
!         K1 values for these magnets are K1[1/m]=0.003*I[A] 
! ---- Skew quadrupole magnet QK0X was added.           04/24/2001  T.Okugi
! ---- Compton chamber for polarized positron experiment 
!        was modified                                   06/20/2001  T.Okugi
! --- Add three sextupoles, SD1X SD2X SF1X. QS1X and ZH3X are moved
!                                                       03/07/2005  K.Kubo
! --- Chang the location of QK0X                        12/01/2005  T.Okugi
! --- Change way of coordinate transformation           02/23/2007  S.Kuroda
! --- Change layout at the end of extraction            05/24/2007  T.Okugi
! --- MS3X was moved to 0.355m downstream of QF2X       02/07/2008  T.Okugi
!
MOMENTUM = 1.542282 GEV;
OFF ECHO CTIME;ON COD RFSW RADCOD;
 me=0.511e-3;
 gamma=1.542282/me;
!
! -----  Define the effective length for each type of magnet
!
! *** Quadrupole magnet ***
 dlQHtype01  = 0.018765;
 dlQHtype02  = 0.018670; 
 dlQHtype03  = 0.018470; 
 dlQHtype04  = 0.018745;
 dlQHtype05  = 0.018610;
 dlQTOKIN06  = 0.018907;
 dlQIDXskew  = 0.018670; ! This value was set to be as well as HITACHI Type2.
!
! *** Bending Magnet ***
! dlBSHItypeC = 0.022960;
! dlBSHItypeH = 0.047710;
!    modified by T.Okugi (11/11/1999)
 dlBSHItypeC =-0.00686;
 dlBSHItypeH =-0.00222;
!
! *** Steering Magnet ***
 dlZNKKtypeH = 0.051921;
 dlZNKKtypeV = 0.064800;
!
!
!  ----- Asign the magnet type to individual magnets
!
! *** Quadrupole magnet ***
 dlQM5RX = dlQHtype04;
 dlQM6RX = dlQHtype04;
 dlQM7RX = dlQTOKIN06;
 dlQD1X  = dlQHtype02;
 dlQD2X  = dlQHtype02;
 dlQD3X  = dlQHtype05;
 dlQD4X  = dlQHtype05;
 dlQD5X  = dlQHtype05;
 dlQD6X  = dlQTOKIN06;
 dlQD7X  = dlQHtype05;
 dlQD8X  = dlQHtype04;
 dlQD9X  = dlQHtype04;
 dlQF1X  = dlQHtype02;
 dlQF2X  = dlQHtype01;
 dlQF3X  = dlQHtype05;
 dlQF4X  = dlQHtype05;
 dlQF5X  = dlQHtype05;
 dlQF6X  = dlQHtype05;
 dlQF7X  = dlQHtype04;
 dlQK1X  = dlQIDXskew;
 dlQK2X  = dlQIDXskew;
 dlQK3X  = dlQIDXskew;
 dlQK4X  = dlQIDXskew;
!
! *** Bending Magnet ***
 dlBH1X  = dlBSHItypeH;
 dlBH2X  = dlBSHItypeH;
 dlBH3X  = dlBSHItypeH;
 dlBH4X  = dlBSHItypeC;
 dlBH5X  = dlBSHItypeC;
 dlBS1X  = 0.0;
 dlBS2X  = 0.0;
 dlBS3X  = 0.0;
!
! *** Steering Magnet ***
 dlZH1X  = dlZNKKtypeH;
 dlZH2X  = dlZNKKtypeH;
 dlZH3X  = dlZNKKtypeH;
 dlZH4X  = dlZNKKtypeH;
 dlZH5X  = dlZNKKtypeH;
 dlZH6X  = dlZNKKtypeH;
 dlZH7X  = dlZNKKtypeH;
 dlZV1X  = dlZNKKtypeV;
 dlZV2X  = dlZNKKtypeV;
 dlZV3X  = dlZNKKtypeV;
 dlZV4X  = dlZNKKtypeV;
 dlZV5X  = dlZNKKtypeV;
 dlZV6X  = dlZNKKtypeV;
 dlZV7X  = dlZNKKtypeV;
 dlZV8X  = dlZNKKtypeV;
 dlZV9X  = dlZNKKtypeV;
 dlZV10X = dlZNKKtypeV;
!
! ---------  Define the Optics Components
!
 DRIFT	L001X	= ( L = 0.9125 )
	L002X	= ( L = 1.4525 )
	L003X	= ( L = 0.0750 )
	L004X	= ( L = 0.1200 )
	L005X	= ( L = 0.1600 )
	L006X	= ( L = 0.0800 )
	L007X	= ( L = 0.0800 )
	L101X	= ( L = 0.2200 )
	L102X	= ( L = 0.3600 )
	L1021X	= ( L = 0.2050 )
	L1022X	= ( L = 0.0800 )
	L103X	= ( L = 0.1300 )
	L104X	= ( L = 0.2000 )
	L105X	= ( L = 0.0100 )
	L106X	= ( L = 0.3200 )
	L201X	= ( L = 0.2600 )
	L202X	= ( L = 0.1600 )
	L203X	= ( L = 0.4550 )
	L204X	= ( L = 0.0100 )
	L205X   = ( L = 0.2950 )
	L2051X	= ( L = 0.0200 )
	L2052X	= ( L = 0.2000 )
!	L3011X	= ( L = 0.9500 )
	L3011X	= ( L = 0.7683 )
!	L3012X	= ( L = 0.0400 )
	L30121X	= ( L = 0.11 )
	L30122X	= ( L = 0.0355 )
	L302X	= ( L = 0.0100 )
	L3031X	= ( L = 0.3350 )
	L3032X	= ( L = 0.5200-0.3350 )
	L304X	= ( L = 0.1300 )
	L305X	= ( L = 0.5100 )
	L306X	= ( L = 0.3300 )
	L307X	= ( L = 0.0100 )
	L308X	= ( L = 0.1600 )
	L309X	= ( L = 0.7400 )
	L310X	= ( L = 0.2800 )
	L311X	= ( L = 0.1900 )
	L312X	= ( L = 0.2600 )
	L313X	= ( L = 0.0100 )
	L314X	= ( L = 0.6800 )
	L315X	= ( L = 0.2800 )
!	L401X	= ( L = 0.3500 )
	L4011X	= ( L = 0.1716 )
	L4012X	= ( L = 0.1022 )
	L402X	= ( L = 0.0100 )
	L4031X	= ( L = 0.2100 )
	L4032X	= ( L = 0.1300 )
	L404X	= ( L = 0.0900 )
	L405X	= ( L = 0.1350 )
	L4061X	= ( L = 0.2550 )
	L4062X	= ( L = 0.2650 )
	L4071X	= ( L = 0.2650 )
!	L4072X	= ( L = 0.6000 )
	L4072X	= ( L = 0.5382 )
!	L408X	= ( L = 0.1500 )
	L4081X	= ( L = 0.1213 )
	L4082X	= ( L = 0.1143 )
	L409X	= ( L = 0.0100 )
!	L410X	= ( L = 0.3700 )
	L4101X	= ( L = 0.1630 )
	L4102X	= ( L = 0.1070 )
	L411X	= ( L = 0.1940 )
	L501X	= ( L = 0.3450 )
	L502X	= ( L = 0.0500 )
	L503X	= ( L = 0.1350 )
	L504X	= ( L = 0.0500 )
	L505X	= ( L = 0.2950 )
	L506X	= ( L = 0.3900 )
	L507X	= ( L = 0.2600 )
	L508X	= ( L = 0.0100 )
	L509X	= ( L = 0.0550 )
	L510X	= ( L = 0.0500 )
	L5110X  = ( L = 0.1850 )
	L5111X  = ( L = 0.1050 )
	L5120X  = ( L = 0.3200 )
	L5121X  = ( L = 0.5670 )
	L5122X  = ( L = 0.4460 )
	L513X	= ( L = 0.0120 )
	L514X	= ( L = 0.0550 )
	L515X	= ( L = 0.0500 )
	L516X	= ( L = 0.2300 )
	L517X	= ( L = 0.3500 )
	L5180X  = ( L = 0.1950 )
	L5181X  = ( L = 0.9060-0.1950 )
	L5182X  = ( L = 0.1740 )
	L519X	= ( L = 0.0250 )
	L520X	= ( L = 0.0450 )
	L521X	= ( L = 0.0500 )
	L522X	= ( L = 0.2450 )
	L5230X  = ( L = 1.1960 )
	L5231X  = ( L = 0.2290 )
	L524X	= ( L = 0.1600 )
	L525X	= ( L = 0.0500 )
	L526X	= ( L = 0.2600 )
	L5290X  = ( L = 1.0830 )
	L5291X  = ( L = 0.3270-0.110 )
	L530X	= ( L = 0.1600+0.110 )
	L531X	= ( L = 0.0500+0.080 )
	L532X	= ( L = 0.2400+0.030 )
	L533X	= ( L = 0.3050-0.110 )
	L601X	= ( L = 0.6500 )
	L602X	= ( L = 0.6850 )
;
 DRIFT	LDQM6R	= ( L = 0.0200 - dlQM6RX/2  )
	LDQM7R	= ( L = 0.0200 - dlQM7RX/2  )
	LDQD1X	= ( L = 0.0200 - dlQD1X/2  )
	LDQD2X	= ( L = 0.0200 - dlQD2X/2  )
	LDQD3X	= ( L = 0.0200 - dlQD3X/2  )
	LDQD4X	= ( L = 0.0200 - dlQD4X/2  )
	LDQD5X	= ( L = 0.0200 - dlQD5X/2  )
	LDQD6X	= ( L = 0.0200 - dlQD6X/2  )
	LDQD7X	= ( L = 0.0200 - dlQD7X/2  )
	LDQD8X	= ( L = 0.0200 - dlQD8X/2  )
	LDQD9X	= ( L = 0.0200 - dlQD9X/2  )
	LDQF1X	= ( L = 0.0200 - dlQF1X/2  )
	LDQF2X	= ( L = 0.0200 - dlQF2X/2  )
	LDQF3X	= ( L = 0.0200 - dlQF3X/2  )
	LDQF4X	= ( L = 0.0200 - dlQF4X/2  )
	LDQF5X	= ( L = 0.0200 - dlQF5X/2  ) 
	LDQF6X	= ( L = 0.0200 - dlQF6X/2  )
	LDQF7X	= ( L = 0.0200 - dlQF7X/2  )
	LDQK1X	= ( L = 0.0200 - dlQK1X/2  )
	LDQK2X	= ( L = 0.0200 - dlQK2X/2  )
	LDQK3X	= ( L = 0.0200 - dlQK3X/2  )
	LDQK4X	= ( L = 0.0200 - dlQK4X/2  )
	LDBS1X	= ( L = 0.0200 - dlBS1X/2  )
	LDBS2X	= ( L = 0.0200 - dlBS2X/2  )
	LDBS3X	= ( L = 0.0200 - dlBS3X/2  )
	LDBH1X	= ( L = 0.0200 - dlBH1X/2  )
	LDBH2X	= ( L = 0.0200 - dlBH2X/2  )
	LDBH3X	= ( L = 0.0200 - dlBH3X/2  )
	LDBH4X	= ( L = 0.0200 - dlBH4X/2  )
	LDBH5X	= ( L = 0.0200 - dlBH5X/2  )
 	LDZH1X	= ( L = 0.0200 - dlZH1X/2  )
	LDZH2X	= ( L = 0.0200 - dlZH2X/2  )
	LDZH3X	= ( L = 0.0200 - dlZH3X/2  )
	LDZH4X	= ( L = 0.0200 - dlZH4X/2  )
	LDZH5X	= ( L = 0.0200 - dlZH5X/2  )
	LDZH6X	= ( L = 0.0200 - dlZH6X/2  )
	LDZH7X	= ( L = 0.0200 - dlZH7X/2  )
 	LDZV1X	= ( L = 0.0200 - dlZV1X/2  )
	LDZV2X	= ( L = 0.0200 - dlZV2X/2  )
	LDZV3X	= ( L = 0.0200 - dlZV3X/2  )
	LDZV4X	= ( L = 0.0200 - dlZV4X/2  )
	LDZV5X	= ( L = 0.0200 - dlZV5X/2  )
	LDZV6X	= ( L = 0.0200 - dlZV6X/2  )
	LDZV7X	= ( L = 0.0200 - dlZV7X/2  )
	LDZV8X	= ( L = 0.0200 - dlZV8X/2  )
	LDZV9X	= ( L = 0.0200 - dlZV9X/2  )
	LDZV10X	= ( L = 0.0200 - dlZV10X/2 )
;
 BEND   KE1X    = ( L = 0.2500 ANGLE = -0.0025 K0 = -3.93158757868E-7 E1 = 0 )
        KE2X    = ( L = 0.2500 ANGLE = -0.0025 K0 = -3.93158757868E-7 E2 = 2 )
	KE3X    = ( L = 0.2500 ANGLE = -0.0025 K0 = -3.93158757868E-7 E1 = 1 )
        KE4X    = ( L = 0.2500 ANGLE = -0.0025 K0 = -3.93158757868E-7 E2 = 1 )
        BS1X    = ( L = 0.3000+dlBS1X/2 ANGLE = -0.0140178327098 K0 =-9.715403090680E-5 )
        BS2X    = ( L = 0.4000+dlBS2X/2 ANGLE = -0.0371716830661 K0 =-2.576274749620E-4 )
        BS3X    = ( L = 0.5000+dlBS3X/2 ANGLE = -0.1175110129077 K0 = 1.438702877444E-4 )
        BH1X    = ( L = 0.4000+dlBH1X/2 ANGLE = -0.0935043315000 )
        BH2X    = ( L = 0.4000+dlBH2X/2 ANGLE =  0.0935043315000 )
        BH3X    = ( L = 0.4000+dlBH3X/2 ANGLE =  0.0935043315000 )
        BH4X    = ( L = 0.6750+dlBH4X/2 ANGLE =  0.1561995915000 )
        BH5X    = ( L = 0.6750+dlBH5X/2 ANGLE = -0.1561995915000 )
;
 BEND	ZS1X	= ( L = 0.0000 ANGLE =  0.0000  ROTATE = 180 DEG )
	ZS2X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZS3X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZX1X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZX2X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZX3X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZX4X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZX5X	= ( L = 0.0000 ANGLE =  0.0000   ROTATE = 180 DEG )
	ZH1X	= ( L = 0.0600+dlZH1X  ANGLE = 0.0000   ROTATE = 180 DEG )
	ZH2X	= ( L = 0.0600+dlZH2X  ANGLE = 0.0000   ROTATE = 180 DEG )
	ZH3X	= ( L = 0.0600+dlZH3X  ANGLE = 0.0000   ROTATE = 180 DEG )
	ZH4X	= ( L = 0.0600+dlZH4X  ANGLE = 0.0000   ROTATE = 180 DEG )
	ZH5X	= ( L = 0.0600+dlZH5X  ANGLE = 0.0000   ROTATE = 180 DEG )
	ZH6X	= ( L = 0.0600+dlZH6X  ANGLE = 0.0000   ROTATE = 180 DEG )
	ZH7X	= ( L = 0.0600+dlZH7X  ANGLE = 0.0000   ROTATE = 180 DEG )

	ZV1X	= ( L = 0.0600+dlZV1X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV2X	= ( L = 0.0600+dlZV2X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV3X	= ( L = 0.0600+dlZV3X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV4X	= ( L = 0.0600+dlZV4X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV5X	= ( L = 0.0600+dlZV5X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV6X	= ( L = 0.0600+dlZV6X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV7X	= ( L = 0.0600+dlZV7X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV8X	= ( L = 0.0600+dlZV8X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV9X	= ( L = 0.0600+dlZV9X  ANGLE = 0.0000 ROTATE = 90 DEG )
	ZV10X	= ( L = 0.0600+dlZV10X ANGLE = 0.0000 ROTATE = 90 DEG )
;
 QUAD   QDR6    = ( L = 0.1800+dlQM6RX  K1 = -0.7121175306066 )
        QDR7    = ( L = 0.0600+dlQM7RX  K1 =  0.3980779044020 )
        QD1X    = ( L = 0.0600+dlQD1X  K1 = -0.0504290000000 )
        QD2X    = ( L = 0.0600+dlQD2X  K1 = -0.4759400000000 )
        QD3X    = ( L = 0.1800+dlQD3X  K1 = -0.4333100000000 )
        QD4X    = ( L = 0.1800+dlQD4X  K1 = -0.6917400000000 )
        QD5X    = ( L = 0.1800+dlQD5X  K1 = -0.3049200000000 )
        QD6X    = ( L = 0.0600+dlQD6X  K1 =  0.2279126278103 )
        QD7X    = ( L = 0.1800+dlQD7X  K1 = -0.5020674963304 )
        QD8X    = ( L = 0.1800+dlQD8X  K1 = -0.3527213202021 )
        QD9X    = ( L = 0.1800+dlQD9X  K1 = -0.3527213202021 )
        QF1X    = ( L = 0.0600+dlQF1X  K1 =  0.3989500000000 )
        QF2X    = ( L = 0.0600+dlQF2X  K1 =  0.1759500000000 )
        QF3X    = ( L = 0.1800+dlQF3X  K1 =  1.0338000000000 )
        QF4X    = ( L = 0.1800+dlQF4X  K1 =  1.4677000000000 )
        QF5X    = ( L = 0.1800+dlQF5X  K1 =  0.4273291380396 )
        QF6X    = ( L = 0.1800+dlQF6X  K1 =  0.7637531660967 )
        QF7X    = ( L = 0.1800+dlQF7X  K1 =  0.8808394678728 )
        QK0X    = ( L = 0.0750         K1 =  0.0000  ROTATE = 45 DEG )
        QK1X    = ( L = 0.0600+dlQK1X  K1 =  0.0000  ROTATE = 45 DEG )
        QK2X    = ( L = 0.0600+dlQK2X  K1 =  0.0000  ROTATE = 45 DEG )
        QK3X    = ( L = 0.0600+dlQK3X  K1 =  0.0000  ROTATE = 45 DEG )
        QK4X    = ( L = 0.0600+dlQK4X  K1 =  0.0000  ROTATE = 45 DEG )
        QS1X    = ( L = 0.1000         K1 =  0.0000  ROTATE = 45 DEG )
        QS2X    = ( L = 0.1000         K1 =  0.0000  ROTATE = 45 DEG )
;
 SEXT
        SD1X = (L=0.0762 K2=-0.)
        SD2X = (L=0.0762 K2=-0.)
        SF1X = (L=0.0762 K2=0.)
;
 COORD  C1      = ( DX =  .001250002604173 CHI1 =  5.0000000000e-3 DIR = 1 )
        C2      = ( DX =  .022705554330302 CHI1 =-.001139637337491 DIR = 0 )
;
 MONI	MB1X  = ()
	MB2X  = ()
	ML1X  = ()
	ML2X  = ()
	ML3X  = ()
	ML4X  = ()
	ML5X  = ()
	ML6X  = ()
	ML7X  = ()
	ML8X  = ()
	ML9X  = ()
	ML10X = ()
	ML11X = ()
	ML12X = ()
	ML13X = ()
	ML14X = ()
	MS1X  = ()
	MS2X  = ()
	MS3X  = ()
	MS4X  = ()
	MS5X  = ()
	MS6X  = ()
	MS11X = ()
	MS12X = ()
	MW0X  = ()
	MW1X  = ()
	MW2X  = ()
	MW3X  = ()
	MW4X  = ()
	MC1X  = ()
	MC2X  = ()
	MC3X  = ()
	MT1X  = ();
!
! ----------For Cavity BPM (T.Imai)
!
!	Cavity BPM 
!	       reference cavity   MM0X
!	       sensor cavity      MM1X,MM2X,MM3X,MM4X,MM5X
!			
!		cf install position
!			          ML9X    (MM1X,MM2X)   ML10X
!				  ML10X	  (MM3X)	ML11X
!				  ML11X   (MM4X)        ML12X
!				  ML12X   (MM5X)        ML13X
  MONI    MM0X = ()
	  MM1X = ()
	  MM2X = ()
	  MM3X = ()
	  MM4X = ()
	  MM5X = ()
;                                                           
 MARK   IEX   = ( AX =  1.1509975629194	  BX  = 7.2123362234044
		  AY = -1.7211735963534	  BY  = 2.9030719347847 
		  EMITX = 1.5e-09  EMITY = 1.0e-10 DP = 6.0e-4 )
	IEX1  = ()
	IP2   = ()
	IEX3  = ()
 	IP01  = ()
	IP02  = ()
	IPT1  = ()
	IPT2  = ();
!
! ------  Definition of the subsub-line for magnet devices.
!
 LINE	LKE1X	= ( KE1X IP01 KE2X )
	LKE2X	= ( KE3X IP02 KE4X )
	LBS1X	= ( LDBS1X C2 BS1X ZS1X BS1X LDBS1X )
	LBS2X	= ( LDBS2X BS2X ZS2X BS2X LDBS2X )
	LBS3X	= ( LDBS3X BS3X ZS3X BS3X LDBS3X )
	LBH1X	= ( LDBH1X BH1X ZX1X BH1X LDBH1X )
	LBH2X	= ( LDBH2X BH2X ZX2X BH2X LDBH2X )
	LBH3X	= ( LDBH3X BH3X ZX3X BH3X LDBH3X )
	LBH4X	= ( LDBH4X BH4X ZX4X BH4X LDBH4X )
	LBH5X	= ( LDBH5X BH5X ZX5X BH5X LDBH5X )
!	LQM6X	= ( LDQM6R C1 QDR6 C2 IPT1 CC1 LDQM6R )
!	LQM7X	= ( LDQM7R C3 QDR7 C4 IPT2 CC2 LDQM7R )
	LQM6X	= ( LDQM6R QDR6 IPT1 LDQM6R )
	LQM7X	= ( LDQM7R QDR7 IPT2 LDQM7R )
	LQD1X	= ( LDQD1X QD1X LDQD1X )
	LQD2X	= ( LDQD2X QD2X LDQD2X )
	LQD3X	= ( LDQD3X QD3X LDQD3X )
	LQD4X	= ( LDQD4X QD4X LDQD4X )
	LQD5X	= ( LDQD5X QD5X LDQD5X )
	LQD6X	= ( LDQD6X QD6X LDQD6X )
	LQD7X	= ( LDQD7X QD7X LDQD7X )
	LQD8X	= ( LDQD8X QD8X LDQD8X )
	LQD9X	= ( LDQD9X QD9X LDQD9X )
	LQF1X	= ( LDQF1X QF1X LDQF1X )
	LQF2X	= ( LDQF2X QF2X LDQF2X )
	LQF3X	= ( LDQF3X QF3X LDQF3X )
	LQF4X	= ( LDQF4X QF4X LDQF4X )
	LQF5X	= ( LDQF5X QF5X LDQF5X )
	LQF6X	= ( LDQF6X QF6X LDQF6X )
	LQF7X	= ( LDQF7X QF7X LDQF7X )
	LQK1X	= ( LDQK1X QK1X LDQK1X )
	LQK2X	= ( LDQK2X QK2X LDQK2X )
	LQK3X	= ( LDQK3X QK3X LDQK3X )
	LQK4X	= ( LDQK4X QK4X LDQK4X )
	LZH1X	= ( LDZH1X ZH1X LDZH1X )
	LZH2X	= ( LDZH2X ZH2X LDZH2X )
	LZH3X	= ( LDZH3X ZH3X LDZH3X )
	LZH4X	= ( LDZH4X ZH4X LDZH4X )
	LZH5X	= ( LDZH5X ZH5X LDZH5X )
	LZH6X	= ( LDZH6X ZH6X LDZH6X )
	LZH7X	= ( LDZH7X ZH7X LDZH7X )
	LZV1X	= ( LDZV1X ZV1X LDZV1X )
	LZV2X	= ( LDZV2X ZV2X LDZV2X )
	LZV3X	= ( LDZV3X ZV3X LDZV3X )
	LZV4X	= ( LDZV4X ZV4X LDZV4X )
	LZV5X	= ( LDZV5X ZV5X LDZV5X )
	LZV6X	= ( LDZV6X ZV6X LDZV6X )
	LZV7X	= ( LDZV7X ZV7X LDZV7X )
	LZV8X	= ( LDZV8X ZV8X LDZV8X )
	LZV9X	= ( LDZV9X ZV9X LDZV9X )
	LZV10X	= ( LDZV10X ZV10X LDZV10X );

 LINE	LCB1X = ( L5110X MM1X L5111X)
	LCB2X = ( L5120X MM0X L5121X MM2X L5122X)
	LCB3X = ( L5180X MT1X L5181X MM3X L5182X)
	LCB4X = ( L5230X MM4X L5231X)
	LCB5X = ( L5290X MM5X L5291X);
!
!  -----  Definition of the LINE for KEK-ATF extraction line.
!
 LINE	EX0 = ( IEX   LKE1X  C1
                      L001X  LQM6X  L002X  MB1X   L003X  LQM7X  L004X
		      LBS1X  L005X  LBS2X  L006X  MB2X   L007X  LBS3X )
	EX1 = ( IEX1  L101X  MC1X   
!L102X --------------------------- Replaced to "L1021X QK0X L1022X"  2005/12/01 by T.Okugi
                      L1021X QK0X L1022X 
                      LZV1X  L103X  MS1X   L104X  ML1X
		      L105X  LQD1X  L106X  LBH1X  L201X  LQD2X  L202X  LZV2X
		      L203X  ML2X   L204X  LQF1X  
!L2051X QK0X L2052X -------------- Replaced to "L205X"   2005/12/01 by T.Okugi 
                      L205X  LBH2X 
		      L3011X QS1X   L30121X  SD1X L30122X ML3X
		      L302X  LQF2X  L3031X  MS3X L3032X LZV3X  L304X  LZH1X  L305X  LZV4X
		      L306X  ML4X   L307X  LQD3X  L308X  LZV5X  L309X
		      L310X  LZH2X  L311X  LKE2X  L312X  ML5X   L313X  LQF3X
		      L314X  MC2X   L315X  LBH3X  L4011X SD2X L4012X  ML6X   L402X  LQF4X
		      L4031X QS2X   L4032X LQD4X  L404X  LZV6X  L405X  LZH7X  
		      L4061X MS11X  L4062X MS6X   L4071X MS12X  L4072X
		      L4081X SF1X L4082X  ML7X   L409X  LQD5X  L4101X  LZH3X  L4102X  MS4X
		      L411X  LBH4X  )
	EX2  =  (IP2  L501X  LQF5X  L502X  LQK1X  L503X  LQD6X  L504X  ML8X
		      L505X  LZV7X  L506X  MW0X   L507X  LZH4X  L508X  LQK2X
		      L509X  LQD7X  L510X  ML9X   LCB1X  LZV8X  LCB2X  LZH5X
		      L513X  LQK3X  L514X  LQF6X  L515X  ML10X  L516X  MS5X
		      L517X  MW1X   LCB3X  LZV9X  L519X  LQK4X  L520X  LQD8X
		      L521X  ML11X  L522X  MW2X   LCB4X  LZH6X  L524X  LQF7X
		      L525X  ML12X  L526X  MW3X   LCB5X  LZV10X L530X  LQD9X
	              L531X  ML13X  L532X  MW4X   L533X  LBH5X  L601X
		      ML14X  L602X  IEX3 )
	EXT = ( EX0 EX1 EX2 )
!
! ---------------------------------------------   Added by T.Okugi (1999/12/02)
	EXR = ( IPDRX LR01X MB18R LR02X QDR5 LR03X EXT )
;
MONI  MB18R = ();
DRIFT LR01X = ( L = 0.100-0.057 )
      LR02X = ( L = 0.057-dlQM5RX/2 )  
      LR03X = ( L = 0.400-dlQM5RX/2 );
QUAD  QDR5  = ( L = .18+dlQM5RX K1 = 1.0660334417671 );
MARK  IPDRX   = ( AX = -6.1005911555691  BX =  5.8416532278146
		AY =  0.8277158089986  BY =  1.8266418344704 
		EX = 0.0 EPX = 0.0 EY = 0.0 EPY = 0.0 
		EMITX = 1.5e-09  EMITY = 1.0e-10  DP = 6.0e-4);

!FFS USE=EXT;
!  dr:=(FFS["out '/users/kuroda/a' draw bx &by & ex ey q* term out tdr '/users/kuroda/a'&"]);
!  dro:=(FFS["out '/users/kuroda/a' draw dx &dy q* term out tdr '/users/kuroda/a'&"]);
!cal;end
!STOP;












