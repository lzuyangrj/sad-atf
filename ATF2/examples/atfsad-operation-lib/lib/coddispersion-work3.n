
 

  CorrectCODDispersion:=Module[{pm,
    fixsteers=FixSteer/.Option/.FixSteer->{},
    bpmlist1=ATFBPM/.ATFData[1]/.ATFBPM->{},
    bpmlist2=ATFBPM/.ATFData[2]/.ATFBPM->{},
    k0list=ATFSteer/.ATFData[1]/.ATFSteer->{},
    chop=ChopIntensity/.Option/.ChopIntensity->0,
    tunelist=ATFTune/.ATFData[1]/.ATFTune->{},
    
    mx=NSteerX/.(EtaCorrect/.Option)/.(NSteerX ->0),
    my=NSteerY/.(EtaCorrect/.Option)/.(NSteerY ->0),
    ratio=1.414,de,
    k0listx,k0listy,dzh,dzv,usezh,usezv,A,bpmx,bpmy,dispx0,dispx1,dispy0,dispy1,
    datazh,datazv,bpmnamelist1,maxdzh,maxdzv,mindzh,mindzv,datalist,my0,mx0,iflimit,
    etaxlist0,etaxlist1,etaylist0,etaylist1,
    maxk0,soft,dk0
    },
!    bpmlist1=ATFBPM/.ATFData[1]/.ATFBPM->{};
    fchange=FrequencyChange/.(EtaCorrect/.Option)/.(FrequencyChange ->5e3);
    momentumcomp=MomentumCompaction/.(EtaCorrect/.Option)/.(MomentumCompaction ->2e-3);
    de=-fchange/714e6/momentumcomp;

    Print["de=",de];

    bpmlist1=Select[bpmlist1, ((#[2]<0 || #[2]>=0)&&(#[3]<0 || #[3]>=0)&&(#[4]<0 || #[4]>=0))&];
    bpmlist2=Select[bpmlist2, ((#[2]<0 || #[2]>=0)&&(#[3]<0 || #[3]>=0)&&(#[4]<0 || #[4]>=0))&];

    bpmlist1=Select[bpmlist1,(#[4]>chop)&];
    bpmlist1=Select[bpmlist1,MemberQ[goodbpmlist,#[1]]&];
    bpmlist1=Map[{#[[1]],#[[2]]/de,#[[3]]/de,#[[4]]}&,bpmlist1];
    bpmlist2=Select[bpmlist2,(#[4]>chop)&];
    bpmlist2=Select[bpmlist2,MemberQ[goodbpmlist,#[1]]&];
      FFS["Z* K0 0;"];
    NXm=tunelist[1];NYm=tunelist[2];
    FFS["FIT NX NXm NY NYm"];
!Print["ab1"];
    FFS["FREE QF* go; cal"];
!Print["ab2"];
    FFS["Reject Total;"]; 
    FFS["FIX Q* "];
    

    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    k0list=Select[k0list,MemberQ[LINE["Name","Z*R"],#[1]]&];

 

    maxk0=0.16138E-02;
    tolerance=1.e-8;


    bpmfitlist1=Map[(
      If[((LINE["Position",#[[1]]]=>LINE["Position","M.19"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.80"])),
      {#[[1]],#[[2]],#[[3]],0,0},
      {#[[1]],#[[2]],#[[3]],Twiss["EX",#[[1]]],0}])&,bpmlist1];

    bpmfitlist2=Map[(
      If[((LINE["Position",#[[1]]]=>LINE["Position","M.19"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.80"])),
      {#[[1]],#[[2]],#[[3]],Twiss["DX",#[[1]]],0},
      {#[[1]],#[[2]],#[[3]],0,0}])&,bpmlist2];

    If[ifextdsp,fc1=1000,fc1=1.e-6];
    If[ifextobt,fc2=1000,fc2=1.e-6];     
    If[ifextdsp,bpmfitlist1=Select[bpmfitlist1,Not[MemberQ[{"M.19","M.20","M.21"},#[1]]]&]];
    If[ifextobt,bpmfitlist2=Select[bpmfitlist2,Not[MemberQ[{"M.19","M.20","M.21"},#[1]]]&]];

   
 bpmfitlist1=Append[bpmfitlist1,{"KIX",0,0,0,0}];
 bpmfitlist1=Append[bpmfitlist1,{"M.19",0,0,0,0}];
 bpmfitlist1=Append[bpmfitlist1,{"M.20",0,0,0,0}];
 bpmfitlist1=Append[bpmfitlist1,{"M.21",0,0,0,0}];    

 bpmfitlist2=Append[bpmfitlist2,{"KIX",0,0,0,0}];
 bpmfitlist2=Append[bpmfitlist2,{"M.19",0,0,0,0}];
 bpmfitlist2=Append[bpmfitlist2,{"M.20",0,0,0,0}];
 bpmfitlist2=Append[bpmfitlist2,{"M.21",0,0,0,0}];    

   
!     bpmfitlist1=Append[bpmfitlist1,{"KIX",fc1*Twiss["EX","KIX"],fc1*Twiss["EY","KIX"],fc1*Twiss["EX","KIX"],fc1*Twiss["EY","KIX"]}];    
!     bpmfitlist1=Append[bpmfitlist1,{"M.19",fc1*Twiss["EX","M.19"],fc1*Twiss["EY","M.19"],fc1*Twiss["EX","M.19"],fc1*Twiss["EY","M.19"]}];
!     bpmfitlist1=Append[bpmfitlist1,{"M.20",fc1*Twiss["EX","M.20"],fc1*Twiss["EY","M.20"],fc1*Twiss["EX","M.20"],fc1*Twiss["EY","M.20"]}];
!     bpmfitlist1=Append[bpmfitlist1,{"M.21",fc1*Twiss["EX","M.21"],fc1*Twiss["EY","M.21"],fc1*Twiss["EX","M.21"],fc1*Twiss["EY","M.21"]}];

!     bpmfitlist2=Append[bpmfitlist2,{"KIX",fc2*Twiss["DX","KIX"],fc2*Twiss["DY","KIX"],fc2*Twiss["DX","KIX"],fc2*Twiss["DY","KIX"]}];
!     bpmfitlist2=Append[bpmfitlist2,{"M.19",fc2*Twiss["DX","M.19"],fc2*Twiss["DY","M.19"],fc2*Twiss["DX","M.19"],fc2*Twiss["DY","M.19"]}];
!     bpmfitlist2=Append[bpmfitlist2,{"M.20",fc2*Twiss["DX","M.20"],fc2*Twiss["DY","M.20"],fc2*Twiss["DX","M.20"],fc2*Twiss["DY","M.20"]}];
!     bpmfitlist2=Append[bpmfitlist2,{"M.21",fc2*Twiss["DX","M.21"],fc2*Twiss["DY","M.21"],fc2*Twiss["DX","M.21"],fc2*Twiss["DY","M.21"]}];

!    extname={"KIX","M.19","M.20","M.21"};
    nlist1=Length[bpmfitlist1];
    nlist2=Length[bpmfitlist2];

    bpmnamelist1=Map[#[1]&,bpmfitlist1];
    bpmnamelist2=Map[#[1]&,bpmfitlist2];


!    soft=2;
    factor1=0.05;

    If[mx<=0 && my>=1, 
     k0listy=Select[k0list,(StringMatchQ[#[1],"ZV*R"])&];

     k0listy=Select[k0listy,Not[MemberQ[fixsteers,#[1]]]&];

     Scan[(maxdzv[#[1]]=#[2]+maxk0)&,k0listy];
     Scan[(mindzv[#[1]]=#[2]-maxk0)&,k0listy];
         
     usezv=Select[Map[#[1]&,k0listy],(maxdzv[#]>0 && mindzv[#]<0)&];
     dk0=1e-4;
     datazveta=Map[(steer=#;
      FFS["CAL"]; 
      ii=0;
      dispy0=Map[(ii=ii+1;If[ii>=nlist1-3,fct=fc1,fct=1]; fct*Twiss["EY",#])&,bpmnamelist1]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      ii=0;
      dispy1=Map[(ii=ii+1;If[ii>=nlist1-3,fct=fc1,fct=1]; fct*Twiss["EY",#])&,bpmnamelist1];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (dispy1-dispy0)/dk0)&,usezv];

     datazvcod=Map[(steer=#;
      FFS["CAL"]; 
      ii=0;
      cody0=Map[(ii=ii+1;If[ii>=nlist2-3,fct=fc2,fct=1]; fct*Twiss["DY",#])&,bpmnamelist2]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"];
      ii=0; 
      cody1=Map[(ii=ii+1;If[ii>=nlist2-3,fct=fc2,fct=1]; fct*Twiss["DY",#])&,bpmnamelist2];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (cody1-cody0)/dk0)&,usezv];

     datazv=Transpose[Join[Transpose[datazveta]*factor1,Transpose[datazvcod]]];

     bpmy=Join[Map[(#[3])&,bpmfitlist1]*factor1,Map[(#[3])&,bpmfitlist2]];

      selectedzv={};
      remainzv=usezv;
      respzv={};
      datazvi=datazv;
      nzv=0;
      residual=1.e30;
     While[nzv<my && residual>tolerance && nzv<Length[usezv],
      jzv=0;
      izv=0;
      minresidual=1.e30;
      Scan[(izv=izv+1; 
        tryzv=Append[selectedzv,#];
        tryrespzv=Append[respzv,datazvi[izv]]; 
        tryrespzv00=tryrespzv;
        tryzv00=tryzv;
        bpmy0=bpmy;

        iflimit=2; 
        trydzv=LinearSolve[Transpose[tryrespzv],bpmy]; 
        Scan[(trydzv00[#[1]]=#[2])&,Thread[{tryzv00,trydzv}]];

       While[Length[tryzv]>=1 && iflimit=>1,
          tryzv0=tryzv;
          trydzv0=trydzv;
          iflimit=0;
          iizv=Length[tryzv0]+1;
          Scan[(iizv=iizv-1;
              If[#[2]>maxdzv[#[1]],
                   bpmy=bpmy-maxdzv[#[1]]*tryrespzv[iizv];
                   trydzv00[#[1]]=maxdzv[#[1]];iflimit=1;tryzv=Drop[tryzv,{iizv}];tryrespzv=Drop[tryrespzv,{iizv}];
              ];
              If[#[2]<mindzv[#[1]], 
                   bpmy=bpmy-mindzv[#[1]]*tryrespzv[iizv];
                   trydzv00[#[1]]=mindzv[#[1]];iflimit=1;tryzv=Drop[tryzv,{iizv}];tryrespzv=Drop[tryrespzv,{iizv}];
              ];
          )&,Reverse[Thread[{tryzv0,trydzv0}]]]; 

          If[Length[tryrespzv]==0,Print[" Cannot correct.  Too big dispersion???????????????"]];
          If[Length[tryrespzv]==0,Print[" Cannot correct.  Too big dispersion???????????????"]];
          If[Length[tryrespzv]==0,Print[" Cannot correct.  Too big dispersion???????????????"]];

         If[iflimit==1,
           trydzv=LinearSolve[Transpose[tryrespzv],bpmy];  
           Scan[(trydzv00[#[1]]=#[2])&,Thread[{tryzv,trydzv}]];
         ];
       ];

   
        trydzv=Table[0,{Length[tryzv00]}];
	iizv=0;
        Scan[(iizv=iizv+1;trydzv[iizv]=trydzv00[#])&,tryzv00];

        residuali=Apply[Plus,(bpmy0-Transpose[tryrespzv00].trydzv)^2];
        If[residuali<minresidual, minresidual=residuali; jzv=izv; dzv=trydzv; minrespzv=tryrespzv00];
      )&,remainzv];

      datazvi=Drop[datazvi,{jzv}];
      selectedzv=Append[selectedzv,remainzv[jzv]];
      remainzv=Drop[remainzv,{jzv}];
      respzv=minrespzv;
      residual=minresidual;
      nzv=nzv+1;
 Print["residual=",residual];
     ];
      
     Scan[(LINE["K0",#[1]]=#[2])&,Thread[{selectedzv,dzv}]];
   ];

!!!!!!!!!!!!!!!!!!!


    If[my<=0 && mx>=1, 
     k0listx=Select[k0list,(StringMatchQ[#[1],"ZH*R"])&];

     k0listx=Select[k0listx,Not[MemberQ[fixsteers,#[1]]]&];

     Scan[(maxdzh[#[1]]=#[2]+maxk0)&,k0listx];
     Scan[(mindzh[#[1]]=#[2]-maxk0)&,k0listx];
         
     usezh=Select[Map[#[1]&,k0listx],(maxdzh[#]>0 && mindzh[#]<0)&];

     dk0=1e-4;
     datazheta=Map[(steer=#;
      FFS["CAL"]; 
      ii=0;
      dispx0=Map[(ii=ii+1;If[ii>=nlist1-3,fct=fc1,fct=1]; fct*Twiss["EX",#])&,bpmnamelist1]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      ii=0;
      dispx1=Map[(ii=ii+1;If[ii>=nlist1-3,fct=fc1,fct=1]; fct*Twiss["EX",#])&,bpmnamelist1];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (dispx1-dispx0)/dk0)&,usezh];

     datazhcod=Map[(steer=#;
      FFS["CAL"]; 
      ii=0;
      codx0=Map[(ii=ii+1;If[ii>=nlist2-3,fct=fc2,fct=1]; fct*Twiss["DX",#])&,bpmnamelist2]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"];
      ii=0; 
      codx1=Map[(ii=ii+1;If[ii>=nlist2-3,fct=fc2,fct=1]; fct*Twiss["DX",#])&,bpmnamelist2];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (codx1-codx0)/dk0)&,usezh];

     datazh=Transpose[Join[Transpose[datazheta]*factor1,Transpose[datazhcod]]];

     bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist1]*factor1,Map[(#[2]-#[4])&,bpmfitlist2]];

!!!!!!!!!!


      selectedzh={};
      remainzh=usezh;
      respzh={};
      datazhi=datazh;
      nzh=0;
      residual=1.e30;
     While[nzh<mx && residual>tolerance && nzh<Length[usezh],
      jzh=0;
      izh=0;
      minresidual=1.e30;
      Scan[(izh=izh+1;
        tryzh=Append[selectedzh,#];
        tryrespzh=Append[respzh,datazhi[izh]];
        tryrespzh00=tryrespzh;
        tryzh00=tryzh;
        bpmx0=bpmx;
        iflimit=2;
        trydzh=LinearSolve[Transpose[tryrespzh],bpmx];
        Scan[(trydzh00[#[1]]=#[2])&,Thread[{tryzh00,trydzh}]];

       While[Length[tryzh]>=1 && iflimit=>1,
          tryzh0=tryzh;
          trydzh0=trydzh;
          iflimit=0;
          iizh=Length[tryzh0]+1;
          Scan[(iizh=iizh-1;
              If[#[2]>maxdzh[#[1]], 
                  bpmx=bpmx-maxdzh[#[1]]*tryrespzh[iizh];
                  trydzh00[#[1]]=maxdzh[#[1]];iflimit=1;tryzh=Drop[tryzh,{iizh}];tryrespzh=Drop[tryrespzh,{iizh}];
                  
              ];
              If[#[2]<mindzh[#[1]],
                   bpmx=bpmx-mindzh[#[1]]*tryrespzh[iizh];
                   trydzh00[#[1]]=mindzh[#[1]];iflimit=1;tryzh=Drop[tryzh,{iizh}];tryrespzh=Drop[tryrespzh,{iizh}];
              ];
          )&,Reverse[Thread[{tryzh0,trydzh0}]]];
         If[iflimit==1,
           trydzh=LinearSolve[Transpose[tryrespzh],bpmx];
           Scan[(trydzh00[#[1]]=#[2])&,Thread[{tryzh,trydzh}]];
         ];
       ];

        trydzh=Table[0,{Length[tryzh00]}];
	iizh=0;
        Scan[(iizh=iizh+1;trydzh[iizh]=trydzh00[#])&,tryzh00];

        residuali=Apply[Plus,(bpmx0-Transpose[tryrespzh00].trydzh)^2];
        If[residuali<minresidual, minresidual=residuali; jzh=izh; dzh=trydzh; minrespzh=tryrespzh00];
      )&,remainzh];

      datazhi=Drop[datazhi,{jzh}];
      selectedzh=Append[selectedzh,remainzh[jzh]];
      remainzh=Drop[remainzh,{jzh}];
      respzh=minrespzh;
      residual=minresidual;
      nzh=nzh+1;
 Print[residual];
     ];     

     Scan[(LINE["K0",#[1]]=#[2])&,Thread[{selectedzh,dzh}]];

   ];


    FFS["CAL"];
    DrawDispersion[bpmlist1]; 
    orbitdata=ATFRingOrbit;
    dispdata=ATFRingDispersion;
    injectchange=Twiss[{"DX","DPX","DY","DPY"},"IIN"];  
    Scan[Check[LINE["K0",#[1]]=-reductionfactor*LINE["K0",#[1]]+#[2],]&,k0list];
  ];



