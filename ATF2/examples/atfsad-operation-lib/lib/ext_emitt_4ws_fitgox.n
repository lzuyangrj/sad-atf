
fitgox:=[
!  VariableRange["IEX","AX",v_]:=v>-1000 && v<1000;
!  VariableRange["IEX","BX",v_]:=v>0.001 && v<1000;
  VariableRange["IEX","AX",v_]:=v>minalphx && v<maxalphx;
  VariableRange["IEX","BX",v_]:=v>minbetax && v<maxbetax;
  FitFunction:=kaixi;

  FFS["FIT MW0X BXM 250;"];
  FFS["FIT MW1X BXM 250;"];
  FFS["FIT MW2X BXM 250;"];
  FFS["FIT MW3X BXM 250;"];
  FFS["FIT MW4X BXM 300;"];

  FFS["FREE AXI BXI  ;GO"];
  
  mresx=MatchingResidual;

  ib=0;ia=11;
  While[mresx>50 && ib<=40, 
    If[ia==11,ia=-10; ib=ib+1; , ia=ia+1];
      FFS["AXI "//ia];
      FFS["BXI "//ib*0.5];
      FFS["CAL"];
      FFS[" GO;"];
      mresx=MatchingResidual;
  ];

  Print["BA"];Print[Twiss["BX","IEX"]," ",FFS["VAR"]];
  Print[chi2x];
  emitX=sqrtemix^2;Print[" emitx ",emitX];
  chi2xfitted=chi2x;
  alphxfitted=alphX;
  betaxfitted=betaX;
  alphxiex=Twiss["AX","IEX"];
  betaxiex=Twiss["BX","IEX"];

  demit=Max[emitX*0.9,1e-12];
  emi0=emitX;
  minchi2=0;
  While[Abs[demit]>emitX*1e-2,
   While[minchi2<1+chi2xfitted,
    emi0=emi0+demit;
    minchi2=minchi2xfixe[emi0];
   ];
   emi0=emi0-demit;
   minchi2=minchi2xfixe[emi0];
   demit=demit*0.5;
  ];
   Print[minchi2," ",chi2x];
  erremitXp=emi0-emitX;
  demit=emitX*0.9;
  emi0=emitX;
  minchi2=0;
  While[Abs[demit]>emitX*1e-2,
   While[minchi2<1+chi2xfitted,
    If[emi0<=demit, demit=emi0*0.4];
    emi0=emi0-demit;
    minchi2=minchi2xfixe[emi0];
   ];
   emi0=emi0+demit;
   minchi2=minchi2xfixe[emi0];
   demit=demit*0.5;
  ];
   Print[minchi2," ",chi2x];
  erremitXm=-emi0+emitX;
  erremitX=(erremitXp+erremitXm)/2;


     pxdesignp[x_]:=(-alphX0/betaX0*x+Sqrt[betaX0*emitX-x^2]/betaX0);
     pxdesignm[x_]:=(-alphX0/betaX0*x-Sqrt[betaX0*emitX-x^2]/betaX0);
     xmax0=Sqrt[betaX0*emitX];
    If[xmax0>0,
     xlim10=Table[t,{t,-xmax0*0.99,xmax0*0.99,xmax0*1.e-2}];
     xlim20=Table[t,{t,xmax0*0.99,-xmax0*0.99,-xmax0*1.e-2}];
    ];
     bmagX=Bmag[alphX0,betaX0,alphX,betaX];


    FFS["IEX OFFSET "//(LINE["POSITION",ent]-1)];
    FFS[" axi "//alphxfitted];
    FFS[" bxi "//betaxfitted];
    If[emitX>0,
     EMITX=emitX;
    ];
    FFS["CALC"]; 
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    f1="./output/plotdata_wsemitt_x1";
    frame1={{0.154,0.615},{0.2,0.8}};

     $FORM="7.3";
    text11={"x(m)",0.35,0.1,0};
    text12={"dx/ds",0.05,0.45,90};
    text13={DateString[]//" K.Kubo",0.631,0.89,0};
    text14={"emittanceX "//emitX*1.e9//" +"//erremitXp*1.e9//" -"//erremitXm*1.e9//" (nm)",0.3,0.84,0};
    text15={"alphaX at "//ent//" "//alphxfitted//" ",0.631,0.75,0};
    text16={"betaX  at "//ent//" "//betaxfitted//"(m)",0.631,0.70,0};
    text17=Flatten[Map[(
      {{"alphaX at "//monix[#]//" "//Twiss["AX",monix[#]]//" ",0.631,0.75-#*0.1,0},
      {"betaX  at "//monix[#]//" "//Twiss["BX",monix[#]]//"(m)",0.631,0.70-#*0.1,0}}
    )&,Table[i,{i,1,Length[monix]}]],1];
    text18={"BmagX "//bmagX//" ",0.631,0.65-Length[monix]*0.1,0};
    text1=Join[{text11,text12,text13,text14,text15,text16},text17,{text18}];

    $FORM="";

     errsizex=sigx;
     cx=Map[#[1,1]&,matx];
     sx=Map[#[1,2]&,matx];

   If[emitX<=0 || emitX>0,
     pxp[x_]:=(-alphX/betaX*x+Sqrt[betaX*emitX-x^2]/betaX);
     pxm[x_]:=(-alphX/betaX*x-Sqrt[betaX*emitX-x^2]/betaX);
     xmax=Sqrt[betaX*emitX];
    If[xmax>0,
     xlim1=Table[t,{t,-xmax*0.99,xmax*0.99,xmax*1.e-2}];
     xlim2=Table[t,{t,xmax*0.99,-xmax*0.99,-xmax*1.e-2}];
    ];

     data1={};

     dxyax=0;
    If[xmax>0,
     x11=xlim1;
     y11=Map[(pxp[#])&,xlim1]; 
     x12=xlim2;
     y12=Map[(pxm[#])&,xlim2]; 
     dymax=Max[Abs[y11]];
     range1={{-xmax*2,xmax*2},{-dymax*2,dymax*2}};
     data1=Join[data1,{{1,0,Join[x11,x12],Join[y11,y12]}}];
     ,
     range1={{-1,1},{-1,1}};
    ];
    If[xmax0>0,
     x13=xlim10;
     y13=Map[(pxdesignp[#])&,xlim10]; 
     x14=xlim20;
     y14=Map[(pxdesignm[#])&,xlim20]; 
     data1=Join[data1,{{2,0,Join[x13,x14],Join[y13,y14]}}];
    ];

     nmplotx=8;nmploty=1-nmplotx;

     data15=Flatten[Map[(
          {{1,0,{nmplotx*#[1]/#[2],nmploty*#[1]/#[2]},{nmploty*#[1]/#[3],nmplotx*#[1]/#[3]}},
           {2,0,{nmplotx*(#[1]+#[4])/#[2],nmploty*(#[1]+#[4])/#[2]},{nmploty*(#[1]+#[4])/#[3],nmplotx*(#[1]+#[4])/#[3]}},
           {2,0,{nmplotx*(#[1]-#[4])/#[2],nmploty*(#[1]-#[4])/#[2]},{nmploty*(#[1]-#[4])/#[3],nmplotx*(#[1]-#[4])/#[3]}}}
     )&,Thread[{sizex,cx,sx,errsizex}]],1];

    data1=Join[data1,data15,{{0,1,{0},{0}}}];


   ];

    $FORM="";
   DrawGraph[{{data1,range1,frame1,text1}},f1];

     data2={};     

   If[emitX>0,
    f2="./output/plotdata_wsemitt_x2";
    frame2={{0.154,0.615},{0.2,0.8}};
    range2={{-2,2},{-2,2}};

     xnorm[x_,y_]:=(x/Sqrt[emitX*betaxfitted]);
     ynorm[x_,y_]:=(x*alphxfitted/Sqrt[emitX*betaxfitted]+y*Sqrt[betaxfitted]/Sqrt[emitX]);
     xnmax=Sqrt[betaX*emitX];
     xnlim1=Table[t,{t,-xnmax*0.99,xnmax*0.99,xnmax*1.e-2}];
     xnlim2=Table[t,{t,xnmax*0.99,-xnmax*0.99,-xnmax*1.e-2}];

     xnmax0=2*xnmax;
     xnlim10=Table[t,{t,-xnmax0*0.99,xnmax0*0.99, xnmax0*1.e-2}];
     xnlim20=Table[t,{t,xnmax0*0.99,-xnmax0*0.99,-xnmax0*1.e-2}];

    $FORM="7.4";

     x21=Map[(xnorm[#,pxp[#]])&,xnlim1];
     y21=Map[(ynorm[#,pxp[#]])&,xnlim1];
     x22=Map[(xnorm[#,pxm[#]])&,xnlim2];
     y22=Map[(ynorm[#,pxm[#]])&,xnlim2];
     data2=Join[data2,{{1,0,Join[x21,x22],Join[y21,y22]}}];
    
     x23=Map[(xx=ynorm[#,pxdesignp[#]];
         If[Im[xx]==0, xnorm[#,pxdesignp[#]],-1e10])&,xnlim10];
     y23=Map[(xx=ynorm[#,pxdesignp[#]];
         If[Im[xx]==0, xx,-1e10])&,xnlim10];
     x24=Map[(xx=ynorm[#,pxdesignm[#]];
         If[Im[xx]==0, xnorm[#,pxdesignm[#]],-1e10])&,xnlim20];
     y24=Map[(xx=ynorm[#,pxdesignm[#]];
         If[Im[xx]==0, xx,-1e10])&,xnlim20];
     x234=Select[Join[x23,x24],(#>-9e9)&];
     y234=Select[Join[y23,y24],(#>-9e9)&];
     data2=Join[data2,{{2,0,x234,y234}}];

     data22=Flatten[Map[(
           x10=xnorm[#[1]/#[2],0];y10=ynorm[#[1]/#[2],0];
           x20=xnorm[0,#[1]/#[3]];y20=ynorm[0,#[1]/#[3]];
           x1a=xnorm[(#[1]+#[4])/#[2],0];
           y1a=ynorm[(#[1]+#[4])/#[2],0];
           x2a=xnorm[0,(#[1]+#[4])/#[3]];
           y2a=ynorm[0,(#[1]+#[4])/#[3]];
           x1b=xnorm[(#[1]-#[4])/#[2],0];
           y1b=ynorm[(#[1]-#[4])/#[2],0];
           x2b=xnorm[0,(#[1]-#[4])/#[3]];
           y2b=ynorm[0,(#[1]-#[4])/#[3]];

           x00=(y10*x20-y20*x10)/(y10-y20);
           y00=(x10*y20-x20*y10)/(x10-x20);
           x0a=(y1a*x2a-y2a*x1a)/(y1a-y2a);
           y0a=(x1a*y2a-x2a*y1a)/(x1a-x2a);
           x0b=(y1b*x2b-y2b*x1b)/(y1b-y2b);
           y0b=(x1b*y2b-x2b*y1b)/(x1b-x2b);

           nmplotx=8;nmploty=1-nmplotx;

           {{1,0,{nmploty*x00,nmplotx*x00},{nmplotx*y00,nmploty*y00}},
            {2,0,{nmploty*x0a,nmplotx*x0a},{nmplotx*y0a,nmploty*y0a}},
            {2,0,{nmploty*x0b,nmplotx*x0b},{nmplotx*y0b,nmploty*y0b}}}
    )&,Thread[{sizex,cx,sx,errsizex}]],1];


    data2=Join[data2,data22,{{0,1,{0},{0}}}];

   
    $FORM="";
   DrawGraph[{{data2,range2,frame2,{{" ",.1,.1,0}}}},f2];

   ];

   
    f3="./output/plotdata_wsemitt_x3";
    frame3={{0.154,0.846},{0.2,0.8}};

    $FORM="F11.5";
    text3={{"chi2="//chi2x,0.154,0.85,0},{DateString[],0.538,0.85,0},{"s(m)",0.45,0.1,0},{"sizeX(m)",0.05,0.45,90}};

    If[LINE["Position",endplot],
     allelement=Map[LINE["Name",#]&,Table[i,{i,LINE["Position",ent],LINE["Position",endplot]}]],
     allelement=Map[LINE["Name",#]&,Table[i,{i,LINE["Position",ent],LINE["Position",$$$]}]];
    ];
     x31=Map[LINE["S",#]&,allelement];
     y31=Map[Sqrt[EMITX*Twiss["BX",#]]&,allelement];
     
     x32=Map[(LINE["S",monix[#]])&,Table[i,{i,1,Length[monix]}]];
     y32=Map[(sizex[#])&,Table[i,{i,1,Length[monix]}]];
     dy32=Map[(sigx[#])&,Table[i,{i,1,Length[monix]}]];

     data3={{1,0,x31,y31},{0,1,x32,y32,dy32}};

     range3={{Min[x31],Max[x31]},{0,Max[Join[y31,y32]]*1.2}};

    $FORM="";
   DrawGraph[{{data3,range3,frame3,text3}},f3];
  
 FFS[" FIX *;"];
];

