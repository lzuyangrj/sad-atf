 Drhdispbyq:=Module[{
    bpmlist1=ATFBPM/.ATFData[1]/.ATFBPM->{},
    bpmlist2=ATFBPM/.ATFData[2]/.ATFBPM->{}
 },
   
    momentumcomp=MomentumCompaction/.(EtaCorrect/.Option)/.(MomentumCompaction ->2e-3);
    de=-fchange/714e6/momentumcomp;
    fchange=FrequencyChange/.(EtaCorrect/.Option)/.(FrequencyChange ->5e3);

 quads={"QM1R","QM3R","QM4R","QM20R","QM21R","QM22R","QM23R"};

 kafactors["QM1R.1"]=1/0.00868;
 kafactors["QM1R.2"]=1/0.00868;
 kafactors["QM3R.1"]=-1/0.003569;
 kafactors["QM3R.2"]=-1/0.003569;
 kafactors["QM4R.1"]=1/0.0084708;
 kafactors["QM4R.2"]=1/0.0084708;
 kafactors["QM20R.1"]=1/0.0076041;
 kafactors["QM20R.2"]=1/0.0076041;
 kafactors["QM21R.1"]=-1/0.0034259;
 kafactors["QM21R.2"]=-1/0.0034259;
 kafactors["QM22R.1"]=-1/0.0035175;
 kafactors["QM22R.2"]=-1/0.0035175;
 kafactors["QM23R.1"]=0.28/0.0024;
 kafactors["QM23R.2"]=0.28/0.0024;

! w=Window[];

 frame2=Frame[w,Side -> "top", PadX ->20, PadY ->20];
 chquad=TextMessage[frame2, Side -> "top", Text -> "Choose Quads", Width -> 150];
 sb=ScrollBar[frame2,Side -> "right" ,Orient -> "vertical", Side->"right", Fill->"y"];

 ab=ListBox[frame2, Side -> "right", YscrollCommand :>sb[Set], Insert -> {"end",quads},Side -> "right", SelectMode -> "multiple"];

 
  TkWait[];
 selectedq=ab[GetText[Selection]];
  Print[selectedq];

    bpmlist1=Select[bpmlist1,MemberQ[goodbpmlist,#[1]]&];
    bpmlist1=Map[{#[[1]],#[[2]]/de,#[[3]]/de,#[[4]]}&,bpmlist1];
!    bpmlist11=bpmlist1;
    bpmlist11=Select[bpmlist1,
         ((LINE["Position",#[[1]]]>=LINE["Position","M.19"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.80"]))&];

    FFS["INDEP QM*R.*"];
    selectedq=Flatten[Map[({#//".1",#//".2"})&,selectedq]];


    k1q0=LINE["K1",selectedq];
    FFS["CALC"];

    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    
    x1=Map[(LINE["S",#])&,iplot];
    y11a=Map[(Twiss["EX",#])&,iplot];
    y21a=Map[(Twiss["EY",#])&,iplot];


    bpmfitlist11=Map[({#[[1]],-#[[2]]+Twiss["EX",#[1]]})&,bpmlist11];

!    bpmfitlist11=Map[(
!         If[((LINE["Position",#[[1]]]>=LINE["Position","M.19"] && 
!           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
!          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
!           LINE["Position",#[[1]]]<=LINE["Position","M.80"])),
!!             {#[[1]],-#[[2]]+Twiss["EX",#[1]]},
!             {#[[1]],-#[[2]]},
!             {#[[1]],-#[[2]]+Twiss["EX",#[1]]}]
!     )&,bpmlist11];

!    Print[bpmfitlist11];

    tunex0=Twiss["NX",$$$]/2/Pi;
    tuney0=Twiss["NY",$$$]/2/Pi;
    
!    FFS[" FIT NX "//tunex0//";"];
!    FFS[" FIT NY "//tuney0//";"];

!    Scan[(FFS[" FIT "//#[1]//" EX "//#[2]//";"])&,bpmfitlist11];

    FitFunction:=Map[(Twiss["EX",#[1]]-#[2])&,bpmfitlist11];
!    FitFunction:=Map[(
!          If[MemberQ[extbpm,#[1]],(Twiss["EX",#[1]]-#[2])*extweight,(Twiss["EX",#[1]]-#[2])]
!    )&,bpmfitlist11];

!!    FFS[" Q* K1 MINMAX 3."];
    Scan[(FFS[" "//#[2]//" K1 MIN "//ToString[#[1]-0.05]//";"])&,Thread[{k1q0,selectedq}]];
    Scan[(FFS[" "//#[2]//" K1 MAX "//ToString[#[1]+0.05]//";"])&,Thread[{k1q0,selectedq}]];


    Scan[(FFS[" FREE "//#//";"])&,selectedq];
    FFS[" GO;GO;GO;"];
    FFS[" GO;GO;GO;"];

    Print[" Matching residual= ",MatchingResidual];

    k1q=LINE["K1",selectedq];
    dk1q=k1q-k1q0;
!!    dk1q=-k1q+k1q0;

    dampq=Map[(kafactors[#[1]]*#[2])&,Thread[{selectedq,dk1q}]];
 

    $FORM="F6.4";

!    fitresult=Map[(" "//#[1]//" original K1  "//#[2]//"    Change K1 by  "//#[3]//" ")&,Thread[{selectedq,k1q0,dk1q}]];

    fitresult=Map[(" "//#[1]//" original K1  "//#[2]//"    Change K1 by  "//#[3]//" Amp "//#[4]//" ")&,Thread[{selectedq,k1q0,dk1q,dampq}]];


!    outresult=TextMessage[frame2, Side -> "left", TextVariable -> fitresult, Width -> 340];
    outresult=TextMessage[frame2, Side -> "left", TextVariable -> fitresult, Width -> 400];
!    outresult=TextMessage[ TextVariable -> fitresult, Width -> 330];
!!!    outresult=Map[(TextMessage[TextVariable -> "  aa "])&,fitresult];

 
 DeleteWidget[chquad];
 DeleteWidget[bbbbb];
 bbbbb=Button[frame1, Side -> "top", Text -> "Plot Result", Command :> TkReturn["ReturnToSAD"]]; 

  TkWait[];

!!    Scan[(LINE["K1",#[1]]=#[2]*2-#[3])&,Thread[{selectedq,k1q0,k1q}]];
!!    Scan[(LINE["K1",#[1]]=#[2])&,Thread[{selectedq,k1q0,k1q}]];
    FFS["CAL;"];  

    y11b=Map[(Twiss["EX",#])&,iplot];
    y21b=Map[(Twiss["EY",#])&,iplot];

!!    y11=y11b-y11a;
!!    y21=y21b-y21a;
    y11=-y11b+y11a;
    y21=-y21b+y21a;

    Get["./lib/drawgraph_canvas.n"];
    Get["./lib/drawdrex.n"];
    DrawDispersion2[bpmlist1]; 

 ];
