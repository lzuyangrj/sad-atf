! Title Get beam size at the ODR/CDR target
! Notes:
! 1) modify the name of magnets setting file (marked by #1)
! 2) get beta function at the OTR0X by means of mOTR and set at place marked by #2 (check logbook)
! 3) Set vertical dispersion at the MQM14FF (marked by #3)
!
! 4) set QM15 and QM14 currents  (mark by #4)
! * vertical beam size is approximated by Sqrt[emity*bety + (PEY * sigp)^2]

 ON ECHO; OFF CTIME;

 read "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)
 
 FFS USE=ATF2;

 SeedRandom[17];
 
 FFS["NPARA = 8"];
 path0 = "/Users/rj/Fellow-CERN/SAD/ATF2/lattice/";
!Get[path0//"tracking.n"];
 Get[path0//"multipole.n"];
 Get[path0//"atf2lib.n"];
 Get[path0//"atf2lib_ultralow.n"];
 
 TRPT;
 INS;
 FFS["cal"];

 (* parametres *) 
 KtIQM14 =  0.0219423/2; ! Current [A] to K1 of QM14FF
 KtIQM15 = 0.02198408/2;
 sigp = 7e-4; ! rms energy spread
 dr:=(FFS["draw IEX IP bx by & pex pey q*"];);

 (* load optics and magnets settings *)
 ! #1 
 LoadATF2Optics["ATF2_20181116_pencil"];
 FFS["cal"];

 Z* K0 0;
 QS* K1 0;
 QK* K1 0;
 SK* K2 0;
 FFS["cal"];

 (* matching initial horizonal dispersion *)
 FREE EXI EPXI;
 FIT OTR0X EX 0;
 FIT OTR1X EX 0;
 Do[FFS["go"],{II,100}]; 
 FIT REJECT TOTAL;
 FIX EXI EPXI;
 
 FFS["cal"];

 (* matching initial beta-function wrt. OTR0X measurements *)
 ! #2 
 FREE BXI BYI AXI AYI;
 FIT OTR0X BX 15.7438;  ! betax
 FIT OTR0X AX  -8.5831; ! alpha x
 FIT OTR0X BY 8.6011;   ! beta y
 FIT OTR0X AY 3.8286;   ! alpha y
 Do[FFS["go"],{II,100}];
 FIT REJECT TOTAL;
 FIX BXI BYI AXI AYI;
 FFS["cal"];  

 (* matching vertical dispersion at the MQM14FF *)
 !#3
 FFS["FREE QS1X QS2X"];
 FFS["FIT MQM14FF EY 5e-3"];
 Do[FFS["go"],{II,100}];go;
 FFS["cal"];
 FIT REJECT TOTAL;
 FIX QS1X QS2X;


 (* Set QM15/QM14 current , unit A*)
 !#4
 QM14Current = 50;
 QM15Current = 50;
 FFS["QM15FF K1 "//ToString[KtIQM15*QM15Current]];
 FFS["QM14FF K1 "//ToString[KtIQM14*QM14Current]];
 FFS["CAL"];
 par0 = Twiss[{"BY", "EY", "SIGY"}, "LW1FF"];
 {sigy0, sigy2} = {Sqrt[#1*EMITY + (#2*sigp)^2], #3}&@@par0;
 $FORM = "S10.3";
 Print["\n >> Ver. size at LW1FF: ", sigy0, " (m)\n"];
 
end;
abort;

