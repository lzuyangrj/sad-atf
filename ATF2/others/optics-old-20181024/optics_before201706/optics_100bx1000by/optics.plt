# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top box #set position of the titles of points of lines

set output "optic_100bx1000by.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol b} [m]"

# set grid off
set xrange[63:94]
# set format x "%.0e"
# set yrange[0:35]

plot  'TwissParameter.dat' u 2:(sqrt($4)) pt -1 ps 1.5 lc rgb 'black' lw 3 w lp t " {/Symbol b}_{/Italic x}" ,\
     'TwissParameter.dat' u 2:(sqrt($7)) pt -1 ps 1.5 lc rgb 'cyan' lw 3 w lp t " {/Symbol b}_{/Italic y}" ,\

#########################################
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file

set key font ",22"
set key spacing 1.5
set key left top box #set position of the titles of points of lines

set output "Dispersion_100bx1000by.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol h} [m]"

# set grid off
set xrange[63:94]
# set format x "%.0e"
# set yrange[0:35]

plot  'TwissParameter.dat' u 2:10 pt -1 ps 1.5 lc rgb 'green' lw 3 w lp t " {/Symbol h}_{/Italic x}" ,\
     'TwissParameter.dat' u 2:12 pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t " {/Symbol h}_{/Italic y}" ,\

