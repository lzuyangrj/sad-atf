printCOD[]:=
    Module[{},
           !----------------------------------------------------------------------------------
           ! COD print optics
           !----------------------------------------------------------------------------------
           opticsID="ATF_apdr";

           !NORAD;
           !COD;
           !NORADCOD;
           !RFSW;  
           !EMIOUT;

           ! One-turn matrix
           !e = Emittance[Matrix->True,OneTurnInformation->True]; 
           !Print[TransferMatrices[[-1]]/.e];
           !Print[TableForm[OneTurnTransferMatrix/.e]];

           ! Print COD data to file
           Nelement=LINE["POSITION","$$$"];
           Print[Nelement];
           !FSHIFT=-3.0917608738685442e-06 - 1.12428703e-10
           !Print[FSHIFT];
           !Print[DP0];
           Print[PHICAV];
           !  FFS["CALC; EMIT; EMIOUT;"];
           !  FFS["COD;CODPLOT;"];

           filename="COD_"//opticsID//".dat";
           fn=OpenWrite[filename];
           Do[
               WriteString[fn,i,"  ",LINE["NAME",i],"  ",LINE["TYPENAME",i],"  ",LINE["S",i],"  ",
                           Twiss["DX",i],"  ",Twiss["DPX",i],"  ",Twiss["DY",i],"  ",
                           Twiss["DPY",i],"  ",Twiss["DZ",i],"  ",Twiss["DDP",i],"\n"];
               ,{i,Nelement}];
           Close[fn];
           
           
           
           filename="Optics_Parameters_"//opticsID//".dat";
           fn=OpenWrite[filename];
           Do[
               WriteString[fn,i,"   ",LINE["NAME",i],
                           "   ",LINE["TYPENAME",i],"  ", LINE["S",i],
                           "   ",Twiss["BX",i],"   ",Twiss["AX",i],"   ",Twiss["NX",i]/2/Pi,
                           "   ",Twiss["EX",i],"   ",Twiss["EPX",i],
                           "   ",Twiss["BY",i],"   ",Twiss["AY",i],"   ",Twiss["NY",i]/2/Pi,
                           "   ",Twiss["EY",i],"   ",Twiss["EPY",i],
                           "   ",Twiss["R1",i],"   ",Twiss["R2",i],
                           "   ",Twiss["R3",i],"   ",Twiss["R4",i],
                           "\n"];
               ,{i,Nelement}];
           Close[fn];

           filename="Optics_Parameters_"//opticsID//"_2.dat";
           fn=OpenWrite[filename];
           Do[
               WriteString[fn,i,"  ",LINE["NAME",i],
                           "  ",LINE["TYPENAME",i],"  ", LINE["S",i],
                           "  ", LINE["L",i],"  ", LINE["ANGLE",i],
                           "  ", LINE["K0",i],"  ", LINE["K1",i],"  ", LINE["K2",i],"  ", LINE["K3",i],
                           "\n"];
               ,{i,Nelement}];
           Close[fn];

           filename="Optics_Parameters_"//opticsID//"_3.dat";
           fn=OpenWrite[filename];
           Do[
               WriteString[fn,i,"  ",LINE["NAME",i],
                           "  ",LINE["TYPENAME",i],"  ", LINE["S",i],
                           "  ", LINE["L",i],
                           "  ", LINE["R1",i],"  ", LINE["R2",i],"  ", LINE["R3",i],"  ", LINE["R4",i],
                           "\n"];
               ,{i,Nelement}];
           Close[fn];
          ]