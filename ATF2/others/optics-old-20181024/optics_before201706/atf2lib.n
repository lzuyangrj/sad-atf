! lib for ATF2 beam halo tracking (R. Yang 2017-01-02)
! 

!==============================================================
! set start time
!==============================================================
T0[]:=Module[{},
    time0=Date[];
    Print[time0];
];

!==============================================================
! print how much time used
!==============================================================

T1[]:=Module[{},
    time1=Date[];
    timeused=(((time1[[3]]*24+time1[[4]])*60+time1[[5]])*60+time1[[6]])-(((time0[[3]]*24+time0[[4]])*60+time0[[5]])*60+time0[[6]]);
    Print[timeused," ","seconds is used"];
];

!==============================================================
! modify 2016-12-19
! Reset seed for random functions
! according to machine time
!==============================================================
 ResetSeed[]:=
   Module[{time0,time1,time2},
          time0 = FromDate[];
          time1 = ToString[time0];
          time2 = time1[-3,-1];
          SeedRandom[ToExpression[time2]]
          ]
!==============================================================
! random Gaussian
!==============================================================
MakeRandomGauss[NumberOfRandomNumber_,sizeG_]:=Module[{ddd},
  ddd = Table[0,{NumberOfRandomNumber}];
  grandom = Map[(sizeG*GaussRandom[])&,ddd]
];

!==============================================================
! random an integer between min. and max.
!==============================================================
RandomInteger[min_,max_]:=
  (* random an integer between min. and max.*)
  Module[{num,out},
         num = (max-min)*Random[]+min;
         out=If[num<Floor[num]+0.5,
                Floor[num],
                Ceiling[num]]
         ]

!==============================================================
! write aperture to apt.dat (2016-09-06)
!==============================================================

WriteApert[]:=
(* can apply to any lattice*)
  Module[{libxx},
         libxx = Transpose[LINE[{"S","AX","AY"},"*"]];
         fr = OpenWrite["./apt.dat"];
         Do[
            Write[fr, i," ",libxx[[i,1]]," ",libxx[[i,2]]," ",libxx[[i,3]]];
            ,{i,1,Length[libxx]}];
         Close[fr];
  ]

!==============================================================
! build the library for the all the elements and Twiss parameters
! at the entrance of each element
! 2012-12-14 in deltai=1 case, also append to listEles/posEles
! 2016-12-19 modify deltai=1 case again ( fine for IHEP servers) 
! 2016-12-20 modify Wholeelespos = LINE["S","WholeEle"] --> WES = LINE["S","*"]
! to avoid the "null" term occur in WEs
! 2016-12-21 apply the local dispersion( PEX, PEPX, PEY, PEPY )
!==============================================================
EleLibrary[]:=
  (* way to utilize: {totalLength,matrixTwiss}=ELeLibrary[];*)
  Module[{wholeEles,wholeElesPos,totalElesNum,listEles,deltai,i},
         wholeEles=LINE["NAME","*"];
         wholeElesPos=LINE["S","*"];
         totalElesNum=Length[wholeEles];
         totalLength=LINE["S",wholeEles[-1]];

         i=1;
         listEles = {};
         posEles = {};
         numEles = {};
         While[i<totalElesNum,
               deltai = Count[wholeElesPos,wholeElesPos[i]];
               listEles = AppendTo[listEles,wholeEles[i]];
               posEles = AppendTo[posEles,wholeElesPos[i]];
               numEles = AppendTo[numEles,i];
               i=i+deltai;
               ];
	       If[i<totalElesNum,Print["   Errors: EleLibrary[]"]];
         matrixTwiss=Transpose@{Twiss["BX",listEles],Twiss["AX",listEles],Twiss["BY",listEles],Twiss["AY",listEles],Twiss["EX",listEles],Twiss["EPX",listEles],Twiss["EY",listEles],Twiss["EPY",listEles], Twiss["DX",listEles], Twiss["DPX",listEles], Twiss["DY",listEles], Twiss["DPY",listEles]};
         If[Length[Position[matrixTwiss,{}]]>0, Print["   Errors: nulls in Twiss Matrix."]];
         {posEles,totalLength,matrixTwiss,wholeEles,listEles,numEles}
    ]

!==============================================================
! 2016-12-16
! export distribuition to "TwissParameter.dat" with MMA format
! BX, AX, NX, BY, AY, NY, EX, EPX, EY, EPY, DX ,DPX, DY, DPY
!==============================================================

  ExportTwiss[]:=
  (*to export initial distribution as the format which MMA could import easily
    Input: distribution to export*)
    Module[{wholeEles,wholeElePos,totalElesNum,totalLength,listEles,posEles,numEles,Input},
         wholeEles=LINE["NAME","*"];
         wholeElesPos=LINE["S","*"];
         totalElesNum=Length[wholeEles];
         totalLength=LINE["S",wholeEles[-1]];

         i=1;
         listEles = {};
         posEles = {};
         While[i<totalElesNum,
               deltai = Count[wholeElesPos,wholeElesPos[i]];
               listEles = AppendTo[listEles,wholeEles[i]];
               posEles = AppendTo[posEles,wholeElesPos[i]];
               i=i+deltai;
               ];
	       If[i<totalElesNum,Print["   Errors: EleLibrary[]"]];
         matrixTwiss=Transpose@{Twiss["BX",listEles],Twiss["AX",listEles],Twiss["NX",listEles],Twiss["BY",listEles],Twiss["AY",listEles],Twiss["NY",listEles],Twiss["EX",listEles],Twiss["EPX",listEles],Twiss["EY",listEles],Twiss["EPY",listEles], Twiss["DX",listEles], Twiss["DPX",listEles], Twiss["DY",listEles], Twiss["DPY",listEles]};
         If[Length[Position[matrixTwiss,{}]]>0, Print["   Errors: nulls in Twiss Matrix."]];
	 Input=matrixTwiss;
           ! write optical functions
           $FORM="S10.4";
           fw = OpenWrite["TwissParameter.dat"];
           Do[
               Write[fw,posEles[i]," ",Input[i,1]," ",Input[i,2]," ",Input[i,3]," ",Input[i,4]," ",Input[i,5]," ",Input[i,6]," ",Input[i,7]," ",Input[i,8]," ",Input[i,9]," ",Input[i,10]," ",Input[i,11]," ",Input[i,12]," ",Input[i,13]," ",Input[i,14], "\n"]      
               ,{i,1,Length[Input]}];
           Close[fw];
          ]

!==============================================================
! export .dat for the longitudinal positions of quadrupoles
!==============================================================
ExportNamePosQuad[]:=
  (* way to utilize: {totalLength,matrixTwiss}=ELeLibrary[];*)
  Module[{wholeEles,wholeElesPos,totalElesNum,posa,posb,pos2,namea,nameb,name2},
         wholeEles=LINE["NAME","*"];
         wholeElesPos=LINE["S",wholeEles];
         totalElesNum=Length[wholeEles];
         totalLength=LINE["S",wholeEles[-1]];

         posa=LINE["S","QF*"];
         posb=LINE["S","QM*"];
         pos2 = Join[posa,posb];

         namea = LINE["NAME","QF*"];
         nameb = LINE["NAME","QM*"];
         name2 = Join[namea,nameb];

         fr = OpenWrite["./name_pos.dat"];
         Do[
            Write[fr,pos2[[i]], " ",name2[[i]]]
            ,{i,1,Length[pos2]}];
         Close[fr]
    ]

!==============================================================
! generate/input beam distribution 
!==============================================================

ImportDistribution[breakFlag_,name_]:=
  (*breakFlag=/0 means tracking based on the previous results; breakFlag=0 means the first turn;
    name: dis_core.txt or dis_halo.txt *)
  Module[{xDist,xDist0,pxDist,yDist,pyDist,zDist,eDist,flagDist,beamDis},
           If[breakFlag==0,
              beamDis = {}
              ,
              fr = OpenRead[name];
              xDist = {};
              pxDist = {};
              yDist = {};
              pyDist = {};
              zDist = {};
              eDist = {};
              flagDist = {};
              Do[
                 xDist0 = Read[fr, Real]; (*decide the end of file*)
                If[ToString[xDist0]=="EndOfFile",Break[]];
                xDist = AppendTo[xDist,xDist0];
                pxDist = AppendTo[pxDist, Read[fr, Real]];
                yDist = AppendTo[yDist, Read[fr, Real]];
                pyDist = AppendTo[pyDist, Read[fr, Real]];
                zDist = AppendTo[zDist, Read[fr, Real]];
                eDist = AppendTo[eDist, Read[fr, Real]];
                flagDist = AppendTo[flagDist, Read[fr, Real]]
                ,{j, 1, 1000000}
                ];
              Close[fr];
              beamDis = {1,{xDist,pxDist,yDist,pyDist,zDist,eDist,flagDist}},
              ];
         beamDis
    ]

!==============================================================
! 1D Gauss fit to get beam center and beam size 
! also concerning beam loss
!==============================================================

fitBeamSizeGauss[dis1D_,nbins_]:=
  (*dis1D is the 1D particles distribution; nbins is the num of bins in -10~10 sigma region; some part refers to BEPC user-defined function*)
  Module[{xmean,xrms2,xsigma,hh,ll,binL,xlist,hist,xlist1,histdata,fitX,fitmu0,fitmu1,fitmu2,fitsigma0,fitsigma1,fitsigma2},
         ! mean and rms size
         xmean = Plus@@dis1D/Length[dis1D];
         xrms2 = Plus@@((dis1D-xmean)^2)/Length[dis1D];
         xsigma = Sqrt@xrms2;
         hh = xrms2+10*xsigma;
         ll = xrms2-10*xsigma;
         binL = (hh-ll)/nbins;
         xlist = Join[{0},Range[nbins]];
         xlist = xlist*binL+ll;
         hist = Table[0,{nbins}];
         Scan[(Do[If[xlist[[ii-1]]<=# && xlist[[ii]]>#, hist[[ii-1]] = hist[[ii-1]]+1;Break[]],{ii,2,Length[xlist]}])&,dis1D];
         facNorm = 1/Plus@@hist; ! nomalize to 1
         histF = hist*facNorm;
         xlist1 = Drop[xlist,1];
         histdata = Thread[{xlist1,histF}];
                     
         fitX = Fit[histdata,n*Exp[-(x-u)^2/(2*v^2)],x,{u,xmean},{v,xsigma},{n,1}];
         fitmu0 = ToString[fitX[1]];
         fitmu1 = StringReplace[fitmu0,{"u->"->"","("->"",")"->""}];
         fitmu2 = ToExpression[fitmu1];

         fitsigma0 = ToString[fitX[2]];
         fitsigma1 = StringReplace[fitsigma0,{"v->"->"","("->"",")"->""}];
         fitsigma2 = Abs[ToExpression[fitsigma1]]; !Abs because of fitting with ^2
         {fitmu2,fitsigma2}
]

!==============================================================
! advanced final distribution output at j-th turn
! 2017-01-03 name -> core_ip..
!==============================================================

  WriteDistribution[beamFinal_,name_]:=
  Module[{Nparticles},
         Nparticles=Length[beamFinal[[2,7]]];
         fnwrite3=OpenWrite[StringJoin["dis_",name,".dat"]];
         Do[
            WriteString[fnwrite3,beamFinal[[2,1,i]],"  ",beamFinal[[2,2,i]],"  ",beamFinal[[2,3,i]],"  ",beamFinal[[2,4,i]],"  ",beamFinal[[2,5,i]],"  ",beamFinal[[2,6,i]],"  ",beamFinal[[2,7,i]],"\n"]
            ,{i,1,Nparticles}
            ];
         Close[fnwrite3];
  ]


!==============================================================
! 2017-01-16
! input the output of DR to ATF2 with matching Beta functions
! twiss0 of IEX in DR lattice (ax, bx, ay, by)
! filename: stringform, name of initial distribution
! breakFlag: =0 dis={}; =/0 import distribution
!==============================================================
InputParticlesDRToATF2[twiss0_, fileame_, breakFlag_]:=
  (*breakFlag=/0 means tracking based on the previous results; breakFlag=0 means the first turn
  twiss0 = {1.023391886641696,6.561546765606948,-1.8765365370949252,3.194485268724348}*)
  Module[{xDist,xDist0,pxDist,yDist,pyDist,zDist,eDist,flagDist,beamDis,xDist2,pxDist2,yDist2,pyDist2,twiss2},
           If[breakFlag==0,
              beamDis = {},
              fr = OpenRead[filename];
              xDist = {};
              pxDist = {};
              yDist = {};
              pyDist = {};
              zDist = {};
              eDist = {};
              flagDist = {};
              Do[
                 xDist0 = Read[fr, Real]; (*decide the end of file*)
                If[ToString[xDist0]=="EndOfFile",Break[]];
                xDist = AppendTo[xDist,xDist0];
                pxDist = AppendTo[pxDist, Read[fr, Real]];
                yDist = AppendTo[yDist, Read[fr, Real]];
                pyDist = AppendTo[pyDist, Read[fr, Real]];
                zDist = AppendTo[zDist, Read[fr, Real]];
                eDist = AppendTo[eDist, Read[fr, Real]];
                flagDist = AppendTo[flagDist, Read[fr, Real]]
                ,{j, 1, 1e6}
                ];
              Close[fr];
              
              ! c-s parameters @ start for ATF2 lattice               
              twiss2 = Twiss[{"AX","BX","EX","EPX","AY","BY","EY","EPY"},"^^^"]; 

              xDist2 = xDist*Sqrt[twiss2[[2]]*twiss0[[2]]] + twiss2[[3]]*eDist;
              pxDist2 = ((twiss0[[1]]-twiss2[[1]])/Sqrt[twiss2[[2]]*twiss0[[2]]]*xDist + Sqrt[twiss0[[2]]/twiss2[[2]]]*pxDist) + twiss2[[4]]*eDist;
              yDist2 = yDist*Sqrt[twiss2[[6]]*twiss0[[4]]] + twiss2[[7]]*eDist;
              pyDist2 = ((twiss0[[3]]-twiss2[[5]])/Sqrt[twiss2[[6]]*twiss0[[4]]]*yDist + Sqrt[twiss0[[4]]/twiss2[[6]]]*pyDist) + twiss2[[8]]*eDist;
              beamDis = {1,{xDist2,pxDist2,yDist2,pyDist2,zDist,eDist,flagDist}},
              ];
         beamDis
    ]
