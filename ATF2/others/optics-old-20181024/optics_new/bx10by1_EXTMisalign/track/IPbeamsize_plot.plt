# plot IP Twiss parameters for different EXT orbit offset

reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set key left top  #set position of the titles of points of lines

set output "IP_sigmay.eps"
set xlabel "{/Symbol d}_x={/Symbol d}_y for Q/D in EXT [{/Symbol m}m]"
set ylabel "{/Symbol s}_y [nm]"

# set grid off
set yrange[37.5:37.8]
set ytics 0.1
# set format x "%.0e"
# set format y "10^{%T}"
# set logscale y

plot  'IPbeamsize_EXTOrbit.dat' u 1:2 pt 7 ps 1 lc rgb 'red' lw 3 w lp t "wo. correction" ,\
      'IPbeamsize_EXTOrbit.dat' u 1:4 pt 5 ps 1 lc rgb 'blue' lw 3 w lp t "w. correction" ,\

####
reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set key left top  #set position of the titles of points of lines

set output "IP_sigmax.eps"
set xlabel "{/Symbol d}_x={/Symbol d}_y for Q/D in EXT [{/Symbol m}m]"
set ylabel "{/Symbol s}_x [{/Symbol m}m]"

# set grid off
set yrange[6.93:6.95]
set ytics 0.01
# set format x "%.0e"
# set format y "10^{%T}"
# set logscale y

plot  'IPbeamsize_EXTOrbit.dat' u 1:3 pt 7 ps 1 lc rgb 'red' lw 3 w lp t "wo. correction" ,\
      'IPbeamsize_EXTOrbit.dat' u 1:5 pt 5 ps 1 lc rgb 'blue' lw 3 w lp t "w. correction" ,\
