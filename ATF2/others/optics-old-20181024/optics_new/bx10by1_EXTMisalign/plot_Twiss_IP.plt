# plot IP Twiss parameters for different EXT orbit offset

reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set key right bottom  #set position of the titles of points of lines

set output "TwissIP_EX.eps"
set xlabel "{/Symbol d}_x={/Symbol d}_y for Q/D in EXT [{/Symbol m}m]"
set ylabel "{/Symbol h}_x [{/Symbol m}m]"

# set grid off
# set yrange[0:35]
# set format x "%.0e"
set format y "10^{%T}"
set logscale y

plot  'twissIP_lib_before.dat' u ($1*1e6):(abs($4)*1e6) pt 7 ps 1 lc rgb 'red' lw 3 w lp t "wo. correction" ,\
      'twissIP_lib_after.dat' u ($1*1e6):(abs($4)*1e6) pt 5 ps 1 lc rgb 'blue' lw 3 w lp t "w. correction" ,\

####
reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set key right bottom  #set position of the titles of points of lines

set output "TwissIP_AX.eps"
set xlabel "{/Symbol d}_x={/Symbol d}_y for Q/D in EXT [{/Symbol m}m]"
set ylabel "{/Symbol a}_x " # [arb. unit]"

# set grid off
# set yrange[0:35]
# set format x "%.0e"
set format y "10^{%T}"
set logscale y

plot  'twissIP_lib_before.dat' u ($1*1e6):(abs($3)) pt 7 ps 1 lc rgb 'red' lw 3 w lp t "wo. correction" ,\
      'twissIP_lib_after.dat' u ($1*1e6):(abs($3)) pt 5 ps 1 lc rgb 'blue' lw 3 w lp t "w. correction" ,\


####
reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set key right bottom  #set position of the titles of points of lines

set output "TwissIP_AY.eps"
set xlabel "{/Symbol d}_x={/Symbol d}_y for Q/D in EXT [{/Symbol m}m]"
set ylabel "{/Symbol a}_y" # [arb. unit]"

# set grid off
# set yrange[0:35]
# set format x "%.0e"
set format y "10^{%T}"
set logscale y

plot  'twissIP_lib_before.dat' u ($1*1e6):(abs($8)) pt 7 ps 1 lc rgb 'red' lw 3 w lp t "wo. correction" ,\
      'twissIP_lib_after.dat' u ($1*1e6):(abs($8)) pt 5 ps 1 lc rgb 'blue' lw 3 w lp t "w. correction" ,\

####
reset
set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set key font ",24"
set key spacing 1.5
set key right bottom  #set position of the titles of points of lines

set output "TwissIP_EY.eps"
set xlabel "{/Symbol d}_x={/Symbol d}_y for Q/D in EXT [{/Symbol m}m]"
set ylabel "{/Symbol h}_y [{/Symbol m}m]"

# set grid off
# set yrange[0:35]
# set format x "%.0e"
set format y "10^{%T}"
set logscale y

plot  'twissIP_lib_before.dat' u ($1*1e6):(abs($9)*1e6) pt 7 ps 1 lc rgb 'red' lw 3 w lp t "wo. correction" ,\
      'twissIP_lib_after.dat' u ($1*1e6):(abs($9)*1e6) pt 5 ps 1 lc rgb 'blue' lw 3 w lp t "w. correction" ,\
