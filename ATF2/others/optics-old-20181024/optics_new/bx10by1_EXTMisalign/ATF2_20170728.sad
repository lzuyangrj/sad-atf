! title: ATF2 beam dynamic with QM6/QM7->quad.
! 2017.06.05
! upload the pre-defined functions 
  ON ECHO; OFF CTIME;
 
  read "/home/rj-yang/PhD/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)
  FFS USE= ATF2; !EXT01;
  TRPT;
  INS;

  FFS["NPARA = 8"];
  path0 = "/home/rj-yang/PhD/SAD/ATF2/lattice/";
  Get[path0//"atf2lib.n"];
  Get[path0//"multipole.n"];
  Get[path0//"atf2Apert.n"];
  Get[path0//"atf2lib_lal.n"];

  ! SetATF2Aperture[];
  LoadATF2Optics["ATF2_20170124"];
end;
r
  ! matching the orbit
  !
  Z*  K0 0;
  QK* K1 0;
  QS* K1 0;
  FFS["cal"];

  !
  k2qm7=0;
  SetElement["QM7K2",,{"K2"->k2qm7}];
  CAL;

  FREE KEX1A K0;
! FIT BPM1 DX 6.9845e-3;
  FIT MQM7R2 DX 22.4995e-3;
  Do[FFS["go"],{II,100}];go;
  SAVE;

  FFS["CAL"];
  type MQM7R2;

  BLX = ExtractBeamLine[];
  posInsert=LINE["POSITION",MQM7R3];
  SetElement["Conv",,{"DX"->0,"CHI1"-> 0}];       ! initializing
  FFS["cal"];
! coordinate transformation parameters
  dxConv = Twiss["DX",MQM7R3];
  dpxConv = Twiss["DPX",MQM7R3];

  SetElement["Conv",,{"DX"-> dxConv, "CHI1"-> -dpxConv}];
  BLX= Insert[BLX,"Conv",posInsert+1];
  FFS["USE BLX"];
  FFS["cal"];

! matching ATF2 optics 
  FitFunction:= {Twiss["ax","QD2X.2"]-Twiss["ax","QD5X.2"],
                Twiss["bx","QD2X.2"]-Twiss["bx","QD5X.2"],
                Twiss["ay","QD2X.2"]-Twiss["ay","QD5X.2"],
                Twiss["by","QD2X.2"]-Twiss["by","QD5X.2"],
                Twiss["ex","QD2X.2"]-Twiss["ex","QD5X.2"],
                Twiss["epx","QD2X.2"]-Twiss["epx","QD5X.2"]
                };
  !
  FREE QF1X QF6X; ! QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QD10x QF11X QD12X QF13X QD14X QF15X QD16X QF17X QD18X
  FIT OTR0X EX 0;
  FIT OTR0X EPX 0;
  Do[FFS["go"],{II,100}];go;
  cal;

  FREE QF3X QF4X; ! QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QD10x QF11X QD12X QF13X QD14X QF15X QD16X QF17X QD18X
  FIT OTR0X EX 0;
  FIT OTR0X EPX 0;
  Do[FFS["go"],{II,100}];go;
  cal;
  FIT REJECT TOTAL;
  FIX *;

  !
  FREE QD0FF QF1FF;
  FIT IP AXM 10;
  FIT IP AYM 10;
  FFS["go"];
  FIT IP EX  0;
  !FIT IP EPX 0;
  FIT IP AY  0;
  go;
  !
  FREE QD20X QF21X QM11FF QM12FF QM13FF QM14FF QM15FF;
  FIT IP BX 0.04;
  FIT IP BY 0.0001;
  FIT IP AX 0;
  FIT IP AY 0;
  Do[FFS["go"],{II,100}];go;

  FIT REJECT TOTAL;
  FIX *;
  FFS["cal"];
! set aperture in IEX-BS1XA
! SetElementDisplacement[{"QM6RX","QM7RX","MBS1X","ZH100RX","ZV100RX","ZH101RX"}]

! SetMultiPoleError;
  FFS["cal"];
! FFS["draw IEX MPIP BX BY & EX EY oT*||Ip||QM7RX*||QM6RX*||YAG"];

! initialize Twiss parameters
  {posEles,totalLength,matrixTwiss,wholeEles,listEles,numEles} = EleLibrary[];
  WriteApert[]; ! export aperture
  ExportTwiss[]; ! export Twiss parameters
  System["gnuplot optics.plt"];

! Orbit bump/knobs
  bpmData = Import[path0//"atf2op/atf2BPM/atf2BPM_20170623.dat",9];
  ! 1,3,5 -> name, dx, dy (um)
  nlis = Drop[bpmData[;;,1],1];   ! name
  {dxN,dyN} = 1e-6*Thread[Drop[bpmData[;;,{3,5}],1]];  ! dx/dy by BPMs 
  OrbitBPM = {};

  Table[
    rmsdR = 50e-6;       ! RMS. alignment errors, 100 um, Gaussian
    namebpm = nlis[j]//"";
    nameM = Element["NAME",namebpm[2,-1]//"*"];    ! name quad
    {dxs,dys} = rmsdR*GaussRandom[2];
    Do[
      SetElement[nameM[jj],,{"DX"->dxs,"DY"->dys}]
      ,{jj,1,Length[nameM]}];
    FFS["cal"]
    ,{j,3,22}];

  FREE QF1X QF6X; ! QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QD10x QF11X QD12X QF13X QD14X QF15X QD16X QF17X QD18X
  FIT OTR3X EX 0;
  FIT OTR3X EPX 0;
  Do[FFS["go"],{II,100}];go;
  cal;

  FREE QF3X QF4X; ! QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QD10x QF11X QD12X QF13X QD14X QF15X QD16X QF17X QD18X
  FIT OTR3X EX 0;
  FIT OTR3X EPX 0;
  Do[FFS["go"],{II,100}];go;
  cal;
  FIT REJECT TOTAL;
  FIX *;

  !
  FREE QD0FF QF1FF;
  FIT IP AXM 10;
  FIT IP AYM 10;
  FFS["go"];
  FIT IP EX  0;
  !FIT IP EPX 0;
  FIT IP AY  0;
  go;
  !
  FREE QD20X QF21X QM11FF QM12FF QM13FF QM14FF QM15FF;
  FIT IP BX 0.04;
  FIT IP BY 0.0001;
  FIT IP AX 0;
  FIT IP AY 0;
  Do[FFS["go"],{II,100}];go;

  FIT REJECT TOTAL;
  FIX *;
  FFS["cal"];

end;

  FREE ZH1FF ZV1FF K0;
  FIT MQF1FF DX 0;
  FIT MQD10BFF DY 0;
  Do[FFS["go"],{II,100}];go;
  FFS["cal"];

end;
(*
! Insert additional correctors between QF21 and QM16FF
  {ps1, ps2} = LINE["POSITION",{"MW3X","OTR3X"}]; ! number position

  SetElement["ZVST1","BEND",{"ANGLE"->0,"ROTATE"->-Pi/2}];
  SetElement["ZHST1","BEND",{"ANGLE"->0,"K0"->0}];
  SetElement["ZVST2","BEND",{"ANGLE"->0,"ROTATE"->-Pi/2}];
  SetElement["ZHST2","BEND",{"ANGLE"->0,"K0"->0}];

  BLX = ExtractBeamLine[];
  BLX= Insert[BLX,"ZVST1",ps1+1];
  BLX= Insert[BLX,"ZHST1",ps1+2];
  BLX= Insert[BLX,"ZVST2",ps2+3];
  BLX= Insert[BLX,"ZHST2",ps2+4];
  FFS["USE BLX"];
  FFS["cal"];

!  ZHST1 K0 0;
!  ZHST2 K0 0;
!  FFS["cal"];

  FREE ZHST1 K0;
  FIT ZHST2 DX 0;
  Do[FFS["go"],{II,100}];go;
  FFS["cal"];

  FREE ZHST2 K0;
  FIT MQM16FF DX 0;
  Do[FFS["go"],{II,100}];go;
  FFS["cal"];
*)

 end;
 abort;
