! title: ATF2 beam dynamic with QM6/QM7->quad.
! 2017.06.05
! upload the pre-defined functions 
  ON ECHO; OFF CTIME;
 
  read "/home/rj-yang/PhD/SAD/ATF2/lattice/ATF2_QM6QM7_quad_20170605.sad";   ! updated by RJ (20170530)
  FFS USE= ATF2; !EXT01;
  TRPT;
  INS;

  FFS["NPARA = 8"];
  path0 = "/home/rj-yang/PhD/SAD/ATF2/lattice/";
  Get[path0//"atf2lib.n"];
  Get[path0//"multipole.n"];
  Get[path0//"atf2Apert.n"];
  Get[path0//"atf2lib_lal.n"];

  ! SetATF2Aperture[];
  LoadATF2Optics["ATF2_20170124"];

  ! matching the orbit
  !
  Z*  K0 0;
  QK* K1 0;
  QS* K1 0;
  FFS["cal"];

  !
  k2qm7=0;
  SetElement["QM7K2",,{"K2"->k2qm7}];
  CAL;

  FREE KEX1A K0;n
! FIT BPM1 DX 6.9845e-3;
  FIT MQM7R2 DX 22.4995e-3;
  Do[FFS["go"],{II,100}];go;
  SAVE;

  FFS["CAL"];
  type MQM7R2;

  BLX = ExtractBeamLine[];
  posInsert=LINE["POSITION",MQM7R3];
  SetElement["Conv",,{"DX"->0,"CHI1"-> 0}];       ! initializing
  FFS["cal"];
! coordinate transformation parameters
  dxConv = Twiss["DX",MQM7R3];
  dpxConv = Twiss["DPX",MQM7R3];

  SetElement["Conv",,{"DX"-> dxConv, "CHI1"-> -dpxConv}];
  BLX= Insert[BLX,"Conv",posInsert+1];
  FFS["USE BLX"];
  FFS["cal"];

! matching ATF2 optics 
  FitFunction:= {Twiss["ax","QD2X.2"]-Twiss["ax","QD5X.2"],
                Twiss["bx","QD2X.2"]-Twiss["bx","QD5X.2"],
                Twiss["ay","QD2X.2"]-Twiss["ay","QD5X.2"],
                Twiss["by","QD2X.2"]-Twiss["by","QD5X.2"],
                Twiss["ex","QD2X.2"]-Twiss["ex","QD5X.2"],
                Twiss["epx","QD2X.2"]-Twiss["epx","QD5X.2"]
                };
  !
  FREE QF1X QF6X; ! QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QD10x QF11X QD12X QF13X QD14X QF15X QD16X QF17X QD18X
  FIT OTR0X EX 0;
  FIT OTR0X EPX 0;
  Do[FFS["go"],{II,100}];go;
  cal;

  FREE QF3X QF4X; ! QF1X QD2X QF3X QF4X QD5X QF6X QF7X QD8X QF9X QD10x QF11X QD12X QF13X QD14X QF15X QD16X QF17X QD18X
  FIT OTR2X EX 0;
  FIT OTR2X EPX 0;
  Do[FFS["go"],{II,100}];go;
  cal;
  FIT REJECT TOTAL;
  FIX *;
  !
  FREE QD0FF QF1FF;
  FIT IP AXM 10;
  FIT IP AYM 10;
  FFS["go"];
  FIT IP EX  0;
  !FIT IP EPX 0;
  FIT IP AY  0;
  go;

  !
  FREE QF19X QD18X QD20X QF21X;
  FIT MQM16FF BX 9.7612331976372;
  FIT MQM16FF BY 6.175009399103303;
  FIT MQM16FF AX 2.943312942445639;
  FIT MQM16FF AY -2.810773497552735;
  FFS["go"];

  !
  FREE QM11FF QM12FF QM13FF QM14FF QM15FF QM16FF;
  FIT IP BX 0.04;
  FIT IP BY 0.0001;
  FIT IP AX 0;
  FIT IP AY 0;
  Do[FFS["go"],{II,100}];go;

  FIT REJECT TOTAL;
  FIX *;
  FFS["cal"];
! set aperture in IEX-BS1XA
  ! SetElementDisplacement[{"QM6RX","QM7RX","MBS1X","ZH100RX","ZV100RX","ZH101RX"}]

  SetMultiPoleError;
  FFS["cal"];
  ! FFS["draw IEX MPIP BX BY & EX EY oT*||Ip||QM7RX*||QM6RX*||YAG"];

! initialize Twiss parameters
  {posEles,totalLength,matrixTwiss,wholeEles,listEles,numEles} = EleLibrary[];
! WriteApert[]; ! export aperture
  ExportTwiss[]; ! export Twiss parameters
  ! System["gnuplot optics.plt"];

! =============================================================
! Estimate transverse acceptance manually 
  emitx = 1.2e-9;
  emity = 12e-12;
  sigmap=6e-4;

  beam = GenParticleGauss[emitx,emity,0,sigmap,2e4];
 
  pEnd = {"IP"}; !{"MQF9X", "QM13FF","MQF7FF","MQD10AFF","MQD4AFF","MQD2AFF","YAG","SF1FF","QF1FF","QD0FF","IP","DSH"};
  Do[
    beam2 = TrackParticles[beam,pEnd[i]];
    WriteSurvey[beam2,"./data/Newlattice_NewIEX_beta_"//pEnd[i]//"_loadOptics_multi_Core.dat"];
    ,{i,1,Length@pEnd}];

 end;
 abort;
