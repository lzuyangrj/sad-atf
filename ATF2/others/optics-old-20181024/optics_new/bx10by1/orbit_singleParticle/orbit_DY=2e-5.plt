# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "Orbit_DY=2e-5.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol D}_{/Italic y} [mm]"

# set grid off
set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'orbit_DY=2e-5_DP=0.dat' u 1:($3*1e3) pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t "{/Symbol d}_p=0" ,\
     'orbit_DY=2e-5_DP=1e-2.dat' u 1:($3*1e3) pt -1 ps 1.5 lc rgb 'blue' lw 3 w lp t "{/Symbol d}_p=1%" ,\
     'orbit_DY=2e-5_DP=1e-2.dat' u 1:($3*1e3) pt -1 ps 1.5 lc rgb 'black' lw 3 w lp t "{/Symbol d}_p=-1%"