# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "optic_10bx1by.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol b}^{1/2} [m]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'Twiss_KEX1K1original.dat' u 2:(sqrt($4)) pt -1 ps 1.5 lc rgb 'black' lw 3 w lp t "{/Symbol b}_{/Italic x}^{1/2}" ,\
     'Twiss_KEX1K1original.dat' u 2:(sqrt($7)) pt -1 ps 1.5 lc rgb 'cyan' lw 3 w lp t "{/Symbol b}_{/Italic y}^{1/2}" ,\
     'Twiss_KEX1K1=0.dat' u 2:(sqrt($4)) pt -1 ps 1.5 lc rgb 'blue' lw 3 w lp t "{/Symbol b}_{/Italic x}^{1/2}" ,\
     'Twiss_KEX1K1=0.dat' u 2:(sqrt($7)) pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t "{/Symbol b}_{/Italic y}^{1/2}" ,\

#########################################
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file

set key font ",22"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "Dispersion_10bx1by.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol h} [m]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'Twiss_KEX1K1original.dat' u 2:10 pt -1 ps 1.5 lc rgb 'green' lw 3 w lp t "K1=-0.36" ,\
     'Twiss_KEX1K1=0.dat' u 2:10 pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t "K1=0" ,\

