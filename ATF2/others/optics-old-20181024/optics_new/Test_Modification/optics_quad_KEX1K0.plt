# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "betax_EXT1_quad_KEX1K0.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol b}@_x^{1/2} [m]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'Twiss_quad_K0=0.dat' u 2:(sqrt($4)) pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t "K_0=0" ,\
     'Twiss_quad_KEX1K0Matched.dat' u 2:(sqrt($4)) pt -1 ps 1.5 lc rgb 'green' lw 3 w lp t "K_0=-6.6e-3" ,\

####
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "betay_EXT1_quad_KEX1K0.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol b}@_y^{1/2} [m]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'Twiss_quad_K0=0.dat' u 2:(sqrt($7)) pt -1 ps 1.5 lc rgb 'black' lw 3 w lp t "K_0=0" ,\
     'Twiss_quad_KEX1K0Matched.dat' u 2:(sqrt($7)) pt -1 ps 1.5 lc rgb 'blue' lw 3 w lp t "K_0=-6.6e-3" ,\

#########################################
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file

set key font ",22"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "Dispersion_EXT1_quad_KEX1K0.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol h}_x vs. KEX1 K_0[m]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'Twiss_quad_K0=0.dat' u 2:10 pt -1 ps 1.5 lc rgb 'green' lw 3 w lp t "K_0=0" ,\
     'Twiss_quad_KEX1K0Matched.dat' u 2:10 pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t "K_0=-6.6e-3" ,\


#########################################
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file

set key font ",22"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "Dx_EXT1_quad_KEX1K0.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Italic Dx} vs. KEX1 K_0[m]"

# set grid off
# set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'Twiss_quad_K0=0.dat' u 2:14 pt -1 ps 1.5 lc rgb 'green' lw 3 w lp t "K_0=0" ,\
     'Twiss_quad_KEX1K0Matched.dat' u 2:14 pt -1 ps 1.5 lc rgb 'red' lw 3 w lp t "K_0=-6.6e-3" ,\

