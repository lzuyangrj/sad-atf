# plot dynamic aperture w./w.o. SR at quads
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file
set key font ",22"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "DA_ATF2_NewLatticeX.eps"
set xlabel "{/Symbol d}_p [%]"
set ylabel "x/{/Symbol s}_x"
#set grid off
# set xrange[-5e-4:5e-4]
# set yrange[0:35]

plot 'dynamicApert_x.dat'  u (100*6e-4*$1):2 pt 5 ps 1 lc rgb 'red' lw 5 t "new lattice" w lp

###

set output "DA_ATF2_NewLatticeY.eps"
set xlabel "{/Symbol d}_p [%]"
set ylabel "y/{/Symbol s}_y"
#set grid off
# set xrange[-5e-4:5e-4]
# set yrange[0:35]

plot 'dynamicApert_y.dat'  u (100*6e-4*$1):2 pt 5 ps 1 lc rgb 'red' lw 5 t "new lattice" w lp


