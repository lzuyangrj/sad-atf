# plot orbit of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left bottom #set position of the titles of points of lines

set output "Orbit_bpm.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol D}_{/Italic x,y} [mm]"

# set grid off
set title "Orbit, BPMs"
set xrange[0:90]
set yrange[-3:3]
# set format x "%.0e"
# set yrange[0:35]

plot  'orbitBPM.dat' u 2:($3*1e3) pt 5 ps 1.5 lc rgb 'red' lw 3 w lp t "{/Symbol D}_{/Italic x}" ,\
     'orbitBPM.dat' u 2:($4*1e3) pt 7 ps 1.5 lc rgb 'black' lw 3 w lp t "{/Symbol D}_{/Italic y}" ,\


####
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "Offset_bpm.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol D}_{/Italic x,y} [mm]"

# set grid off
set title "Misalignment, BPMs"
set xrange[0:90]
set yrange[-0.2:0.2]
# set format x "%.0e"
# set yrange[0:35]

plot  'orbitBPM.dat' u 2:($5*1e3) pt 5 ps 1.5 lc rgb 'red' lw 3 w lp t "{/Symbol D}_{/Italic x}" ,\
     'orbitBPM.dat' u 2:($6*1e3) pt 7 ps 1.5 lc rgb 'black' lw 3 w lp t "{/Symbol D}_{/Italic y}" ,\

####
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "BPMReading.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol D}_{/Italic x,y} [mm]"

# set grid off
set title "BPMs Reading"
set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'orbitBPM.dat' u 2:($7*1e3) pt 5 ps 1.5 lc rgb 'red' lw 3 w lp t "{/Symbol D}_{/Italic x}" ,\
     'orbitBPM.dat' u 2:($8*1e3) pt 7 ps 1.5 lc rgb 'black' lw 3 w lp t "{/Symbol D}_{/Italic y}" ,\