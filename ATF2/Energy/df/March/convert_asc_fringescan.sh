#!/bin/sh
#-------------------------------------------
# FringeScan convert to ascii file
#     Created by Y.Tsukada(KIS) 2015/12/28
#-------------------------------------------
# 実行時に指定された引数が入っていなければエラー終了
if [ $# -ne 1 ]; then
  echo "指定された引数は$#個です。" 1>&2
  echo "実行するには1個の引数が必要です。" 1>&2
  exit 1
fi

# ファイル名のキーワード取得
filename=$1
# 変換後のファイル名の頭につける名前
template_header="fringescan_asc_"
# 変換後のファイル名の拡張子
suffix_asc=".dat"
# 設定ファイルを読み込む
for line in `cat ${filename}`
do
  echo ${line}
  ascname=$template_header${line}$suffix_asc
  # ascii 変換プログラムの実行
  /userhome/tsukada/development/IPBSM/Qt/Gui/DumpFringeData/DumpFringeData ${line} > $ascname

done

