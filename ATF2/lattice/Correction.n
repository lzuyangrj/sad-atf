OpticsCorrection=Class[{},{olx},{
  cdy="QD*|QT*|QRD*|Q{ABCLSMNGIUH}{13579}*",
!  cdy="QT*|QRD*|Q{ABCLSMNGIUH}{13579}*|S{FD}*",
  qtune="QR*",
  cdx="SF*",cey="S{CFD}*",cex="S{CFD}*",cxy="S{FD}*",
  kxy="QRF*",
!  mon="Q{CABTLSGMNHIUR}*|S{FD}*",
  mon="Q{FDCABSLGUIHMNT}*|S{CYFD}*",
  emon="Q{FDYCGLSHIABT}*|S{FD}*|Q{MN}{567}*",
  bmon="Q{YCGLSRHIABTMN}*|S{FD}*",
  sext="S{FD}*",alelm="Q{^CY}*",allsext="S*",sysext="SY*",
  sextf="SF*",sextd="SD*",
  pcdy,pcdx,pcey,pcex,pcex1,pcex2,pcey1,pcey2,pcxy,pcxy1,pcxy2,pmon,pemon,pbmon,paelm,
  psext,psysext,psf,psd,
  nal,nsext,nallsext,npmon,npemon,
  dxa,dya,dxsa,dysa,k1mon,
  pnearbyq,pall,pcsall,pallsext,
  mux,muy,cmux,cmuy,smux,smuy,dcmu,
  refdx,refdy,refex,refey,refbx,refby,refnx,refny,
  erefdx,erefdy,erefex,erefey,erefbx,erefby,erefnx,erefny,
  brefbx,brefby,brefnx,brefny,aveerefbx,aveerefby,lastdex,lastdey,lastdxy,lastdn,
  cddx,cddy,cdey,cdbx,cdby,cdnx,cdny,bxip,byip,
  cexex1,cexey2,cexnx1,cexnx2,cexbx1,cexbx2,
  ceyex1,ceyex2,ceyny1,ceyny2,ceyby1,ceyby2,
  cxybx,cxyby,cxynx,cxyny,
  msdx=0,msdy=0,
  rmdx,rmdy,rmex,rmey,trmey1,trmey2,trmxy1,trmxy2,rmr2,rmdxi,rmdyi,rmni,rmnx,rmny,rmn,
  dzi={0,0,0,0},aerefex,svdrm,rmvw,rm,
  emiymin,smin,emiygoal=2.5e-12,dkxy=3e-5,dkstep=5e-5,pkxys,nxy=6,tunedamp=0.9,
  dxtol=0.01,dytol=0.01,extol=0.01,eytol=0.01,xytol=0.01,ntol=0.01,exthre=0.1,dmu1={0.23,0.31},tath=0.03*2*Pi,
  txmin=9e-5,tymin=9e-5,tcmin=9e-5,redtx=1.02,redty=Sqrt[2],redtc=Sqrt[2],wmu=30,readm=True,rdef,rdef1,
  dxcorr=3e-3,dycorr=3e-3,seed=3,dstop=0.03,
  output=6,fmat=""},

  Constructor[]=(
    FFS["FIT CAL LENG- CAL;SAVE;GEOFIX;"];
    {pcdy,pcdx,pcey,pcex,pcxy,pkxy,pmon,pemon,pbmon,paelm,psext,pallsext,psysext,psf,psd}=
      LINE["POSITION",{cdy,cdx,cey,cex,cxy,kxy,mon,emon,bmon,alelm,sext,allsext,sysext,sextf,sextd}];
    pnearbyq=NearbyQ/@psext;
    pmon=Complement[pmon,pnearbyq];
    pemon=Complement[pemon,pnearbyq];
    nal=Length[paelm];
    nsext=Length[psext];
    nallsext=Length[pallsext];
    npmon=Length[pmon];
    npemon=Length[pemon];
    npbmon=Length[pbmon];
    {pcex1,pcex2}=Thread[Partition[pcex,2]];
    {pcey1,pcey2}=Thread[Partition[pcey,2]];
    {pcxy1,pcxy2}=Thread[Partition[pcxy,2]];
    pcsall=Union[Flatten[{pcex,pcey,pcxy}]];
    pall=Union[paelm,pallsext];
    {mux,muy}=Twiss[{"NX","NY"},"$$$"];
    {cmux,cmuy}=Cos[{mux,muy}];
    {smux,smuy}=Sin[{mux,muy}];
    dcmu=cmux-cmuy;
    {bxip,byip}=Twiss[{"BX","BY"},"IP.1"];
    { refdx, refdy, refex, refey, refbx, refby, refnx, refny}=Twiss[{"DX","DY","EX","EY","BX","BY","NX","NY"},pmon];
    {erefdx,erefdy,erefex,erefey,erefbx,erefby,erefnx,erefny}=Twiss[{"DX","DY","EX","EY","BX","BY","NX","NY"},pemon];
    {brefbx,brefby,brefnx,brefny}=Twiss[{"BX","BY","NX","NY"},pbmon+0.5];
    {cdbx,cdnx}=Twiss[{"BX","NX"},pcdx+0.5];
    {cdby,cdny}=Twiss[{"BY","NY"},pcdy+0.5];
    {cxybx,cxyby,cxynx,cxyny}=Twiss[{"BX","BY","NX","NY"},pcxy1+0.5];
    {cexbx1,cexnx1,cexex1}=Twiss[{"BX","NX","EX"},pcex1+0.5];
    {cexbx2,cexnx2,cexex2}=Twiss[{"BX","NX","EX"},pcex2+0.5];
    {ceyby1,ceyny1,ceyex1}=Twiss[{"BY","NY","EX"},pcey1+0.5];
    {ceyby2,ceyny2,ceyex2}=Twiss[{"BY","NY","EX"},pcey2+0.5];
    k1mon=Sqrt[Abs[LINE["K1",pmon]]];
    {refbx,refby,erefbx,erefby,cdbx,cdby,cexbx1,cexbx2,ceyby1,ceyby2,cxybx,cxyby}=
      Sqrt[{refbx,refby,erefbx,erefby,cdbx,cdby,cexbx1,cexbx2,ceyby1,ceyby2,cxybx,cxyby}];
    aveerefbx=(Plus@@erefbx)/npemon;
    aveerefby=(Plus@@erefby)/npemon;
    rmdx=refbx^2*Outer[(#2[[1]]*Cos[Abs[#-#2[[2]]]-mux/2])&,refnx,Thread[{cdbx,cdnx}]]/2/Sin[mux/2];
    rmdy=refby^2*Outer[(#2[[1]]*Cos[Abs[#-#2[[2]]]-muy/2])&,refny,Thread[{cdby,cdny}]]/2/Sin[muy/2];
    rmex=erefbx^2*Outer[(#2[[1]]*(Cos[Abs[#-#2[[2]]]-mux/2]-Cos[Abs[#-#2[[3]]]-mux/2]))&,
      erefnx,Thread[{cexbx1*cexex1,cexnx1,cexbx2*cexex2,cexnx2}]]/2/Sin[mux/2];
    rmey=erefby^2*Outer[(#2[[1]]*Cos[Abs[#-#2[[2]]]-muy/2]-#2[[3]]*Cos[Abs[#-#2[[4]]]-muy/2])&,
      erefny,Thread[{ceyby1*ceyex1,ceyny1,ceyby2*ceyex2,ceyny2}]]/2/Sin[muy/2];
    trmex1=cexbx1*Outer[(#2[[1]]*Cos[Abs[#2[[2]]-#]-mux/2])&,
      cexnx1,Thread[{erefbx^2,erefnx}]]/2/Sin[mux/2];
    trmex2=cexbx2*Outer[(#2[[1]]*Cos[Abs[#2[[2]]-#]-mux/2])&,
      cexnx2,Thread[{erefbx^2,erefnx}]]/2/Sin[mux/2];
    trmey1=ceyby1*Outer[(#2[[1]]*Cos[Abs[#2[[2]]-#]-muy/2])&,
      ceyny1,Thread[{erefby^2,erefny}]]/2/Sin[muy/2];
    trmey2=ceyby2*Outer[(#2[[1]]*Cos[Abs[#2[[2]]-#]-muy/2])&,
      ceyny2,Thread[{erefby^2,erefny}]]/2/Sin[muy/2];
    trmxy1=cxyby*Outer[(#2[[1]]*Cos[Abs[#2[[2]]-#]-muy/2])&,
      cxyny,Thread[{erefby^2,erefny}]]/2/Sin[muy/2];
    trmxy2=cxyby*Outer[(#2[[1]]*Cos[Abs[#2[[2]]-#]-muy/2])&,
      cxyny+Pi,Thread[{erefby^2,erefny}]]/2/Sin[muy/2];
    rmr2=erefbx*erefby*Outer[(#2[[3]]*(
      Cos[#[[2]]-#2[[2]]]*Sin[#[[1]]-#2[[1]]]*smuy
      -(Cos[#[[1]]-#2[[1]]]*smux+dcmu*Sin[#[[2]]-#2[[2]]])*Sin[#[[2]]-#2[[2]]])/dcmu)&,
      Thread[{erefnx+mux,erefny+muy}],Thread[{cxynx,cxyny,cxybx*cxyby}]];
    rmnx=Outer[(#2[[1]]*
      Which[#<#2[[2]],
        Cos[mux-2*#2[[2]]+#]*Sin[#],
        #2[[3]]<#,
        smux-Cos[2*#2[[2]]-#]*Sin[mux-#],
        1,
        (Cos[mux-2*#2[[3]]+#]*Sin[#]+smux-Cos[2*#2[[2]]-#]*Sin[mux-#])/2]/smux)&,
      brefnx,Thread[{cxybx^2,cxynx,cxynx+Pi}]];
    rmny=-Outer[(#2[[1]]*
      Which[#<#2[[2]],
        Cos[muy-2*#2[[2]]+#]*Sin[#],
        #2[[3]]<#,
        smuy-Cos[2*#2[[2]]-#]*Sin[muy-#],
        1,
        (Cos[muy-2*#2[[3]]+#]*Sin[#]+smuy-Cos[2*#2[[2]]-#]*Sin[muy-#])/2]/smuy)&,
      brefny,Thread[{cxyby^2,cxyny,cxyny+Pi}]];
    rmn=Join[rmnx,rmny,wmu*{cxybx^2,-cxyby^2}];
    Clear[rmdxi,rmdyi,rmni,rmexi,rmeyi,svdrm,rmvw,rm];
    rmdxi[tol_]:=(rmdxi[tol]=FindInverse["DX",rmdx,tol]);
    rmdyi[tol_]:=(rmdyi[tol]=FindInverse["DY",rmdy,tol]);
    rmexi[tol_]:=(rmexi[tol]=FindInverse["EX",rmex,tol]);
    rmeyi[tol_]:=(rmeyi[tol]=FindInverse["EY",rmey,tol]);
    rmni[tol_]:= (rmni[tol]=FindInverse["N",rmn,tol]);
    rmvw[label_,tol_]:=Module[{{w,v}=svdrm[label][[{2,3}]],wth},
      wth=Max[w]*tol;
      rmvw[label,tol]=Transpose[(w/(wth^2+w^2))*v]];
    rm["DX"]=rmdx;
    rm["DY"]=rmdy;
    rm["EX"]=rmex;
    rm["EY"]=rmey;
    rm["N"]=rmn;
    ChooseXYK[nxy];
    aerefex=Sqrt[(Plus@@((erefex*erefbx)^2))/npemon];
    If[fmat==="",
      fmat=olx@fnameroot//".mat"];
    If[readm,
      Check[rdef=Get[fmat],rdef={}];
      If[Head[rdef]===Hold,
        rdef=Take[(Hold@@[rdef,{2}])[[1]]/.{SetDelayed->List}/.{_[s_String]:>s},{2,-2}]/.Hold->List;
        (svdrm[#]=#2)&@@[rdef,{1}];
        ]];
    svdrm[label_]:=(svdrm[label]=SingularValues[rm[label],Inverse->False,Tolerance->1e-8]);
    );

  Show[]:=StandardForm[
    Print["{dxtol,dytol,extol,eytol,xytol,ntol,exthre,dkxy,fmat} = ",
      {dxtol,dytol,extol,eytol,xytol,ntol,exthre,dkxy,fmat}]];

  SaveMat[]:=Module[{f=OpenWrite[fmat]},
    StandardForm[
      Write[f,?svdrm]];
    Close[f]];

  SortSVD[m_]:=Module[{{u,w,v}=SingularValues[m,Inverse->False,Tolerance->1e-8],us,ws,vs},
    {ws,us,vs}=Thread[Sort[Thread[{w,u,v}]]];
    {us,ws,vs}];

  FindInverse[label_,m_,tol_]:=Module[{mi=svdrm[label],u,v,w,wth},
    {u,w,v}=If[mi<=>{},mi,svdrm[label]=SingularValues[m,Inverse->False,Tolerance->1e-8]];
    wth=Max[w]*tol;
    Transpose[(w/(wth^2+w^2))*v].u];

  Solve[label_,dy_,tol_]:=rmvw[label,tol].(svdrm[label][[1]].dy);

  NearbyQ[s_]:=Module[{q1=s-1,q2=s+1},
    While[LINE["NAME",q1][1]<>"Q",q1--];
    While[LINE["NAME",q2][1]<>"Q",q2++];
    d1=LINE["S",s]-LINE["S",q1+1];
    d2=LINE["S",q2]-LINE["S",q2+1];
    If[d1>=d2,q2,q1,q1]];

  With[{def={DX->0,DY->0,SDX->0,SDY->0,MSDX->0,MSDY->0}},
    Align[opt___]:=Module[{{dx,dy,sdx,sdy,mdx,mdy}={DX,DY,SDX,SDY,MSDX,MSDY}/.{opt}/.def},
      dxa=dx*GaussRandom[nal];
      dxsa=sdx*GaussRandom[nsext];
      dya=dy*GaussRandom[nal];
      dysa=sdy*GaussRandom[nsext];
      msdx=mdx*GaussRandom[nsext];
      msdy=mdy*GaussRandom[nsext];
      LINE["DX",paelm]=dxa;
      LINE["DY",paelm]=dya;
      ScanThread[(LINE["DX",#]=LINE["DX",#2]+#3)&,{psext,pnearbyq,dxsa}];
      ScanThread[(LINE["DY",#]=LINE["DY",#2]+#3)&,{psext,pnearbyq,dysa}];
      ]
    ];

  AlignSext[]:=(AlignSextX[];AlignSextY[]);
  AlignSextS[]:=(AlignSextXS[];AlignSextYS[]);

  AlignSextX[sx_:psext]:=Module[{p=LINE["POSITION",sx],dx,dy,dk1,dk0},
    dy=Twiss["DY",p]-LINE["DY",p]+msdy;
    dx=Twiss["DX",p]-LINE["DX",p]+msdx;
    dk1=-dx*LINE["K2",p];-LINE["K1",p];
    dk0=(dx^2-dy^2)*LINE["K2",p]/2-LINE["K0",p];
    CorrStab2[p,dk1,"K1",p,dk0,"K0",4,"AlignSX"]];

  AlignSextY[sx_:psext]:=Module[{p=LINE["POSITION",sx],dx,dy,dks1,dsk0},
    dy=Twiss["DY",p]-LINE["DY",p]+msdy;
    dx=Twiss["DX",p]-LINE["DX",p]+msdx;
    dsk1=-dy*LINE["K2",p]-LINE["SK1",p];
    dsk0=-dy*dx*LINE["K2",p]-LINE["SK0",p];
    CorrStab2[p,dsk1,"SK1",p,dsk0,"SK0",4,"Align SY"]];

  AlignSextXS[]:=Module[{p=LINE["POSITION",psext],dx,dy,dxs,dys,p1,p2,damp=0.3,
    dk11,dk01,dk12,dk02,n},
    dy=Twiss["DY",p]-LINE["DY",p]+msdy;
    dx=Twiss["DX",p]-LINE["DX",p]+msdx;
    dys=(Plus@@[Partition[dy,2],{1}])/2;
    dxs=(Plus@@[Partition[dx,2],{1}])/2;
    {p1,p2}=Thread[Partition[p,2]];
    dk11=(-dxs*LINE["K2",p1]-LINE["K1",p1])*damp;
    dk01=((dxs^2-dys^2)/2*LINE["K2",p1]-LINE["K0",p1])*damp;
    dk12=(-dxs*LINE["K2",p2]-LINE["K1",p2])*damp;
    dk02=((dxs^2-dys^2)/2*LINE["K2",p2]-LINE["K0",p2])*damp;
    n=Ceiling[Max[Abs[{dk11,dk12}]]/dkstep];
    CorrStab2[p1,dk11,"K1",p2,dk12,"K1",n,"AlignSXS1",{EmitY,1e10,Pi,False}];
    CorrStab2[p1,dk01,"K0",p1,dk02,"K0",n,"AlignSXS0",{EmitY,1e10,Pi,False}]];

  AlignSextYS[]:=Module[{p=LINE["POSITION",psext],dx,dy,dxs,dys,p1,p2,damp=0.3,
    dsk11,dsk01,dsk12,dsk02,n},
    dy=Twiss["DY",p]-LINE["DY",p]+msdy;
    dx=Twiss["DX",p]-LINE["DX",p]+msdx;
    dys=(Plus@@[Partition[dy,2],{1}])/2;
    dxs=(Plus@@[Partition[dx,2],{1}])/2;
    {p1,p2}=Thread[Partition[p,2]];
    dsk11=(-dys*LINE["K2",p1]-LINE["SK1",p1])*damp;
    dsk01=(-dys*dxs*LINE["K2",p1]-LINE["SK0",p1])*damp;
    dsk12=(-dys*LINE["K2",p2]-LINE["SK1",p2])*damp;
    dsk02=(-dys*dxs*LINE["K2",p2]-LINE["SK0",p2])*damp;
    n=Ceiling[Max[Abs[{dsk11,dsk12}]]/dkstep];
    CorrStab2[p1,dsk11,"SK1",p2,dsk12,"SK1",n,"AlignSYS1",{EmitY,1e10,Pi,False}];
    CorrStab2[p1,dsk01,"SK0",p1,dsk02,"SK0",n,"AlignSYS0",{EmitY,1e10,Pi,False}]];

  ChooseXYK[n_]:=Module[{nx=Sort[Thread[{FractionalPart[Twiss["NX",pkxy+0.5]/Pi],pkxy}]],
    m=Length[pkxy]+1,nx1,dnx1,p},
    While[--m>n,
      nx1=Append[nx,nx[[1]]+{1,0}];
      dnx1=Thread[{Difference[nx1[[,1]]],Range[m]}];
      p=Sort[dnx1][[1,2]];
      nx=Sort[Delete[nx,p]]];
    pkxys=nx[[,2]]];

  Type[x___]:=If[output,
    Print[x]];

  CorrStab1[pc_,c0_,key_,n_,tag_,eval_:{0&,1,Pi}]:=Module[
    {c1=c0,k0=LINE[key,pc],dz=dzi,v=eval[[1]][]*eval[[2]]},
!    If[~?STABLE,Throw[$FORM="";{0,"Unstable initial optics: CorrStab1 "//tag//" "//key,0,0}]];
    Do[
      LINE[key,pc]+=c1;
      Calc[];
      If[?STABLE && dTuneA[] < eval[[3]] && eval[[1]][]<=v,Return[1]];
      LINE[key,pc]=k0;
      c1/=Sqrt[2],
      {n}];
    Type["CorrStab1 0: ",tag," ",key];
    dzi=dz;
    Calc[];
    0
    ];

  CorrStab[pc_,cxy_,key_,n_,tag_]:=CorrStab2[pc[[,1]],cxy,key,pc[[,2]],-cxy,key,n,tag];
  CorrStabA[pc1_,pc2_,cxy_,key_,n_,tag_,eval_:{0&,1,Pi}]:=CorrStab2[pc1,cxy,key,pc2,-cxy,key,n,tag,eval];
  CorrStabS[pc1_,pc2_,cxy_,key_,n_,tag_,eval_:{0&,1,Pi}]:=CorrStab2[pc1,cxy,key,pc2, cxy,key,n,tag,eval];

  CorrStab2[pc1_,c10_,key1_,pc2_,c20_,key2_,n_,tag_,eval_:{0&,1,Pi/2,False}]:=
    Module[{{c1n,c2n}={c10,c20}/n,{refdx0,refdy0}={refdx,refdy},{v1,v2,v3}={-1,-1,-1},
      k10=LINE[key1,pc1],k20=LINE[key2,pc2],v=eval[[1]][]*eval[[2]],dz=dzi,k,ka},
!      If[~?STABLE,Throw[$FORM="";{0,"Unstable initial optics: CorrStab2 "//tag//" "//key1//" "//key2,0,0}]];
      ka=0;
      Do[
        {refdx,refdy}={dDX[],dDY[]};
        LINE[key1,pc1]=k10+c1n*k;
        LINE[key2,pc2]=k20+c2n*k;
        dz=dzi;
        Calc[];
        If[?STABLE && (v1=dTuneA[])< (v2=eval[[3]]) && (v3=eval[[1]][])<=v,
          ka=k;
          If[eval[[4]],
            CorrectOrbitXi[CheckEmit->True];
            CorrectOrbitYi[CheckEmit->True]],
          LINE[key1,pc1]=k10+c1n*(k-1);
          LINE[key2,pc2]=k20+c2n*(k-1);
          dzi=dz;
!          Print["CorrStab2: Break: ",EmitY[]];
          Break[]],
        {k,n}];
      {refdx,refdy}={refdx0,refdy0};
      Calc[output];
      StandardForm[
        Type["CorrStab2: ",tag," ",ka//"/"//n," {dn, dn_limit, f_try, f_limit}: ",$FORM="10.5";{v1,v2,v3,v}]];
      ka/n
      ];

  MeasureXY1[p_]:=Module[{k0=LINE["K0",p],dz=dzi,
    xy0={Twiss["DX",pcxy],Twiss["DY",pemon]},dxy},
    CorrStab1[p,dkxy,"K0",20,"MeasXY"];
    dxy={Twiss["DX",pcxy],Twiss["DY",pemon]}-xy0;
    LINE["K0",p]=k0;
    dzi=dz;
    Calc[];
    dxy];

  MeasureXY[]:=MeasureXY1/@pkxys;

  dEXB[]:=(Twiss["PEX",pemon]-erefex)*erefbx;
  dEXBA[]:=(lastdex=Sqrt[Plus@@(dEXB[]^2)/npemon]/aveerefbx);
  dEYB[]:=(Twiss["PEY",pemon]-erefey)*erefby;
  dEYBA[]:=(lastdey=Sqrt[Plus@@(dEYB[]^2)/npemon]/aveerefby);
  dXYA[]:=Module[{xy=MeasureXY[],xf,yf,d},
    {xf,yf}=Flatten/@Thread[xy];
    lastdxy=Sqrt[(Plus@@(yf^2))/Length[yf] / Max[1e-100,((Plus@@(xf^2))/Length[xf])]]];
  dN[]:=Flatten[Twiss[{"NX","NY"},pbmon]-{brefnx,brefny}];
  dNA[]:=Module[{dn=dN[]},
    lastdn=Sqrt[Plus@@((dn-(Plus@@dn)/2/npbmon)^2)/npbmon/2]];
  dDX[]:=Twiss["DX",pmon]-LINE["DX",pmon];
  dDY[]:=Twiss["DY",pmon]-LINE["DY",pmon];

  With[{
    dxdef={Tolerance->dxtol,Factor->1,CheckEmit->False},
    dydef={Tolerance->dytol,Factor->1,CheckEmit->False},
    exdef={Tolerance->extol,Factor->1},
    eydef={Tolerance->eytol,Factor->1,EYAThreshold->0.1},
    xydef={Tolerance->xytol,Factor->1,CheckEmit->True,XYAThreshold->Infinity},
    ndef ={Tolerance->ntol, Factor->1,CheckEmit->True,NAThreshold->Infinity}},

    CorrectOrbitX[opt___]:=Module[{dx,cx,{tol,fact}={Tolerance,Factor}/.{opt}/.dxdef},
      dx=(dDX[]-refdx)*refbx;
      cx=LinearSolve[rmdx,dx,Tolerance->tol]*fact;
      CorrStab1[pcdx,cx,"K0",10,"DX"]];

    CorrectOrbitXi[opt___]:=Module[{dx,cx,
      {tol,fact,emi}={Tolerance,Factor,CheckEmit}/.{opt}/.dxdef},
      dx=(dDX[]-refdx)*refbx;
      cx=Solve["DX",dx,tol]*fact;
      If[emi,
        CorrStab1[pcdx,cx,"K0",10,"DXi",{EmitY,1e10,Pi}],
        CorrStab1[pcdx,cx,"K0",10,"DXi"]]];

    CorrectOrbitY[opt___]:=Module[{dy,cy,{tol,fact}={Tolerance,Factor}/.{opt}/.dydef},
      dy=(dDY[]-refdy)*refby;
      cy=LinearSolve[rmdy,dy,Tolerance->tol]*fact;
      CorrStab1[pcdy,-cy,"SK0",10,"DY"]];

    CorrectOrbitYi[opt___]:=Module[{dy,cy,
      {tol,fact,emi}={Tolerance,Factor,CheckEmit}/.{opt}/.dydef},
      dy=(dDY[]-refdy)*refby;
      cy=Solve["DY",dy,tol]*fact;
      If[emi,
        CorrStab1[pcdy,-cy,"SK0",10,"DYi",{EmitY,1e10,Pi}],
        CorrStab1[pcdy,-cy,"SK0",10,"DYi"]]];

    CorrectXY[opt___]:=Module[
      {rm={},dy={},x,y,x1,x2,cy,n,{xf,yf}={{},{}},s=0,
        {tol,fact,emi,xyath}={Tolerance,Factor,CheckEmit,XYAThreshold}/.{opt}/.xydef},
      Scan[({x,y}=MeasureXY1[#];
        xf={xf,x};
        yf={yf,y};
        {x1,x2}=Thread[Partition[x,2]];
        dy=Join[dy,y*erefby];
        rm=Join[rm,Transpose[trmxy1*x1+trmxy2*x2]])&,pkxys];
      xf=Flatten[xf];
      yf=Flatten[yf];
      lastdxy=Sqrt[(Plus@@(yf^2))/Length[yf] / Max[1e-100,((Plus@@(xf^2))/Length[xf])]];
!      Print["CorrectXY: ",{emi,lastdxy,xyath}];
      emi=emi && lastdxy < xyath;
      cy=LinearSolve[rm,dy,Tolerance->tol]*fact;
      n=Ceiling[Max[Abs[cy]]/dkstep];
      If[
        s=If[emi,
          CorrStabS[pcxy1,pcxy2,-cy,"SK1",n,"XY",{EmitYL,1.01,Pi/2,False}],
          CorrStabS[pcxy1,pcxy2,-cy,"SK1",n,"XY",{EmitY,1e10,Pi,False}]],
        dXYA[]];
      s];

    CorrectR2[opt___]:=Module[{r2=Twiss["R2",pemon],cy,fact=Factor/.{opt}/.def},
      cy=LinearSolve[rmr2,r2,opt]*fact;
      CorrStabS[pcxy1,pcxy2,-cy,"SK1",4,"R2"];
      ];

    CorrectN[opt___]:=Module[{dn=Join[Twiss["NX",pbmon]-brefnx,Twiss["NY",pbmon]-brefny,
      (Twiss[{"NX","NY"},"$$$"]-{mux,muy})*wmu],
      dk,{tol,fact}={Tolerance,Factor}/.{opt}/.ndef},
      dk=LinearSolve[rmn,dn,Tolerance->tol]*fact;
      CorrStabS[pcxy1,pcxy2,-dk,"K1",4,"N"];
      ];

    CorrectNi[opt___]:=Module[
      {dn=Join[Twiss["NX",pbmon]-brefnx,Twiss["NY",pbmon]-brefny,
        (Twiss[{"NX","NY"},"$$$"]-{mux,muy})*wmu],n,
        dk,{tol,fact,emi,nth}={Tolerance,Factor,CheckEmit,NAThreshold}/.{opt}/.ndef},
      dk=Solve["N",dn,tol]*fact;
      n=Ceiling[Max[Abs[dk]]/dkstep];
      emi=emi && dNA[] < nth;
      If[emi,
        CorrStabS[pcxy1,pcxy2,-dk,"K1",n,"Ni",{EmitYL,1.1,Pi/2,False}],
        CorrStabS[pcxy1,pcxy2,-dk,"K1",n,"Ni",{EmitY,1e10,Pi,False}]]
      ];

    CorrectDispX[opt___]:=Module[{},
      If[dEXBA[]<aerefex*exthre,
        CorrectDispXi[opt],
        CorrectDispX1[opt]]];

    CorrectDispXi[opt___]:=Module[{pex=dEXB[],n,
      cx,{tol,fact}={Tolerance,Factor}/.{opt}/.exdef},
      cx=Solve["EX",pex,tol]*fact;
      n=Ceiling[Max[Abs[cx]]/dkstep];
      CorrStabA[pcex1,pcex2,cx,"K1",n,"EXi",{EmitYL,1.05,Pi/2,False}];
      Sqrt[(Plus@@(Twiss["PEX",pemon]-erefex)^2)/npemon]
      ];

    CorrectDispX1[opt___]:=Module[{n,pex,cx,{tol,fact}={Tolerance,Factor}/.{opt}/.exdef},
      pex=dEXB[];
      cx=LinearSolve[Transpose[trmex1*Twiss["PEX",pcex1]
        -trmex2*Twiss["PEX",pcex2]],pex,Tolerance->tol]*fact;
      n=Ceiling[Max[Abs[cx]]/dkstep];
      CorrStabA[pcex1,pcex2,cx,"K1",n,"EX1",{EmitY,1e8,Pi/2,False}];
      Sqrt[(Plus@@(Twiss["PEX",pemon]-erefex)^2)/npemon]
      ];

    CorrectDispY[opt___]:=Module[{},
      If[dEXBA[]<aerefex*exthre,
        CorrectDispYi[opt],
        CorrectDispY1[opt]]];

    CorrectDispYi[opt___]:=Module[{n,pey=dEYB[],cy,
      {tol,fact,eyth}={Tolerance,Factor,EYAThreshold}/.{opt}/.eydef},
      cy=Solve["EY",pey,tol]*fact;
      n=Ceiling[Max[Abs[cy]]/dkstep];
      If[dEYBA[]>eyth,
        CorrStabA[pcey1,pcey2,-cy,"SK1",n,"EYi",{EmitY,1e10,Pi,False}],
        CorrStabA[pcey1,pcey2,-cy,"SK1",n,"EYi",{EmitYL,1,Pi/2,False}]];
      Sqrt[(Plus@@(Twiss["PEY",pemon])^2)/npemon]
      ];

    CorrectDispY1[opt___]:=Module[{n,pey=dEYB[],cy,{tol,fact}={Tolerance,Factor}/.{opt}/.eydef},
      cy=LinearSolve[Transpose[trmey1*Twiss["PEX",pcey1]
        -trmey2*Twiss["PEX",pcey2]],pey,Tolerance->tol]*fact;
      n=Ceiling[Max[Abs[cy]]/dkstep];
      CorrStabA[pcey1,pcey2,-cy,"SK1",n,"EY1",{EmitY,1e8,Pi/2,False}];
      Sqrt[(Plus@@(Twiss["PEY",pemon])^2)/npemon]
      ];

    ];

  CheatSY[]:=Module[{p=LINE["POSITION","SY*"],pc=LINE["POSITION","SC*"],dx,dy},
    {dx,dy}=Twiss[{"DX","DY"},pc];
    dx=Flatten[Thread[{dx,dx}]]-LINE["DX",p];
    dy=Flatten[Thread[{dy,dy}]]-LINE["DY",p];
    CorrStab1[p,dx,"DX",8,"SY"];
    CorrStab1[p,dy,"DY",8,"SY"];
    0];

  CheatXY[]:=Module[{dys,fp=Flatten[pcxy],sk1s,r=1,s=1,sk10},
    sk1s=(Plus@@[LINE["SK1",pcxy],{1}])/2;
    dys=LINE["K2",pcxy[[,1]]]*(-Plus@@[(Twiss["DY",pcxy+0.5]-LINE["DY",pcxy]),{1}])/2-sk1s;
    dys=Flatten[Thread[{dys,dys}]];
    sk10=LINE["SK1",fp];
    While[s,
      LINE["SK1",fp]=sk10+dys*r;
      Calc[];
      s=~?STABLE;
      If[s,
        r=r/2;
        If[r<0.1,LINE["SK1",fp]=sk10;Calc[];Return[0]]]];
    r];

  Clear[]:=(
    LINE["DX",pall]*=0;
    LINE["DY",pall]*=0;
    LINE["K0",pcdx]*=0;
    LINE["SK0",pcdy]*=0;
    LINE["K1",pallsext]*=0;
    LINE["SK1",pallsext]*=0;
    dzi*=0;
    );

  GetCorr[]:={
    LINE["DX",pall],
    LINE["DY",pall],
    LINE["K0",pcdx],
    LINE["SK0",pcdy],
    LINE["K1",pallsext],
    LINE["SK1",pallsext],
    dzi};

  Restore[c_,cal_:Null[]]:=(
    LINE["DX",pall]=c[[1]];
    LINE["DY",pall]=c[[2]];
    LINE["K0",pcdx]=c[[3]];
    LINE["SK0",pcdy]=c[[4]];
    LINE["K1",pallsext]=c[[5]];
    LINE["SK1",pallsext]=c[[6]];
    dzi=c[[7]];
    Calc[cal]);

  Calc[out_:Null[]]:=(
    LINE[{"DX","DPX","DY","DPY"},1]=dzi;
    FFS["CALC NOEXP;",out];
    If[~?STABLE,FFS["dxi 0 ;dpxi 0; dyi 0;dpyi 0;calc noexp;",out]];
    If[?STABLE,
      dzi=Twiss[{"DX","DPX","DY","DPY"},1],
      LINE[{"DX","DPX","DY","DPY"},1]=dzi];
    dzi
    );

  AdjustTuneF[]:=Module[{s0=GetCorr[],c0=CONVERGENCE,
    mu0=(Calc[];
      If[?STABLE,Twiss[{"NX","NY"},"$$$"],
        {mux,muy}]),dmu,k,n},
    Type["AdjustTuneF ",mu0/2/Pi];
    dmu={mux,muy}-mu0;
    If[Max[Abs[dmu]]>Pi/2,
      StandardForm[FFS["rej total;fix *;free s{fd}* sk1;\
       fit nx "//mux/2/Pi//
         " ny "//muy/2/Pi//
           ";cal leng-;cell;go noexp;",output]];
      If[?CONV,
        dzi=Twiss[{"DX","DPX","DY","DPY"},1];
        CONVERGENCE=c0;
        Return[]];
      If[~?STABLE,
        FFS["REC;"];
        LINE[{"DX","DPX","DY","DPY"},1]=dzi];
      FFS["free s{fd}* k1;go noexp;",output];
      If[?CONV,
        dzi=Twiss[{"DX","DPX","DY","DPY"},1];
        CONVERGENCE=c0;
        Return[]];
      mu0=Twiss[{"NX","NY"},"$$$"];
      dmu={mux,muy}-mu0;
      If[Max[Abs[dmu]]>Pi/2,
        Restore[s0];
        Return[]]];
    n=Min[Ceiling[Max[Abs[dmu]]/(0.01 Pi)],10];
    dmu/=n;
    CONVERGENCE=1e-4;
    Do[
      LINE[{"DX","DPX","DY","DPY"},1]=dzi;
      StandardForm[FFS["rej total;fix *;free s{fd}* k1;\
       fit nx "//(mu0[[1]]+dmu[[1]]*k)/2/Pi//
         " ny "//(mu0[[2]]+dmu[[2]]*k)/2/Pi//
           ";cal leng-;cell;go noexp;",output]];
      If[~?CONV,
        FFS["FREE S{FD}* SK1; GO NOEXP",output]];
      If[~?STABLE,
        Restore[s0];
        LINE[{"DX","DPX","DY","DPY"},1]=dzi;
        CONVERGENCE=c0;
        Return[0]];
      s0=GetCorr[];
      dzi=Twiss[{"DX","DPX","DY","DPY"},1],
      {k,n}];
    CONVERGENCE=c0;
    1];

  AdjustTune2[]:=Module[{n0=Twiss[{"NX","NY"},"$$$"],dk,dk1,d=1e-6,k0=LINE["K1",psext],dz=dzi},
    dk1=If[FractionalPart[#/Pi]<0.5, d, -d]&/@n0;
    m=Transpose[MapThread[(
      LINE["K1",#]+=#2;
      Calc[];
      LINE["K1",psext]=k0;
      Twiss[{"NX","NY"},"$$$"]-n0)&,{{psf,psd},dk1}]/dk1];
    dzi=dz;
    Calc[];
    If[~?STABLE,Type["AT2 Unstable"];Return[0]];
    dk=LinearSolve[m,n0-{mux,muy}];;
    Type["AT2 ",{m,dk,(n0-{mux,muy})/2/Pi}];
    CorrStab2[psf,-dk[[1]],"K1",psd,-dk[[2]],"K1",4,"Tune2",{0&,1,Pi,False}];
    Return[?STABLE]];

  AdjustTune1[mu1_:{mux,muy}]:=Module[{dmu,dk,n},
    Calc[];
    If[?STABLE,
      dmu=Twiss[{"NX","NY"},"$$$"]-mu1;
      dk=LinearSolve[{cxybx^2,-cxyby^2},dmu,Tolerance->ntol];
      n=Ceiling[Max[Abs[dk]]/dkstep];
      CorrStabS[pcxy1,pcxy2,-dk,"K1",4,"Tune1",{EmitY,1e10,Pi,False}];
      Return[?STABLE]];
    0
    ];

  AdjustTune1X[mu1_:{mux,muy}]:=Module[{dmu,dk},
    Calc[];
    If[?STABLE,
      dmu=Twiss[{"NX","NY"},"$$$"]-mu1;
      dk=LinearSolve[{cxybx^2,-cxyby^2},dmu,Tolerance->ntol];
      CorrStabS[pcxy1,pcxy2,-dk,"K1",1,"Tune1",{0&,1,Pi,False}];
      Return[?STABLE]];
    0
    ];

  AdjustTune[fact_:tunedamp]:=Module[{c0,{dnx,dny}={1,1},conv=CONVERGENCE,
    mx,my,nx1,ny1,dz=Calc[],v0=FFS["fix * ;free "//qtune//"; VAR"],r=?STABLE,tol=0.001},
    If[?STABLE,
      Do[
        c0=GetCorr[];
        {mx,my}=Twiss[{"NX","NY"},"$$$"];
        {dnx,dny}=(Mod[{mx-mux,my-muy}/2/Pi+0.5,1]-0.5)*fact;
        Clear[];
        FFS["dxi 0 ;dpxi 0; dyi 0;dpyi 0;calc noexp;"];
        n01={nx1,ny1}=Twiss[{"NX","NY"},"$$$"]/2/Pi;
        FFS["rej total;dxi 0 dpxi 0 dyi 0 dpyi 0;\
         fit nx "//(dn01x=nx1-dnx)//"; fit ny "//(dn01y=ny1-dny)//";\
         fit ip ax 0 ay 0 bx "//bxip//"; by "// byip//";\
         fit frf ax 0 ay 0; go",6];
        LINE[{"DX","DPX","DY","DPY"},1]=dz;
        Restore[c0,6];
        If[?STABLE,
          Return[AdjustTune1[]]];
        olx@Reset[v0];
        dzi=dz;
        Calc[output];
        fact/=2,
        {3}]];
    0
    ];

  BootOrbitY[dy_,dys_,ns_,opt___]:=Module[{dy1,n,cy1,dyn=dy/ns,r},
    ResetCorrY[];
    Align[DY->dyn,SDY->dys/ns];
    dy1=LINE["DY",pall];
    If[~AdjustTune[],Type["BootOrbitY: Unstable ",dyn];Return[0]];
    CorrectOrbitY[opt];
    If[~AdjustTune[],Type["BootOrbitY: Unstable correction ",dyn];Return[0]];
    cy1=LINE["SK0",pcdy];
    Do[
      LINE["DY",pall]=dy1*n;
      LINE["SK0",pcdy]=cy1*n;
      Type["Boot Try ",dyn*n];
      Do[
        If[r=AdjustTune[],Break[]];
        LINE["DY",pall]=dy1*(n-1);
        LINE["SK0",pcdy]=cy1*(n-1);
        Type["Boot Retry ",dyn*(n-1)];
        If[~AdjustTune[],
          Type["BootOrbitY: Unstable tune ",dyn*(n-1)];Return[0]];
        CorrectOrbitY[opt];
        Type["Boot Correction ",dyn*(n-1)];
        If[~AdjustTune[],
          Type["BootOrbitY: Unstable correction ",dyn*(n-1)];
          LINE["DY",pall]=dy1*(n-1);
          LINE["SK0",pcdy]=cy1*(n-1);
          Return[0]];
        cy1=LINE["SK0",pcdy]/(n-1);
        LINE["DY",pall]=dy1*n;
        LINE["SK0",pcdy]=cy1*n;
        Type["Boot Retry after correction ",dyn*n];
        r=AdjustTune[],
        {3}];
      If[~r,
        Type["BootOrbitY: Unstable re-correction ",dyn*n];Return[0]],
      {n,2,ns}];
    1];

  dTuneA[mu0_:{mux,muy}]:=Module[{mu=Twiss[{"NX","NY"},"$$$"],fmu},
    Which[
      Max[Abs[Floor[mu/Pi]-Floor[mu0/Pi]]]>0,10*Pi,
      (fmu=FractionalPart[mu/Pi])<dstop || fmu>1-dstop,8*Pi,
      True,
      Max[Abs[mu-mu0]]]];

  BootSext[dxy_,dxys_,dxym_,dr0_,opt___]:=Module[
    {k2s,n,s,s0,k2sy,cs=0.05,dxya,na=4,r,r0,r00,dr=dr0,drmax=0.1,k1,
      mu0={mux,muy},mu1,ncmax=Floor[10/dr0],nc=0,dz},
    mu1=(Floor[mu0/2/Pi]+dmu1)*2*Pi;
    ResetCorrY[];
    Align[
      DX->dxy[[1]],SDX->dxys[[1]],MSDX->dxym[[1]],
      DY->dxy[[2]],SDY->dxys[[2]],MSDY->dxym[[2]]];
    dxya=LINE[{"DX","DY"},paelm]/na;
    k2s=LINE["K2",psext];
    k2sy=LINE["K2",psysext];
    LINE["K2",psext]*=0;
    LINE["K2",psysext]*=0;
    LINE[{"DX","DY"},paelm]=dxya;
    If[~AdjustTune1X[],Type["BootSextY: Unstable 0: ",1];Return[0]];
    StableEmit[];
    If[~AdjustTune1X[],Type["BootSextY: Unstable correction 0: ",1];Return[0]];
    k1=LINE[{"K0","SK0"},pcdy];
    Do[
      LINE[{"DX","DY"},paelm]=dxya*n;
      LINE[{"K0","SK0"},pcdy]=k1*n;
      If[~AdjustTune1X[],
        LINE[{"DX","DY"},paelm]=dxya*(n-1);
        LINE[{"K0","SK0"},pcdy]=k1*(n-1);
        If[~AdjustTune1X[];Type["BootSextY: Unstable 0: ",n-1];Return[0]];
        StableEmit[];
        If[~AdjustTune1X[],Type["BootSextY: Unstable correction 0: ",n-1];Return[0]];
        sk1=LINE[{"K0","SK0"},pcdy]/(n-1);
        LINE[{"DX","DY"},paelm]=dxa*n;
        LINE[{"K0","SK0"},pcdy]=k1*n;
        If[~AdjustTune1X[];Type["BootSextY: Unstable after correction 0: ",n];Return[0]]],
      {n,2,na}];
    {mux,muy}=mu1;
    StableEmit[];
    AdjustTune1X[];
    ScanThread[(
      r0=0;
      dr=#3;
      While[0<=r0<1,
        r=Min[1,r0+dr];
        s0=GetCorr[];
        r00=r0;
        Type["BootSext Try ",#4," ",r," ",dTuneA[]];
        LINE["K2",#2]=#*r;
        dz=dzi;
        If[nc++;s=AdjustTune1X[],
          If[dTuneA[]>tath,
            Do[
              AlignSextS[];
              StableEmit[];
              AdjustTuneF[];
              If[dTuneA[]<tath,Break[]],
              {5}]];
          If[dTuneA[]>Pi,
            Restore[s0];
            r=r00;
            dr/=2,
            dr=Min[drmax,dr*2]];
          r0=r,

          If[nc>ncmax,Return[0]];
          Do[
            r=(r+r0)/2;
            LINE["K2",#2]=#*r;
            s=AdjustTune1X[];
            If[s,Break[]],
            {5}];
          dr=Max[dr0/16,dr/2];
          If[~s,r0=r-dr/1.5;
            dzi=dz;
            Continue[]];
          Type["BootSextY Correction ",#4," ",r];
          AlignSextS[];
          StableEmit[];
          If[~AdjustTuneF[],
            Restore[s0];
            r-=dr/1.5];
          r0=r]];
      If[r0<0,Return[0]];
      )&,
      {{k2sy,k2s},{psysext,psext},{dr,dr/2},{"SY","SFD"}}];
    {mux,muy}=mu0;
    AdjustTuneF[];
    1
    ];

  Draw[]:=If[output,FFS["draw dx & dy & pex & pey & detr q*;"]];

  EmitY[]:=Module[{{ey,stab}=({Emittances,Stable}/.
    Emittance[SaveEMIT->False,ExpandElementValues->False,
      InitialOrbit->Join[dzi,{0,0}]]/.NaN->1e100)},
    If[~stab,Return[1e10]];
    If[ey[[1]]>0 && ey[[2]]>0 && ey[[3]]>0,ey[[2]],Abs[ey[[2]]]*1e20,1e10]];

  EmitYL[]:=Module[{ey=EmitY[]},
    If[ey>1,ey,
      Min[ey,emiygoal*1000]]];

  CorrXYF[lim_,n_:5]:=
    Do[
      If[dXYA[]<0.2,Break[]];
      CorrectXY[CheckEmit->False],
      {n}];
  
  CorrNF[lim_,n_:5]:=
    Do[
      If[dNA[]<0.2,Break[]];
      CorrectNi[CheckEmit->False],
      {n}];

  CorrXYNF[lim_,n_:5]:=Module[{r},
    Do[
      If[~Plus@@{CorrectXY[CheckEmit->False],
        CorrectNi[CheckEmit->False]},Break[]];
      If[r={dXYA[],dNA[]};Plus@@r<lim,Break[]];
      Draw[],
      {n}];
    Type["CorrXYNF: ",r];
    r];

  StableEmit[]:=Module[{ey},
    Do[
      If[CorrectOrbitXi[]+CorrectOrbitYi[],
        If[(ey=EmitY[])<1,Return[ey]],
        Return[EmitY[]]],
      {10}];
    ey];

  CorrectableN[]:=Module[{},
    Do[
      If[CorrectNi[],Return[]];
      AlignSextS[];
      AdjustTune1[];
      StableEmit[];
      If[CorrectNi[],Return[]];
      CorrectXY[];
      StableEmit[],
      {3}]];
  
  LET1[tx_,ty_,tc_]:=Module[{},
    CorrectXY[Tolerance->tc,Factor->1,CheckEmit->True,XYAThreshold->0.2];
    CorrectNi[CheckEmit->True,NAThreshold->0.12];
    Draw[];
    CorrectDispX[Tolerance->tx,Factor->1];
    Draw[];
    CorrectDispY[Tolerance->ty,Factor->1];
    Draw[];
    EmitY[]];

  LET2[tx0_,ty0_,tc0_,hist0_]:=Module[{ey0=Infinity,ey=EmitY[],s1=GetCorr[],ty=ty0,tc=tc0,tx=tx0,
    tth1=0.8,tth2=0.96,tth3=1.1,red=0.95,diffs,hist=hist0},
    While[True,
      If[ey<ey0,
        ey0=ey,
        s1=GetCorr[]];
      ey=Catch[LET1[tx,ty,tc]];
      If[ListQ[ey],
        Type["LET Unstable ",ey[[2]]];
        Return[{0,0,0,0,hist}]];
      If[ey<emiymin,
        emiymin=ey;
        smin=GetCorr[]];
      StandardForm[$FORM="7.5";
        Print["** LET2: Seed:",StandardForm[("   "//seed)[-3,-1]]," EmitY =",{ey*1e12,ey/ey0}," Tols =",{tx,ty,tc},
          " dEX,EY,XY,N =",diffs={dEXBA[],dEYBA[],lastdxy,dNA[]}]];
      AppendTo[hist,{ey,ey/ey0,{tx,ty,tc},diffs}];
      Which[ey<=emiygoal || (ey<emiygoal*10 || ty<=ty0/1.9) && ey==ey0 || ey>1,
        Return[{ey,tx,ty,tc,hist}],

        ey>ey0*tth3,
        Restore[s1];
        Return[{ey0,tx,ty,tc,hist}],

        tth2*ey0>=ey>tth1*ey0,
        tx=Max[tx/redtx,txmin];
        ty=Max[ty/redty,tymin];
        tc=Max[tc/redtc,tcmin],

        ey>ey0*tth2,
        If[ty<=tymin,ey0=ey0*red];
        tx=Max[tx/redtx,txmin];
        ty=Max[ty/redty,tymin];
        tc=Max[tc/redtc,tcmin]]]];

  LET[tx0_,ty0_,tc0_,n_:200]:=Module[
    {tx=tx0,ty=ty0,ey=Infinity,ey0,tx1,ty1,tc1,tc=tc0,s,nc=0,ncmax=10,
      tox=dxtol,toy=dytol,hist={}},
    AlignSextS[];
    StableEmit[];
    Draw[];
    emiymin=Infinity;
    ey0=0;
    While[Length[hist]<n,
      ey=StableEmit[];
      CorrXYNF[0.1,5];
      Draw[];
      ey0=EmitY[];
      {ey,tx1,ty1,tc1,hist}=LET2[tx,ty,tc,hist];
      If[ey<=emiygoal,
        emiymin=ey;
        Break[]];
      AlignSextS[];
      CorrectOrbitXi[];
      CorrectOrbitYi[];
      If[StableEmit[]>=1,
        Restore[smin];
        CorrXYNF[0.06,Min[++nc,ncmax]];
        ey0=emiymin];
      ty=Min[ty0,ty1*4];
      tc=Min[tc0,tc1*4]];
    Restore[smin];
    Draw[];
    {seed,emiymin,hist}
    ];

  Simulate[seed0_,{delxy_,dsextxy_,dmon_},{tx_,ty_,tc_},n_:200,bstep_:0.05]:=Module[{r},
    seed=seed0;
    SeedRandom[seed];
    r=BootSext[delxy,dsextxy,dmon,bstep];
    Draw[];
!    Return[0];
    If[r,
      Check[LET[tx,ty,tc,n],{EmitY[],{}}],
      {EmitY[],{}}]];

  ];
