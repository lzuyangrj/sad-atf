
  ADrawRingOrbit[bpmlist_:{},xdp_:0,xdx_:0,xdxp_:0,ydy_:0,ydyp_:0]:=Module[{f},
    DXI=xdx;DPXI=xdxp;DYI=ydy;DPYI=ydyp;
    DP0=xdp;
    FFS["CALC"];
        f="./output/plotdata_ringorbit";
    frame1={{0.15,0.85},{0.55,0.9}};
    text1={{"s(m)",0.4,0.5,0},{"DX(m)",0.045,0.7,90}};  
    frame2={{0.15,0.85},{0.1,0.45}};
    text2={{"s(m)",0.4,0.05,0},{"DY(m)",0.045,0.25,90}}; 
    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    

    x1=Map[(LINE["S",#])&,iplot];
    y11=Map[(Twiss["DX",#])&,iplot];
    data11={1,0,x1,y11};
    range1={{0,LINE["S",$$$]},{Min[y11]-0.1*(Max[y11]-Min[y11]),Max[y11]+0.1*(Max[y11]-Min[y11])}};
    data1={{data11},range1,frame1,text1};

    y21=Map[(Twiss["DY",#])&,iplot];
    data21={1,0,x1,y21};
    range2={{0,LINE["S",$$$]},{Min[y21]-0.1*(Max[y21]-Min[y21]),Max[y21]+0.1*(Max[y21]-Min[y21])}};
    data2={{data21},range2,frame2,text2};

    If[Length[bpmlist]>0, 
     x2=Map[(LINE["S",#[1]])&,bpmlist];
     y12=Map[(#[2])&,bpmlist];
     data12={0,2,x2,y12};
    range1={{0,LINE["S",$$$]},{Min[Join[y11,y12]]-0.1*(Max[Join[y11,y12]]-Min[Join[y11,y12]]),Max[Join[y11,y12]]+0.1*(Max[Join[y11,y12]]-Min[Join[y11,y12]])}};
    data1={{data11,data12},range1,frame1,text1};

     y22=Map[(#[3])&,bpmlist];
     data22={0,2,x2,y22};
    range2={{0,LINE["S",$$$]},{Min[Join[y21,y22]]-0.1*(Max[Join[y21,y22]]-Min[Join[y21,y22]]),Max[Join[y21,y22]]+0.1*(Max[Join[y21,y22]]-Min[Join[y21,y22]])}};
    data2={{data21,data22},range2,frame2,text2};
    ];

!    Print[range1];
!    Print[frame1];
!    Print[range2];
!    Print[frame2];
    
    DrawGraph[{data1,data2},f];
   ];



  DrawCOD[bpmlist_:{},dp_:0]:=Module[{f},
!    Print["DrawCOD a1",dp];
    DP0=dp;
!    Print["DrawCOD aa"];
    FFS["CALC"];
        f="./output/plotdata_cod";

    frame1={{0.15,0.85},{0.55,0.9}};
    text1={{"s(m)",0.4,0.5,0},{"DX(m)",0.045,0.7,90}};  
    frame2={{0.15,0.85},{0.1,0.45}};
    text2={{"s(m)",0.4,0.05,0},{"DY(m)",0.045,0.25,90}}; 
    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    
    x1=Map[(LINE["S",#])&,iplot];
    y11=Map[(Twiss["DX",#])&,iplot];
    data11={1,0,x1,y11};
    range1={{0,LINE["S",$$$]},{Min[y11]-0.1*(Max[y11]-Min[y11]),Max[y11]+0.1*(Max[y11]-Min[y11])}};
    data1={{data11},range1,frame1,text1};

    y21=Map[(Twiss["DY",#])&,iplot];
    data21={1,0,x1,y21};
    range2={{0,LINE["S",$$$]},{Min[y21]-0.1*(Max[y21]-Min[y21]),Max[y21]+0.1*(Max[y21]-Min[y21])}};
    data2={{data21},range2,frame2,text2};

    If[Length[bpmlist]>0, 
     x2=Map[(LINE["S",#[1]])&,bpmlist];
     y12=Map[(#[2])&,bpmlist];
     data12={0,2,x2,y12};
     ymin=Min[Join[y11,y12]];
     ymax=Max[Join[y11,y12]];
     range1={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
!!    range1={{0,LINE["S",$$$]},{Min[Join[y11,y12]]-0.1*(Max[Join[y11,y12]]-Min[Join[y11,y12]]),Max[Join[y11,y12]]+0.1*(Max[Join[y11,y12]]-Min[Join[y11,y12]])}};
    data1={{data11,data12},range1,frame1,text1};

     y22=Map[(#[3])&,bpmlist];
     data22={0,2,x2,y22};
    ymin=Min[Join[y21,y22]];
    ymax=Max[Join[y21,y22]];
    range2={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
!!    range2={{0,LINE["S",$$$]},{Min[Join[y21,y22]]-0.1*(Max[Join[y21,y22]]-Min[Join[y21,y22]]),Max[Join[y21,y22]]+0.1*(Max[Join[y21,y22]]-Min[Join[y21,y22]])}};
    data2={{data21,data22},range2,frame2,text2};
    ];

    DrawGraph[{data1,data2},f];

   ];


  exADrawRingOrbit[bpmlist_,xdp_:0,xdx_:0,xdxp_:0,ydy_:0,ydyp_:0]:=ADrawRingOrbit[bpmlist,xdp,xdx,xdxp,ydy,ydyp];

 

  DrawDispersion[bpmlist_]:=Module[{f},
    f="./output/plotdata_dispersion";

    frame1={{0.15,0.85},{0.55,0.9}};
    text1={{"s(m)",0.4,0.5,0},{"EX(m)",0.045,0.7,90}};  
    frame2={{0.15,0.85},{0.1,0.45}};
    text2={{"s(m)",0.4,0.05,0},{"EY(m)",0.045,0.25,90}}; 
    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    
    x1=Map[(LINE["S",#])&,iplot];
    y11=Map[(Twiss["EX",#])&,iplot];
    data11={1,0,x1,y11};
    range1={{0,LINE["S",$$$]},{Min[y11]-0.1*(Max[y11]-Min[y11]),Max[y11]+0.1*(Max[y11]-Min[y11])}};
    data1={{data11},range1,frame1,text1};

    y21=Map[(Twiss["EY",#])&,iplot];
    data21={1,0,x1,y21};
    range2={{0,LINE["S",$$$]},{Min[y21]-0.1*(Max[y21]-Min[y21]),Max[y21]+0.1*(Max[y21]-Min[y21])}};
    data2={{data21},range2,frame2,text2};

    If[Length[bpmlist]>0, 
     x2=Map[(LINE["S",#[1]])&,bpmlist];
     y12=Map[(#[2])&,bpmlist];
     data12={0,2,x2,y12};
     ymin=Min[Join[y11,y12]];
     ymax=Max[Join[y11,y12]];
     range1={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data1={{data11,data12},range1,frame1,text1};

     y22=Map[(#[3])&,bpmlist];
     data22={0,2,x2,y22};
     ymin=Min[Join[y21,y22]];
     ymax=Max[Join[y21,y22]];
     range2={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data2={{data21,data22},range2,frame2,text2};
    ];

    DrawGraph[{data1,data2},f];
   ];



  DrawEXTPhDispersion[bpmlist_]:=Module[{f},
    f="./output/plotdata_extphdispersion";
    bpmlistw=Select[bpmlist,(LINE["S",#[1]]>LINE["S","QF5X"])&];

    frame1={{0.15,0.85},{0.6,0.9}};
    text1={{"s(m)",0.4,0.55,0},{"PEX(m)",0.045,0.7,90},{Map[#[1]&,bpmlistw],0.1,0.51},{Map[#[2]&,bpmlistw],0.1,0.48}}; 
    frame2={{0.15,0.85},{0.1,0.4}};
    text2={{"s(m)",0.4,0.07,0},{"PEY(m)",0.045,0.25,90},{Map[#[1]&,bpmlistw],0.1,0.05},{Map[#[3]&,bpmlistw],0.1,0.02}}; 
    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    
    x1=Map[(LINE["S",#])&,iplot];
    y11=Map[(Twiss["PEX",#])&,iplot];
    data11={1,0,x1,y11};
    range1={{0,LINE["S",$$$]},{Min[y11]-0.1*(Max[y11]-Min[y11]),Max[y11]+0.1*(Max[y11]-Min[y11])}};
    data1={{data11},range1,frame1,text1};

    y21=Map[(Twiss["PEY",#])&,iplot];
    data21={1,0,x1,y21};
    range2={{0,LINE["S",$$$]},{Min[y21]-0.1*(Max[y21]-Min[y21]),Max[y21]+0.1*(Max[y21]-Min[y21])}};
    data2={{data21},range2,frame2,text2};

    If[Length[bpmlist]>0, 
     x2=Map[(LINE["S",#[1]])&,bpmlist];
     y12=Map[(#[2])&,bpmlist];
     data12={0,2,x2,y12};
     ymin=Min[Join[y11,y12]];
     ymax=Max[Join[y11,y12]];
     range1={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data1={{data11,data12},range1,frame1,text1};

     y22=Map[(#[3])&,bpmlist];
     data22={0,2,x2,y22};
     ymin=Min[Join[y21,y22]];
     ymax=Max[Join[y21,y22]];
     range2={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data2={{data21,data22},range2,frame2,text2};
    ];

    DrawGraph[{data1,data2},f];
   ];


  DrawEXTDispersion[bpmlist_]:=Module[{f,fn,i},
    f="./output/plotdata_extdispersion";
    bpmlistw=Select[bpmlist,(LINE["S",#[1]]>LINE["S","QF5X"])&];

    frame1={{0.15,0.85},{0.6,0.9}};
    text1={{"s(m)",0.4,0.55,0},{"EX(m)",0.045,0.7,90},{Map[#[1]&,bpmlistw],0.1,0.51},{Map[#[2]&,bpmlistw],0.1,0.48}}; 
    frame2={{0.15,0.85},{0.1,0.4}};
    text2={{"s(m)",0.4,0.07,0},{"EY(m)",0.045,0.25,90},{Map[#[1]&,bpmlistw],0.1,0.05},{Map[#[3]&,bpmlistw],0.1,0.02}}; 
    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    
    x1=Map[(LINE["S",#])&,iplot];
    y11=Map[(Twiss["EX",#])&,iplot];
    data11={1,0,x1,y11};
    range1={{0,LINE["S",$$$]},{Min[y11]-0.1*(Max[y11]-Min[y11]),Max[y11]+0.1*(Max[y11]-Min[y11])}};
    data1={{data11},range1,frame1,text1};

    y21=Map[(Twiss["EY",#])&,iplot];
    data21={1,0,x1,y21};
    range2={{0,LINE["S",$$$]},{Min[y21]-0.1*(Max[y21]-Min[y21]),Max[y21]+0.1*(Max[y21]-Min[y21])}};
    data2={{data21},range2,frame2,text2};

    If[Length[bpmlist]>0, 
     x2=Map[(LINE["S",#[1]])&,bpmlist];
     y12=Map[(#[2])&,bpmlist];
     data12={0,2,x2,y12};
     ymin=Min[Join[y11,y12]];
     ymax=Max[Join[y11,y12]];
     range1={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data1={{data11,data12},range1,frame1,text1};

     y22=Map[(#[3])&,bpmlist];
     data22={0,2,x2,y22};
     ymin=Min[Join[y21,y22]];
     ymax=Max[Join[y21,y22]];
     range2={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data2={{data21,data22},range2,frame2,text2};
    ];

    DrawGraph[{data1,data2},f];
   ];


  DrawPhDispersion[bpmlist_]:=Module[{f,fn,i},
    f="./output/plotdata_phdispersion";

    frame1={{0.15,0.85},{0.55,0.9}};
    text1={{"s(m)",0.4,0.5,0},{"PEX(m)",0.045,0.7,90}};  
    frame2={{0.15,0.85},{0.1,0.45}};
    text2={{"s(m)",0.4,0.05,0},{"PEY(m)",0.045,0.25,90}}; 
    iplot=Select[Table[i,{i,1,LINE["LENGTH"]}],(LINE["L",#]>1e-3)&];
    
    x1=Map[(LINE["S",#])&,iplot];
    y11=Map[(Twiss["PEX",#])&,iplot];
    data11={1,0,x1,y11};
    range1={{0,LINE["S",$$$]},{Min[y11]-0.1*(Max[y11]-Min[y11]),Max[y11]+0.1*(Max[y11]-Min[y11])}};
    data1={{data11},range1,frame1,text1};

    y21=Map[(Twiss["PEY",#])&,iplot];
    data21={1,0,x1,y21};
    range2={{0,LINE["S",$$$]},{Min[y21]-0.1*(Max[y21]-Min[y21]),Max[y21]+0.1*(Max[y21]-Min[y21])}};
    data2={{data21},range2,frame2,text2};

    If[Length[bpmlist]>0, 
     x2=Map[(LINE["S",#[1]])&,bpmlist];
     y12=Map[(#[2])&,bpmlist];
     data12={0,2,x2,y12};
     ymin=Min[Join[y11,y12]];
     ymax=Max[Join[y11,y12]];
     range1={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data1={{data11,data12},range1,frame1,text1};

     y22=Map[(#[3])&,bpmlist];
     data22={0,2,x2,y22};
     ymin=Min[Join[y21,y22]];
     ymax=Max[Join[y21,y22]];
     range2={{0,LINE["S",$$$]},{ymin-0.1*(ymax-ymin),ymax+0.1*(ymax-ymin)}};
     data2={{data21,data22},range2,frame2,text2};
    ];

    DrawGraph[{data1,data2},f];
   ];

