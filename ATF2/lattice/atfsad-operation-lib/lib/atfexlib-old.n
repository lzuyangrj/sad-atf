  bendex=Element["Name","BH*X"];
  quadex=Element["Name","Q*X"];
  sthex=Element["Name","ZH*X"];
  stvex=Element["Name","ZV*X"];
  stxex=Element["Name","ZX*X"];
  Scan[(stname[#]=#)&,Join[sthex,stvex]];
  Scan[(bname[#]=#)&,Join[sthex,stvex]];
  bname["ZX1X"]="BH1X.1";bname["ZX2X"]="BH1X.2";bname["ZX3X"]="BH1X.3";
  bname["ZX4X"]="BH2X.1";bname["ZX5X"]="BH2X.2";
  stname["BH1X.1"]="ZX1X";stname["BH1X.2"]="ZX2X";stname["BH1X.3"]="ZX3X";
  stname["BH2X.1"]="ZX4X";stname["BH2X.2"]="ZX5X";
! ---- Added by T.Okugi (2001/05/09) -----
  quaddr=Element["Name","QM*R"];
  qdrex=Element["Name","QDR*"];
  Scan[(qexname[#]=#)&,Join[quadex,quaddr]];
  qexname["QM6R"]="QDR6";
  qexname["QM7R"]="QDR7";
! ----------------------------------------
  pairedQ={{"QD8X","QD9X"}};

  SetExtQuad[n_:1]:=Module[{
    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{}},
! --- Modified by T.Okugi (2001/05/09) ---
!    k1list=Select[k1list,MemberQ[quadex,#[1]]&];
!    Scan[Check[Element["K1",#[1]]=#[2],]&,k1list]];
    k1list=Map[{qexname[#[[1]]],#[[2]]}&,k1list];
    k1list=Select[k1list,(MemberQ[quadex,#[1]] || MemberQ[qdrex,#[1]])&];Print[k1list];
    Scan[Check[LINE["K1",#[1]]=#[2],]&,k1list]];
! ----------------------------------------
  SetExtSteer[n_:1]:=Module[{
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{}},
    k0list=Map[{stname[#[[1]]],#[[2]]}&,k0list];
    k0list=Select[k0list,(MemberQ[sthex,#[1]] || MemberQ[stvex,#[1]] || MemberQ[stxex,#[1]])&];
    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0list]];
!  SetExtBend[n_:1]:=Module[{
!    k0blist=ATFBend/.ATFData[n]/.ATFBend->{}},
!    k0blist=Select[k0list,MemberQ[bendex,#[1]]&];
!    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0blist]];

!!!!!!!
  SaveExtOptics[f_]:=Module[
        {fn=OpenWrite["./ringoptics/"//f]},
!----- Modified by T.Okugi (2001/05/09) -----
!    qv=Thread[{quadex,Element["K1",quadex]}];
    qexv=Thread[{quadex,Element["K1",quadex]}];
    qdrv=Thread[{qdrex,Element["K1",qdrex]}];
!-----------------------------------------
    sthv=Thread[{sthex,Element["K0",sthex]}];
    stvv=Thread[{stvex,Element["K0",stvex]}];
    stxv=Thread[{stxex,Element["K0",stxex]}];
!    bendv=Thread[{bendex,Element["K0",bendex]}];
    $FORM="";
    Write[fn,
!----- Modified by T.Okugi (2001/05/09) -----
!      "qvex=",qv,
      "qexvex=",qexv,
      ";\n\nqdrvex=",qdrv,
!-----------------------------------------
      ";\n\nsthvex=",sthv,
      ";\n\nstvvex=",stvv,
      ";\n\nstxvex=",stxv,
!      ";\n\nbendvex=",bendv,
    ";"];
    Close[fn];
    System["chmod 664 "//f]];

  LoadExtOptics[f_]:=Module[{},
!  {qvex,sthvex,stvvex,bendvex},
    Get["./ringoptics/"//f];
    Map[(Element["K1",#[1]]=#[2])&,qvex];
!----- Added by T.Okugi (2001/05/09) -----
    Map[(Element["K1",#[1]]=#[2])&,qexvex];
    Map[(Element["K1",#[1]]=#[2])&,qdrvex];
!-----------------------------------------
    Map[(Element["K0",#[1]]=#[2])&,sthvex];
    Map[(Element["K0",#[1]]=#[2])&,stvvex];
    Map[(Element["K0",#[1]]=#[2])&,stxvex];
!    Map[(Element["K0",#[1]]=#[2])&,bendvex];
!!    qv=Select[qv,MemberQ[quadex,#[1]]&];
!!    sthv=Select[sthv,MemberQ[sthex,#[1]]&];
!!    stvv=Select[stvv,MemberQ[stvex,#[1]]&];
  ];

  WriteExtOptics[f_,epilog_,opt___]:=Module[{qv,sthv,stvv,bendv,
        fn=OpenWrite["./output/"//f],
        def={SADFile->"",Steer->True},sadfile,steer},
    sadfile=SADFile/.{opt}/.def;
    steer=Steer/.{opt}/.def;
    Write[fn,"!"//"    "//sadfile];
    $FORM="F12.6";
    qv=Thread[{quadex,Element["K1",quadex],Element["L",quadex]}];
    sthv=Thread[{sthex,Element["K0",sthex]}];
    stvv=Thread[{stvex,Element["K0",stvex]}];
    stxv=Thread[{stxex,Element["K0",stxex]}];
    stxv=Map[{bname[#[[1]]],#[[2]]}&,stxv];
!    bendv=Thread[{bendex,Element["K0",bendex];}];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,bendv];
    If[steer,
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stxv]];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];
    Close[fn];
    System["chmod 664 "//f]];
  
 CorrectEXTOrbit[n_:1]:=Module[{steer},
    bpmlist=ATFBPM/.ATFData[n]/.ATFBPM->{};
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{};
    steer=UseSteer/.Option/.UseSteer->{};
    k0list=Select[k0list,StringMatchQ[#[[1]],"*X*"]&];
    k0list=Select[k0list,Not[StringMatchQ[#[[1]],"BS*"]]&];
    k0list=Map[{stname[#[[1]]],#[[2]]}&,k0list];
    k0list=Select[k0list,StringMatchQ[#[[1]],"Z*X"]&];
    k0uselist=Select[k0list,MemberQ[steer,#[[1]]]&];
    bpmlist=Select[bpmlist,StringMatchQ[#[[1]],"ML*X"]&];
    FFS["Z* K0 0"];
    FFS["VARY K0 Z*"];
    Scan[FFS["FREE "//#[[1]]]&,k0uselist];
    Scan[(FFS["FIT "//#[[1]]//" DX "//#[[2]]];
          FFS["FIT "//#[[1]]//" DY "//#[[3]]])&,bpmlist];
    FFS["GO"];

    exADrawRingOrbit[bpmlist];

    orbit=ATFEXTOrbit;
    dispersion=ATFEXTDispersion;
!    newk0list=Map[{#[[1]],#[[2]]-LINE["K0",#[[1]]]}&,k0list]; 
    Scan[(LINE["K0",#[[1]]]=#[[2]]-LINE["K0",#[[1]]])&,k0list];  
 ]; 


 CorrectEXTDispersion[n_:1]:=Module[{steer},
    bpmlist=ATFBPM/.ATFData[n]/.ATFBPM->{};
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{};
    steer=UseSteer/.Option/.UseSteer->{};
    k0list=Select[k0list,StringMatchQ[#[[1]],"*X*"]&];
    k0list=Select[k0list,Not[StringMatchQ[#[[1]],"BS*"]]&];
    k0list=Map[{stname[#[[1]]],#[[2]]}&,k0list];
    k0list=Select[k0list,StringMatchQ[#[[1]],"Z*X"]&];
    k0uselist=Select[k0list,MemberQ[steer,#[[1]]]&];
    bpmlist=Select[bpmlist,StringMatchQ[#[[1]],"ML*X"]&];
    fchange=FrequencyChange/.(EtaCorrect/.Option)/.(FrequencyChange ->5e3);
    momentumcomp=MomentumCompaction/.(EtaCorrect/.Option)/.(MomentumCompaction ->2e-3);
    de=-fchange/714e6/momentumcomp;
    bpmlist=Map[{#[[1]],#[[2]]/de,#[[3]]/de,#[[4]]}&,bpmlist];
    FFS["Z* K0 0"];
    FFS["VARY K0 Z*"];
    Scan[FFS["FREE "//#[[1]]]&,k0uselist];

    If[1<2,
     
     fitfpey:=Map[((Twiss["PEY",#[1]]-#[3])/1.e-3)&,bpmlist];
     fitfdy1:=Map[((Twiss["DY",#[1]])/5e-5)&,bpmlist];
     fitfdy2:=Map[((Twiss["DY",#])/5e-4)&,LINE["Name","Q*X"]];
     FitFunction:=Join[fitfpey,fitfdy1,fitfdy2];
!     Print["abc ",FitFunction];
    ,
     Scan[(FFS["FIT "//#[[1]]//" PEX "//#[[2]]];
          FFS["FIT "//#[[1]]//" PEY "//#[[3]]])&,bpmlist];
    ];

    FFS["GO"];
    orbit=ATFEXTOrbit;
    dispersion=ATFEXTDispersion;

    DrawEXTDispersion[bpmlist];

!    newk0list=Map[{#[[1]],#[[2]]-LINE["K0",#[[1]]]}&,k0list]; 
    Scan[(LINE["K0",#[[1]]]=#[[2]]-LINE["K0",#[[1]]])&,k0list];  
 ]; 


  WriteExtOpticsOrbit[optics_,orbit_,disper_,f_:"RING_ORBIT.SAD"]:=Module[{i,qv,sthv,stvv,bendv,
        fn=OpenWrite["./output/"//f]},
    $FORM="F12.6";
    qv=Thread[{quadex,Element["K1",quadex],Element["L",quadex]}];
    sthv=Thread[{sthex,Element["K0",sthex]}];
    stvv=Thread[{stvex,Element["K0",stvex]}];
    stxv=Thread[{stxex,Element["K0",stxex]}];
    stxv=Map[{bname[#[[1]]],#[[2]]}&,stxv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stxv];

    $FORM="11.7";
    Scan[(Write[fn," OPTICS ",#[[2]]," ",#[[3]]," ",#[[4]]," ",#[[5]]," ",#[[6]]])&,optics];
    Scan[(Write[fn," ORBIT ",#[[2]]," ",#[[3]]," ",#[[4]]])&,orbit];
    Scan[(Write[fn," DISPER ",#[[2]]," ",#[[3]]," ",#[[4]]])&,disper];
    Close[fn];
    $FORM="";
    Close[fn];
    System["chmod 664 "//f];
    ];



 ATFEXTOrbit:=Map[{#,LINE["S",#],Twiss["DX",#],Twiss["DY",#]}&,LINE["Name","{QSBZM}*"]];
 ATFEXTTwiss:=Map[{#,LINE["S",#],Twiss["BX",#],Twiss["BY",#],Twiss["EX",#],Twiss["EY",#]}
                    &,LINE["Name","{QSBZM}*"]];
 ATFEXTDispersion:=Map[{#,LINE["S",#],Twiss["EX",#],Twiss["EY",#]}&,LINE["Name","{QSBZM}*"]];

  Bmag[a1_,b1_,a2_,b2_]:=((b2/b1+b1/b2+b1*b2*(a1/b1-a2/b2)^2)/2);
