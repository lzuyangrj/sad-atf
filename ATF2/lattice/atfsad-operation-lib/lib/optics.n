
  AverageBeta:=Module[{},allp=LINE["Position","*"];bxsum=0;bysum=0;lsum=0;
   Scan[(bxsum=bxsum+Twiss["BX",#+0.5]*LINE["L",#];
         bysum=bysum+Twiss["BY",#+0.5]*LINE["L",#];
         lsum=lsum+LINE["L",#];
   )&,allp];
   {bxsum/lsum,bysum/lsum,lsum}];

  
  AverageOneOverBeta:=Module[{},allp=LINE["Position","*"];obxsum=0;obysum=0;lsum=0;
   Scan[(obxsum=obxsum+1/Twiss["BX",#+0.5]*LINE["L",#];
         obysum=obysum+1/Twiss["BY",#+0.5]*LINE["L",#];
         lsum=lsum+LINE["L",#];
   )&,allp];
   {obxsum/lsum,obysum/lsum,lsum}];


  DrawDispersion:=Module[{x,y,fn=OpenWrite["aa.top"],alle=LINE["Name","{QSBZM}*"]},
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["EX",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," new"];
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["EY",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   System[" tdr -v X aa.top"];
  ];  

  DrawPlotDispersion[dlist_:{}]:=Module[{x,y,fn=OpenWrite["aa.top"],alle=LINE["Name","{QSBZM}*"]},
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["EX",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," set symbol 1O"];
   Scan[(x=LINE["S",#[[1]]];If[x<1.e-3,x=0];  
         Write[fn,x," ",#[[2]]])&,dlist];
   Write[fn," plot"];
   Write[fn," new"];
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["EY",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," set symbol 1O"];
   Scan[(x=LINE["S",#[[1]]];If[x<1.e-3,x=0];  
         Write[fn,x," ",#[[3]]])&,dlist];
   Write[fn," plot"];
   System[" tdr -v X aa.top"];
  ];  

  DrawPlotDispersionE[dlist_:{}]:=Module[{x,y,fn=OpenWrite["aa.top"],alle=LINE["Name","{QSBZM}*"]},
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["EX",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," set symbol 1O"];
   Write[fn," set order x y dy"];
   Scan[(x=LINE["S",#[[1]]];If[x<1.e-3,x=0];  
         Write[fn,x," ",#[[2]]," ",#[[6]]])&,dlist];
   Write[fn," plot"];
   Write[fn," new"];
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["EY",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," set symbol 1O"];
   Write[fn," set order x y dy"];
   Scan[(x=LINE["S",#[[1]]];If[x<1.e-3,x=0];  
         Write[fn,x," ",#[[3]]," ",#[[7]]])&,dlist];
   Write[fn," plot"];
   System[" tdr -v X aa.top"];
  ];  

  DrawOrbit:=Module[{x,y,fn=OpenWrite["aa.top"],alle=LINE["Name","{QSBZM}*"]},
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["DX",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," new"];
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["DY",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   System[" tdr -v X aa.top"];
  ];  

  DrawPlotOrbit[orbit_]:=Module[{x,y,fn=OpenWrite["aa.top"],alle=LINE["Name","{QSBZM}*"]},
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["DX",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," set order x y dy"];
   Scan[(Write[fn,LINE["S",#[[1]]]," ",#[[2]]," ",#[[3]]])&,orbit];
   Write[fn," plot"];
   Write[fn," new"];
   Write[fn," set order x y"];
   Scan[(x=LINE["S",#];y=Twiss["DY",#];
         If[x<1.e-3,x=0];If[Abs[y]<1.e-12,y=0];
         Write[fn,x," ",y])&,alle];
   Write[fn," join 1"];
   Write[fn," set order x y dy"];
   Scan[(Write[fn,LINE["S",#[[1]]]," ",#[[4]]," ",#[[5]]])&,orbit];
   Write[fn," plot"];
   System[" tdr -v X aa.top"];
  ];  


 ATFRingOrbit:=Map[{#,LINE["S",#],Twiss["DX",#],Twiss["DY",#]}&,LINE["Name","{QSBZM}*"]];
 ATFRingTwiss:=Map[{#,LINE["S",#],Twiss["BX",#],Twiss["BY",#],Twiss["EX",#],Twiss["EY",#]}
                    &,LINE["Name","{QSBZM}*"]];
 

  WriteRingOpticsOrbit[optics_,orbit_]:=Module[{i,
        fn=OpenWrite["RING_ORBIT.DAT"]},
    $FORM="";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    cavv=Thread[{cav,Element["K0",cav]}];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];

    $FORM="11.7";
    Scan[(Write[fn," OPTICS ",#[[2]]," ",#[[3]]," ",#[[4]]," ",#[[5]]," ",#[[6]]])&,optics];
    Scan[(Write[fn," ORBIT ",#[[2]]," ",#[[3]]," ",#[[4]]])&,orbit];
    Close[fn];
    $FORM="";
    System["chmod 664 "//f];
  ];


 ReturnRingOpticsOrbit:=Module[{optics,orbit},
  optics=ATFRingTwiss;
  orbit=ATFRingOrbit;
  WriteRingOpticsOrbit[optics,orbit]
 ];


 SetBendLength:=(dp00=0.002;scfact=1+dp00;
               SetBendK0K1;ScaleRingK[scfact];DP0=dp00);

 ResetBendLength:=(dp00=0.002;scfact=1+dp00;ReScaleRingK[scfact]);

