
 

  ChangeOrbitAtEXTKicker:=Module[{pm,
    tunelist=ATFTune/.ATFData[1]/.ATFTune->{},
    k0list=ATFSteer/.ATFData[1]/.ATFSteer->{},
    
    mx=NSteerX/.(EtaCorrect/.Option)/.(NSteerX ->0),
    my=NSteerY/.(EtaCorrect/.Option)/.(NSteerY ->0),
    ratio=1.414,de,
!    k0listx,k0listy,dzh,dzv,usezh,usezv,A,bpmx,bpmy,dispx0,dispx1,dispy0,dispy1,
!    datazh,datazv,bpmnamelist1,maxdzh,maxdzv,mindzh,mindzv,datalist,my0,mx0,iflimit,
    etaxlist0,etaxlist1,etaylist0,etaylist1,
    maxk0,soft,dk0
    },

      FFS["Z* K0 0;"];
!    NXm=tunelist[1];NYm=tunelist[2];
!    FFS["FIT NX NXm NY NYm"];
!    FFS["FREE QF* go; cal"];
!    FFS["Reject Total;"]; 
!    FFS["FIX Q* "];    

    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    k0list=Select[k0list,MemberQ[LINE["Name","Z*R"],#[1]]&];

    maxk0=0.16138E-02;
    tolerance=1.e-8;

    dxext=0;
    dpxext=0;
    dyext=0;
    dpyext=0;
    exext=0;
    epxext=0;
    eyext=0;
    epyext=0;

    f2=Frame[wcod,Side -> "top"];
    f2dx=Frame[wcod,Side -> "top"];
    f2dy=Frame[wcod,Side -> "top"];
    f2ex=Frame[wcod,Side -> "top"];
    f2ey=Frame[wcod,Side -> "top"];



!    bbbbb2=Button[f2, Text -> "enter", Command :> TkReturn["ReturnToSAD"]];
    txtext=TextLabel[f2, Text -> " Change at EXT Kicker (in mm and mrad)",Side -> "top", PadX -> 10];

    onoffdx=CheckButton[f2dx,Text->" Set orbit X?     ",  Side -> "left", Variable :> ifextdx];
    txtdxext=TextLabel[f2dx, Text -> " DX:  ", Side -> "left"];
    entdx=Entry[f2dx,TextVariable :> dxext, Side -> "left"];
    txtdpxext=TextLabel[f2dx, Text -> " DX': ", Side -> "left"];
    entdpx=Entry[f2dx,TextVariable :> dpxext, Side -> "left"];

    onoffdy=CheckButton[f2dy,Text->" Set orbit Y?     ",  Side -> "left", Variable :> ifextdy];
    txtdyext=TextLabel[f2dy, Text -> " DY:  ", Side -> "left"];
    entdy=Entry[f2dy,TextVariable :> dyext, Side -> "left"];
    txtdpyext=TextLabel[f2dy, Text -> " DY': ", Side -> "left"];
    entdpy=Entry[f2dy,TextVariable :> dpyext, Side -> "left"];

    onoffex=CheckButton[f2ex,Text->" Set dispersion X?",  Side -> "left", Variable :> ifextex];
    txtexext=TextLabel[f2ex, Text -> " EX:  ", Side -> "left"];
    entex=Entry[f2ex,TextVariable :> exext, Side -> "left"];
    txtepxext=TextLabel[f2ex, Text -> " EX': ", Side -> "left"];
    entepx=Entry[f2ex,TextVariable :> epxext, Side -> "left"];

    onoffey=CheckButton[f2ey,Text->" Set dispersion Y?",  Side -> "left", Variable :> ifextey];
    txteyext=TextLabel[f2ey, Text -> " EY:  ", Side -> "left"];
    entey=Entry[f2ey,TextVariable :> eyext, Side -> "left"];
    txtepyext=TextLabel[f2ey, Text -> " EY': ", Side -> "left"];
    entepy=Entry[f2ey,TextVariable :> epyext, Side -> "left"];

  TkWait[];

    setpos=LINE["Position","KIX"];

    usezh={};
    dzh={};
    usezv={};
    dzv={};

    If[ifextdx && ifextex, 
        usezh={"ZH6R","ZH7R","ZH8R","ZH9R","ZH10R","ZH11R","ZH12R","ZH13R"};
        xlist={ToExpression[dxext],ToExpression[dpxext],ToExpression[exext],ToExpression[epxext]}*1.e-3;

      dk0=1e-4;
      datazh=Map[(steer=#;
        FFS["CAL"]; 
        LINE["K0",steer]=LINE["K0",steer]+dk0;
        x0={Twiss["DX",setpos],Twiss["DPX",setpos],Twiss["EX",setpos],Twiss["EPX",setpos]};
        FFS["CAL"]; 
        x1={Twiss["DX",setpos],Twiss["DPX",setpos],Twiss["EX",setpos],Twiss["EPX",setpos]};
        LINE["K0",steer]=LINE["K0",steer]-dk0;
      (x1-x0)/dk0)&,usezh];

        dzh=LinearSolve[Transpose[datazh],xlist];

    ];
    If[ifextdx && Not[ifextex], 
        usezh={"ZH8R","ZH9R","ZH10R","ZH11R"};
        xlist={ToExpression[dxext],ToExpression[dpxext]}*1.e-3;

      dk0=1e-4;
      datazh=Map[(steer=#;
        FFS["CAL"]; 
        LINE["K0",steer]=LINE["K0",steer]+dk0;
        x0={Twiss["DX",setpos],Twiss["DPX",setpos]};
        FFS["CAL"]; 
        x1={Twiss["DX",setpos],Twiss["DPX",setpos]};
        LINE["K0",steer]=LINE["K0",steer]-dk0;
      (x1-x0)/dk0)&,usezh];

        dzh=LinearSolve[Transpose[datazh],xlist];

    ];
    If[Not[ifextdx] && ifextex, 
        usezh={"ZH8R","ZH9R","ZH10R","ZH11R"};
        xlist={ToExpression[exext],ToExpression[epxext]}*1.e-3;

      dk0=1e-4;
      datazh=Map[(steer=#;
        FFS["CAL"]; 
        LINE["K0",steer]=LINE["K0",steer]+dk0;
        x0={Twiss["EX",setpos],Twiss["EPX",setpos]};
        FFS["CAL"]; 
        x1={Twiss["EX",setpos],Twiss["EPX",setpos]};
        LINE["K0",steer]=LINE["K0",steer]-dk0;
      (x1-x0)/dk0)&,usezh];

        dzh=LinearSolve[Transpose[datazh],xlist];

    ];


    If[ifextdy && ifextey, 
        usezv={"ZV6R","ZV7R","ZV8R","ZV9R","ZV10R","ZV11R","ZV12R","ZV13R"};
        ylist={ToExpression[dyext],ToExpression[dpyext],ToExpression[eyext],ToExpression[epyext]}*1.e-3;

      dk0=1e-4;
      datazv=Map[(steer=#;
        FFS["CAL"]; 
        LINE["K0",steer]=LINE["K0",steer]+dk0;
        y0={Twiss["DY",setpos],Twiss["DPY",setpos],Twiss["EY",setpos],Twiss["EPY",setpos]};
        FFS["CAL"]; 
        y1={Twiss["DY",setpos],Twiss["DPY",setpos],Twiss["EY",setpos],Twiss["EPY",setpos]};
        LINE["K0",steer]=LINE["K0",steer]-dk0;
      (y1-y0)/dk0)&,usezv];

        dzv=LinearSolve[Transpose[datazv],ylist];

    ];
    If[ifextdy && Not[ifextey], 
        usezv={"ZV8R","ZV9R","ZV10R","ZV11R"};
        ylist={ToExpression[dyext],ToExpression[dpyext]}*1.e-3;

      dk0=1e-4;
      datazv=Map[(steer=#;
        FFS["CAL"]; 
        LINE["K0",steer]=LINE["K0",steer]+dk0;
        y0={Twiss["DY",setpos],Twiss["DPY",setpos]};
        FFS["CAL"]; 
        y1={Twiss["DY",setpos],Twiss["DPY",setpos]};
        LINE["K0",steer]=LINE["K0",steer]-dk0;
      (y1-y0)/dk0)&,usezv];

        dzv=LinearSolve[Transpose[datazv],ylist];

    ];
    If[Not[ifextdy] && ifextey, 
        usezv={"ZV8R","ZV9R","ZV10R","ZV11R"};
        ylist={ToExpression[eyext],ToExpression[epyext]}*1.e-3;

      dk0=1e-4;
      datazv=Map[(steer=#;
        FFS["CAL"]; 
        LINE["K0",steer]=LINE["K0",steer]+dk0;
        y0={Twiss["EY",setpos],Twiss["EPY",setpos]};
        FFS["CAL"]; 
        y1={Twiss["EY",setpos],Twiss["EPY",setpos]};
        LINE["K0",steer]=LINE["K0",steer]-dk0;
      (y1-y0)/dk0)&,usezv];

        dzv=LinearSolve[Transpose[datazv],ylist];

    ];

     Scan[(LINE["K0",#[1]]=#[2])&,k0list];
     Scan[(LINE["K0",#[1]]=LINE["K0",#[1]]+#[2])&,Thread[{usezh,dzh}]];
     Scan[(LINE["K0",#[1]]=LINE["K0",#[1]]+#[2])&,Thread[{usezv,dzv}]];

!!    WriteRingOpticsOrbitSteer[opticsdata,orbitdata];

  ];

