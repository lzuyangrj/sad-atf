
! input arguments are [datalist,f]
!datalist={{data,range,frame,text},{data,range,frame,text},{data,range,frame,text}, , , ,}
!
! data={data[1],data[2],data[3], , ,,data[n]}  (n>=1) 
!  data[i]={option1,option2,xlist,ylist,dylist}
! xlist is list of horizontal points
! ylist is list of vertical points
! dylist is list of vertical error of points. If there is no dylist, no error bar.
!
! option1 decide style of line
!     0: no line
!     1: solid
!     2: dashed
!
! option2 decide style of point
!     0: none
!     1: cross
!     2: star 
! 
! range={{xlow,xup},{ylow,yup}} decide limits of h and v values
! 
! frame={{xbottom,xtop},{ybottom,ytop}} decide size of frame
!   positions of bottoms and tops are (may be) relative to the screen size
!
! text={text[1],text[2],,,} is list of text data
!  text[i]={string, xposition, yposition,angle}
!    positions are (may be) relative to the screen size
!
! f is output file name, may not be used depending tools.

!!!! This should be changed depending on graphic tool
!!   DrawGraph[data_,range_,frame_,text_,f_:"dummyfileforfigure"]:=TopDrawGraph[data,range,frame,text,f];
   DrawGraph[datalist_,f_]:=TopDrawGraph[datalist,f];

! This is for topdrawer.
TopDrawGraph[datalist_,f_]:=Module[
! {},
{data,range,frame,text,fn,xlist,ylist,dylist,opt1,opt2,scalex=13,scaley=10},
!! Print[datalist[1,5]];
 Print[f];
  fn=OpenWrite[f];
 Scan[(
  data=#[1];range=#[2];frame=#[3];text=#[4];
  Print[frame]; Print[range];Print[text];

  round=100;

  $FORM="6.2";
  Write[fn," set window x ",frame[1,1]*scalex," ",frame[1,2]*scalex];
  Write[fn," set window y ",frame[2,1]*scaley," ",frame[2,2]*scaley];
  If[Length[text[1]]>=1,
   Scan[(
    Write[fn," titl ",#[2]*scalex," ",#[3]*scaley," ANGLE ",#[4]," '"//#[1]//"'"];
   )&,text];
  ]; 

!  Print[frame]; Print[range];Print[text];

  Write[fn," set limit x ",range[1,1]," ",range[1,2]];
  Write[fn," set limit y ",range[2,1]," ",range[2,2]];
  
  
  $FORM="";
  Scan[(
   xlist=#[3];
   ylist=#[4];
   opt1=#[1];
   opt2=#[2];
   If[Length[#]=>5,dylist=#[5]];
   If[opt2==1,Write[fn,' set symbol 1O']];
   If[opt2==2,Write[fn,' set symbol 4O']];
   If[opt1==2, Write[fn," set pattern 0.2 0.08"]];

   If[Length[#]=>5,
    Write[fn," set order x y dy"];
    Scan[(Write[fn,#[1]," ",#[2]," ",#[3]])&,Thread[{xlist,ylist,dylist}]];
   , 
    Write[fn," set order x y "];
    Scan[(Write[fn,#[1]," ",#[2]])&,Thread[{xlist,ylist}]];
   ]; 

      
   If[opt2>=1, Write[fn," plot"]];
   If[opt1==1, Write[fn," join 1"]];
   If[opt1==2, Write[fn," join 1 patterned"]];
 
  )&,data];
 )&,datalist];  

 Close[fn];
 System[" chmod 664 "//f];
 System[" tdr -v X "//f];

 ];

