!
  SaveRingExtOptics[f_]:=Module[
        {fn=OpenWrite["/users/atfopr/sad/ringoptics/"//f]},
    qv=Thread[{quad,Element["K1",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    bendv=Thread[{bend,Element["K0",bend]}];
    cavv=Thread[{cav,Element["VOLT",cav]}];
    qvex=Thread[{quadex,Element["K1",quadex]}];
    sthvex=Thread[{sthex,Element["K0",sthex]}];
    stvvex=Thread[{stvex,Element["K0",stvex]}];
    bendvex=Thread[{bendex,Element["K0",bendex]}];
    $FORM="";
    Write[fn,
      "qv=",qv,
      ";\n\nsthv=",sthv,
      ";\n\nstvv=",stvv,
      ";\n\nsxtv=",sxtv,
      ";\n\nbendv=",bendv,
      ";\n\ncavv=",cavv,
      ";\n\nqvex=",qvex,
      ";\n\nsthvex=",sthvex,
      ";\n\nstvvex=",stvvex,
      ";\n\nbendvex=",bendvex,";"];
    Close[fn];
    System["chmod 664 "//f]];

  LoadRingExtOptics[f_]:=Module[{qv,sxtv,sthv,stvv,cavv,bendv,qvex,sthvex,stvvex,bendvex},
    Get["/users/atfopr/sad/ringoptics/"//f];
    Map[(Element["K1",#[1]]=#[2])&,qv];
!
!    Map[(Print[Element["K2",#[1]]," ",#[2]])&,sxtv];
!
    Map[(Element["K2",#[1]]=#[2])&,sxtv];
    Map[(Element["K0",#[1]]=#[2])&,sthv];
    Map[(Element["K0",#[1]]=#[2])&,stvv];
    Map[(Element["K0",#[1]]=#[2])&,bendv];
    Map[(Element["VOLT",#[1]]=#[2])&,cavv];

    Map[(Element["K1",#[1]]=#[2])&,qvex];
    Map[(Element["K0",#[1]]=#[2])&,sthvex];
    Map[(Element["K0",#[1]]=#[2])&,stvvex];
    Map[(Element["K0",#[1]]=#[2])&,bendvex];];

  WriteRingExtOptics[f_,epilog_,opt___]:=Module[{
        fn=OpenWrite[f],
        def={SADFile->"",Steer->True},sadfile,steer},
    sadfile=SADFile/.{opt}/.def;
    steer=Steer/.{opt}/.def;
    Write[fn,"!"//"    "//sadfile];
    $FORM="F12.6";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    bendv=Thread[{bend,Element["K0",bend]}];
    cavv=Thread[{cav,Element["K0",cav]}];

    qvex=Thread[{quadex,Element["K1",quadex]}];
    sthvex=Thread[{sthex,Element["K0",sthex]}];
    stvvex=Thread[{stvex,Element["K0",stvex]}];
    bendvex=Thread[{bendex,Element["K0",bendex]}];

    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,bendv];
    If[steer,
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv]];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];
    Close[fn];
    System["chmod 664 "//f]];
