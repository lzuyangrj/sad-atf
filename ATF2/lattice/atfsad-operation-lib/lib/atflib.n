!
!  Library for ATF
!
! 'GI's are changed 1997.4.15. by K.kubo
! Correction with 'Qcorr's  added 1997.6.   by K.kubo
! 'SetQuad' is modified for paired quads  1997.7.8. by K.Kubo
! 'WriteQall' is modified for paired quads and trim coils  1997.7.8. by K.Kubo
! New Correction with 'Qcorr's  added  1997.7.8.   by K.kubo
! Effective Length of BT steers 0.09 --> 0.123  1998.1.16. 15:50 K.K.
! GI of BT bend trim (ZX,ZY) 40turns --> 20 turns  1998.1.16. 15:50 K.K.
!
!
!!  allQ=LINE["NAME","Q*"];
  allQ=LINE["NAME","Q*{L,T}*"];
  pairedQ={{"QF20T","QF21T"},{"QD12T","QD30T"},{"QF11T","QF30T"},
           {"QD11T","QD31T"},{"QF10T","QF31T"},{"QD10T","QD32T"},
           {"QF42T","QF43T"}};
  

  If[Length[Position[allQ,"QD2L",1,1]]>0,
     QIAposition=Position[allQ,"QD2L",1,1][1,1]];
  Qt["QD10T"]="QICA";  Qt["QF10T"]="QICA";  Qt["QD11T"]="QICB";  Qt["QF11T"]="QICA";
  Qt["QD12T"]="QICB";  Qt["QF20T"]="QICC";  Qt["QF21T"]="QICC";  Qt["QD30T"]="QICB";
  Qt["QF30T"]="QICA";  Qt["QD31T"]="QICB";  Qt["QF31T"]="QICA";  Qt["QD32T"]="QICA";
  Qt["QF40T"]="QICA";  Qt["QD40T"]="QICA";  Qt["QF41T"]="QICA";  Qt["QD41T"]="QICB";
  Qt["QF42T"]="QICB";  Qt["QD42T"]="QICB";  Qt["QF43T"]="QICB";  Qt["QD50T"]="QICA";
  Qt["QF50T"]="QICA";  Qt["QD51T"]="QICA";  Qt["QF51T"]="QICB";

  Qt["QF52T"]="QICC";  Qt["QD52T"]="QICB";  Qt["QF53T"]="QICB";
  Qt["QX20T"]="QICD";  Qt["QX21T"]="QICD";  Qt["QX22T"]="QICD";  Qt["QX23T"]="QICD";

  Qt["QA1L"]="QIAS";Qt["QA2L"]="QIAM";Qt["QA3L"]="QIAM";Qt["QA4L"]="QIAM";
  Qt["QA5L"]="QIAS";Qt["QD1L"]="QIAS";Qt["QF1L"]="QIAM";Qt["QD2L"]="QIAS";
  Qt["QF2L"]="QIBS";Qt["QD3L"]="QIBS";Qt["QF3L"]="QIBS";Qt["QD4L"]="QIBS";
  Qt["QF4L"]="QIBS";Qt["QD5L"]="QIBS";Qt["QF5L"]="QIBS";Qt["QD6L"]="QIBS";
  Qt["QF6L"]="QIBS";Qt["QD7L"]="QIBS";Qt["QS5L"]="QIBS";Qt["QS6L"]="QIBS";
  Qt["QS7L"]="QIBS";Qt["QS8L"]="QIBS";Qt["QM1L"]="QIBM";Qt["QM2L"]="QIBL";
  Qt["QM3L"]="QIBM";
  Qt["QF3P"]="QIAL";

  Qt[_]="QIBX";
  Qt[_?(StringMatchQ[#,"Q*P"]&)]="QIP";

  Qtype=Map[Qt,allQ];

  STt["ZH1L"]="ST1L";
  STt["ZH2L"]="ST1L";
  STt["ZH3L"]="ST1L";
  STt["ZV1L"]="ST1L";
  STt["ZV2L"]="ST1L";
  STt["ZV3L"]="ST1L";

  STt["ZH4L"]="ST2L";
  STt["ZH5L"]="ST2L";
  STt["ZH6L"]="ST2L";
  STt["ZH7L"]="ST2L";
  STt["ZV4L"]="ST2L";
  STt["ZV5L"]="ST2L";
  STt["ZV6L"]="ST2L";
  STt["ZV7L"]="ST2L";

  STt[_?(StringMatchQ[#,"Z*P"]&)]="STP";
  
  STt["ZH10T"]="ST1T";
  STt[_?(StringMatchQ[#,"ZH*T"]&)]="ST2T";
  STt["ZX10T"]="ST7T";
  STt["ZY22T"]="ST8T";
  STt[_?(StringMatchQ[#,"ZX*T"]&)]="ST3T";
  STt["ZV10T"]="ST4T";
  STt[_?(StringMatchQ[#,"ZV*T"]&)]="ST5T";
  STt[_?(StringMatchQ[#,"ZY*T"]&)]="ST6T";

  STt[_]="ST3L";
!
!  Effective lengths
!
  Elt["QIAS"]=0;  Elt["QIAM"]=0;  Elt["QIBS"]=0;
  Elt["QIBM"]=0;  Elt["QIBL"]=0;  Elt["QIBT"]=0;
  Elt["QICA"]=0;
  Elt["QICB"]=0;
  Elt["QICC"]=0;
  Elt["QIP"]=0.032;
  Elt[_]=0;
  Elq[q_]:=Elt[Qt[q]]+LINE["L",q];
  Elq[q_List]:=Map[Elq,q];

  ElST["ST1L"]=0.12;
  ElST["ST2L"]=0.11;
  ElST["ST3L"]=0.11;

!  ElST["ST1T"]=0.09;
!  ElST["ST2T"]=0.09;
  ElST["ST1T"]=0.123;
  ElST["ST2T"]=0.123;
  ElST["ST3T"]=1.35;
!  ElST["ST4T"]=0.09;
!  ElST["ST5T"]=0.09;
  ElST["ST4T"]=0.123;
  ElST["ST5T"]=0.123;
  ElST["ST6T"]=0.815;
  ElST["ST7T"]=1.35;
  ElST["ST8T"]=0.815;
  ElST["STP"]=0.13;
!
!   Excitation curves
!
  GI["QIAS"]={{0,0},{1.318,5},{2.716,10},
              {4.116,15},{4.847,17.6},{5.508,20},{1e10,20}};
  GI["QIAM"]={{0,0},{1.424,5},{2.848,10},
              {4.236,15},{4.971,17.6},{5.649,20},
              {5.649*1.25,25},{1e10,25}};
  GI["QIAL"]={{0,0},{1.433,5},{2.840,10},
              {4.264,15},{4.987,17.6},{5.667,20},{1e10,20}};
  GI["QIBS"]={{0,0},{6.432,20},{13.073,40},
              {19.605,60},{24.064,74},{25.904,80},{1e10,80}};
  GI["QIBM"]={{0,0},{6.524,20},{13.017,40},
    {19.521,60},{24.016,74},{25.942,80},
    {(25.942-24.016)/6*20+25.942,100},{1e10,100}};
  GI["QIBL"]={{0,0},{6.432,20},{13.058,40},
              {19.627,60},{24.286,74},{26.264,80},{1e10,80}};
  GI["QIBT"]={{0,0},{0.8/0.016,60+20*0.5/16.5},{0.9/0.016,60+20*10.5/26.5},
              {1.0/0.016,80-20*2.5/26.5},{1.2-0.2*7/26.5,100},{1e10,100}};
  GI["QIBX"]=GI["QIBL"];
  GI["QICA"]= {{0,0},{48.96,75.},{53.07,85.},{55.80,95.},{1e10,100}};
  GI["QICB"]= {{0,0},{36.26,75.},{41.00,85.},{43.36,90.},{45.68,95},{1e10,100}};
  GI["QICC"]= {{0,0},{18.72,70.},{20.04,75.},{21.35,80.},{22.66,85},
               {23.97,90},{1e10,100}};
  GI["QICD"]={{0,0},{1e10,0}};
  GI["QIP"]={{0,0},{9.0,20},{18.1,40},{27.5,60},{36.2,80},{40,90},{42.5,100},{44.5,110},{1e10,110}};
  GI["ST1L"]=0.015/5;
  GI["ST2L"]=0.04/6;
  GI["ST3L"]=0.07/4;
  GI["ST1T"]= 0.054/5;
  GI["ST2T"]=-0.054/5;
!!!  GI["ST3T"]=-1.188*40/36720;
!  GI["ST3T"]=1.188*40/36720;
!  GI["ST7T"]=-1.188*40/36720;
  GI["ST3T"]=1.188*20/36720;
  GI["ST7T"]=-1.188*20/36720;

  GI["ST4T"]=-0.054/5;
  GI["ST5T"]=0.054/5;
!  GI["ST6T"]=-1.2*40/33912;
!  GI["ST8T"]=1.2*40/33912;
  GI["ST6T"]=-1.2*20/33912;
  GI["ST8T"]=1.2*20/33912;
  GI["STP"]=-0.082/5;
!
!all quads' correction arereset 1997.12.8.
!
!  Qcorr["QD10T"]=   0.000
!  Qcorr["QF10T"]=   -.089
!  Qcorr["QD11T"]=    .048
!  Qcorr["QF11T"]=   -.002
!  Qcorr["QD12T"]=    .097
!  Qcorr["QF20T"]=    .098
!  Qcorr["QF21T"]=   -.077
!  Qcorr["QD30T"]=    .034
!  Qcorr["QF30T"]=    .006
!  Qcorr["QD31T"]=   -.066
!  Qcorr["QF31T"]=   -.026
!  Qcorr["QD32T"]=   -.074
!  Qcorr["QF40T"]=   -.018
!  Qcorr["QD40T"]=   -.043
!  Qcorr["QF41T"]=   -.073
!  Qcorr["QD41T"]=    .073
!  Qcorr["QF42T"]=   -.029
!  Qcorr["QD42T"]=   -.030
!  Qcorr["QF43T"]=   -.029
!  Qcorr["QD50T"]=   -.047
!  Qcorr["QF50T"]=    .002
!  Qcorr["QD51T"]=   -.014
!  Qcorr["QF51T"]=   -.014
!  Qcorr["QF52T"]=   0.000
!  Qcorr["QD52T"]=   0.000
!  Qcorr["QF53T"]=   0.000
!! added 1997.7.8.
!Qcorr["QD10T"]=Qcorr["QD10T"]+       .000;
!Qcorr["QF10T"]=Qcorr["QF10T"]+      -.020;
!Qcorr["QD11T"]=Qcorr["QD11T"]+       .013;
!Qcorr["QF11T"]=Qcorr["QF11T"]+      -.005;
!Qcorr["QD12T"]=Qcorr["QD12T"]+       .003;
!Qcorr["QF20T"]=Qcorr["QF20T"]+  -4.647E-4;
!Qcorr["QF21T"]=Qcorr["QF21T"]+       .015;
!Qcorr["QD30T"]=Qcorr["QD30T"]+       .003;
!Qcorr["QF30T"]=Qcorr["QF30T"]+       .002;
!Qcorr["QD31T"]=Qcorr["QD31T"]+      -.001;
!Qcorr["QF31T"]=Qcorr["QF31T"]+       .003;
!Qcorr["QD32T"]=Qcorr["QD32T"]+      -.049;
!Qcorr["QF40T"]=Qcorr["QF40T"]+  -9.202E-7;
!Qcorr["QD40T"]=Qcorr["QD40T"]+  -7.032E-4;
!Qcorr["QF41T"]=Qcorr["QF41T"]+      -.004;
!Qcorr["QD41T"]=Qcorr["QD41T"]+       .039;
!Qcorr["QF42T"]=Qcorr["QF42T"]+      -.003;
!Qcorr["QD42T"]=Qcorr["QD42T"]+      -.001;
!Qcorr["QF43T"]=Qcorr["QF43T"]+      -.003;
!Qcorr["QD50T"]=Qcorr["QD50T"]+       .008;
!Qcorr["QF50T"]=Qcorr["QF50T"]+  3.3378E-4;
!Qcorr["QD51T"]=Qcorr["QD51T"]+      -.027;
!Qcorr["QF51T"]=Qcorr["QF51T"]+      -.061;
!Qcorr["QF52T"]=Qcorr["QF52T"]+       .026;
!Qcorr["QD52T"]=Qcorr["QD52T"]+      -.020;
!Qcorr["QF53T"]=Qcorr["QF53T"]+      -.002;

  Qcorr[_]=0;
  QcorrFac[q_]:=1-Qcorr[q];
  QcorrFac[q_List]:=Map[QcorrFac,q];

!  ratio of Trim / Main
   RTMQ["QF21T"]=-20/27;
   RTMQ["QD30T"]=-20/49;
   RTMQ["QF30T"]=-20/67;
   RTMQ["QD31T"]=-20/49;
   RTMQ["QF31T"]=-20/67;
   RTMQ["QD32T"]=-20/67;
   RTMQ["QF43T"]=-20/49;

!
  QCur[q_,g_]:=Module[{t,i},t=GI[q];i=Position[t,pp_?((#[1]>g)&),1,1][1,1];
    t[i-1,2]+(t[i,2]-t[i-1,2])*(g-t[i-1,1])/(t[i,1]-t[i-1,1])];
  Qg[q_,cur_]:=Module[{t,i},t=GI[q];i=Position[t,pp_?((#[2]>cur)&),1,1][1,1];
    t[i-1,1]+(t[i,1]-t[i-1,1])*(cur-t[i-1,2])/(t[i,2]-t[i-1,2])];
!  QK[q_,cur_]:=Qg[Qt[q],cur]/LINE["GAMMA",q]/MASS*SpeedOfLight*
!    Elq[q]*Sign[LINE["K1",q]];
  QK[q_,cur_]:=QcorrFac[q]*Qg[Qt[q],cur]/LINE["GAMMA",q]/MASS*SpeedOfLight*
    Elq[q]*Sign[LINE["K1",q]];
  STK[st_,cur_]:=Module[{n=STt[LINE["NAME",st]]},cur*GI[n]*ElST[n]/LINE["GAMMA",st]/MASS*SpeedOfLight];
  STCur[st_,k_]:=k/STK[st,1];
!  QC[q_,k_]:=QCur[Qt[q],Abs[k]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]];
  QC[q_,k_]:=(Print[{q,k}];QCur[Qt[q],Abs[k]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]]/
             QcorrFac[q]);
!  WriteQall:=WriteQCur[allQ];

  PrintQall:=Module[{file,Qallg,Qtype1,q,q2,dif,allQ1},
    allQ1=Select[allQ,MemberQ[Element["Name","Q*"],#]&];
    pair=Select[pairedQ,MemberQ[allQ1,#[[2]]]&];
    q2=Map[#[2]&,pair];
    q=Select[allQ1,Not[MemberQ[q2,#]]&];
    Print[{q,q2}];
!    Qallg=Abs[LINE["K1",q]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]];
    Qallg=Abs[LINE["K1",q]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]]/
          QcorrFac[q];
    Qtype1=Map[Qt[#]&,q];
    dif=Map[(QC[#[2],(LINE["K1",#[2]]-LINE["K1",#[1]]*QcorrFac[#[2]]/QcorrFac[#[1]])]/
        RTMQ[#[2]]*
        Sign[Abs[LINE["K1",#[2]]]-Abs[LINE["K1",#[1]]*QcorrFac[#[2]]/QcorrFac[#[1]]]])&,
        pair];
    $FORM="F12.6";
    file=6;
    Scan[Write[file,(#[1]//"          ")[1,10],QCur[#[3],#[2]]]&,
      Transpose[{q,Qallg,Qtype1}]];
    Scan[Write[file,(#[1]//"_S        ")[1,10],#[2]]&,
      Transpose[{q2,dif}]];
    file=OpenWrite["./output/LNBT_ORBIT.SAD"];
   ];


  WriteQall:=Module[{file,Qallg,Qtype1,q,q2,dif,allQ1},
    allQ1=Select[allQ,MemberQ[Element["Name","Q*"],#]&];
    pair=Select[pairedQ,MemberQ[allQ1,#[[2]]]&];
    q2=Map[#[2]&,pair];
    q=Select[allQ1,Not[MemberQ[q2,#]]&];
    Print[{q,q2}];
!    Qallg=Abs[LINE["K1",q]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]];
    Qallg=Abs[LINE["K1",q]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]]/
          QcorrFac[q];
    Qtype1=Map[Qt[#]&,q];
    dif=Map[(QC[#[2],(LINE["K1",#[2]]-LINE["K1",#[1]]*QcorrFac[#[2]]/QcorrFac[#[1]])]/
        RTMQ[#[2]]*
        Sign[Abs[LINE["K1",#[2]]]-Abs[LINE["K1",#[1]]*QcorrFac[#[2]]/QcorrFac[#[1]]]])&,
        pair];
    $FORM="F12.6";
    file=6;
    Scan[Write[file,(#[1]//"          ")[1,10],QCur[#[3],#[2]]]&,
      Transpose[{q,Qallg,Qtype1}]];
    Scan[Write[file,(#[1]//"_S        ")[1,10],#[2]]&,
      Transpose[{q2,dif}]];
    file=OpenWrite["./output/LNBT_ORBIT.SAD"];
    Scan[Write[file,(#[1]//"          ")[1,10],QCur[#[3],#[2]]]&,
      Transpose[{q,Qallg,Qtype1}]];
    Scan[Write[file,(#[1]//"_S        ")[1,10],#[2]]&,
      Transpose[{q2,dif}]];
     Write[file,"! Final Energy=",LINE["GAMMA","QD10T"]*MASS*1.e-9,"GeV"]; 
    Close[file];
    System["chmod 664 ./output/LNBT_ORBIT.SAD"]];


  WriteQCur[q_]:=Module[{file,Qallg},
!    Qallg=Abs[LINE["K1",q]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]];
    Qallg=Abs[LINE["K1",q]*LINE["GAMMA",q]*MASS/SpeedOfLight/Elq[q]]/
          QcorrFac[q];
    $FORM="F12.6";
    file=6;
    Scan[Write[file,(#[1]//"          ")[1,10],QCur[#[3],#[2]]]&,
      Transpose[{q,Qallg,Qtype}]];
    file=OpenWrite["./output/LNBT_ORBIT.SAD"];
    Scan[Write[file,(#[1]//"          ")[1,10],QCur[#[3],#[2]]]&,
      Transpose[{q,Qallg,Qtype}]];
    Write[file,"! Final Energy=",LINE["GAMMA","QD10T"]*MASS*1.e-9,"GeV"]; 
    Close[file];
    System["chmod 664 ./output/LNBT_ORBIT.SAD"]];
  ResetQ[q_]:=Scan[FFS["reset "//#]&,q];
  qmed=If[Length[Position[allQ,"QF4L",1]]>0,Take[allQ,{Position[allQ,"QF4L",1][1,1],-1}],{},{}];
  qlend=If[Length[Position[allQ,"QM1L",1]]>0,Take[allQ,{Position[allQ,"QM1L",1][1,1],-1}],{},{}];
  qbeg=Complement[allQ,qmed];
  qlin=Complement[allQ,qlend];
!
!  Reading Vc & phase
!
  ReadVcPhi:=Print["ReadVcPhi is obsolete. Use SetVcPhi."];
  SetVcPhi[n_:1]:=Module[{
    vclist=ATFVcPhi/.ATFData[n]/.ATFVcPhi->{}},
    Print[vclist];
    vclist=DeleteCases[vclist,{"CA0L",__},1];
    Scan[Check[LINE["VOLT",#[1]]=#[2],]&,vclist]];
!!!!    Scan[Check[LINE["VOLT",#[1]]=#[2]*0.915,]&,vclist]];
!
!  
  ReadQuad:=Print["ReadQuad is obsolete. Use SetQuad."];
!  SetQuad[n_:1]:=Module[{
!    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{}},
!    Map[Check[LINE["K1",#[1]]=QK[#[1],#[2]],]&,k1list];
!    Scan[(LINE["K1",#[2]]=LINE["K1",#[2]])&,pairedQ]];

  SetQuad[n_:1]:=Module[{
    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{},pair},
    k1list0=Select[k1list,MemberQ[allQ,#[1]]&];
    pair=Select[pairedQ,MemberQ[allQ,#[[2]]]&];
    k1list1=Select[k1list0,(Not[#[1][6,7]=="_S"])&];
    k1list2=Select[k1list0,(#[1][6,7]=="_S")&];
    Map[Check[LINE["K1",#[1]]=QK[#[1],Max[#[2],0]],Print["error1"]]&,k1list1];
    Scan[Check[(LINE["K1",#[2]]=LINE["K1",#[1]]*QcorrFac[#[2]]/QcorrFac[#[1]]),Print["error22"]]&,pair];
    k1list2=Map[{#[1][1,5],#[2]}&,k1list2];
    Map[Check[(LINE["K1",#[1]]=LINE["K1",#[1]]+
    Sign[#[2]*RTMQ[#[1]]]*QK[#[1],Abs[#[2]*RTMQ[#[1]]]]),Print["error2"]]&,k1list2];
];

!
!  Read Orbit 
!
  GetATFOrbit[n_:1]:=Module[{
    bpmdata=Thread[DeleteCases[ATFBPM/.ATFData[n]/.ATFBPM->{},
      _?(#[1][1,2]<=>"ML"&),1]]},
!    Print[bpmdata];
    If[bpmdata<=>{{}},
        bpm=Map[LINE["POSITION",#]&,bpmdata[1]];
        {bpmx,bpmy,bpmi}=bpmdata[{2,3,4}]*{1e-6,1e-6,1},
      bpm=bpmx=bpmy=bpmi={}]];

  GetATFOrbitLN[n_:1]:=Module[{
    bpmdataln=DeleteCases[ATFBPM/.ATFData[n]/.ATFBPM->{},
      _?(#[1][1,2]<=>"ML"&),1]},
!    Print["bpmdataln ",bpmdataln];
    bpmdataln=Select[bpmdataln,StringMatchQ[#[1],"*L"]&];
    bpmdataln=Thread[bpmdataln];
    If[bpmdataln<=>{{}},
        bpm=Map[LINE["POSITION",#]&,bpmdataln[1]];
        {bpmx,bpmy,bpmi}=bpmdataln[{2,3,4}]*{1e-6,1e-6,1},
      bpm=bpmx=bpmy=bpmi={}]];

      
  GetATFOrbitBT[n_:1]:=Module[{
    bpmdatabt=DeleteCases[ATFBPM/.ATFData[n]/.ATFBPM->{},
      _?(#[1][1,2]<=>"ML"&),1]},
    bpmdatabt=Select[bpmdatabt,StringMatchQ[#[1],"*T"]&];
    bpmdatabt=Thread[bpmdatabt];
    If[bpmdatabt<=>{{}},
        bpm=Map[LINE["POSITION",#]&,bpmdatabt[1]];
        {bpmx,bpmy,bpmi}=bpmdatabt[{2,3,4}]*{1e-6,1e-6,1},
      bpm=bpmx=bpmy=bpmi={}]];



!
!  Read Steering
!
  GetATFSteer[n_:1]:=Module[{
    z=Map[{LINE["POSITION",#[1]],#[2]}&,ATFSteer/.ATFData[n]/.ATFSteer->{}]},
    zcurr=Cases[z,_?(#[1]>0&),1]];
  SetATFSteer[n_:1]:=Module[{
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{}},
    k0list=Select[k0list,(MemberQ[LINE["Name","*"],#[1]])&];
    Scan[Check[LINE["K0",#[1]]=STK[#[1],#[2]],]&,k0list]];
!
!  1D Quad-Emit Scan: returns {emit, alpha, beta}
!  
  QuadEmit1D[xy0_,quad_,mon_,ref_,data_]:=Module[
    {iq,k,size,mat,m11,m12,mk,mb,c,form0,sig,x,
      kmin,kmax,xx,xxp,xpxp,emixm,axm,bxm,matent,me,axe,bxe,ql,xy},
    iq=LINE["POSITION",quad];
    ql=LINE["L",iq]/2;
   If[Length[data[1]]==2,
    {k,size}=Transpose[Sort[data]];
   ,
    {k,size,ersize}=Transpose[Sort[data]];
   ];
    mat=TransferMatrix[iq+1,mon];
    xy=ToUpperCase[xy0];
    If[xy=="X",
        m11=mat[1,1];m12=mat[1,2],
      m11=mat[3,3];m12=mat[3,4];k=-k];
    m12=m12+m11*ql;
    k=k-k^2*ql/3;
    mk=Transpose[{Table[1,{Length[k]}],k,k^2}];

!    c=LinearSolve[mk/size,size,Threshold->1e-10];

   If[Length[data[1]]==2,
    ersize2=size;
   ,
    ersize2=Sqrt[4.*size^2*ersize^2+3.*ersize^4];
   ]; 
    svx=SingularValues[mk/ersize2,Tolerance->1.e-41];
    invx=Transpose[svx[3]].DiagonalMatrix[svx[2]].svx[1];
    c=invx.(size^2/ersize2);
    ermatc=

    If[plotscan,

 w=Window[];
 b=Button[w,Text -> "Next", Command :> TkReturn["ReturnToSAD"]];
 $DisplayFunction = CanvasDrawer;
   If[Length[data[1]]==2,
        g1=ListPlot[Transpose[{k,size^2}],DisplayFunction->Null,PointSize->1];
   ,
        g1=ListPlot[Transpose[{k,size^2,ersize2}],DisplayFunction->Null,PointSize->1];
   ];
        kmin=Min[k];
        kmax=Max[k];
        g2=Plot[{1,x,x^2}.c,{x,kmin,kmax},DisplayFunction->Null]];
    xx=c[3]/m12^2;
    xxp=(-c[2]/2-xx*m11*m12)/m12^2;
    xpxp=(c[1]-m11*(m11*xx+2*m12*xxp))/m12^2;

!    emixm=Sqrt[xx*xpxp-xxp^2];
    emixm=Sqrt[c[3]*(c[1]+m11/m12*c[2]+m11^2/m12^2*c[3])]/m12^2;    

    eremixm=Sqrt[((c[3]*invx[1,1]+c[1]*invx[3,1]+m11/m12*(c[3]*invx[2,1]+c[1]*invx[3,1])+m11^2/m12^2*2*c[3]*invx[3,1])^2
             +(c[3]*invx[1,2]+c[1]*invx[3,2]+m11/m12*(c[3]*invx[2,2]+c[1]*invx[3,2])+m11^2/m12^2*2*c[3]*invx[3,2])^2
             +(c[3]*invx[1,3]+c[1]*invx[3,3]+m11/m12*(c[3]*invx[2,3]+c[1]*invx[3,3])+m11^2/m12^2*2*c[3]*invx[3,3])^2)
            /4/(c[3]*(c[1]+m11/m12*c[2]+m11^2/m12^2*c[3]))]/m12^2;

    gamemixm=emixm*LINE["GAMMA",ref];
    axm=-xxp/emixm;
    bxm=xx/emixm;
    matent=TransferMatrix[ref,iq];
    If[xy=="X",
        me=matent[{1,2},{1,2}];sig=" ",
      me=matent[{3,4},{3,4}];sig="-"];
    me=Inverse[{{1,ql},{0,1}}.me];
    bxe=me[1,1]^2*bxm-2*me[1,1]*me[1,2]*axm+me[1,2]^2*(1+axm^2)/bxm;
    axe=-me[2,1]*me[1,1]*bxm+(me[2,1]*me[1,2]+me[1,1]*me[2,2])*axm-
      me[2,2]*me[1,2]*(1+axm^2)/bxm;

    axmodel=Twiss["A"//xy,ref];
    bxmodel=Twiss["B"//xy,ref];
    bmag1=((bxe/bxmodel+bxmodel/bxe+bxmodel*bxe*(axmodel/bxmodel-axe/bxe)^2)/2);

    If[plotscan,
        form0=$FORM;
        $FORM="12.5";

      If[Length[data[1]]==2,
      Show[g1,g2,FrameLabel->{quad//" "//sig//"K1_eff (1/m)","SIG"//xy//"^2"},
          Graphics[Text[{
            "EMIT"//xy//" ="//emixm//" m,  gammma*EMIT"//xy//" ="//gamemixm//" m",
            {2.5,8.7}}]],
          Graphics[Text[{
            "ALPHA"//xy//" ="//axe//",  BETA"//xy//" ="//bxe//" m   at "//ref,
            {2.5,8.4}}]],
          Graphics[Text[{
            "Bmag"//xy//"="//bmag1,
            {2.5,8.1}}]],
          Graphics[Text[{DateString[],{9.5,8.1}},TextSize->0.8]]];
          $FORM=form0;  TkWait[];
      ,
        Show[g1,g2,FrameLabel->{quad//" "//sig//"K1_eff (1/m)","SIG"//xy//"^2"},
          Graphics[Text[{
            "EMIT"//xy//" ="//emixm//" +- "//eremixm//" m,  gammma*EMIT"//xy//" ="//gamemixm//" m",
            {2.5,8.7}}]],
          Graphics[Text[{
            "ALPHA"//xy//" ="//axe//",  BETA"//xy//" ="//bxe//" m   at "//ref,
            {2.5,8.4}}]],
          Graphics[Text[{
            "Bmag"//xy//"="//bmag1,
            {2.5,8.1}}]],
          Graphics[Text[{DateString[],{9.5,8.1}},TextSize->0.8]]];
          $FORM=form0;  TkWait[];
     ];
    ];    
    {emixm,gamemixm,axe,bxe,c,{k,size^2}}
 ];

  plotscan=False;

  WriteATFCorr:=Module[{f,c,n,i},
    f=OpenWrite["./output/LNBT_ORBIT.SAD"];
    $FORM="F14.7";
    Scan[
      (i=Position[zcurr,{#[1],_},1,1];i=If[i==={},0,i[1,1]];
      If[i==0,zcurr=Append[zcurr,#],
        zcurr[i,2]=zcurr[i,2]+STCur[#[1],-#[2]]])&,
      Thread[{Join[xcorr,ycorr],Join[xkick,-ykick]}]];
    Scan[
      (n=(LINE["NAME",#[1]]//"        ")[1,8];
      Write[f,n," ",#[2]])&,zcurr];
    Do[Write[f,"!Orbit_",(LINE["NAME",i]//"            ")[1,12],
      LINE["S",i]," ",Twiss["DX",i]*1e6," ",
      Twiss["DY",i]*1e6],{i,1,LINE["LENGTH"]}];
    $FORM="";
    Close[f];
    System["chmod 775 ./output/LNBT_ORBIT.SAD"]];

  WriteATFCorrLN:=Module[{f,c,n,i},
    f=OpenWrite["./output/LNBT_ORBIT.SAD"];
    $FORM="F14.7";
    Scan[
      (i=Position[zcurr,{#[1],_},1,1];i=If[i==={},0,i[1,1]];
      If[i==0,zcurr=Append[zcurr,#],
        zcurr[i,2]=zcurr[i,2]+STCur[#[1],-#[2]]])&,
      Thread[{Join[xcorrL,ycorrL],Join[xkick,-ykick]}]];
    Scan[
      (n=(LINE["NAME",#[1]]//"        ")[1,8];
      If[StringMatchQ[n,"*L*"],Write[f,n," ",#[2]]])&,zcurr];
    Do[Write[f,"!Orbit_",(LINE["NAME",i]//"            ")[1,12],
      LINE["S",i]," ",Twiss["DX",i]*1e6," ",
      Twiss["DY",i]*1e6],{i,1,LINE["Position","QD10T"]}];
    $FORM="";
    Close[f];
    System["chmod 775 ./output/LNBT_ORBIT.SAD"]];

  WriteATFCorrBT:=Module[{f,c,n,i},
    f=OpenWrite["./output/LNBT_ORBIT.SAD"];
    $FORM="F14.7";
    Scan[
      (i=Position[zcurr,{#[1],_},1,1];i=If[i==={},0,i[1,1]];
      If[i==0,zcurr=Append[zcurr,#],
        zcurr[i,2]=zcurr[i,2]+STCur[#[1],-#[2]]])&,
      Thread[{Join[xcorrT,ycorrT],Join[xkick,-ykick]}]];
    Scan[
      (n=(LINE["NAME",#[1]]//"        ")[1,8];
      If[StringMatchQ[n,"*T*"],Write[f,n," ",#[2]]])&,zcurr];
    Do[Write[f,"!Orbit_",(LINE["NAME",i]//"            ")[1,12],
      LINE["S",i]," ",Twiss["DX",i]*1e6," ",
      Twiss["DY",i]*1e6],{i,LINE["Position","QD10T"],LINE["LENGTH"]}];
    $FORM="";
    Close[f];
    System["chmod 775 ./output/LNBT_ORBIT.SAD"]];

  xcorr=LINE["POSITION","Z{HX}*"];
  ycorr=LINE["POSITION","Z{VY}*"];
  ycorr=DeleteCases[ycorr,LINE["POSITION","ZV11T"]];
  xcorrL=LINE["POSITION","Z{HX}*L"];
  ycorrL=LINE["POSITION","Z{VY}*L"];
  xcorrT=LINE["POSITION","Z{HX}*T"];
  ycorrT=LINE["POSITION","Z{VY}*T"];
  ycorrT=DeleteCases[ycorrT,LINE["POSITION","ZV11T"]];

!  xcorr=Join[xcorr,LINE["POSITION",{"ZX50T"}]];
!  xcorr=DeleteCases[xcorr,LINE["POSITION","ZH10T"]|LINE["POSITION","ZH30T"]];
!  DummyBPM=LINE["POSITION",
!    {"QD11T","QD12T","QD30T","QD31T","QD40T"}];
!  Dummydata=Table[0,{Length[DummyBPM]}];
!  bpmx=bpmx+0.0001*Table[GaussRandom[],{Length[bpmx]}];
!  bpmy=bpmy+0.0001*Table[GaussRandom[],{Length[bpmy]}];

  CorrectATFOrbit:=Module[{bpm1=bpm},
    xmat=Outer[Times,Sqrt[Twiss["BX",bpm1+0.5]/LINE["GAMMA",bpm1]],
        Sqrt[Twiss["BX",xcorr+0.5]*LINE["GAMMA",xcorr+0.5]]]*
      Sin[Outer[Max[0,#1+#2]&,Twiss["NX",bpm1+0.5],-Twiss["NX",xcorr+0.5]]];
    xdisp=Twiss["EX",bpm1];xmat=MapThread[Append[#1,#2]&,{xmat,xdisp}];
    ymat=Outer[Times,Sqrt[Twiss["BY",bpm1+0.5]/LINE["GAMMA",bpm1]],
        Sqrt[Twiss["BY",ycorr+0.5]*LINE["GAMMA",ycorr+0.5]]]*
      Sin[Outer[Max[0,#1+#2]&,Twiss["NY",bpm1+0.5],-Twiss["NY",ycorr+0.5]]];
    xkick=LinearSolve[xmat,bpmx,Threshold->cortol];
    xdp=xkick[-1];xkick=Drop[xkick,-1];
    Scan[(LINE["K0",#[1]]=-#[2])&,Thread[{xcorr,xkick}]];
    ykick=LinearSolve[ymat,bpmy,Threshold->cortol];
    Scan[(LINE["K0",#[1]]= #[2])&,Thread[{ycorr,ykick}]];
    FFS["CALC"];
    If[plotorbit,
      g1=OpticsPlot[{{"DX",{Thread[{bpm,bpmx}],FrameLabel->{"",""},Unit->Meter}},
        {"DY",{Thread[{bpm,bpmy}],FrameLabel->{"",""},Unit->Meter}}},
        Names->"ML*",Detach->True]];
  ];

  CorrectATFOrbitLN:=Module[{bpm1=bpm},
    xmat=Outer[Times,Sqrt[Twiss["BX",bpm1+0.5]/LINE["GAMMA",bpm1]],
        Sqrt[Twiss["BX",xcorrL+0.5]*LINE["GAMMA",xcorrL+0.5]]]*
      Sin[Outer[Max[0,#1+#2]&,Twiss["NX",bpm1+0.5],-Twiss["NX",xcorrL+0.5]]];
    xdisp=Twiss["EX",bpm1];xmat=MapThread[Append[#1,#2]&,{xmat,xdisp}];
    ymat=Outer[Times,Sqrt[Twiss["BY",bpm1+0.5]/LINE["GAMMA",bpm1]],
        Sqrt[Twiss["BY",ycorrL+0.5]*LINE["GAMMA",ycorrL+0.5]]]*
      Sin[Outer[Max[0,#1+#2]&,Twiss["NY",bpm1+0.5],-Twiss["NY",ycorrL+0.5]]];
    xkick=LinearSolve[xmat,bpmx,Threshold->cortol];
    xdp=xkick[-1];xkick=Drop[xkick,-1];
    Scan[(LINE["K0",#[1]]=-#[2])&,Thread[{xcorrL,xkick}]];
    ykick=LinearSolve[ymat,bpmy,Threshold->cortol];
    Scan[(LINE["K0",#[1]]= #[2])&,Thread[{ycorrL,ykick}]];
    FFS["CALC"];
    If[plotorbit,
      g1=OpticsPlot[{{"DX",{Thread[{bpm,bpmx}],FrameLabel->{"",""},Unit->Meter}},
        {"DY",{Thread[{bpm,bpmy}],FrameLabel->{"",""},Unit->Meter}}},
        Names->"ML*",Detach->True]];
  ];

  CorrectATFOrbitBT:=Module[{bpm1=bpm},
    xmat=Outer[Times,Sqrt[Twiss["BX",bpm1+0.5]/LINE["GAMMA",bpm1]],
        Sqrt[Twiss["BX",xcorrT+0.5]*LINE["GAMMA",xcorrT+0.5]]]*
      Sin[Outer[Max[0,#1+#2]&,Twiss["NX",bpm1+0.5],-Twiss["NX",xcorrT+0.5]]];
    xdisp=Twiss["EX",bpm1];xmat=MapThread[Append[#1,#2]&,{xmat,xdisp}];
    ymat=Outer[Times,Sqrt[Twiss["BY",bpm1+0.5]/LINE["GAMMA",bpm1]],
        Sqrt[Twiss["BY",ycorrT+0.5]*LINE["GAMMA",ycorrT+0.5]]]*
      Sin[Outer[Max[0,#1+#2]&,Twiss["NY",bpm1+0.5],-Twiss["NY",ycorrT+0.5]]];
    xkick=LinearSolve[xmat,bpmx,Threshold->cortol];
    xdp=xkick[-1];xkick=Drop[xkick,-1];
    Scan[(LINE["K0",#[1]]=-#[2])&,Thread[{xcorrT,xkick}]];
    ykick=LinearSolve[ymat,bpmy,Threshold->cortol];
    Scan[(LINE["K0",#[1]]= #[2])&,Thread[{ycorrT,ykick}]];
    FFS["CALC"];
    If[plotorbit,
      g1=OpticsPlot[{{"DX",{Thread[{bpm,bpmx}],FrameLabel->{"",""},Unit->Meter}},
        {"DY",{Thread[{bpm,bpmy}],FrameLabel->{"",""},Unit->Meter}}},
        Names->"ML*",Detach->True]];
  ];

  plotorbit=False;


  ScaleVc[scale_:1]:=Module[{allcav},
    allcav=LINE["NAME","C*L"];
    allcav=Select[allcav,Not[#=="CA0L"]&];
    Print['scale=',scale];
    Scan[Check[LINE["VOLT",#]=LINE["VOLT",#]*scale,]&,allcav]];
!

  ChangeVc:=Module[{},
   LINE["VOLT","CA1L"]=LINE["VOLT","CA1L"]*Sqrt[1.05591];
   LINE["VOLT","CA2L"]=LINE["VOLT","CA2L"]*Sqrt[1.05591];
   LINE["VOLT","CA3L"]=LINE["VOLT","CA3L"]*Sqrt[0.842968];
   LINE["VOLT","CA4L"]=LINE["VOLT","CA4L"]*Sqrt[0.842968];
   LINE["VOLT","CA5L"]=LINE["VOLT","CA5L"]*Sqrt[0.903231];
   LINE["VOLT","CA6L"]=LINE["VOLT","CA6L"]*Sqrt[0.903231];
   LINE["VOLT","CA7L"]=LINE["VOLT","CA7L"]*Sqrt[0.846527];
   LINE["VOLT","CA8L"]=LINE["VOLT","CA8L"]*Sqrt[0.846527];
   LINE["VOLT","CA9L"]=LINE["VOLT","CA9L"]*Sqrt[0.986058];
   LINE["VOLT","CA10L"]=LINE["VOLT","CA10L"]*Sqrt[0.986058];
   LINE["VOLT","CA11L"]=LINE["VOLT","CA11L"]*Sqrt[0.806663];
   LINE["VOLT","CA12L"]=LINE["VOLT","CA12L"]*Sqrt[0.806663];
   LINE["VOLT","CA13L"]=LINE["VOLT","CA13L"]*Sqrt[1.06065];
   LINE["VOLT","CA14L"]=LINE["VOLT","CA14L"]*Sqrt[1.06065];
   LINE["VOLT","CA15L"]=LINE["VOLT","CA15L"]*Sqrt[1];
   LINE["VOLT","CA16L"]=LINE["VOLT","CA16L"]*Sqrt[1];
 ];
!  


  allZ=LINE["NAME","Z*"];
  WriteZall[file_]:=Module[{f},
    f=OpenWrite[file];
    $FORM="F14.7";
     Map[Write[f,#," ",STCur[#,LINE["K0",#]]]&,allZ];
    Close[f]];


  PrintKall:=Module[{},
   Scan[Print[#//"  K1  ",LINE["K1",#]]&,allQ];
   Scan[Print[#//"  K0  ",LINE["K0",#]]&,allZ];
  ];


 allBtQ=LINE["NAME","Q*T"];  
  SetBtQuad[n_:1]:=Module[{
    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{},pair},
    k1list0=Select[k1list,MemberQ[allBtQ,#[1]]&];
    pair=Select[pairedQ,MemberQ[allBtQ,#[[2]]]&];
    k1list1=Select[k1list0,(Not[#[1][6,7]=="_S"])&];
    k1list2=Select[k1list0,(#[1][6,7]=="_S")&];
    Map[Check[LINE["K1",#[1]]=QK[#[1],#[2]],Print["error1"]]&,k1list1];
    Scan[Check[(LINE["K1",#[2]]=LINE["K1",#[1]]*QcorrFac[#[2]]/QcorrFac[#[1]]),Print["error22"]]&,pair];
    k1list2=Map[{#[1][1,5],#[2]}&,k1list2];
    Map[Check[(LINE["K1",#[1]]=LINE["K1",#[1]]+
    Sign[#[2]*RTMQ[#[1]]]*QK[#[1],Abs[#[2]*RTMQ[#[1]]]]),Print["error2"]]&,k1list2];
];

  Bmag[a1_,b1_,a2_,b2_]:=((b2/b1+b1/b2+b1*b2*(a1/b1-a2/b2)^2)/2);



