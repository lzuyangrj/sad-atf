

 SelectMax[list_,n_]:=Module[{v0,name0,name,v,i,j,jj,newlist}, 
  If[Length[list]>n && n>=1,
    v0=Table[0,{n}];
    name0=Table["",{n}];
    For[i=1,i<=Length[list],i=i+1,
     name=list[i,1];
     v=list[i,2];
     For[j=1,j<=n,j=j+1,
      If[Abs[v]>=Abs[v0[j]],
        For[jj=n,jj>j,jj=jj-1,
         v0[jj]=v0[jj-1];
         name0[jj]=name0[jj-1];
        ];
       v0[j]=v;
       name0[j]=name;
       j=n+1;
      ];
     ];
    ];
     newlist=Thread[{name0,v0}];
  , If[n<1,
     newlist={};
    ,
     newlist=list;
    ];
  ];
  newlist];


  NewCorrectCOD:=Module[{
    fixsteers=FixSteer/.Option/.FixSteer->{},
    k0list=ATFSteer/.ATFData[1]/.ATFSteer->{},
    chop=ChopIntensity/.Option/.ChopIntensity->0,
    tunelist=ATFTune/.ATFData[1]/.ATFTune->{},
    ratio=1.414},
    mx=NSteerX/.(CODCorrect/.Option)/.(NSteerX ->0);
    my=NSteerY/.(CODCorrect/.Option)/.(NSteerY ->0);
    bpmlist=ATFBPM/.ATFData[1]/.ATFBPM->{};
    bpmlist=Select[bpmlist,(#[4]>chop)&];
    bpmlist=Select[bpmlist,MemberQ[goodbpmlist,#[1]]&];
!    injectbpm={"M.17","M.18","M.19","M.20","M.21","M.22","M.23","M.24"};
!    nofitbpm={"M.17","M.18","M.19","M.20","M.21","M.22","M.23","M.24",
!     "M.25","M.26","M.27","M.28","M.29",,"M.30",
!     "M.67","M.68","M.69","M.70","M.71","M.72","M.73","M.74","M.75",
!      "M.76","M.77","M.78","M.79"};
!    bpmlist=Select[bpmlist,Not[MemberQ[injectbpm,#[1]]]&]; bpmlist0=bpmlist;
!    bpmlist=Select[bpmlist,Not[MemberQ[nofitbpm,#[1]]]&];
      FFS["Z* K0 0;"];
    NXm=tunelist[1];NYm=tunelist[2];
    FFS["FIT NX NXm NY NYm"];
    Print[NXm," ",NYm];
    FFS["FREE QF* go; cal"];
    FFS["Reject Total;"]; 
    FFS["FIX Q* "];

    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    k0list=Select[k0list,MemberQ[LINE["Name","Z*R"],#[1]]&];

    maxk0=0.16138E-02;

    bpmfitlist=Map[(
      If[((LINE["Position",#[[1]]]=>LINE["Position","M.19"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.80"])),
      {#[[1]],#[[2]],#[[3]],Twiss["DX",#[[1]]],0},
      {#[[1]],#[[2]],#[[3]],0,0}])&,bpmlist];

    bpmnamelist=Map[#[1]&,bpmfitlist];

    soft=2;

    If[mx<=0 && my>=1, 
     k0listy=Select[k0list,(StringMatchQ[#[1],"ZV*R"])&];
     k0listy=Select[k0listy,MemberQ[LINE["Name","ZV*R"],#[1]]&];

     k0listy=Select[k0listy,Not[MemberQ[fixsteers,#[1]]]&];

     Scan[(maxdzv[#[1]]=#[2]+maxk0)&,k0listy];
     Scan[(mindzv[#[1]]=#[2]-maxk0)&,k0listy];
         
     usezv=Select[Map[#[1]&,k0listy],(maxdzv[#]>0 && mindzv[#]<0)&];
     dk0=1e-4;
     datazv=Map[(steer=#;
      FFS["CAL"]; 
      cody0=Map[(Twiss["DY",#])&,bpmnamelist]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      cody1=Map[(Twiss["DY",#])&,bpmnamelist];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (cody1-cody0)/dk0)&,usezv];
     A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
     bpmy=Join[Map[(#[3])&,bpmfitlist],Table[0,{Length[usezv]}]];
     dzv=LinearSolve[A,bpmy];


     If[Length[usezv]>my,
      While[Length[usezv]>my*ratio && my=>1,
       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezv,datazv,dzv,Abs[dzv]}]];
       datalist=SelectMax[datalist,Length[usezv]/ratio];
       usezv=Transpose[Transpose[datalist][1]][1];
       datazv=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
       bpmy=Join[Map[(#[3])&,bpmfitlist],Table[0,{Length[usezv]}]];
       dzv=LinearSolve[A,bpmy];
      ];

       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezv,datazv,dzv,Abs[dzv]}]];
       datalist=SelectMax[datalist,my];
       usezv=Transpose[Transpose[datalist][1]][1];
       datazv=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
       bpmy=Join[Map[(#[3])&,bpmfitlist],Table[0,{Length[usezv]}]];
       dzv=LinearSolve[A,bpmy];

     ];

     bpmfitlist0=bpmfitlist;
     my0=my;
     dzvfix={};

     iflimit=0;
     Scan[If[#[2]>maxdzv[#[1]] || #[2]<mindzv[#[1]], 
     iflimit=1]&,Thread[{usezv,dzv}]];
     
     While[iflimit  && my=>1, 
      iz=0;
      dzv0=dzv;
      usezv0=usezv;
      Scan[(iz=iz+1;
       If[#[2]>maxdzv[#[1]],
        datazv=Drop[datazv,{iz,iz}];
        usezv=Drop[usezv,{iz,iz}]; 
        iz=iz-1;
        dzvfix=Append[dzvfix,{#[1],maxdzv[#[1]]}];
        LINE["K0",#[1]]=maxdzv[#[1]]];
       If[#[2]<mindzv[#[1]], 
        datazv=Drop[datazv,{iz,iz}];
        usezv=Drop[usezv,{iz,iz}]; 
        iz=iz-1;
        dzvfix=Append[dzvfix,{#[1],mindzv[#[1]]}];
        LINE["K0",#[1]]=mindzv[#[1]]];
      )&,Thread[{usezv0,dzv0}]];
      If[Length[usezv]=>1,
       codylist0=Map[Twiss["DY",#[1]]&,bpmfitlist];
       FFS["CAL"];
       codylist1=Map[Twiss["DY",#[1]]&,bpmfitlist];
       bpmfitlist=Map[{#[1,1],#[1,2],#[1,3]-#[2],#[1,4],#[1,5]}&,
         Thread[{bpmfitlist,codylist1-codylist0}]];
       A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
       bpmy=Join[Map[(#[3])&,bpmfitlist],Table[0,{Length[usezv]}]];
       dzv=LinearSolve[A,bpmy];
      ,
       dzv={};
      ];
      my=my0-Length[dzvfix];
      iflimit=0;
      Scan[If[#[2]>maxdzv[#[1]] || #[2]<mindzv[#[1]], 
      iflimit=1]&,Thread[{usezv,dzv}]];
     ];

     Print[my," ",Length[dzv]," ",ratio];
     Scan[(LINE["K0",#[1]]=#[2])&,dzvfix];
     Scan[(LINE["K0",#[1]]=#[2])&,Thread[{usezv,dzv}]];
   ];

    If[my<=0 && mx>=1, 
     k0listx=Select[k0list,(StringMatchQ[#[1],"ZH*R"])&];
     k0listx=Select[k0listx,MemberQ[LINE["Name","ZH*R"],#[1]]&];

     k0listx=Select[k0listx,Not[MemberQ[fixsteers,#[1]]]&];

     Scan[(maxdzh[#[1]]=#[2]+maxk0)&,k0listx];
     Scan[(mindzh[#[1]]=#[2]-maxk0)&,k0listx];
         
     usezh=Select[Map[#[1]&,k0listx],(maxdzh[#]>0 && mindzh[#]<0)&];
     dk0=1e-4;
     datazh=Map[(steer=#;
      FFS["CAL"]; 
      codx0=Map[(Twiss["DX",#])&,bpmnamelist]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      codx1=Map[(Twiss["DX",#])&,bpmnamelist];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (codx1-codx0)/dk0)&,usezh];
     A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
     bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist],Table[0,{Length[usezh]}]];
     dzh=LinearSolve[A,bpmx];


     If[Length[usezh]>mx,
      While[Length[usezh]>mx*ratio && mx=>1, 
       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezh,datazh,dzh,Abs[dzh]}]];
       datalist=SelectMax[datalist,Length[usezh]/ratio];
       usezh=Transpose[Transpose[datalist][1]][1];
       datazh=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
       bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist],Table[0,{Length[usezh]}]];
       dzh=LinearSolve[A,bpmx];
      ];

       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezh,datazh,dzh,Abs[dzh]}]];
       datalist=SelectMax[datalist,mx];
       usezh=Transpose[Transpose[datalist][1]][1];
       datazh=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
       bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist],Table[0,{Length[usezh]}]];
       dzh=LinearSolve[A,bpmx];

     ];

     Print["bpmx ",bpmx];
     Print["usezh ",usezh];
     Print["dzh ",dzh];

     bpmfitlist0=bpmfitlist;
     mx0=mx;
     dzhfix={};

     iflimit=0;
     Scan[If[#[2]>maxdzh[#[1]] || #[2]<mindzh[#[1]], 
     iflimit=1]&,Thread[{usezh,dzh}]];
     
     While[iflimit  && mx=>1, 
      iz=0;

      dzh0=dzh;
      usezh0=usezh;
      Scan[(iz=iz+1;
       If[#[2]>maxdzh[#[1]],
        datazh=Drop[datazh,{iz,iz}];
        usezh=Drop[usezh,{iz,iz}]; 
        iz=iz-1;
        dzhfix=Append[dzhfix,{#[1],maxdzh[#[1]]}];
        LINE["K0",#[1]]=maxdzh[#[1]]];
       If[#[2]<mindzh[#[1]], 
        datazh=Drop[datazh,{iz,iz}];
        usezh=Drop[usezh,{iz,iz}]; 
        iz=iz-1;
        dzhfix=Append[dzhfix,{#[1],mindzh[#[1]]}];
        LINE["K0",#[1]]=mindzh[#[1]]];
      )&,Thread[{usezh0,dzh0}]];
      If[Length[usezh]=>1,
       codxlist0=Map[Twiss["DX",#[1]]&,bpmfitlist];
       FFS["CAL"];
       codxlist1=Map[Twiss["DX",#[1]]&,bpmfitlist];
       bpmfitlist=Map[{#[1,1],#[1,2]-#[2],#[1,3],#[1,4],#[1,5]}&,
         Thread[{bpmfitlist,codxlist1-codxlist0}]];
       A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
       bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist],Table[0,{Length[usezh]}]];
       dzh=LinearSolve[A,bpmx];
      ,
       dzh={};
      ];
      mx=mx0-Length[dzhfix];
      iflimit=0;
      Scan[If[#[2]>maxdzh[#[1]] || #[2]<mindzh[#[1]], 
      iflimit=1]&,Thread[{usezh,dzh}]];
     ];

     Print[mx," ",Length[dzh]," ",ratio];
     Scan[(LINE["K0",#[1]]=#[2])&,dzhfix];
     Scan[(LINE["K0",#[1]]=#[2])&,Thread[{usezh,dzh}]];
   ];

    DrawCOD[bpmfitlist,0];
    orbitdata=ATFRingOrbit;
    injectchange=Twiss[{"DX","DPX","DY","DPY"},"IIN"];
    Print["return k0"];
    Scan[Check[LINE["K0",#[1]]=-reductionfactor*LINE["K0",#[1]]+#[2],]&,k0list];


!    LINE["K0","ZH1R"]=1e-3;
!    LINE["K0","ZH2R"]=1e-3;
!    LINE["K0","ZH3R"]=1e-3;
!    LINE["K0","ZH4R"]=1e-3;
!    LINE["K0","ZH5R"]=1e-3;
!    LINE["K0","ZH6R"]=.5e-3;
!    LINE["K0","ZH48R"]=.5e-3;

  ];


  GoCorrectCODFixinjFun:=Module[{
    k0list=ATFSteer/.ATFData[1]/.ATFSteer->{},
    chop=ChopIntensity/.Option/.ChopIntensity->0,
    tunelist=ATFTune/.ATFData[1]/.ATFTune->{},
    ratio=1.414},
    mx=NSteerX/.(CODCorrect/.Option)/.(NSteerX ->0);
    my=NSteerY/.(CODCorrect/.Option)/.(NSteerY ->0);
    bpmlist=ATFBPM/.ATFData[1]/.ATFBPM->{};
    bpmlist=Select[bpmlist,(#[4]>chop)&];
    bpmlist=Select[bpmlist,MemberQ[goodbpmlist,#[1]]&];
    injectbpm={"M.17","M.18","M.19","M.20","M.21","M.22","M.23","M.24"};
    nofitbpm={"M.17","M.18","M.19","M.20","M.21","M.22","M.23","M.24",
     "M.25","M.26","M.27","M.28","M.29",,"M.30",
     "M.67","M.68","M.69","M.70","M.71","M.72","M.73","M.74","M.75",
      "M.76","M.77","M.78","M.79"};
!    bpmlist=Select[bpmlist,Not[MemberQ[injectbpm,#[1]]]&]; bpmlist0=bpmlist;
!    bpmlist=Select[bpmlist,Not[MemberQ[nofitbpm,#[1]]]&];
      FFS["Z* K0 0;"];
    NXm=tunelist[1];NYm=tunelist[2];
    FFS["FIT NX NXm NY NYm"];
    Print[NXm," ",NYm];
    FFS["FREE Q* go; cal"];
    FFS["Reject Total;"]; 
    FFS["FIX Q* "];

!!    injfitweight=10;
    fitfuncinj:=Join[Twiss[{"DX","DY","DPX","DPY"},"IIN"],
                 Twiss[{"DX","DY","DPX","DPY"},"KII"]]*injfitweight;
!!  fitfuncinj:={};
    dxytol=If[cortol>0,cortol,0];
    fitfuncx:=If[mx>=1,Map[(Max[0,Abs[Twiss["DX",#[[1]]]-#[[2]]]-dxytol])&,bpmlist],{}];
    fitfuncy:=If[my>=1,Map[(Max[0,Abs[Twiss["DY",#[[1]]]-#[[3]]]-dxytol])&,bpmlist],{}];
!    FitFunction:=Join[fitfuncx,fitfuncy,fitfuncinj];
    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    If[mx>0,
     FitFunction:=Join[fitfuncx,fitfuncinj];
     k0listx=Select[k0list,(StringMatchQ[#[1],"ZH*R"])&],
     k0listx={};
    ];
    If[my>0,
      FitFunction:=Join[fitfuncy,fitfuncinj];
      k0listy=Select[k0list,(StringMatchQ[#[1],"ZV*R"])&],
      k0listy={};
    ];

    Scan[(If[#[2]-maxk0>-4e-4,FFS[" "//#[1]//" MIN "//#[2]-maxk0]])&,k0listx];
    Scan[(If[#[2]+maxk0<4e-4,FFS[" "//#[1]//" MAX "//#[2]+maxk0]])&,k0listx];
    Scan[(If[#[2]-maxk0>-4e-4,FFS[" "//#[1]//" MIN "//#[2]-maxk0]])&,k0listy];
    Scan[(If[#[2]+maxk0<4e-4,FFS[" "//#[1]//" MAX "//#[2]+maxk0]])&,k0listy];

    Scan[FFS["Free "//#[1]]&,k0listx];
    Scan[FFS["Free "//#[1]]&,k0listy];
    FFS["MAXI 3;go"]; 

    While[ Length[k0listx]>mx*ratio || Length[k0listy]>my*ratio,
      k0listx=Map[{#[1],LINE["K0",#[1]]}&,k0listx];
      k0listy=Map[{#[1],LINE["K0",#[1]]}&,k0listy];

      If[Length[k0listx]>mx*ratio,
       k0listx=SelectMax[k0listx,Length[k0listx]/ratio];
      ];
      If[Length[k0listy]>my*ratio,
       k0listy=SelectMax[k0listy,Length[k0listy]/ratio];
      ];
      Print["Selected ",Length[k0listx]];
      Scan[Print[" "//#[1]]&,k0listx];
      Print["Selected ",Length[k0listy]];
      Scan[Print[" "//#[1]]&,k0listy];
      Print[" "];
      FFS["Z* K0 0"];
      FFS["Fix Z*"];
      Scan[FFS["Free "//#[1]]&,k0listx];
      Scan[FFS["Free "//#[1]]&,k0listy];
      FFS["MAXI 3;go"];
    ];

      k0listx=Map[{#[1],LINE["K0",#[1]]}&,k0listx];
      k0listy=Map[{#[1],LINE["K0",#[1]]}&,k0listy];
    k0listx=SelectMax[k0listx,mx];
    k0listy=SelectMax[k0listy,my];

    FFS["Z* K0 0"];
    FFS["Fix Z*"];
    Scan[FFS["Free "//#[1]]&,k0listx];
    Scan[FFS["Free "//#[1]]&,k0listy];
    Scan[Print[" "//#[1]]&,k0listx];
    Scan[Print[" "//#[1]]&,k0listy];
    FFS["MAXI 3;go"]; 
    Print["matching residual ",MatchingResidual];
    DrawCOD[bpmlist,0];
    orbitdata=ATFRingOrbit;
    injectchange=Twiss[{"DX","DPX","DY","DPY"},"IIN"];
    Print["return k0"];
    Scan[Check[LINE["K0",#[1]]=-reductionfactor*LINE["K0",#[1]]+#[2],]&,k0list];
  ];




  ChangeSteer[optics_,k0file_]:=Module[
    LoadRingOptics[optics];
!    Get["/users/atfopr/sad/steerchange.dat"];
    Get[k0file];
    Scan[(LINE["K0",#[[1]]]=LINE["K0",#[[1]]]+#[[2]])&,k0change];
  ];




  GoCorrectCODFit:=Module[{
    k0list=ATFSteer/.ATFData[1]/.ATFSteer->{},
    chop=ChopIntensity/.Option/.ChopIntensity->0,
    tunelist=ATFTune/.ATFData[1]/.ATFTune->{},
    ratio=1.414},
    mx=NSteerX/.(CODCorrect/.Option)/.(NSteerX ->0);
    my=NSteerY/.(CODCorrect/.Option)/.(NSteerY ->0);
    bpmlist=ATFBPM/.ATFData[1]/.ATFBPM->{};
    bpmlist=Select[bpmlist,(#[4]>chop)&];
    bpmlist=Select[bpmlist,MemberQ[goodbpmlist,#[1]]&];
    injectbpm={"M.17","M.18","M.19","M.20","M.21","M.22","M.23","M.24"};
    nofitbpm={"M.17","M.18","M.19","M.20","M.21","M.22","M.23","M.24",
     "M.25","M.26","M.27","M.28","M.29",,"M.30",
     "M.67","M.68","M.69","M.70","M.71","M.72","M.73","M.74","M.75",
      "M.76","M.77","M.78","M.79"};
!    bpmlist=Select[bpmlist,Not[MemberQ[injectbpm,#[1]]]&]; bpmlist0=bpmlist;
!    bpmlist=Select[bpmlist,Not[MemberQ[nofitbpm,#[1]]]&];
      FFS["Z* K0 0;"];
    NXm=tunelist[1];NYm=tunelist[2];
    FFS["FIT NX NXm NY NYm"];
    Print[NXm," ",NYm];
    FFS["FREE Q* go; cal"];
    FFS["Reject Total;"]; 
    FFS["FIX Q* "];

!!!    injfitweight=10;
!    fitfuncinj:=Join[Twiss[{"DX","DY","DPX","DPY"},"IIN"],
!                 Twiss[{"DX","DY","DPX","DPY"},"KII"]]*injfitweight;
!!!  fitfuncinj:={};
!    dxytol=If[cortol>0,cortol,0];
!    fitfuncx:=If[mx>=1,Map[(Max[0,Abs[Twiss["DX",#[[1]]]-#[[2]]]-dxytol])&,bpmlist],{}];
!    fitfuncy:=If[my>=1,Map[(Max[0,Abs[Twiss["DY",#[[1]]]-#[[3]]]-dxytol])&,bpmlist],{}];
!    FitFunction:=Join[fitfuncx,fitfuncy,fitfuncinj];
    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    If[mx>0,
     Scan[FFS["FIT "//#[1]//" DX "//#[2]]&,bpmlist];
     k0listx=Select[k0list,(StringMatchQ[#[1],"ZH*R"])&],
     k0listx={};
    ];
    If[my>0,
      Scan[FFS["FIT "//#[1]//" DY "//#[3]]&,bpmlist];
      k0listy=Select[k0list,(StringMatchQ[#[1],"ZV*R"])&],
      k0listy={};
    ];

    Scan[(If[#[2]-maxk0>-4e-4,FFS[" "//#[1]//" MIN "//#[2]-maxk0]])&,k0listx];
    Scan[(If[#[2]+maxk0<4e-4,FFS[" "//#[1]//" MAX "//#[2]+maxk0]])&,k0listx];
    Scan[(If[#[2]-maxk0>-4e-4,FFS[" "//#[1]//" MIN "//#[2]-maxk0]])&,k0listy];
    Scan[(If[#[2]+maxk0<4e-4,FFS[" "//#[1]//" MAX "//#[2]+maxk0]])&,k0listy];

    Scan[FFS["Free "//#[1]]&,k0listx];
    Scan[FFS["Free "//#[1]]&,k0listy];
    FFS["MAXI 3;go"]; 

    While[ Length[k0listx]>mx*ratio || Length[k0listy]>my*ratio,
      k0listx=Map[{#[1],LINE["K0",#[1]]}&,k0listx];
      k0listy=Map[{#[1],LINE["K0",#[1]]}&,k0listy];

      If[Length[k0listx]>mx*ratio,
       k0listx=SelectMax[k0listx,Length[k0listx]/ratio];
      ];
      If[Length[k0listy]>my*ratio,
       k0listy=SelectMax[k0listy,Length[k0listy]/ratio];
      ];
      Print["Selected ",Length[k0listx]];
      Scan[Print[" "//#[1]]&,k0listx];
      Print["Selected ",Length[k0listy]];
      Scan[Print[" "//#[1]]&,k0listy];
      Print[" "];
      FFS["Z* K0 0"];
      FFS["Fix Z*"];
      Scan[FFS["Free "//#[1]]&,k0listx];
      Scan[FFS["Free "//#[1]]&,k0listy];
      FFS["MAXI 3;go"];
    ];

      k0listx=Map[{#[1],LINE["K0",#[1]]}&,k0listx];
      k0listy=Map[{#[1],LINE["K0",#[1]]}&,k0listy];
    k0listx=SelectMax[k0listx,mx];
    k0listy=SelectMax[k0listy,my];

    FFS["Z* K0 0"];
    FFS["Fix Z*"];
    Scan[FFS["Free "//#[1]]&,k0listx];
    Scan[FFS["Free "//#[1]]&,k0listy];
    Scan[Print[" "//#[1]]&,k0listx];
    Scan[Print[" "//#[1]]&,k0listy];
    FFS["MAXI 3;go"]; 
    Print["matching residual ",MatchingResidual];
    DrawCOD[bpmlist,0];
    orbitdata=ATFRingOrbit;
    injectchange=Twiss[{"DX","DPX","DY","DPY"},"IIN"];
    Print["return k0"];
    Scan[Check[LINE["K0",#[1]]=-reductionfactor*LINE["K0",#[1]]+#[2],]&,k0list];
  ];





