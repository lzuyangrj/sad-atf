  bend=Element["Name","BW*"];
  quad=Element["Name","Q*R"];
  sth=Element["Name","ZH*R"];
  sthw=Element["Name","ZHW*R"];
!  sth=Element["Name","Z{HW}*R"];
  sth=Select[sth,Not[MemberQ[sthw,#]]&];
  stv=Element["Name","ZV*R"];
  sxt=Element["Name","S{FD}*R"];
  cav=Element["Name","CA*"];
  If[Length[cav]==0,cav={cav}];

 ringtune:={Twiss["NX",LINE["Length"]]/PI/2,Twiss["NY",LINE["Length"]]/PI/2};
  
!  SetRingSext[n_:1]:=Module[{
!   k2list=ATFSext/.ATFData[n]/.ATFSext->{}},
!    Scan[Check[Element["K2",#[1]]=#[2],]&,k2list]];
!  SetRingQuad[n_:1]:=Module[{
!    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{}},
!    Scan[Check[Element["K1",#[1]]=#[2],]&,k1list]];
!  SetRingSteer[n_:1]:=Module[{
!    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{}},
!    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0list]];
!  SetRingVc[n_:1]:=Module[{
!    vclist=ATFVcPhi/.ATFData[n]/.ATFVcPhi->{}},
!    Scan[Check[LINE["VOLT",#[1]]=#[2],]&,vclist]];

  SetRingSext[n_:1]:=Module[{
   k2list=ATFSext/.ATFData[n]/.ATFSext->{}},
    k2list=Select[k2list,MemberQ[sxt,#[1]]&];
    Scan[Check[Element["K2",#[1]]=#[2],]&,k2list];
    If[Element["K2","SD1R"]>0,
      Element["K2","SD1R"]=-Element["K2","SD1R"]]];
  SetRingQuad[n_:1]:=Module[{
    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{}},
    k1list=Select[k1list,MemberQ[quad,#[1]]&];
    Scan[Check[Element["K1",#[1]]=#[2],]&,k1list]];
  SetRingSteer[n_:1]:=Module[{
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{}},
    k0list=Select[k0list,(MemberQ[sth,#[1]] || MemberQ[stv,#[1]])&];
    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0list]];
  SetRingBend[n_:1]:=Module[{
    k0blist=ATFBend/.ATFData[n]/.ATFBend->{}},
    k0blist=Select[k0list,MemberQ[bend,#[1]]&];
    Scan[Check[LINE["K0",#[1]]=#[2],]&,k0blist]];
  SetRingVc[n_:1]:=Module[{
    vclist=ATFVcPhi/.ATFData[n]/.ATFVcPhi->{}},
    vclist=Select[vclist,MemberQ[cav,#[1]]&];
    Scan[Check[LINE["VOLT",#[1]]=#[2],]&,vclist]];

!!!!!!!
  SaveRingOptics[f_]:=Module[
        {fn=OpenWrite["./ringoptics/"//f]},
    qv=Thread[{quad,Element["K1",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    bendv=Thread[{bend,Element["K0",bend]}];
    cavv=Thread[{cav,Element["VOLT",cav]}];
    $FORM="";
    Write[fn,
      "qv=",qv,
      ";\n\nsthv=",sthv,
      ";\n\nstvv=",stvv,
      ";\n\nsxtv=",sxtv,
      ";\n\nbendv=",bendv,
      ";\n\ncavv=",cavv,";"];
    Close[fn];
    System["chmod 664 "//f]];

  LoadRingOptics[f_]:=Module[{},
!   {qv,sxtv,sthv,stvv,cavv,bendv},
!   Get["./ringoptics/"//f];
    Get["./"//f];
    Map[(Element["K1",#[1]]=0)&,Element["Name","Q*"]];
    Map[(Element["K1",#[1]]=#[2])&,qv];
!    Print[qv];
!
!    Map[(Print[Element["K2",#[1]]," ",#[2]])&,sxtv];
!
    Map[(Element["K2",#[1]]=#[2])&,sxtv];
    Map[(Element["K0",#[1]]=#[2])&,sthv];
    Map[(Element["K0",#[1]]=#[2])&,stvv];
!    Map[(Element["K0",#[1]]=#[2])&,bendv];
!    Map[(Element["VOLT",#[1]]=#[2])&,cavv];
 ];

  WriteRingOptics[f_,epilog_,opt___]:=Module[{
        fn=OpenWrite[f],
        def={SADFile->"",Steer->True},sadfile,steer},
    sadfile=SADFile/.{opt}/.def;
    steer=Steer/.{opt}/.def;
    Write[fn,"!"//"    "//sadfile];
    $FORM="F12.6";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    bendv=Thread[{bend,Element["K0",bend]}];
    cavv=Thread[{cav,Element["K0",cav]}];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,bendv];
    If[steer,
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
      Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv]];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];
    Close[fn];
    System["chmod 664 "//f]];


  WriteRingOrbit[f_,epilog_]:=Module[{i,
        fn=OpenWrite[f]},
    $FORM="";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    cavv=Thread[{cav,Element["K0",cav]}];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];

    $FORM="12.6";
    Do[Write[fn,'Orbit_',(LINE["NAME",i]//'            ')[1,12],
      LINE["S",i]," ",Twiss["DX",i]*1e0," ",
      Twiss["DY",i]*1e0],{i,1,LINE["LENGTH"]}];

    Close[fn];
    $FORM="";
    System["chmod 664 "//f];
    ];

   RenumberBPM[b_]:=(
        $FORM="";
        Map[MapAt[("M."//Mod[ToExpression[#[3,-1]]+96-22,96]+1)&,#,{1}]&,b]);


  GoCorrectOneTurnOrbit[n_:1]:=Module[{
    bpmlist=ATFBPM/.ATFData[n]/.ATFBPM->{},
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{},
    chop=ChopIntensity/.Option/.ChopIntensity->0},
    bpmlist=Select[bpmlist,(#[4]>chop)&];
    k0list=Select[k0list,(#[1][1]=="Z")&];
    k0list=Select[k0list,(LINE["S",#[1]]>0)&];
    Scan[FFS["Fit "//#[1]//" DX "//#[2]//" DY "//#[3]]&,bpmlist];
    Scan[FFS["Free "//#[1]]&,k0list];
!    FFS["Free DPI"];
!    FFS["Free DXI"];FFS["Free DPXI"];
!    FFS["Free DYI"];FFS["Free DPYI"];
    FFS["MAXI 3;go"];
!     DrawCOD[bpmlist,DPI];
     DrawCOD[bpmlist,0];
    Scan[Check[LINE["K0",#[1]]=-LINE["K0",#[1]]+#[2],]&,k0list];
  ];

 SetNoTrimBendK1:=Scan[(LINE["K1",#]=-1.15943)&,LINE["Name","BH1R*"]];

!! In the case Considering effective length of the bend
!!  fitted total K1 becomes about 1.022 times that in the case assuming length=1m
!!   because of strong change of betaY in the bend
  SetBendOldK0K1:=Module[{anglebh=.1745329252,k1bh7=-1.1755845,k1bh1=-1.2013416},
  LINE["K0","BH1R.1"]=anglebh*(1/(1+0.022)-1);
  Scan[(LINE["K0",#]=LINE["K0","BH1R.1"])&,LINE["Name","BH1R.*"]];
  LINE["K0","BHE"]=anglebh*0.011/(1+.022);
  Scan[(LINE["K0",#]=LINE["K0","BHE.1"])&,LINE["Name","BHE.*"]];

  LINE["K1","BH1R.7"]=k1bh7/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.7"])&,LINE["Name","BH1R.*"]];
  LINE["K1","BH1R.1"]=k1bh1/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.1"])&,LINE["Name","BH1R.{23456}"]];

  LINE["K1","BHE.13"]=k1bh7*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.13"])&,LINE["Name","BHE.*"]];
  LINE["K1","BHE.1"]=k1bh1*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.1"])&,LINE["Name","BHE.{23456789}"]];
  Scan[(LINE["K1",#]=LINE["K1","BHE.1"])&,LINE["Name","BHE.1{012}"]];
 ];

 SetBendK0K1:=Module[{anglebh=.1745329252,k1bh8=-1.1823,k1bh7=-1.1672,k1bh1=-1.2039},
  LINE["K0","BH1R.1"]=anglebh*(1/(1+0.022)-1);
  Scan[(LINE["K0",#]=LINE["K0","BH1R.1"])&,LINE["Name","BH1R.*"]];
  LINE["K0","BHE"]=anglebh*0.011/(1+.022);
  Scan[(LINE["K0",#]=LINE["K0","BHE.1"])&,LINE["Name","BHE.*"]];
   typebh7={"BH1R.7","BH1R.10","BH1R.13","BH1R.15","BH1R.17","BH1R.18",
             "BH1R.19","BH1R.20","BH1R.23","BH1R.26","BH1R.27","BH1R.28",
             "BH1R.29","BH1R.34","BH1R.35","BH1R.36"};
   typebh1={"BH1R.1","BH1R.2","BH1R.3","BH1R.4","BH1R.5","BH1R.6"};
  LINE["K1","BH1R.8"]=k1bh8/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.8"])&,LINE["Name","BH1R.*"]];
  LINE["K1","BH1R.7"]=k1bh7/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.7"])&,typebh7];
  LINE["K1","BH1R.1"]=k1bh1/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.1"])&,typebh1];

   typebhe7={"BHE.13","BHE.14","BHE.19","BHE.20","BHE.25","BHE.26","BHE.29","BHE.30",
             "BHE.33","BHE.34","BHE.35","BHE.36","BHE.37","BHE.38","BHE.39","BHE.40",
             "BHE.45","BHE.46","BHE.51","BHE.52","BHE.53","BHE.54","BHE.55","BHE.56",
             "BHE.57","BHE.58","BHE.67","BHE.68","BHE.69","BHE.70","BHE.71","BHE.72"};
   typebh1={"BHE.1","BHE.2","BHE.3","BHE.4","BHE.5","BHE.6","BHE.7","BHE.8",
            "BHE.9","BHE.10","BHE.11","BHE.12"};
  LINE["K1","BHE.15"]=k1bh8*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.15"])&,LINE["Name","BHE.*"]];
  LINE["K1","BHE.13"]=k1bh7*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.13"])&,typebhe7];
  LINE["K1","BHE.1"]=k1bh1*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.1"])&,typebhe1];
 ];

 SetNotrimBendK0K1:=Module[{anglebh=.1745329252,k1bh8=-1.18493746,k1bh7=-1.18493746,k1bh1=-1.18493746},
  LINE["K0","BH1R.1"]=anglebh*(1/(1+0.022)-1);
  Scan[(LINE["K0",#]=LINE["K0","BH1R.1"])&,LINE["Name","BH1R.*"]];
  LINE["K0","BHE"]=anglebh*0.011/(1+.022);
  Scan[(LINE["K0",#]=LINE["K0","BHE.1"])&,LINE["Name","BHE.*"]];
   typebh7={"BH1R.7","BH1R.10","BH1R.13","BH1R.15","BH1R.17","BH1R.18",
             "BH1R.19","BH1R.20","BH1R.23","BH1R.26","BH1R.27","BH1R.28",
             "BH1R.29","BH1R.34","BH1R.35","BH1R.36"};
   typebh1={"BH1R.1","BH1R.2","BH1R.3","BH1R.4","BH1R.5","BH1R.6"};
  LINE["K1","BH1R.8"]=k1bh8/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.8"])&,LINE["Name","BH1R.*"]];
  LINE["K1","BH1R.7"]=k1bh7/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.7"])&,typebh7];
  LINE["K1","BH1R.1"]=k1bh1/(1+0.022);
  Scan[(LINE["K1",#]=LINE["K1","BH1R.1"])&,typebh1];

   typebhe7={"BHE.13","BHE.14","BHE.19","BHE.20","BHE.25","BHE.26","BHE.29","BHE.30",
             "BHE.33","BHE.34","BHE.35","BHE.36","BHE.37","BHE.38","BHE.39","BHE.40",
             "BHE.45","BHE.46","BHE.51","BHE.52","BHE.53","BHE.54","BHE.55","BHE.56",
             "BHE.57","BHE.58","BHE.67","BHE.68","BHE.69","BHE.70","BHE.71","BHE.72"};
   typebh1={"BHE.1","BHE.2","BHE.3","BHE.4","BHE.5","BHE.6","BHE.7","BHE.8",
            "BHE.9","BHE.10","BHE.11","BHE.12"};
  LINE["K1","BHE.15"]=k1bh8*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.15"])&,LINE["Name","BHE.*"]];
  LINE["K1","BHE.13"]=k1bh7*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.13"])&,typebhe7];
  LINE["K1","BHE.1"]=k1bh1*0.011/(1+.022);
  Scan[(LINE["K1",#]=LINE["K1","BHE.1"])&,typebhe1];
 ];

 ScaleRingK[scal_]:=(Scan[(LINE["K1",#]=LINE["K1",#]*scal)&,LINE["Name","{BQ}*R*"]];
                     Scan[(LINE["K2",#]=LINE["K2",#]*scal)&,LINE["Name","S*R*"]];
                     Scan[(LINE["K0",#]=LINE["K0",#]*scal)&,LINE["Name","Z*R*"]]);

 ScaleRingQSZ[scal_]:=(Scan[(LINE["K1",#]=LINE["K1",#]*scal)&,LINE["Name","Q*R*"]];
                     Scan[(LINE["K2",#]=LINE["K2",#]*scal)&,LINE["Name","S*R*"]];
                     Scan[(LINE["K0",#]=LINE["K0",#]*scal)&,LINE["Name","Z*R*"]]);

 ReScaleRingK[scal_]:=(Scan[(LINE["K1",#]=LINE["K1",#]/scal)&,LINE["Name","{BQ}*R*"]];
                       Scan[(LINE["K2",#]=LINE["K2",#]/scal)&,LINE["Name","S*R*"]];
                       Scan[(LINE["K0",#]=LINE["K0",#]/scal)&,LINE["Name","Z*R*"]]);

 ReScaleRingQSZ[scal_]:=(Scan[(LINE["K1",#]=LINE["K1",#]/scal)&,LINE["Name","Q*R*"]];
                       Scan[(LINE["K2",#]=LINE["K2",#]/scal)&,LINE["Name","S*R*"]];
                       Scan[(LINE["K0",#]=LINE["K0",#]/scal)&,LINE["Name","Z*R*"]]);


 ATFRingOrbit:=Map[{#,LINE["S",#],Twiss["DX",#],Twiss["DY",#]}&,LINE["Name","{QSBZM}*"]];
 ATFRingTwiss:=Map[{#,LINE["S",#],Twiss["BX",#],Twiss["BY",#],Twiss["EX",#],Twiss["EY",#]}
                    &,LINE["Name","{QSBZM}*"]];
 ATFRingDispersion:=Map[{#,LINE["S",#],Twiss["EX",#],Twiss["EY",#]}&,LINE["Name","{QSBZM}*"]];



  WriteRingOpticsOrbit[optics_,orbit_]:=Module[{i,
        fn=OpenWrite["./output/RING_ORBIT.SAD"]},
    $FORM="";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    cavv=Thread[{cav,Element["K0",cav]}];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
!!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];

    $FORM="11.7";
    Scan[(Write[fn," OPTICS ",#[[2]]," ",#[[3]]," ",#[[4]]," ",#[[5]]," ",#[[6]]])&,optics];
    Scan[(Write[fn," ORBIT ",#[[2]]," ",#[[3]]," ",#[[4]]])&,orbit];
    Close[fn];
    $FORM="";
    System["chmod 664 "//f];
  ]; 


  WriteRingOpticsOrbitSteer[optics_,orbit_]:=Module[{i},
        f="./output/RING_ORBIT.SAD";
        Print[f];
        fn=OpenWrite[f];
    $FORM="";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    cavv=Thread[{cav,Element["K0",cav]}];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
!!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];

    $FORM="11.7";
    Scan[(Write[fn," OPTICS ",#[[2]]," ",#[[3]]," ",#[[4]]," ",#[[5]]," ",#[[6]]])&,optics];
    Scan[(Write[fn," ORBIT ",#[[2]]," ",#[[3]]," ",#[[4]]])&,orbit];
    Close[fn];
    $FORM="";
    System["chmod 664 "//f];
  ];

  WriteRingOpticsOrbitDispersionSteer[optics_,orbit_,disper_]:=Module[{i,
        fn=OpenWrite["./output/RING_ORBIT.SAD"]},
    $FORM="";
    qv=Thread[{quad,Element["K1",quad],Element["L",quad]}];
    sxtv=Thread[{sxt,Element["K2",sxt]}];
    sthv=Thread[{sth,Element["K0",sth]}];
    stvv=Thread[{stv,Element["K0",stv]}];
    cavv=Thread[{cav,Element["K0",cav]}];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,qv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sxtv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,sthv];
    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,stvv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,cavv];
!    Map[Write[fn,(#[1]//"        ")[1,8],#[2]]&,epilog];

    $FORM="11.7";
    Scan[(Write[fn," OPTICS ",#[[2]]," ",#[[3]]," ",#[[4]]," ",#[[5]]," ",#[[6]]])&,optics];
    Scan[(Write[fn," ORBIT ",#[[2]]," ",#[[3]]," ",#[[4]]])&,orbit];
    Scan[(Write[fn," DISPER ",#[[2]]," ",#[[3]]," ",#[[4]]])&,disper];
    Close[fn];
    $FORM="";
    System["chmod 664 "//f];
  ];



 ReturnRingOpticsOrbit[]:=Module[{optics,orbit},
  optics=ATFRingTwiss;
  orbit=ATFRingOrbit;
  WriteRingOpticsOrbit[optics,orbit];
 ];

