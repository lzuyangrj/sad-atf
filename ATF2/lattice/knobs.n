!==============================================================
! linear knobs wrt. file "LinearKnob20151111.dat" on atf server
! functions for tuning simulation
! Require "atflib_ultralow.n"
! by Renjun Yang, 2019.02
!==============================================================
SextOff[]:=Module[{k2l}, k2l = Element["K2", "S*FF"]; FFS["S*FF K2 0; CALC NOEXP;"]; k2l];
SextOn[k2l_]:=Module[{}, Element["K2", "S*FF"] = k2l; FFS["CALC NOEXP;"]; Element["K2", "S*FF"]];

Matching=Class[{}, {}, {},
  ! betaOTR: {BX, AX, BY, AY}
  ! OpticsMatch[90e-3, 30e-6, {5.349, -2.5613, 9.418, 4.70}];
  With[{def={MatchErr->False}},
    IPBeta[bxIP_, byIP_, betaOTR_:{}, opt___]:= Module[{op, II, bxIP2, byIP2, err0=0.},
      op=Override[def, opt];
      If[MatchErr/.op, err0=0.05];
      {bxIP2, byIP2} = {bxIP, byIP}*(1+err0*GaussRandom[2]); 
         
      If[Length@betaOTR>0,
        FFS["FREE AXI BXI AYI BYI;"];
        FFS["FIT OTR0X BX "//betaOTR[1]//";"];
        FFS["FIT OTR0X AX "//betaOTR[2]//";"];
        FFS["FIT OTR0X BY "//betaOTR[3]//";"]; 
        FFS["FIT OTR0X AY "//betaOTR[4]//";"];
        Do[FFS["go"],{II,10}];
        FFS["CALC NOEXP;"];
        FFS["REJ TOTAL; FIX AXI BXI AYI BYI;"];
        ! beam waist
        FFS["FREE QD0FF QF1FF;"];
        FFS["FIT IP AXM 10; FIT IP AYM 10;"];
        FFS["go"];
        FFS["FIT IP AX  0; FIT IP AY  0;"];
        Do[FFS["go"],{II,100}];
        FFS["CALC NOEXP; FIX *;"];
        ];
      ! IP beta
      FitFunction:= {Element["BX", "AQD6FF"]-Element["BX", "AQD8FF"],
                     Element["BY", "AQD6FF"]-Element["BY", "AQD8FF"]};
      FFS["FREE QF21X QD20X QM16FF QM15FF QM14FF QM13FF QM12FF QM11FF;"];    
      FFS["QM* MIN -1.3 MAX 1.3;"];
      FFS["FIT ZHFB1FF IP NX 2.000; FIT ZVFB1FF IP NY 2.000;"];   
      FFS["FIT IP AX 0.; FIT IP AY 0.0;"];
      FFS["FIT IP BX "//bxIP2//"; FIT IP BY "//byIP2//";"];
      Do[FFS["go"],{II,10}]; 
      FFS["CALC NOEXP; FIX *;"];
      ];
    ];
   ! dispersion at OTR0; eta = {EX, EPX, EY, EPY}
   ! change to disperison in the Physical coordinates (29/08/2019)
   Dispersion[eta_, opt___]:=Module[{II},
     FFS["FIX *; FREE EXI EPXI;"];
     FFS["FIT OTR0X PEX "//eta[1]//";"];
     FFS["FIT OTR0X PEPX "//eta[2]//";"];
     If[Length@eta==4,
       FFS["FREE EYI EPYI;"];
       FFS["FIT OTR0X PEY "//eta[3]//";"];
       FFS["FIT OTR0X PEPY "//eta[4]//";"]];
     Do[FFS["go"], {II,10}];
     FFS["CALC NOEXP;"];
     FFS["FIX *;"];
     ];
   ];

! call the wave-like motion generator (0.1-50 Hz)
! parameters from B. Bolzon, PAC09, TH5RFP086
! Require Import[] function (atf2_ultralow.n?)
! mag = Element["NAME", "{BQS}*{XF}"];
! genpy, name of the pyscript; fn, filename for saving
!!! ncol -> match the number of bins in the generator !!!
FastGMIntialization[genpy_, mag_, fn_String, opt___]:=Module[{s1, s2=LINE["S", "IP"], ds, dat0, ncol=49, w0, Amplist, phi0},
  If[FileQ[fn], System["rm "//fn]];
  Scan[(s1=LINE["S", #]; ds=Abs@(s2-s1);
    If[~(System["python3 "//genpy//" "//ds//" "//fn]), Print["\terrors with the GM generator.\n"]])&, mag];
  dat0 = Import[fn, ncol];
  w0 = 2*Pi*dat0[1];
  Amplist = Drop[dat0, 1];
  phi0 = 2*Pi*Random[Length@Amplist, Length[Amplist[1]]];
  {Amplist, w0, phi0}];

! ============================================================
! file$ and beam0$ have to be defined prior to using functions
! file$, linear knobs file name; mknob$2 non-linear knobs file
! nshot$, approximated no. of bunches for one IPBSM measurement
! MagGM = Element["NAME", "{BQS}*{XF}"];
! MoverError$Set, independent to MoverError$, to check the tolerance on Mover accuracy
TuneKnobs=Class[{},{},{file$, beam0$, mknob$, MoverError$, MoverError$Set, Jit$, Vibration$, nshot$, GM$fast, GM$slow, MagGM$, FastGMInit$},
  (* an extra approach of Bessel J0 function *)
  J0[x_]:=NIntegrate[Cos[x*Cos[2*Pi*t]], {t, 0, 1}, AccuracyGoal->1e-10];
  
  MisalignOneMag[name_, dx_, dy_]:=Module[{mag, init},
    mag = Flatten@{Element["NAME", name//"*"]};
    Scan[(
      init = Element[{"DX", "DY"}, #];
      Element[{"DX", "DY"}, #] = init+{dx, dy};
      FFS["CALC NOEXP;"])&,mag];
    ];

  (* T0, time wrt. an unique zero-clock *)
  (* drEach, {dx, dy} to each individual element and its high-order components *)
  ! enable GM to x and y planes, simultaneously, on 2020.02.17
  FastGM[init_, t0_]:=Module[{amp, w0, phi0, ti, dr, drEach={}, dx, dy},
    {amp, w0, phi0} = init;
    Scan[(
      ti = 90*Random[]; ! 90 sec
      ! for only y plane
      !dr = amp[#]*Cos[-w0*ti+phi0[#]]/Sqrt[2];
      !{dx, dy} = {0, Plus@@dr};      
      ! {dx, dy} = {amp[#]*Cos[-w0*ti+phi0[#]], -amp[#]*Sin[-w0*ti+phi0[#]]}/Sqrt[2];
      ! above is correct, but the below is used since the Cos[..] was adapted for ver. before
      ! considering dual-plane motion 2020-02-18
      {dx, dy} = {-amp[#]*Sin[-w0*ti+phi0[#]], amp[#]*Cos[-w0*ti+phi0[#]]}/Sqrt[2];      
      {dx, dy} = {Plus@@dx, Plus@@dy};
      MisalignOneMag[MagGM$[#], dx, dy];
      AppendTo[drEach, {dx, dy}];      
      )&, Range[Length@MagGM$]];
    drEach];

  ZeroFastGM[dr_]:=Module[{},
    If[Length@dr>0,
      Scan[MisalignOneMag[MagGM$[#], -dr[#, 1], -dr[#, 2]]&, Range[Length@dr]],
      Print["\t FastGM error wasn't erased.\n"]];
    ];

  (* dT, unit, second; one knob scan, 15 mins *)
  ! A=27e-6 KEKB tunnel value
  SlowGM[dT_]:=Module[{A=27e-6, s1, s2=LINE["S", IP], theta, ds, drEach={}, dx, dy},
    Scan[(s1=LINE["S", #]; ds=Abs@(s2-s1);
      theta = 2*Pi*Random[];
      {dx, dy} = Sqrt[A*dT*ds]*1e-6*{Cos[theta], Sin[theta]};
      MisalignOneMag[#, dx, dy];
      AppendTo[drEach, {dx, dy}];      
      )&, MagGM$];
    drEach];    

   ! Vibration of QF1 and QD0 wrt. the measurement data
   ! increase sigviby from 6.5 nm (measurement in 2009) to 10 nm (desired tolerance value) since the QF1
   ! was replaced after...
   Vibration[]:=Module[{dx0, dy0, dxy0, randx, randy, sigvibx=16.6e-9, sigvibyQF1=10e-9, sigviby=6.5e-9},
     If[Vibration$&&Vibration$<>1, sigvibx=sigviby=Vibration$];
     {dx0, dy0} = Element[{"DX", "DY"}, "Q{DF}{01}FF*"];
     Scan[(
       dxy0 = Element[{"DX", "DY"}, #//"*"];     
       {randx, randy} = GaussRandom[2];
       If[#=="QF1FF",
          Element[{"DX", "DY"}, #//"*"] = dxy0+{sigvibx*randx, sigvibyQF1*randy},
          Element[{"DX", "DY"}, #//"*"] = dxy0+{sigvibx*randx, sigviby*randy}])&, {"QF1FF", "QD0FF"}];     
     FFS["CALC NOEXP;"];
     {dx0, dy0}];

   ! orig, original {DX, DY}
   ZeroVibration[orig_]:=Module[{},
      Element[{"DX", "DY"}, "Q{DF}{01}FF*"] = orig;
      FFS["CALC NOEXP;"]];
      
  (* IP beam size by tracking *)
  ! require FitDistribution[] Class and StandardDeviation[] function
  ! 20% trajactory jitter at the extraction
  ! enable IEX jitter initialization indivually on March 16, 2020
  With[{def={OnlySurvive->False}},
    ! bunch w/ jitters
    Bunch$[beam0_]:=Module[{beam2=beam0, sigj=0.2, sigdx, sigdpx, sigdy, sigdpy},
      If[Jit$||Length@Jit$>1,
        If[Jit$<>1||Length@Jit$>1, sigj=Jit$];
        {sigdx, sigdpx, sigdy, sigdpy} = sigj*Twiss[{"SIGX","SIGPX", "SIGY", "SIGPY"}, "^^^"];
        beam2[2,1] += sigdx*GaussRandom[];
        beam2[2,2] += sigdpx*GaussRandom[];
        beam2[2,3] += sigdy*GaussRandom[];        
        beam2[2,4] += sigdpy*GaussRandom[];
        ];
      beam2];
      
    ! output: x, sigx, y, sigy, y', sigy'
    BeamSizeTrack0[beam0_, end0_:"IP", opt___]:=Module[{op, fd, beamIP, beamIP2, survive, btmp={}, fx, fy, out0},
      op = Override[opt, def];
      fd = FitDistribution[];
      beamIP = TrackParticles[beam0, end0];      
      !beamIP = TrackParticles[beam0, "FONTP3"];
      If[OnlySurvive/.op,
        btmp={};
        survive = Flatten[Position[beamIP[2,7],1]];
        Scan[AppendTo[btmp, beamIP[2,#,survive]]&, Range[7]];
        beamIP2={beamIP[1],btmp},
        beamIP2 = beamIP];
      If[Length[beam0[2,7]]>1, 
        fx = fd@FitGaussian[beamIP2[2,1], Plot->False];  ! mean(x), std(x)
        fy = fd@FitGaussian[beamIP2[2,3], Plot->False];  ! mean(y), std(y)
        fyy = fd@FitGaussian[beamIP2[2,4], Plot->False],        ! mean(y'), std(y')
        fx=Flatten@{0, beamIP2[2,1]};
        fy=Flatten@{0, beamIP2[2,3]}; 
        fyy=Flatten@{0, beamIP2[2,4]};];
      Thread[{fx[2], fx[1], fy[2], fy[1], fyy[2], fyy[1]}]];


    (* single shot or multi-shot with beam jitter *)
    ! output: {x, sigx, y, sigy, y', sigy'} or {sigdx, sigx, sigdy, sigy, sigdy', sigy', sigxy}
    ! sigvibx/y, vibration at 16.6/6/5 nm level (meas.); dt0, 1.5min for one IPBSM scan
    BeamSizeTrack[beam0_, end0_:"IP", opt___]:=Module[{op, errGM, origVib, sigxy={}, dt0=1.5*60},
      op = Override[opt, def];
      Scan[(
        If[GM$fast, errGM = FastGM[FastGMInit$, dt0/nshot$]];
        ! Vibration should be the last step before Tracking and then be firstly erased
        ! only QF1 and QD0
        If[Vibration$, origVib=Vibration[]];
        AppendTo[sigxy, BeamSizeTrack0[Bunch$[beam0], end0, op]]; !IPSizeTrack0[Bunch$[beam0],op];
        If[Vibration$, ZeroVibration[origVib]];
        If[GM$fast, ZeroFastGM[errGM]];
        )&, Range[nshot$]];
      If[nshot$>1, {StandardDeviation[sigxy[;,1]], Mean[sigxy[;,2]], StandardDeviation[sigxy[;,3]], Mean[sigxy[;,4]], StandardDeviation[sigxy[;,5]], Mean[sigxy[;,6]], sigxy}, Flatten@sigxy]];
    ]; ! With[

    IPSizeTrack[beam0_, opt___]:= BeamSizeTrack[beam0, "IP", opt];
    
  (* beam waist scanning *)
  With[{def={Track->False}},
    ! Scan w/ automatic matching
    FDScanFit[]:= Module[{},
      FFS["FIX *; REJECT TOTAL; FREE QD0FF QF1FF;"];
      FFS["FIT IP AXM 10;"];
      FFS["FIT IP AYM 10;"];
      FFS["go"];
      FFS["FIT IP AXM  1E-4;"];
      FFS["FIT IP AYM  1E-4;"];
      Do[FFS["go"],{II,100}];
      FFS["CALC NOEXP"];
      ];
      
    ! manually, output: {sigx, sigy}
    ! Notice that Tracking is not necessary b' beam size is big
    FDScan[opt___]:= Module[{op},
      op = Override[opt, def];
      QF1[-3, 3, 0.5, opt];
      QD0[-3, 3, 0.5, opt];
      QF1[-1.8, 1.8, 0.3, opt];
      QD0[-1.8, 1.8, 0.3, opt];
      {1, If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]]}];

    ! Assume the design current of 123 A
    ! power supply accuracy 1e-5 (rms)
    QF1[Imin_, Imax_, dI_, opt___] := Module[{op, k1l, klm, multi={"K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9"}, I0=123, i, sigx0, sigx={}, res, f, Ibest, sigkl=If[MoverError$, 1e-5, 0]},
      op = Override[opt, def];
      k1l = Element["K1", "QF1FF"];
      klm = Element[#, "QF1FF"//#]&/@multi;
      Do[
        Element["K1", "QF1FF"] = k1l*(1+i/I0);
        Scan[(Element[#, "QF1FF"//#] = klm[Position[multi, #][1,1]]*(1+i/I0))&, multi];        
        FFS["CALC NOEXP;"];
        sigx0 = If[Track/.op, IPSizeTrack[beam0$][2], Twiss["SIGX", IP]];
        AppendTo[sigx, sigx0]
        ,{i, Imin, Imax, dI}];
      res = Thread@{Range[Imin, Imax, dI], sigx^2};
      f = PolynomialFit[res, 2];
      Ibest = -f[1,2]/f[1,3]/2;
      Element["K1", "QF1FF"] = k1l*(1+Ibest/I0)*(1+sigkl*GaussRandom[]);
      Scan[(Element[#, "QF1FF"//#] = klm[Position[multi, #][1,1]]*(1+Ibest/I0)*(1+sigkl*GaussRandom[]))&, multi];              
      FFS["CALC NOEXP;"];
      {If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res}];

    ! Assume the design current of 130 A
    QD0[Imin_, Imax_, dI_, opt___] := Module[{op, k1l, klm, multi={"K2", "K3", "K4", "K5", "K6", "K7", "K8", "K9"}, I0=130, i, sigy0, sigy={}, res, f, Ibest, sigkl=If[MoverError$, 1e-5, 0]},
      op = Override[opt, def];
      k1l = Element["K1", "QD0FF"];
      klm = Element[#, "QD0FF"//#]&/@multi;
      Do[
        Element["K1", "QD0FF"] = k1l*(1+i/I0);
        Scan[(Element[#, "QD0FF"//#] = klm[Position[multi, #][1,1]]*(1+i/I0))&, multi];
        FFS["CALC NOEXP;"];
        sigy0 = If[Track/.op, IPSizeTrack[beam0$][4], Twiss["SIGY", IP]];
        AppendTo[sigy, sigy0]
        ,{i, Imin, Imax, dI}];
      res = Thread@{Range[Imin, Imax, dI], sigy^2};
      f = PolynomialFit[res, 2];
      Ibest = -f[1,2]/f[1,3]/2;
      Element["K1", "QD0FF"] = k1l*(1+Ibest/I0)*(1+sigkl*GaussRandom[]);
      Scan[(Element[#, "QD0FF"//#] = klm[Position[multi, #][1,1]]*(1+Ibest/I0)*(1+sigkl*GaussRandom[]))&, multi];      
      FFS["CALC NOEXP;"];
      {If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res}];

    ! wrt. IP beam size
    DeltaKnob[Imin_, Imax_, dI_, opt___] := Module[{op, k1l, I2k=2.6e-3, K1max=0.013, sigy0, res={}, f, Ib},
      op = Override[opt, def];  
      k1l = Element["K1", "QS*"];
      Do[
        Element["K1", "QS*"] = k1l+ {i*I2k, -i*I2k};
        If[Max[Abs[Element["K1", "QS*"]]]<K1max,
          FFS["CALC NOEXP;"];
          sigy0 = If[Track/.op, IPSizeTrack[beam0$][4], Twiss["SIGY", IP]];
          AppendTo[res, {i, sigy0^2}]]
        ,{i, Imin, Imax, dI}];
      f = PolynomialFit[res, 2];
      Ib = -f[1,2]/f[1,3]/2;      
      If[Max[Abs[k1l+ {Ib*I2k, -Ib*I2k}]]>K1max,
        Ib = res[Position[res[;,2], Min[res[;,2]]][1,1], 1]];
      Element["K1", "QS*"] = k1l+ {Ib*I2k, -Ib*I2k};        
      FFS["CALC NOEXP;"];
      ! find the min. if fit center is not the min.
      sigy0 = If[Track/.op, IPSizeTrack[beam0$][4], Twiss["SIGY", IP]];
      If[sigy0^2>Min[res[;,2]],
        Ib = res[Position[res[;,2], Min[res[;,2]]][1,1], 1];
        Element["K1", "QS*"] = k1l+ {Ib*I2k, -Ib*I2k};        
        FFS["CALC NOEXP;"];
        ];
      {1, If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res}]; ! 1 to uniform the output of knobs


    ! QK knob wrt. IP beam size
    QKKnob[mag_, Imin_, Imax_, dI_, opt___] := Module[{op, I2k=2.6e-3, K1max=0.013, sigy0, res={}, f, Ib, sigkl=If[MoverError$, 1e-3, 0], outp},
      op = Override[opt, def];  
      Do[
        Element["K1", mag] = i*I2k;
        If[Abs[Element["K1", mag]]<K1max,
          FFS["CALC NOEXP;"];
          sigy0 = If[Track/.op, IPSizeTrack[beam0$][4], Twiss["SIGY", IP]*1e9];
          AppendTo[res, {i, sigy0^2}]]
        ,{i, Imin, Imax, dI}];
      f = PolynomialFit[res, 2];
      Ib = -f[1,2]/f[1,3]/2;      
      If[Abs[Ib*I2k]>K1max,
        Ib = res[Position[res[;,2], Min[res[;,2]]][1,1], 1]];
      Element["K1", mag] = Ib*I2k*(1+sigkl*GaussRandom[]);
      FFS["CALC NOEXP;"];
      ! find the min. if fit center is not the min.
      sigy0 = If[Track/.op, IPSizeTrack[beam0$][4], Twiss["SIGY", IP]];
      If[sigy0^2>Min[res[;,2]],
        Ib = res[Position[res[;,2], Min[res[;,2]]][1,1], 1];
        Element["K1", mag] = Ib*I2k*(1+sigkl*GaussRandom[]);
        FFS["CALC NOEXP;"];
        ];
      outp = {1, If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res};
      If[GM$slow, SlowGM[15*60]];
      outp]; ! 1 to uniform the output of knobs

    QK1X[Imin_, Imax_, dI_, opt___] := QKKnob["QK1X", Imin, Imax, dI, opt];
    QK2X[Imin_, Imax_, dI_, opt___] := QKKnob["QK2X", Imin, Imax, dI, opt];
    QK3X[Imin_, Imax_, dI_, opt___] := QKKnob["QK3X", Imin, Imax, dI, opt];
    QK4X[Imin_, Imax_, dI_, opt___] := QKKnob["QK4X", Imin, Imax, dI, opt];
    
    QK[Imin_, Imax_, dI_, opt___] := Module[{op},
      op = Override[opt, def];      
      Scan[QKKnob[#, Imin, Imax, dI, opt]&, {"QK1X", "QK2X", "QK3X", "QK4X"}];
      If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]]];
    
    ]; ! With

  (* hor. dispersion QF6X *)
  ! making hor. dispersion before B5FF small
  EtaXFit[tol_]:=Module[{II},
    FFS["FIX *; FREE QF1X QF6X;"]; 
    FFS["FIT OTR0X PEXM "//tol//";"];
    FFS["FIT OTR3X PEXM "//tol//";"];
    FFS["FIT MQM16FF PEXM "//tol//";"];    
    FFS["FIT MQF9AFF PEXM "//tol//";"];
    Do[FFS["go"],{II,10}];
    FFS["CALC NOEXP"];
    FFS["FIX *; REJ TOTAL"];  
    ];

  EtaXFit[]:=EtaXFit[3e-3];
  
  (* Ver. dispersion QS1X/QS2X *)
  SumKnob[k_]:=Module[{},
    FFS["QS* K1 "//ToString[k]];
    FFS["CALC NOEXP"];    
    ];
    
  SumKnobFit[residual_] := Module[{k1l, sigkl=If[MoverError$, 1e-3, 0], II},
    k1l = Element["K1", "QS*"];
    Element["K1", "QS*"] = k1l+{1e-6, 1e-6};
    FFS["CALC NOEXP;"]; ! perturb initial condition, good for the convergence of matching
    FFS["FIX *; FREE QS1X QS2X;"];
    FitFunction:= {Element["K1", "QS1X"]-Element["K1", "QS2X"]};
    FFS["QS* MIN -0.013 MAX 0.013;"]; ! Max current +/- 5 A
    FFS["FIT QM14FF PEYM "//residual//";"];
    FFS["FIT QD10AFF PEYM "//residual//";"];
    !FFS["FIT QF7FF EYM "//residual//";"]; (* 28/08/2019 keep as few as possible observation points, QM+3 peak bety lcoations *)
    FFS["FIT QD4BFF PEYM "//residual//";"];    
    FFS["FIT QF1FF PEYM "//residual//";"];
    FFS["FIT QD0FF PEYM "//residual//";"];    
    Do[FFS["go"],{II,100}];
    FFS["CALC NOEXP"];
    FFS["FIX *; REJ TOTAL"];
    
    ! add power supply error (0.1%)
    k1l = Element["K1", "QS*"];
    Element["K1", "QS*"]= k1l*(1+sigkl*GaussRandom[2]);
    FFS["CALC NOEXP"];    
    ];

  SumKnobFit[]:=SumKnobFit[10e-3];

  (* manually, recommanded *)
  SquareEY[elem_] := Plus@@((Twiss["PEY", elem])^2);
  SquareEY[]:= Module[{list={"Q{DF}*FF", "S{DF}*FF"}},
    Plus@@(SquareEY[#]&/@list)
    ];

  ! notice that the max0 might out the +/- 5A limiation
  With[{tol=1e-5, max0=0.0518412/4}, ! QS1X.1 and QS1X.2
    ! output residual EY
    SumKnob2[] := Module[{i, list, eyffs={}, cent=0, kmax=max0},
      While[~(Min@eyffs<tol) && cent<max0 && kmax>1e-5,
        eyffs = {};
        list = cent+Range[-#, #, #/100]&@@{kmax};
        Scan[(SumKnob[#]; AppendTo[eyffs, SquareEY["Q{FD}*F"]])&,list];
        cent = list[#]&@@(Flatten[Position[eyffs, Min@eyffs]]);
        kmax /= 4;
        ];
      SumKnob[cent];
      eyffs
      ];
   ];

  (* linear knobs*)
  With[{sexts={"SF6FF", "SF5FF", "SD4FF", "SF1FF", "SD0FF"}, xmax=2000e-6, ymax=1500e-6, def={Track->False}},
    SextOffset[]:= Module[{},Element[{"DX", "DY", "ROTATE"}, sexts]];

    ! Reset to the original position (before knob scan) if "DX/DY is out of range"
    SextMoveX[off0_, rot0_, fdx_, frot_, step_]:=Module[{op, lis, dx2, drol2, {sigmov, sigrol}=If[MoverError$, {0.1e-6,10e-6}, {0,0}], i, tag=1},
      If[Length[MoverError$Set]==2, {sigmov, sigrol}=MoverError$Set];
      Do[
        lis = Flatten@{Element["NAME", sexts[i]//"*"]};
        dx2 = off0[i] + step*fdx[i];
        drol2 = rot0[i] + step*frot[i];
        dx2 += sigmov*GaussRandom[];
        drol2 += sigrol*GaussRandom[];        
        Scan[SetElement[#,,{"DX"->dx2, "ROTATE"->drol2}]&, lis];
        ,{i, Length@sexts}];
      ! Out of range issue
      If[Max[Abs[Element["DX", sexts]]]>xmax,
        Print["\tDX out of range!\tknob = "//step//"\n"];
        Do[
          lis = Flatten@{Element["NAME", sexts[i]//"*"]};
          Scan[SetElement[#,,{"DX"->off0[i], "ROTATE"->rot0[i]}]&, lis];
          ,{i, Length@sexts}];              
        tag=0];
      FFS["CALC NOEXP"];            
      tag];
      
    SextMoveY[off0_, rot0_, fdx_, frot_, step_, opt___]:=Module[{lis, dy2, drol2, {sigmov, sigrol}=If[MoverError$, {0.1e-6,10e-6}, {0,0}], i, tag=1},
      If[Length[MoverError$Set]==2, {sigmov, sigrol}=MoverError$Set];    
      Do[
        lis = Flatten@{Element["NAME", sexts[i]//"*"]};
        dy2 = off0[i] + step*fdx[i];
        drol2 = rot0[i] + step*frot[i];
        dy2 += sigmov*GaussRandom[];
        drol2 += sigrol*GaussRandom[];
        Scan[SetElement[#,,{"DY"->dy2, "ROTATE"->drol2}]&, lis];
        ,{i, Length@sexts}];
      ! Out of range issue
      If[Max[Abs[Element["DY", sexts]]]>xmax,      
        Print["\tDX out of range!\tknob = "//step//"\n"];        
        Do[
          lis = Flatten@{Element["NAME", sexts[i]//"*"]};
          Scan[SetElement[#,,{"DY"->off0[i], "ROTATE"->rot0[i]}]&, lis];
          ,{i, Length@sexts}];              
        tag=0];
      FFS["CALC NOEXP"];    
      tag];

    AXknob[k_, off0_, rot0_] := Module[{offset, rot, i},
      If[file$=="LinearKnob20151111",
        offset = {717.9e-6, 0.0e-6, -7.5e-6, -135.8e-6, -255.4e-6};
        rot = {0., 0., -1e-6, 0., 0.};
        ];
      If[file$=="LinearKnob20191007",
        offset = {3.95e-06, 3.26e-07, 1.08e-06, -1.40e-06, 1.07e-06}*5;
        rot = {0., 0., -1e-6, 0., 0.};
        ];        
      SextMoveX[off0, rot0, offset, rot, k]      
      ];

    EXknob[k_, off0_, rot0_] := Module[{offset, rot, i},
      If[file$ == "LinearKnob20151111",
        offset = {369.1e-6, 0.0e-6, -325.9e-6, 796.9e-6, 952.6e-6};
        rot = {0., 0., 0., 0., 0.};
        ];
      If[file$ == "LinearKnob20191007",
        offset = {8.94e-06, 6.35e-06, -8.11e-07, 1.64e-05, -1.23e-05}*5;
        rot = {0., 0., 0., 0., 0.};
        ];        
      SextMoveX[off0, rot0, offset, rot, k]      
      ];
      
    ! output -> peak position, {knob vs. size}
    AX[kmin_, kmax_, dk_, opt___]:= Module[{op, off0, offy0, rot0, i, step={}, sigx={}, res, f, outp},
      op = Override[opt, def];
      {offx0, offy0, rot0} = SextOffset[];
      Do[
        If[AXknob[i, offx0, rot0], AppendTo[step, i]; AppendTo[sigx, Twiss["SIGX", IP]*1e6]]
        ,{i, kmin, kmax, dk}];
      res = Thread@{step, sigx^2};
      f = PolynomialFit[res, 2];
      outp = {AXknob[-f[1,2]/f[1,3]/2, offx0, rot0], If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res};
      If[GM$slow, SlowGM[15*60]];
      outp];      

    ! EX is measured directly by executing rf-ramp, but not wrt. IP beam size
    EX[kmin_, kmax_, dk_, opt___]:= Module[{op, off0, offy0, rot0, i, sigx={}, step={}, res, f, outp},
      op = Override[opt, def];
      {offx0, offy0, rot0} = SextOffset[];
      Do[
        If[EXknob[i, offx0, rot0], AppendTo[step, i]; AppendTo[sigx, Twiss["EX", IP]*1e3]]
        ,{i, kmin, kmax, dk}];
      res = Thread@{step, sigx^2};
      f = PolynomialFit[res, 2];
      outp = {EXknob[-f[1,2]/f[1,3]/2, offx0, rot0], If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res};
      If[GM$slow, SlowGM[15*60]];
      outp];

    ! EX w/ QF1FF and AX knobs
    EX2[opt___] := Module[{op, tag},
      op = Override[opt, def];
      FFS["FIX *; REJ TOTAL; FREE QF1FF; FIT IP EXM 2E-4;"]; ! How about the tolerance
      FFS["GO; cal noexp;"]; 
      Scan[(tag = AX[-#, #, #/5][1])&, {0.3, 0.2, 0.1}];
      {tag, If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]]}];

    AYknob[k_, off0_, rot0_] := Module[{offset, rot, i},
      If[file$ == "LinearKnob20151111",
        offset = {86.7e-6, 0.0e-6, -194.8e-6, 125.2e-6, -266.5e-6};
        rot = {0., 0., 0., 0., 0.};
        ];
      If[file$ == "LinearKnob20191007",
        offset = {1.7e-06, -5.2e-08, -7.9e-06, -1.57e-06, -3.5e-07}*5;
        rot = {0., 0., 0., 0., 0.};
        ];        
      SextMoveX[off0, rot0, offset, rot, k]
      ];

    EYknob[k_, off0_, rot0_] := Module[{offset, rot, i},
      If[file$ == "LinearKnob20151111",
        offset = {-57.3e-6, 0.0e-6, -372.5e-6, -99.3e-6, 290.4e-6};
        rot = {0., 0., 0., 0., 0.};
        ];
      If[file$ == "LinearKnob20191007",
        offset = {-1.1e-07, 7.05e-06, 5.05e-05, 1.73e-05, -3.61e-05}*5;
        rot = {0., 0., 0., 0., 0.};
        ];        
      SextMoveY[off0, rot0, offset, rot, k]
      ];
      
    Coup2knob[k_, off0_, rot0_] := Module[{offset, rot, i},
      If[file$ == "LinearKnob20151111",
        offset = {100e-6, 0.0e-6, 163.0e-6, 3.7e-6, 121.6e-6};
        rot = {0., 0., 0., 0., 0.};
        ];
      If[file$ == "LinearKnob20191007",
        offset = {2.0e-05, 1.67e-06, 8.7e-06, -4.8e-06, 1.01e-05}*5;
        rot = {0., 0., 0., 0., 0.};
        ];        
      SextMoveY[off0, rot0, offset, rot, k]
      ];

    LinearKnobY[knob_, kmin_, kmax_, dk_, opt___]:= Module[{op, offx0, offy0, rot0, tag, i, step={}, sigy={}, sigy0, sigyfit, res, f, outp},
      op = Override[opt, def]; 
      {offx0, offy0, rot0} = SextOffset[];
      Do[
        If[knob=="AY",
          tag = AYknob[i, offx0, rot0],
          If[knob=="EY",
            tag = EYknob[i, offy0, rot0],
            If[knob=="Coup2"||"R32",
              tag = Coup2knob[i, offy0, rot0],
              Print["\tError w/ linear knob name (AY/EY/Coup2/R32).\n"];
              FFS["end;"];
              ]]];
        sigy0 = If[Track/.op, IPSizeTrack[beam0$][4], Twiss["SIGY", IP]*1e9];
        If[tag,  AppendTo[step, i]; AppendTo[sigy, sigy0]];
        ,{i, kmin, kmax, dk}];
      res = Thread@{step, sigy^2};
      f = PolynomialFit[res, 2];
      sigyfit = f[1,1]-f[1,2]*f[1,2]/f[1,3]/4;
      move = If[sigyfit<Min@sigy, -f[1,2]/f[1,3]/2, res[Position[sigy, Min@sigy][1,1], 1]];
      If[knob=="AY",
         tag = AYknob[move, offx0, rot0],
         If[knob=="EY",
           tag = EYknob[move, offy0, rot0],
           If[knob=="Coup2"||"R32",
             tag = Coup2knob[move, offy0, rot0],
              Print["\tError w/ linear knob name (AY/EY/Coup2/R32).\n"];
              FFS["end;"];
              ]]];                    
      outp = {tag, If[Track/.op, IPSizeTrack[beam0$], Twiss[{"DX", "SIGX", "DY", "SIGY"}, IP]], f, res};
      If[GM$slow, SlowGM[15*60]];
      outp];

    AY[kmin_, kmax_, dk_, opt___]:= LinearKnobY["AY", kmin, kmax, dk, opt];
    EY[kmin_, kmax_, dk_, opt___]:= LinearKnobY["EY", kmin, kmax, dk, opt];    
    Coup2[kmin_, kmax_, dk_, opt___]:= LinearKnobY["Coup2", kmin, kmax, dk, opt];    
    ]; ! With
    
  (* non-linear knobs, beam size by tracking *)
  With[{sex = {"SF6FF", "SF5FF", "SD4FF", "SF1FF", "SD0FF"}, multi={"K3", "K4", "K5", "K6", "K7", "K8", "K9"}, I2K={0.547253, 0.524379, 0.543913, -0.691272, 0.687970}, I2Ksk = 0.558, K2min={0, -0.524379*10/2, 0, -0.691272*50/2, 0}, K2max={0.547253*50/2, 0.524379*10/2, 0.543913*50/2, 0, 0.687970*50/2}, K2skmax=0.558*16/2, def = {Track->True, KnobScan->True}, lenSK=0.23},
    ! k2l, {{norm}, {skew}}: original K2 before one multiknob scan
    ! klm0, initial multipole components
    SextKLBump[dIunit_, nk_, k2l0_, klm0_]:=Module[{k2l, klm, dkl, sigkl=If[MoverError$,1e-5,0], errkl, newkl, i, tag=1},
      k2l = k2l0[1];
      !Print['dIunit', dIunit];
      Do[
        klm = klm0[i];
        dkl = I2K[i]*dIunit[i]*nk/2; ! /2 b' one sext. was split in two in the lattice
        errkl = I2K[i]*sigkl*GaussRandom[]/2; ! consider power supply accuracy
        newkl = (k2l[i] + dkl)*(1+errkl);
        !Print[i, '\t', dkl, '\t', errkl];
        If[tag = (K2min[i]<newkl<K2max[i]),           
          Element["K2", sex[i]] = newkl;
          Scan[(Element[#, sex[i]//#] = klm[Position[multi, #][1,1]]*newkl/k2l[i])&, multi];
          ,          
          Print["\t"//sex[i]//" current out of range.\n"];
          Print[K2min[i],"\t", newkl, "\t", K2max[i], "\t", k2l[i]];          
          Break[]
          ]
        ,{i, Length@sex}];

      k2l = k2l0[2];
      dkl = I2Ksk*dIunit[{6,7,8,9}]*nk/2;
      If[tag && Max[Abs@(k2l+dkl)]<K2skmax, Element["K2", "SK%FF"] = k2l+dkl, Print["\tK2 = "//Max[Abs@(k2l+dkl)]//" S{FDK}%FF: current out of range.\n"]; tag = 0];
      FFS["CALC NOEXP;"];
      tag];

    ! If KnobScan->False, knob was set at dImin !!
    MultiKnobY[knob_, dImin_, dImax_, dstep_, opt___]:=Module[{op, dIunit, i, k2l0, Km0, tag=1, sigy0, res={}, f, dIb},    
      op = Override[opt, def];
      If[mknob$=="multiknob_init_param_160201",
        dIunit = Switch[knob,
          "Y24", {-0.136600, -0.146100, -0.608800, -0.020900, 0.169200, 0.000000, 0.000000, 0.000000, 0.000000},
          "Y46", {0.032700, -0.113100, 1.287200, -0.089800, 0.386900, 0.000000, 0.000000, 0.000000, 0.000000},
          "Y22", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, -0.062000, 0.413000, -0.505000, 2.805000},     
          "Y26", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.213000, -0.357000, 4.892000, 1.877000},
          "Y66", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.067000, -0.139000, -14.039000, -18.382999},
          "Y44", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.029000, 4.600000, -1.630000, -0.972000}
          ]];
      ! knobs defined by Andrii's (implemented on Oct 7, 2019)
      If[mknob$=="multiknob_init_param_191007",
        dIunit = Switch[knob,
          "Y24", {-0.087766*0.1/I2K[1], 0.0447344*0.0718/I2K[2], 0.0518789*0.1/I2K[3], -0.4023553*0.1/I2K[4], -0.8718339*0.1/I2K[5],  0.000000, 0.000000, 0.000000, 0.000000}*2,
          "Y46", {-1.63898598188*0.1/I2K[1], 2.91759276364*0.0718/I2K[2], 5.3674646*0.1/I2K[3], 0.077096632*0.1/I2K[4], 0.59851134*0.1/I2K[5],  0.000000, 0.000000, 0.000000, 0.000000}*2,
          "Y22", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.0285371015382, -0.00127, 0.2211, 0.58285}/I2Ksk*lenSK*3,
          "Y26", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 7.52626281235, -11.5011125135, 1.36446747581, -0.911159014641}/I2Ksk*lenSK,
          "Y66", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 35.6893092301, 23.5609099945, 0.481978574558, -1.8788288}/I2Ksk*lenSK,
          "Y44", {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, -0.0535234951582, 0.0516588277865, 0.584443075715, -0.218965417019}/I2Ksk*lenSK*3
          ]];          
      k2l0 = Element["K2", {"S{FD}*FF", "SK%FF"}];
      Km0 = Table[Element[#, sex[i]//#]&/@multi, {i, Length@sex}];

      If[(KnobScan/.op),
        Do[
          tag = SextKLBump[dIunit, i, k2l0, Km0];
          If[tag,
            sigy0 = IPSizeTrack[beam0$][4];
            Print@sigy0;            
            AppendTo[res, {i, sigy0^2}]]
          ,{i, dImin, dImax, dstep}];

        If[dImax>dImin, 
          f = PolynomialFit[res, 2];
          dIb = -f[1,2]/f[1,3]/2;
          tag = SextKLBump[dIunit, dIb, k2l0, Km0];
          sigy0 = IPSizeTrack[beam0$][4];
      
          ! go to the min. instead of fitting center (fit is not good or out of range)
          If[sigy0^2>Min[res[;,2]]||~tag,
            ! Print["sigy0^2= ", sigy0^2, " Min.= ",Min[res[;,2]], " tag= ", tag];
            dIb = res[Position[res[;,2], Min[res[;,2]]][1,1], 1];
            tag = SextKLBump[dIunit, dIb, k2l0, Km0];
            ];
          ],
        tag = SextKLBump[dIunit, dImin, k2l0, Km0];           
        ];
      {tag, IPSizeTrack[beam0$], f, res}];

    ! for debug
    MultiKnobY[knob_, dI_, opt___]:= Module[{res}, res=MultiKnobY[knob, dI, dI, 1, opt]; If[GM$slow, SlowGM[15*60]]; res];

    Y24[min_, max_, dI_, opt___]:=MultiKnobY["Y24", min, max, dI, opt];
    Y46[min_, max_, dI_, opt___]:=MultiKnobY["Y46", min, max, dI, opt];
    Y22[min_, max_, dI_, opt___]:=MultiKnobY["Y22", min, max, dI, opt];
    Y26[min_, max_, dI_, opt___]:=MultiKnobY["Y26", min, max, dI, opt];
    Y66[min_, max_, dI_, opt___]:=MultiKnobY["Y66", min, max, dI, opt];
    Y44[min_, max_, dI_, opt___]:=MultiKnobY["Y44", min, max, dI, opt]; 
    ]; ! With

  Octupole[mag_, kmin_, kmax_, dk_]:=Module[{sigy0, res={}, i, f, k3b},
    Table[
      Element["K3", mag//"*"]=i;
      FFS["CALC NOEXP;"];
      sigy0=IPSizeTrack[beam0$][4];
      AppendTo[res, {i, sigy0^2}];
      ,{i, kmin, kmax, dk}];
      
    If[kmax>kmin, 
      f = PolynomialFit[res, 2];
      k3b = -f[1,2]/f[1,3]/2;
      Element["K3", mag//"*"]=k3b;
      FFS["CALC NOEXP;"];
      sigy0 = IPSizeTrack[beam0$][4];
      If[sigy0^2>Min[res[;,2]]||k3b<kmin||k3b>kmax,
        k3b = res[Position[res[;,2], Min[res[;,2]]][1,1], 1];
        Element["K3", mag//"*"]=k3b;
        FFS["CALC NOEXP;"];
        ];      
      ];    
    {1, IPSizeTrack[beam0$], f, res}];

  ! Swapped oct. setting (2019.10.8)
  Octupole[mag_:"OCT2"]:=Module[{res},
    If[StringMatchQ[Flatten@{Element["NAME", mag//"*"]}[1], "OCT2*"],
      res=Octupole["OCT2FF", -370, 370, 74],
      If[StringMatchQ[Flatten@{Element["NAME", mag//"*"]}[1], "OCT1*"],
        res=Octupole["OCT1FF", -45, 45, 9],
        Print@"\tWrong Octupole name.\n";
        FFS["ABORT;"]];
      ];
    res]
  ]; ! Class

(* to ease/simplify the scan p *)
ScanLinearKnob[kmin_, kmax_, dk_, res0_]:=Module[{res2=res0, res},
  res = TK@AY[kmin, kmax, dk, Track->True];
  AppendTo[res2, Prepend[res, "AY"]];  
  res =  TK@EY[kmin, kmax, dk, Track->True];
  AppendTo[res2, Prepend[res, "EY"]];
  res =  TK@Coup2[kmin, kmax, dk, Track->True];
  AppendTo[res2, Prepend[res, "R32"]]; 
  res2];

! res = TK@DeltaKnob[-2, 2, 0.5, Track->True];
! res = TK@QK[-5, 5, 1, Track->True];

With[{def={MultiKnob->True}},
  ScanKnobAllRough[res0_, opt___]:=Module[{op, res2=res0, res},
    op = Override[opt, def];
    res =  TK@AY[-0.3, 0.3, 0.1, Track->True];
    AppendTo[res2, Prepend[res, "AY"]];
    res =  TK@EY[-.3, .3, 0.1, Track->True];
    AppendTo[res2, Prepend[res, "EY"]];
    res =  TK@Coup2[-0.3, 0.3, 0.1, Track->True];
    AppendTo[res2, Prepend[res, "R32"]];
  
    res = TK@Y24[-1.5, 1.5, 0.3];
    AppendTo[res2, Prepend[res, "Y24"]];
    res = TK@Y46[-1, 1, .2];
    AppendTo[res2, Prepend[res, "Y46"]]; 

    If[MultiKnob/.op,
      res = TK@Y22[-0.6, .6, .2];
      AppendTo[res2, Prepend[res, "Y22"]]; 
      res = TK@Y26[-0.6, .6, .2];
      AppendTo[res2, Prepend[res, "Y26"]]; 
      res = TK@Y66[-0.2, .2, .05];
      AppendTo[res2, Prepend[res, "Y66"]]; 
      res = TK@Y44[-0.6, .6, .2];
      AppendTo[res2, Prepend[res, "Y44"]];
      ];
    res2];

  ScanKnobAllFine[res0_, opt___]:=Module[{op, res2=res0, res},
    op = Override[opt, def];
    res =  TK@AY[-0.1, 0.1, 0.03, Track->True];
    AppendTo[res2, Prepend[res, "AY"]]; 
    res =  TK@EY[-0.1, 0.1, 0.03, Track->True];
    AppendTo[res2, Prepend[res, "EY"]]; 
    res =  TK@Coup2[-0.1, 0.1, 0.03, Track->True];
    AppendTo[res2, Prepend[res, "R32"]]; 

    res = TK@Y24[-.3, .3, 0.1];
    AppendTo[res2, Prepend[res, "Y24"]];  
    res = TK@Y46[-.45, .45, 0.15];
    AppendTo[res2, Prepend[res, "Y46"]];    
    If[MultiKnob/.op,
      res = TK@Y22[-0.6, .6, .2];
      AppendTo[res2, Prepend[res, "Y22"]];    
      res = TK@Y26[-0.3, .3, .1];
      AppendTo[res2, Prepend[res, "Y26"]];   
      res = TK@Y66[-0.2, .2, .05];
      AppendTo[res2, Prepend[res, "Y66"]];   
      res = TK@Y44[-0.6, .6, .2];
      AppendTo[res2, Prepend[res, "Y44"]];
      ];
    res2];
  ]; ! With[