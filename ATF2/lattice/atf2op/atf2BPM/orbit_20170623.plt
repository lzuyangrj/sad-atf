# plot optic of ATF2
reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file

set key font ",24"
set key spacing 1.5
set key right top #set position of the titles of points of lines

set output "orbit_20170623.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol D}_{x,y} [mm]"

# set grid off
# set xrange[63:94]
# set format x "%.0e"
# set yrange[0:35]

plot  'atf2BPM_20170623.dat' u 2:($3*1e-3) every ::1::38 pt 4 ps 1.5 lc rgb 'red' lw 3 w lp t "{/Symbol D}x" ,\
      'atf2BPM_20170623.dat' u 2:($5*1e-3) every ::1::38 pt 7 ps 1.5 lc rgb 'black' lw 3 w lp t "{/Symbol D}y" ,\
