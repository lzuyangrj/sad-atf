! --- SAD functions for ATF2 beamline
!                         by T.Okugi (2008/10/15)
!
! --- definition of the list of bending magnets
  bendexA=Element["Name","BH*XA"];
  bendexB=Element["Name","BH*XB"];
  bendffA=Element["Name","B*FFA"];
  bendffB=Element["Name","B*FFB"];
  benddump={"BDUMPA","BDUMPB"};
  bendlist=Join[bendexA,bendexB,bendffA,bendffB,benddump];

! --- definition of the list of quadrupole magnets
  quaddr=Element["Name","Q*R"];  
  quadex=Element["Name","Q*X"];
  quadff=Element["Name","Q*FF"];
  quadlist=Join[quaddr,quadex,quadff];

! --- definition of the list of sextupole magnets
  sextlist=Element["Name","S*FF"];

! --- definition of the list of steering magnets
  sthex=Element["Name","ZH*X"];
  stvex=Element["Name","ZV*X"];
  stxex=Element["Name","ZX*X"];
  strff={"ZH1FF","ZV1FF"};
  sterlist=Join[sthex,stvex,stxex,strff];

! --- function of the save optics
  SaveATF2Optics[f_]:=Module[
        {fn=OpenWrite["/atf/op/sad/current/data/"//f]},
! 2012/02/15 change the directory to save & load file
!        {fn=OpenWrite["/atfnfs/linux/sad/"//f]},
!        {fn=OpenWrite["./ringoptics/"//f]},
! 2009/05/08 change the directory to save & load file
!        {fn=OpenWrite["./ringoptics/"//f]},
    qv=Thread[{quadlist,Element["K1",quadlist]*2}];
    sv=Thread[{sextlist,Element["K2",sextlist]*2}];
    zv=Thread[{sterlist,Element["K0",sterlist]}];
    $FORM="";Write[fn,"qvatf2=",qv,";\n\nsvatf2=",sv,";\n\nzvatf2=",zv,";"];
    Close[fn];];

! --- function of the load optics
  LoadATF2Optics[f_]:=Module[{sf6k2,sf5k2,sd4k2,sf1k2,sd0k2},
    Get["/atf/op/sad/current/data/"//f];
! 2012/02/15 change the directory to save & load file
!    Get["/atfnfs/linux/sad/"//f];
!    Get["./ringoptics/"//f];
! 2009/05/08 change the directory to save & load file
!!    Get["./ringoptics/"//f];
    Map[(Element["K1",#[1]]=#[2]/2)&,qvatf2];
    Map[(Element["K2",#[1]]=#[2]/2)&,svatf2];
    Map[(Element["K0",#[1]]=#[2])&,zvatf2];
    sf6k2=LINE["K2","SF6FF"];Element["K2","SF6FF"]=+Abs[sf6k2];
!    sf5k2=LINE["K2","SF5FF"];Element["K2","SF5FF"]=-Abs[sf5k2];
    sd4k2=LINE["K2","SD4FF"];Element["K2","SD4FF"]=+Abs[sd4k2];
    sf1k2=LINE["K2","SF1FF"];Element["K2","SF1FF"]=-Abs[sf1k2];
    sd0k2=LINE["K2","SD0FF"];Element["K2","SD0FF"]=+Abs[sd0k2];];



!!!!! added by K.Kubo  20091210 !!!
  SetATF2Quad[n_:1]:=Module[{
    k1list=ATFQuad/.ATFData[n]/.ATFQuad->{}},
! --- Modified by T.Okugi (2001/05/09) ---
!    k1list=Select[k1list,MemberQ[quadex,#[1]]&];
!    Scan[Check[Element["K1",#[1]]=#[2],]&,k1list]];
!    k1list=Map[{qexname[#[[1]]],#[[2]]}&,k1list];
    k1list=Select[k1list,(MemberQ[quadex,#[1]] || MemberQ[quadff,#[1]])&];Print[k1list];
    Scan[Check[LINE["K1",#[1]]=#[2]*0.5,]&,k1list]];
    Scan[(
        If[#[2]=="D",LINE["K1",#]=-Abs[LINE["K1",#]]];
        If[#[2]=="F",LINE["K1",#]= Abs[LINE["K1",#]]];
    )&,Map[(#[1])&,k1list]];




