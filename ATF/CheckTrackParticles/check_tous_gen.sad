! Titile: Check Tous. scatterred particle generation
! 2020.05.03
  OFF ECHO CTIME;
  
  FFS;
  NPARA = 4;
  
  GetMAIN["atfdr-design-2008-misalign.sad"];
  MOMENTUM=1.3e9;
  Charge=+1;
! Relative shift of RF frequency in ring  
  FSHIFT=0;
! Minimum emit ratio (emity/emitx) for intra-beam scattering effect
  MINCOUP = 0.1;
! num of particles in one bunch
  PBUNCH=3e9;

  USE RING0;
  CELL; CALC;

  wdir = "./";
  ldir = "./";
  ringID = "ATF";

  Get[ldir//"func.n"];
  Get[ldir//"func_halo_gen.n"];

(*
E0 = 0.511e6;
Ek = MOMENTUM-E0;
re = 2.818e-15;
gamma = (E0+Ek)/E0; ! LAB frame
beta = Sqrt[1-1/gamma^2];
c0 = 2.9979e8;

dpmin=1e-2; ! Momemtum acceptance/range
dpmax=50e-2; 
Ne = 3e9; ! e/pulse

alfx = -4.436;
betx = 5.25;
alfy = 1.30;
bety = 1.72;
etax = 0.12;
etapx = 0.1;
etay = -0.006;
etapy = 0.0047;
*)

emitx = 1.2e-9;
emity = 1.2e-11;
sigs = 5.6e-3; ! m
sigp = 6e-4 ;

tw0={-4.436, 5.25, 1.30, 1.72, 0.12, 0.1, -0.006, 0.0047};
emit0 = {1.2e-9, 1.2e-11, 0};

res=ScatRateOnePoint[tw0, emit0, sigs, sigp, 1e-2, 50e-2, PBUNCH];
{tau, Fdt}=res[2];

! RandomOverCDF[FtCDF]
! ScatterRate, (/s); TousLifeTime, (s)
! 
SetAttributes[{ScatterRate, TousLifeTime, ScatterRateCDFOneTurnNorm, ScatterRateDpCDFOneTurnNorm, DpSquareOneTurn}, Constant];

! Rcdf: CDF of scattering rate vs. dp
InitTouschek$[emit0_, sigs_, sigp_, dpmin_, dpmax_, Ne_, opt___]:=Module[{r={}, i, Ee=0.511e6, E0=MOMENTUM, gamma, beta, seq0, s0, nobs, cir0, s1, ds, twall, resAllObs={}, nScatAllObs={}, tau, Fdt, RcdfAll={}, Rcdfi, nScatAccu={}, lifetime},
  gamma = E0/Ee;
  beta = Sqrt[1-1/gamma^2];
  {seq0, s0} = LINE[{"POSITION", "S"}, "{QBS}*"];
  nobs = Length@s0;
  cir0 = LINE["S", "$$$"];
  s1 = Append[s0[Range[nobs-1]+1], cir0];
  ds = s1-s0;
  twall = Thread[Twiss[{"AX", "BX", "AY", "BY", "PEX", "PEPX", "PEY", "PEPY"}, seq0]];

  ! scat. rate at every  point
  Scan[(AppendTo[resAllObs, ScatRateOnePoint[twall[#], emit0, sigs, sigp, dpmin, dpmax, Ne]])&,Range[nobs]];
  temp = resAllObs;
  nScatAllObs = resAllObs[;;,1];
  {tau, Fdt} = Thread[resAllObs[;;,2]];
  Table[
    Rcdfi={};
    Scan[(AppendTo[Rcdfi, Plus@@(Fdt[i, Range[#]])])&,Range[Length@(Fdt[i])]];
    Rcdfi /= Plus@@(Fdt[i]);
    AppendTo[RcdfAll, Rcdfi],{i, Length@Fdt}];
  dpSquareLib$ = tau/(beta^2)/(1+tau); ! delta^2
  nScatDpCDFLib$ = RcdfAll; ! R vs. dp/tau, normalized
  
  ! scattering rate for one turn
  Scan[(AppendTo[nScatAccu, Plus@@((nScatAllObs*ds/cir0)[Range[#]])])&, Range[nobs]];
  nScatOneTurn$ = nScatAccu[-1];
  nScatAccuNorm$ = nScatAccu/nScatOneTurn$; ! nomalized, max=1
  lifetime = 1/nScatOneTurn$*Ne;
  r={ScatterRate->nScatOneTurn$, TousLifeTime->lifetime, ScatterRateCDFOneTurnNorm->nScatAccuNorm$, ScatterRateDpCDFOneTurnNorm->nScatDpCDFLib$, DpSquareOneTurn->dpSquareLib$};
  r];

 res = InitTouschek$[emit0, sigs, sigp, 0.3e-2, 10e-2, PBUNCH];

end;
