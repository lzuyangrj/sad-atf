!======================================================
! Functions for tail/halo particle generation
! by Renjun Yang (created on 3 May, 2020)
!======================================================

(* part1 BGS *)


(* part2 Touschek Scattering*)

!
With[{def={}},
  ! wrt. A. Piwinski, 1998 paper Eq. (42)
  ! fast assessement of integration part F(B1, B2, tau)
  FsigmaInt[t_, tm_, B1_, B2_]:=Module[{},
    ((2+1/t)^2*(t/tm/(1+t)-1)+1-Sqrt[(1+t)*tm/t]-(2/t+1/2/t^2)*Log[t/tm/(1+t)])*Exp[-B1*t]*BesselI[0, B2*t]*Sqrt[t/(1+t)]];
   
  ! tw0: {alfx, betx, alfy, bety, etax, etapx, etay, etapy}
  ! sigs, sigp: could be inferred from emits (to be checked, 2020.05.03
  ! dpmin/dpmx: min/max scattering angle (X, kappa in Piwinski's formulas)
  ! output: scatter rate, {tau_i, F_i}
  ScatRateOnePoint[tw0_, emit0_, sigs_, sigp_, dpmin_, dpmax_, Ne_:PBUNCH, opt___]:=Module[{Ee=0.511e6, E0=MOMENTUM, Ek, re=2.818e-15, gamma, beta, c0=2.9979e8, alfx, betx, alfy, bety, etax, etapx, etay, etapy, emitx, emity, emits, sigxb, sigx, etax2, sigyb, sigy, etay2, sigx2, sigy2, sigh, B1, B2, tmax, tmin, Fdt={}, intg=0, nbin=20000, dt, tt, F, nScat, Tous},
    op = Override[opt, def];
    Ek = E0-Ee;
    gamma = E0/Ee;
    beta = Sqrt[1-1/gamma^2];
    {alfx, betx, alfy, bety, etax, etapx, etay, etapy} = tw0;
    {emitx, emity, emits} = emit0;
    sigxb = Sqrt[emitx*betx]; ! betatron size
    sigx = Sqrt[emitx*betx + etax^2*sigp^2];
    etax2 = alfx*etax + betx*etapx;
 
    sigyb = Sqrt[emity*bety];
    sigy = Sqrt[emity*bety + etay^2*sigp^2];
    etay2 = alfy*etay + bety*etapy;

    sigx2 = Sqrt[emitx*betx + sigp^2*(etax^2 + etax2^2)]; ! sigmax'
    sigy2 = Sqrt[emity*bety + sigp^2*(etay^2 + etay2^2)];

    sigh = 1/Sqrt[1/(sigp^2*sigxb^2*sigyb^2) * (sigx2^2*sigyb^2+sigy2^2*sigxb^2-sigxb^2*sigyb^2)];
    B1 = betx^2/(2*beta^2*gamma^2*sigxb^2)*(1-sigh^2*etax2^2/sigxb^2) + bety^2/(2*beta^2 *gamma^2 *sigyb^2)*(1-sigh^2*etay2^2/sigyb^2);
    B2 = Sqrt[B1^2 - betx^2*bety^2*sigh^2/(beta^4*gamma^4*sigxb^4*sigyb^4*sigp^2)*(sigx^2 *sigy^2 - sigp^4 *etax^2 *etay^2)];
    !tmax = (beta*dpmax)^2;
    tmax = (beta*dpmax)^2/(1-(beta*dpmax)^2);
    tmin = (beta*dpmin)^2/(1-(beta*dpmin)^2);
    dt = (tmax-tmin)/nbin;    
    Scan[(tt = FsigmaInt[tmin+#*dt, tmin, B1, B2]; ! -> tmax = 2%, for instance    
        If[tt>1e-12, AppendTo[Fdt, {tmin+(#-1)*dt, tt}]; intg+=tt*dt];)&, Range[nbin]]; ! left side
      
    F = Sqrt[Pi*(B1^2 -B2^2)]*tmin*intg;
    ! local Scatter rate
    nScat = re^2*c0*Ne^2/(8*Pi*gamma^2*sigs*Sqrt[sigx^2*sigy^2-sigp^4*etax^2*etay^2]*tmin)*F; ! scatter rate (dN/dt)
    {nScat, Thread@Fdt}]; 

  ! CDF, cumulative distribution function (discrete)
  ! general function
  RandomOverCDF[CDF_]:=Module[{h, npos, left=0, mid, right, i, ndiv=2},
    h = Random[];
    npos = Length@CDF;
    mid=Round[npos/2];  
    right=npos;
    Table[If[npos/2^ndiv>20, ndiv++], {20}];
    Scan[(If[h<CDF[mid], right=mid; mid= left+Round[npos/(2^#)], left=mid; mid=left+Round[npos/(2^#)]])&,Range[ndiv]+1];
    pos = Catch[Do[If[h<=CDF[i], Throw[i]], {i, left, right}]];
    Print[h, '\t', left, '\t', mid, '\t', right, '\n'];
    Print@ndiv;
    pos];

  GetScatLocation[CDF_, np_:1]:=Module[{pos={}},
    Table[AppendTo[pos, RandomOverCDF[CDF]],{np}];
    pos];
  ! CDF$ to be filled
  GetScatLocation[np_:1]:=GetScatLocation[CDF$, np];


  ]; ! With
