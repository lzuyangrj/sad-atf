! for the off-momentum optics/beam parameters calculations
! copy from atf2lib_ultralow.n on Jun 7, 2020
!

RFKnobATF=Class[{},{},{},
  Export[name_,data_]:=Module[{nl,nw, fx, i},
    $FORM = "12.6";
    {nl, nw} = Dimensions[data];
    If[Length[Dimensions[data]]<>2,
       nl = Length[data];
       nw = Length[data[1]]
      ];
    System["rm "//name];
    fx = OpenWrite[name];
    Table[
      Table[
        WriteString[fx,data[i,j]," "]
        ,{j,1,nw}];
      WriteString[fx,"\n"];
      ,{i,1,nl}];
    Close[fx];
    $FORM = 'S17.15';
    ];

  TwissWholeRing[]:=Module[{nElem, pos={}, twiss0={}, i},
    nElem = LINE["POSITION", $$$];
    Do[
      If[LINE["S", i-1]<>LINE["S", i],
        AppendTo[pos, LINE["S", i]];
        AppendTo[twiss0, Twiss[{"BX", "AX", "BY", "AY", "DX", "DPX", "DY", "DPY", "PEX", "PEY"}, i]];
        ]
       ,{i, 2, nElem}];
    {pos, twiss0}
    ];

  ClosedOrb[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, orbs, orbs2, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP"];
    e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    
    orbs = ClosedOrbit/.e;
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    orbs2 = Join[{s}, Thread@orbs];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@orbs2];

  DispersionInvariant[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, hx, hy, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP;"];
    FFS["EMIT;"];
     
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    hx = 1/Twiss["BX", #]*(Twiss["PEX", #]^2+(Twiss["BX", #]*Twiss["PEPX", #] + Twiss["AX", #]*Twiss["PEX", #])^2)&/@Range[LINE["POSITION", $$$]];
    hy = 1/Twiss["BY", #]*(Twiss["PEY", #]^2+(Twiss["BY", #]*Twiss["PEPY", #] + Twiss["AY", #]*Twiss["PEY", #])^2)&/@Range[LINE["POSITION", $$$]];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@{s, hx, hy}];

  Dispersion[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, etas, etas2, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP;"];
    FFS["EMIT;"];
    
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    etas = Twiss[{"PEX", "PEPX", "PEY", "PEPY"}, Range[LINE["POSITION", $$$]]];
    etas2 = Join[{s}, etas];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@etas2];

  EmitTwiss[dfmin_:Hz, dfmax_:Hz, dfstep_:Hz, elem_]:= Module[{e, freq0=714e6, freq2p, dE, tune, emits={}, tws = {}},
    e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    freq2p= -1/freq0/(MomentumCompaction/.e);
    Do[
      dE = df*freq2p;
      FFS["DP0="//ToString[dE]];
      FFS["FSHIFT="//ToString[df/freq0]];
      FFS["CAL NOEXP"];
      FFS["EMIT"];
      ! e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
      tune = Twiss[{"NX", "NY", "NZ"}, $$$]/2/Pi;
      AppendTo[emits, Flatten@{EMITX, EMITY, SIGZ, SIGE, FractionPart/@tune}];      
      AppendTo[tws, Twiss[{"BX", "AX", "BY", "AY", "PEX", "PEPX", "PEY", "PEPY", "DX", "DPX", "DY", "DPY", "SIGX", "SIGY"}, elem]]; ! twiss at kicker
      ,{df, dfmin, dfmax, dfstep}];
    FFS["FSHIFT=0; DP0=0"];
    emits = Prepend[Thread@emits, Table[df*freq2p, {df, dfmin, dfmax, dfstep}]];     
    tws = Prepend[Thread@tws, Table[df*freq2p, {df, dfmin, dfmax, dfstep}]];    
    Thread/@{emits, tws}
    ];
  ];
