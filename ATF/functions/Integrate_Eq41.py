#!/usr/bin/env python3
# for fast integration of Eq.(31)/Eq.(41) of Piwinski formula 
# Output:
# an array of integrations and errs
# print("%.6e\t%.6e\t", ...) is madntory !!!

import argparse
from scipy.integrate import quad
import numpy as np
from scipy import special
import warnings

parser = argparse.ArgumentParser()
parser.add_argument('-tm_min',
                    action='store',
                    help='lower limit of tau_m, i.e., min. tau_m')

parser.add_argument('-tm_max',
                    action='store',
                    help='upper limit of tau_m, i.e., max. tau_m')

parser.add_argument('-ntm',
                    action='store',
                    help='number of tm.')

parser.add_argument('-B1', action='store')
parser.add_argument('-B2', action='store')

args = parser.parse_args()

tm_min = float(args.tm_min)
tm_max = float(args.tm_max)
ntm = int(args.ntm)
B1 = float(args.B1)
B2 = float(args.B2)

def f(t, tm):
    return ((2+1/t)**2 *(t/tm/(1+t) -1) + 1 - np.sqrt(1+t)/np.sqrt(t/tm) - 1/(2*t)*(4 + 1/t)*np.log(t/tm/(1+t))) *np.exp(-B1*t, dtype=np.float128)* special.i0e(B2*t)/np.exp(-np.abs(B2*t), dtype=np.float128) * np.sqrt(t)/np.sqrt(1+t)     

warnings.simplefilter("error", RuntimeWarning)
tms = np.linspace(tm_min, tm_max, ntm)
t_max = 0.1

# 0.01 --> 0.0064 --> 0.0049
tm = tms[-1]
try:
    quad(f, tm, t_max, args=tm)
except RuntimeWarning:
    t_max = 0.01
    
for tm in tms:    
    out = quad(f, tm, t_max, args=tm)
    if np.isnan(out[0]) or np.isinf(out[0]):
        break
    else:
        print("%.6e\t%.6e\t"%(out[0], out[1]))
