! for ATF COD bump
! taken from atf2lib_ultralow.n (2020.05.21)

!==============================================================
! COD and emittance/tune perturbations with RF freq change (ATF DR)
! export files:
! COD for all RF frequencies -> pos, bx, ax, by , ay, dx, dpx, dy, dpy
! Orbit at the EXT-IEX -> dE, BX, AX, BY, AY, DX, DPX, DY, DPY
! Tune/emit -> dE, nx, ny, nz, emitx, emity, emitz
!==============================================================
RFKnobATF=Class[{},{},{},
  Export[name_,data_]:=Module[{nl,nw, fx, i},
    $FORM = "12.6";
    {nl, nw} = Dimensions[data];
    If[Length[Dimensions[data]]<>2,
       nl = Length[data];
       nw = Length[data[1]]
      ];
    System["rm "//name];
    fx = OpenWrite[name];
    Table[
      Table[
        WriteString[fx,data[i,j]," "]
        ,{j,1,nw}];
      WriteString[fx,"\n"];
      ,{i,1,nl}];
    Close[fx];
    $FORM = 'S17.15';
    ];

  TwissWholeRing[]:=Module[{nElem, pos={}, twiss0={}, i},
    nElem = LINE["POSITION", $$$];
    Do[
      If[LINE["S", i-1]<>LINE["S", i],
        AppendTo[pos, LINE["S", i]];
        AppendTo[twiss0, Twiss[{"BX", "AX", "BY", "AY", "DX", "DPX", "DY", "DPY", "PEX", "PEY"}, i]];
        ]
       ,{i, 2, nElem}];
    {pos, twiss0}
    ];

  ClosedOrb[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, orbs, orbs2, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP"];
    e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    
    orbs = ClosedOrbit/.e;
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    orbs2 = Join[{s}, Thread@orbs];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@orbs2];

  DispersionInvariant[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, hx, hy, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP;"];
    FFS["EMIT;"];
     
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    hx = 1/Twiss["BX", #]*(Twiss["PEX", #]^2+(Twiss["BX", #]*Twiss["PEPX", #] + Twiss["AX", #]*Twiss["PEX", #])^2)&/@Range[LINE["POSITION", $$$]];
    hy = 1/Twiss["BY", #]*(Twiss["PEY", #]^2+(Twiss["BY", #]*Twiss["PEPY", #] + Twiss["AY", #]*Twiss["PEY", #])^2)&/@Range[LINE["POSITION", $$$]];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@{s, hx, hy}];

  Dispersion[df_]:=Module[{freq2p, freq0=714e6, e=Emittance[ExpandElementValues->False], dE, etas, etas2, s},
    freq2p= -1/freq0/(MomentumCompaction/.e);
    dE = df*freq2p;
    FFS["DP0="//ToString[dE]];
    FFS["FSHIFT="//ToString[df/freq0]];
    FFS["CAL NOEXP;"];
    FFS["EMIT;"];
    
    s = LINE["S", Range[LINE["POSITION", $$$]]];
    etas = Twiss[{"PEX", "PEPX", "PEY", "PEPY"}, Range[LINE["POSITION", $$$]]];
    etas2 = Join[{s}, etas];
    FFS["FSHIFT=0; DP0=0"];          
    Thread@etas2];

  EmitTwiss[dfmin_:Hz, dfmax_:Hz, dfstep_:Hz, elem_]:= Module[{e, freq0=714e6, freq2p, dE, tune, emits={}, tws = {}},
    e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
    freq2p= -1/freq0/(MomentumCompaction/.e);
    Do[
      dE = df*freq2p;
      FFS["DP0="//ToString[dE]];
      FFS["FSHIFT="//ToString[df/freq0]];
      FFS["CAL NOEXP"];
      FFS["EMIT"];
      ! e=Emittance[OneTurnInformation->True, Matrix->True, ExpandElementValues->False];
      tune = Twiss[{"NX", "NY", "NZ"}, $$$]/2/Pi;
      AppendTo[emits, Flatten@{EMITX, EMITY, SIGZ, SIGE, FractionPart/@tune}];      
      AppendTo[tws, Twiss[{"BX", "AX", "BY", "AY", "EX", "EPX", "EY", "EPY", "DX", "DPX", "DY", "DPY", "SIGX", "SIGY"}, elem]]; ! twiss at kicker
      ,{df, dfmin, dfmax, dfstep}];
    FFS["FSHIFT=0; DP0=0"];
    emits = Prepend[Thread@emits, Table[df*freq2p, {df, dfmin, dfmax, dfstep}]];     
    tws = Prepend[Thread@tws, Table[df*freq2p, {df, dfmin, dfmax, dfstep}]];    
    Thread/@{emits, tws}
    ];
  ];

! Set IEX parameters, manully
MatchIEX=Class[{},{},{},
 GetTwiss[]:=Twiss[{"AX", "BX", "AY", "BY"}, IEX]; ! ATF DR
 ! ATF2
 SetTwiss[par_]:=Module[{},
   If[BeamLineName[]=="ATF2",
      SetElement["IEX",,{"AX"->par[1], "BX"->par[2], "AY"->par[3], "BY"->par[4]}];
      FFS["CAL NOEXP"],
      Print["\tATF2 lattice wasn't loaded!\n"];
      ]
   ];
];

! wt, scale factor
OrbitBumpCorrection = Class[{},{},{},
  LoadOrbit[fp_, nbpm_, xmax_, ymax_]:= Module[{fr, posx, posy, flag, Orbitxobj, Orbityobj},  
    fr = OpenRead[fp];
    posx = {};
    posy = {};
    
    Do[
      flag = Read[fr, Real];
      If[ToString[flag]=="EndOfFile", Break[]];
      posx = AppendTo[posx, flag*1e-6];
      posy = AppendTo[posy, Read[fr, Real]*1e-6];
      ,{i, 1, nbpm}];  
    Close[fr];

    Orbitxobj = {};
    Orbityobj = {};
    Do[
      If[Abs[posy[i]]<ymax && Abs[posx[i]]<xmax, ! && posx[i]*posy[i]<>0,
        AppendTo[Orbitxobj, {"M."//i, "X", posx[i]}];
        AppendTo[Orbityobj, {"M."//i, "Y", posy[i]}];
        ]
      ,{i, 1, Length[posx]}];
    {Orbitxobj, Orbityobj}
    ];
    
  LoadOrbit[fp_]:=LoadOrbit[fp, 96, 2e-3, 1e-3];
  
  ! to seperate from Bump[]
  Bump2[orbx_, orby_, wt:{___,___}]:=Module[{bpm, strx, stry},
    ! Print[wt//"\n"];
    DesignOptics=CalculateOptics[1,LINE["LENGTH"],Twiss["*",1],FLAG["CELL"],2]; 
    bpm=Monitor["M*"];
    strx=Steer["ZH*"]; 
    stry=Steer["ZV*"]; 

    ! read ATF BPMs data    
    ! {orbx, orby} = GetWholeRingOrbit[fp, wt];
    
    b1 = Bump[strx, Condition -> orbx];
    b2 = Bump[stry, Condition -> orby];
    MakeBump[b1, strx, DesignOptics];
    MakeBump[b2, stry, DesignOptics];
    
    FFS["CALC NOEXP"];
    
    Clear[DesignOptics, b1, b2];
    ];

  Bump2[orbx_, orby_]:=Bump2[orbx, orby, {1, 1}];

  QuadRorateError[qm_, err_]:=Module[{qm2, roll},
    (* for one single element *)
    qm2 = Flatten@{Element["NAME", qm//"*"]};
    roll = err*GaussRandom[Length[qm2]];
    Scan[(LINE["ROTATE",#[[1]]] = #[[2]];)&, Thread[{qm2, roll}]];
    
    !FFS["SAVE ALL;"];
    FFS["CALC NOEXP;"];
    FFS["Reject Total;"]; 
    FFS["FIX "//name];    
    ];

  SextError[elem_, dyrms_]:=Module[{sd, dy},
    sd=LINE["NAME", elem];
    dy=dyrms*GaussRandom[Length[sd]];
    Scan[(LINE["DY",#[[1]]]=#[[2]];)&, Thread[{sd,dy}]];
    !FFS["SAVE ALL;"];
    FFS["CALC NOEXP;"];
    FFS["Reject Total;"]; 
    FFS["FIX "//elem];    
    ];

  SextBBA[name_, weight_:{1,1}, err_:0]:=Module[{sd, orb0, dx, dy},
    sd=LINE["NAME", name];
    FFS["CAL NOEXP;"];
    orb0 = Twiss[{"DX", "DY"}, name];
    {dx, dy} = orb0 + err*GaussRandom[2, Length[sd]];
    {dx, dy} *=weigth;
    Scan[(LINE["DX",#[[1]]]=#[[2]];)&, Thread[{sd, dx}]];
    Scan[(LINE["DY",#[[1]]]=#[[2]];)&, Thread[{sd, dy}]];
    FFS["CALC NOEXP;"];
    FFS["Reject Total;"]; 
    FFS["FIX "//name];        
    ];
    
  ];

! based on scripts from ATF server, thanks to Kubo-san
! integrated by Renjun on May 28, 2020
With[{def0 = {SaveCODDisp->False, Seed->17, QuadRoll->5e-3, Ldir->"/Users/rj/Fellow-CERN/SAD/ATF/functions/"}},
  CODDispersionBump[opt___]:=Module[{obc, opt2, qm, op, seed0},  
    op = Override[opt, def0];
    If[Seed/.op,
      seed0 = SeedRandom[][2];
      SeedRandom[Seed/.op]];
      
    LINE["ANGLE","KIX"]=0;
    LINE["K0", "KIX"]=0;
    FFS["CALC NOEXP; SAVE;"];
    
    If[~(DirectoryQ[Ldir/.op]), Print["\t Error: Set correct directory for lib. functions."]; FFS["end;"]];
    Get[(Ldir/.op)//"atfsad-operation-lib/lib/coddispersion.n"];
    Get[(Ldir/.op)//"atfsad-operation-lib/lib/drhdispbyq.n"]; ! HOR. dispersion correction
    Get[(Ldir/.op)//"atfsad-operation-lib/lib/atfringlib-new.n"];
    Get[(Ldir/.op)//"atfsad-operation-lib/lib/correctk1-bh1.n"];
    Get[(Ldir/.op)//"atfsad-operation-lib/lib/drawdrex.n"];
    Get[(Ldir/.op)//"atfsad-operation-lib/lib/drawgraph.n"];

    obc = OrbitBumpCorrection[];

    def={BeamLine->"Ring",Command->"Save",OpticsName->"", RingMomentum->1.3E9};
    Get[(Ldir/.op)//"atfsad-operation-lib/Input/RING_ORBIT_20190528.DAT"];

    option=Option;
    {bline,cmd,optics,momring,cortol}=
      ({BeamLine,Command,OpticsName,RingMomentum,Tolerance}/.option)/.def;
    optics=TruncateString[optics];
    If[StringLength[optics]>1,optics=TruncateStringBoth[optics]];
    If[StringLength[cmd]>1,cmd=TruncateStringBoth[cmd]];

    badbpmlist={};
    badsteerlist={};
    goodbpmlist=Select[LINE["Name","M.*"],(Not[MemberQ[badbpmlist,#]])&];
    goodsteerlist=Select[LINE["Name","Z*R"],(Not[MemberQ[badsteerlist,#]])&];

    SetRingQuad[];
    !SetRingQMTrim[]; ! cause unstable
    !SetRingQFTrim[];
    SetRingSext[];
    SetRingSteer[];
    setBH1RK1[];    
    correctQK1[1];

    FFS["Renumber ZH48R;"];
    FFS["cell;  Z* k0 0;  CALC NOEXP;   VARY K0 Z*;  Z* MINMAX 0.0005;"];

    !qm = "QM"//ToString[#]//"R"&/@Range[6, 17];
    qm = Element["NAME", "QM*"];
    Scan[obc@QuadRorateError[#, QuadRoll/.op]&, qm];
    ! obc@QuadRorateError["QM*", 2.5e-3]; 
  
    ! Orbit bump  
    ! {orbx, orby} = obc@LoadOrbit["./drbpm/bpm19mar05_080029_decode.dat", 96, 2e-3, 0.7e-3];
    ! orbx[;;,3]*=0.3;
    ! orby[;;,3]*=0.0;  
    ! obc@Bump2[orbx, orby];  
    ! obc@SextBBA["QM*", {1, 1}, 10e-6]; 
    
    opt2 = {reductionfactor->0.1, ifbyq->False, ifextobt->False, ifextdsp->False};
    bpmlist1 = CorrectCODDispersion[opt2];
    EMIT;
  
    extm = GetMeasuredDispersion[bpmlist1];
    extb = ATFRingDispersion;
    If[SaveCODDisp/.op,
      Export["./Dispersion_measure.dat", extm[;;,{2,3,4}]];
      Export["./Dispersion_bump.dat", extb[;;,{2,3,4}]];];
  
    FFS["FREE Q{M}*; FIT NX 15.17; FIT NY 8.57;"];
    Table[FFS["go noexp;"],{2}];
    FFs["SAVE QM*; FIX *;"]; 
    FFS["CALC NOEXP;"];
  
    bpmlist1 = CorrectCODDispersion[opt];
    !Drhdispbyq;
    FFS["CALC NOEXP;"];
    FFS["EMIT"];
    If[Seed/.op, SeedRandom[seed0]];
    ];    
  ];


Protect[OrbitBumpCorrection, RFKnob];