!==============================================================
! set the aperture for ATF DR (2016-09-06)
! only south traight section is with run track beam pipe (ZV33R -> QM19R)
!==============================================================
SetATFAperture[misalign_:False]:=
  Module[{ll,elelist0,p1,p2},
         SetElement["APT", "APERT", {"DX"->0.012, "DY"->0.012}];
         SetElement["APT2", "APERT", {"DX"->0.0235, "DY"->0.0075}];
         BLApert = ExtractBeamLine[];
         ll = Length[BLApert];

         !(*region of south straight section *)
         elelist0 = LINE["NAME","{BQSIMZ}*"];
         p1=Position[elelist0,"ZV33R"][1][1];
         
         ! depend on align perfectly or with alignment errors
         If[misalign==0,
            p2=Position[elelist0,"QM19R.2"][1][1],
            p2=Position[elelist0,"QM19R2"][1][1]
           ];

         Do[ BLApert = If[p1<ii<p2,
                    BeamLine[Insert[BLApert,APT2,2*ii]],
                    BeamLine[Insert[BLApert,APT,2*ii]]
                    ]
             ,{ii,1,ll-1}];
         
         FFS["USE BLApert"];
         FFS["cell calc",6];
         LineN = Length[BLApert];
     ];
