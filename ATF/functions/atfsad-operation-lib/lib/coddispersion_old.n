
 

  CorrectCODDispersion:=Module[{pm,
    fixsteers=FixSteer/.Option/.FixSteer->{},
!    bpmlist1=ATFBPM/.ATFData[1]/.ATFBPM->{},
    bpmlist2=ATFBPM/.ATFData[2]/.ATFBPM->{},
    k0list=ATFSteer/.ATFData[1]/.ATFSteer->{},
    chop=ChopIntensity/.Option/.ChopIntensity->0,
    tunelist=ATFTune/.ATFData[1]/.ATFTune->{},
    
    mx=NSteerX/.(EtaCorrect/.Option)/.(NSteerX ->0),
    my=NSteerY/.(EtaCorrect/.Option)/.(NSteerY ->0),
    ratio=1.414,de,
    k0listx,k0listy,dzh,dzv,usezh,usezv,A,bpmx,bpmy,dispx0,dispx1,dispy0,dispy1,
    datazh,datazv,bpmnamelist1,maxdzh,maxdzv,mindzh,mindzv,datalist,my0,mx0,iflimit,
    etaxlist0,etaxlist1,etaylist0,etaylist1,
    maxk0,soft,dk0
    },
 bpmlist1=ATFBPM/.ATFData[1]/.ATFBPM->{};
    fchange=FrequencyChange/.(EtaCorrect/.Option)/.(FrequencyChange ->5e3);
    momentumcomp=MomentumCompaction/.(EtaCorrect/.Option)/.(MomentumCompaction ->2e-3);
    de=-fchange/714e6/momentumcomp;

    Print["de=",de];

    bpmlist1=Select[bpmlist1,(#[4]>chop)&];
    bpmlist1=Select[bpmlist1,MemberQ[goodbpmlist,#[1]]&];
    bpmlist1=Map[{#[[1]],#[[2]]/de,#[[3]]/de,#[[4]]}&,bpmlist1];
    bpmlist2=Select[bpmlist2,(#[4]>chop)&];
    bpmlist2=Select[bpmlist2,MemberQ[goodbpmlist,#[1]]&];
      FFS["Z* K0 0;"];
    NXm=tunelist[1];NYm=tunelist[2];
    FFS["FIT NX NXm NY NYm"];
!Print["ab1"];
    FFS["FREE QF* go; cal"];
!Print["ab2"];
    FFS["Reject Total;"]; 
    FFS["FIX Q* "];
    

    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    k0list=Select[k0list,MemberQ[LINE["Name","Z*R"],#[1]]&];

 

    maxk0=0.16138E-02;


    bpmfitlist1=Map[(
      If[((LINE["Position",#[[1]]]=>LINE["Position","M.19"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.80"])),
      {#[[1]],#[[2]],#[[3]],0,0},
      {#[[1]],#[[2]],#[[3]],Twiss["EX",#[[1]]],0}])&,bpmlist1];

    bpmnamelist1=Map[#[1]&,bpmfitlist1];


    bpmfitlist2=Map[(
      If[((LINE["Position",#[[1]]]=>LINE["Position","M.19"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.31"]) ||
          (LINE["Position",#[[1]]]=>LINE["Position","M.66"] && 
           LINE["Position",#[[1]]]<=LINE["Position","M.80"])),
      {#[[1]],#[[2]],#[[3]],Twiss["DX",#[[1]]],0},
      {#[[1]],#[[2]],#[[3]],0,0}])&,bpmlist2];

    bpmnamelist2=Map[#[1]&,bpmfitlist2];


    soft=2;
    factor1=0.05;

    If[mx<=0 && my>=1, 
     k0listy=Select[k0list,(StringMatchQ[#[1],"ZV*R"])&];

     k0listy=Select[k0listy,Not[MemberQ[fixsteers,#[1]]]&];

     Scan[(maxdzv[#[1]]=#[2]+maxk0)&,k0listy];
     Scan[(mindzv[#[1]]=#[2]-maxk0)&,k0listy];
         
     usezv=Select[Map[#[1]&,k0listy],(maxdzv[#]>0 && mindzv[#]<0)&];
     dk0=1e-4;
     datazveta=Map[(steer=#;
      FFS["CAL"]; 
      dispy0=Map[(Twiss["EY",#])&,bpmnamelist1]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      dispy1=Map[(Twiss["EY",#])&,bpmnamelist1];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (dispy1-dispy0)/dk0)&,usezv];

     datazvcod=Map[(steer=#;
      FFS["CAL"]; 
      cody0=Map[(Twiss["DY",#])&,bpmnamelist2]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      cody1=Map[(Twiss["DY",#])&,bpmnamelist2];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (cody1-cody0)/dk0)&,usezv];

     datazv=Transpose[Join[Transpose[datazveta]*factor1,Transpose[datazvcod]]];

     A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
     bpmy=Join[Map[(#[3])&,bpmfitlist1]*factor1,Map[(#[3])&,bpmfitlist2],Table[0,{Length[usezv]}]];
     dzv=LinearSolve[A,bpmy];

AAA=A;bpmyyy=bpmy;
!Print[" abcdeA  ",A];
!Print[" abcdeb  ",bpmy];

     If[Length[usezv]>my,
      While[Length[usezv]>my*ratio && my=>1,
       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezv,datazv,dzv,Abs[dzv]}]];
       datalist=SelectMax[datalist,Length[usezv]/ratio];
       usezv=Transpose[Transpose[datalist][1]][1];
       datazv=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
       bpmy=Join[Map[(#[3])&,bpmfitlist1]*factor1,Map[(#[3])&,bpmfitlist2],Table[0,{Length[usezv]}]];
       dzv=LinearSolve[A,bpmy];
      ];

       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezv,datazv,dzv,Abs[dzv]}]];
       datalist=SelectMax[datalist,my];
       usezv=Transpose[Transpose[datalist][1]][1];
       datazv=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
       bpmy=Join[Map[(#[3])&,bpmfitlist1]*factor1,Map[(#[3])&,bpmfitlist2],Table[0,{Length[usezv]}]];
       dzv=LinearSolve[A,bpmy];

     ];

     bpmfitlist10=bpmfitlist1;
     bpmfitlist20=bpmfitlist2;
     my0=my;
     dzvfix={};

     iflimit=0;
     Scan[If[#[2]>maxdzv[#[1]] || #[2]<mindzv[#[1]], 
     iflimit=1]&,Thread[{usezv,dzv}]];
     
     While[iflimit  && my=>1, 
      iz=0;
      dzv0=dzv;
      usezv0=usezv;
      Scan[(iz=iz+1;
       If[#[2]>maxdzv[#[1]],
        datazv=Drop[datazv,{iz,iz}];
        usezv=Drop[usezv,{iz,iz}]; 
        iz=iz-1;
        dzvfix=Append[dzvfix,{#[1],maxdzv[#[1]]}];
        LINE["K0",#[1]]=maxdzv[#[1]]];
       If[#[2]<mindzv[#[1]], 
        datazv=Drop[datazv,{iz,iz}];
        usezv=Drop[usezv,{iz,iz}]; 
        iz=iz-1;
        dzvfix=Append[dzvfix,{#[1],mindzv[#[1]]}];
        LINE["K0",#[1]]=mindzv[#[1]]];
      )&,Thread[{usezv0,dzv0}]];
      If[Length[usezv]=>1,
       etaylist0=Map[Twiss["EY",#[1]]&,bpmfitlist1];
       codylist0=Map[Twiss["EY",#[1]]&,bpmfitlist2];
       FFS["CAL"];
       etaylist1=Map[Twiss["EY",#[1]]&,bpmfitlist1];
       codylist1=Map[Twiss["EY",#[1]]&,bpmfitlist2];
       bpmfitlist1=Map[{#[1,1],#[1,2],#[1,3]-#[2],#[1,4],#[1,5]}&,
         Thread[{bpmfitlist1,etaylist1-etaylist0}]];
       bpmfitlist2=Map[{#[1,1],#[1,2],#[1,3]-#[2],#[1,4],#[1,5]}&,
         Thread[{bpmfitlist2,codylist1-codylist0}]];

       A=Join[Transpose[datazv],DiagonalMatrix[Table[soft,{Length[usezv]}]]];
       bpmy=Join[Map[(#[3])&,bpmfitlist1]*factor1,Map[(#[3])&,bpmfitlist2],Table[0,{Length[usezv]}]];
       dzv=LinearSolve[A,bpmy];
      ,
       dzv={};
      ];
      my=my0-Length[dzvfix];
      iflimit=0;
      Scan[If[#[2]>maxdzv[#[1]] || #[2]<mindzv[#[1]], 
      iflimit=1]&,Thread[{usezv,dzv}]];
     ];

     Print[my," ",Length[dzv]," ",ratio];
     Scan[(LINE["K0",#[1]]=#[2])&,dzvfix];
     Scan[(LINE["K0",#[1]]=#[2])&,Thread[{usezv,dzv}]];
   ];

    If[my<=0 && mx>=1, 
     k0listx=Select[k0list,(StringMatchQ[#[1],"ZH*R"])&];

     k0listx=Select[k0listx,Not[MemberQ[fixsteers,#[1]]]&];

     Scan[(maxdzh[#[1]]=#[2]+maxk0)&,k0listx];
     Scan[(mindzh[#[1]]=#[2]-maxk0)&,k0listx];
         
     usezh=Select[Map[#[1]&,k0listx],(maxdzh[#]>0 && mindzh[#]<0)&];

     dk0=1e-4;
     datazheta=Map[(steer=#;
      FFS["CAL"]; 
      dispx0=Map[(Twiss["EX",#])&,bpmnamelist1]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      dispx1=Map[(Twiss["EX",#])&,bpmnamelist1];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (dispx1-dispx0)/dk0)&,usezh];

     datazhcod=Map[(steer=#;
      FFS["CAL"]; 
      codx0=Map[(Twiss["DX",#])&,bpmnamelist2]; 
      LINE["K0",steer]=LINE["K0",steer]+dk0;
      FFS["CAL"]; 
      codx1=Map[(Twiss["DX",#])&,bpmnamelist2];
      LINE["K0",steer]=LINE["K0",steer]-dk0;
      (codx1-codx0)/dk0)&,usezh];

     datazh=Transpose[Join[Transpose[datazheta]*factor1,Transpose[datazhcod]]];

     A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
     bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist1]*factor1,Map[(#[2]-#[4])&,bpmfitlist2],Table[0,{Length[usezh]}]];
     dzh=LinearSolve[A,bpmx];


     If[Length[usezh]>mx,
      While[Length[usezh]>mx*ratio && mx=>1, 
       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezh,datazh,dzh,Abs[dzh]}]];
       datalist=SelectMax[datalist,Length[usezh]/ratio];
       usezh=Transpose[Transpose[datalist][1]][1];
       datazh=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
       bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist1]*factor1,Map[(#[2]-#[4])&,bpmfitlist2],Table[0,{Length[usezh]}]];
       dzh=LinearSolve[A,bpmx];
      ];

       datalist=Map[{{#[1],#[2],#[3]},#[4]}&,Thread[{usezh,datazh,dzh,Abs[dzh]}]];
       datalist=SelectMax[datalist,mx];
       usezh=Transpose[Transpose[datalist][1]][1];
       datazh=Transpose[Transpose[datalist][1]][2];
       A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
       bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist1]*factor1,Map[(#[2]-#[4])&,bpmfitlist2],Table[0,{Length[usezh]}]];
       dzh=LinearSolve[A,bpmx];

     ];

!     Print["bpmx ",bpmx];
!     Print["usezh ",usezh];
!     Print["dzh ",dzh];

     bpmfitlist10=bpmfitlist1;
     bpmfitlist20=bpmfitlist2;
     mx0=mx;
     dzhfix={};

     iflimit=0;
     Scan[If[#[2]>maxdzh[#[1]] || #[2]<mindzh[#[1]], 
     iflimit=1]&,Thread[{usezh,dzh}]];
     
     While[iflimit  && mx=>1, 
      iz=0;
!Print["aaa",Length[usezh]];
      dzh0=dzh;
      usezh0=usezh;
      Scan[(iz=iz+1;
       If[#[2]>maxdzh[#[1]],
        datazh=Drop[datazh,{iz,iz}];
        usezh=Drop[usezh,{iz,iz}]; 
        iz=iz-1;
        dzhfix=Append[dzhfix,{#[1],maxdzh[#[1]]}];
        LINE["K0",#[1]]=maxdzh[#[1]]];
       If[#[2]<mindzh[#[1]], 
        datazh=Drop[datazh,{iz,iz}];
        usezh=Drop[usezh,{iz,iz}]; 
        iz=iz-1;
        dzhfix=Append[dzhfix,{#[1],mindzh[#[1]]}];
        LINE["K0",#[1]]=mindzh[#[1]]];
      )&,Thread[{usezh0,dzh0}]];
      If[Length[usezh]=>1,
       etaxlist0=Map[Twiss["EX",#[1]]&,bpmfitlist1];
       codxlist0=Map[Twiss["DX",#[1]]&,bpmfitlist2];
       Print["calc"];FFS["CAL"];
       etaxlist1=Map[Twiss["EX",#[1]]&,bpmfitlist1];
       codxlist1=Map[Twiss["DX",#[1]]&,bpmfitlist2];
       bpmfitlist1=Map[{#[1,1],#[1,2]-#[2],#[1,3],#[1,4],#[1,5]}&,
         Thread[{bpmfitlist1,etaxlist1-etaxlist0}]];
       bpmfitlist2=Map[{#[1,1],#[1,2]-#[2],#[1,3],#[1,4],#[1,5]}&,
         Thread[{bpmfitlist2,codxlist1-codxlist0}]];

       A=Join[Transpose[datazh],DiagonalMatrix[Table[soft,{Length[usezh]}]]];
       bpmx=Join[Map[(#[2]-#[4])&,bpmfitlist1]*factor1,Map[(#[2]-#[4])&,bpmfitlist2],Table[0,{Length[usezh]}]];
       dzh=LinearSolve[A,bpmx];
      ,
       dzh={};
      ];
      mx=mx0-Length[dzhfix];
      iflimit=0;
      Scan[If[#[2]>maxdzh[#[1]] || #[2]<mindzh[#[1]], 
      iflimit=1]&,Thread[{usezh,dzh}]];
     ];

     Print[mx," ",Length[dzh]," ",ratio];
     Scan[(LINE["K0",#[1]]=#[2])&,dzhfix];
     Scan[(LINE["K0",#[1]]=#[2])&,Thread[{usezh,dzh}]];
   ];


    FFS["CAL"];
    DrawDispersion[bpmlist1]; 
!    Scan[Check[Print[#[[1]]," ",LINE["K0",#[1]]],]&,k0list];
    orbitdata=ATFRingOrbit;
    dispdata=ATFRingDispersion;
    injectchange=Twiss[{"DX","DPX","DY","DPY"},"IIN"];  
    Scan[Check[LINE["K0",#[1]]=-reductionfactor*LINE["K0",#[1]]+#[2],]&,k0list];
  ];

