!!!!
!!!! library for extraction line dispersion correction using quads/skew quads
!!!! original was by T.Okugi
!!!!
!!!!   ShowFitResultsG (modified version of ShowFitResults) was attded by K.Kubo
!!!!
 dr:=FFS["out 'a'  draw ML8X $$$ bx by & ex & ey q* term out tdr 'a'"];
 dr2:=FFS["out 'a' draw QD1X $$$ bx by & ex & ey mw* term out tdr 'a'"];

 FitA:=Table[If[DispDataX[2,II,3]==0.0,
     (Twiss["PEX",DispDataX[2,II,1]]*1000.+DispDataX[2,II,2])/0.01,
     (Twiss["PEX",DispDataX[2,II,1]]*1000.+DispDataX[2,II,2])/
    DispDataX[2,II,3]],{II,DispDataX[1]}];
 FitB:=Table[If[DispDataY[2,II,3]==0.0,
     (Twiss["PEY",DispDataY[2,II,1]]*1000.+DispDataY[2,II,2])/0.01,
     (Twiss["PEY",DispDataY[2,II,1]]*1000.+DispDataY[2,II,2])/
    DispDataY[2,II,3]],{II,DispDataY[1]}];

 Chi2:=Module[{SumChi2,DumChi2,II},
	SumChi2=0.0;Do[DumChi2=SumChi2;
	SumChi2=DumChi2+FitFunction[II]^2,{II,Length[FitFunction]}];
	SumChi2];

 ReducedChi2:=(Chi2/Nfree);

 ShowFitResults:=Module[
     {Pos1,Pos2,Xmin,Xmax,TmpEtaX,TmpEtaY,TmpXMax,TmpXMin,TmpYMax,TmpYMin,
	MaxEtaX,MinEtaX,MaxEtaY,MinEtaY,MonEtaX,MonEtaY,MonPos,
	YHmax,YHmax,YHmin,YHmin,YVmax,YVmax,YVmin,YVmin,Loop,
	MW0Xxave,MW1Xxave,MW2Xxave,MW3Xxave,MW4Xxave,
	MW0Xxerr,MW1Xxerr,MW2Xxerr,MW3Xxerr,MW4Xxerr,
	MW0Xyave,MW1Xyave,MW2Xyave,MW3Xyave,MW4Xyave,
	MW0Xyerr,MW1Xyerr,MW2Xyerr,MW3Xyerr,MW4Xyerr,
	EXXave,EXXerr,EXX1,EXX2,EYYave,EYYerr,EYY1,EYY2,
	EPXave,EPXerr,EPX1,EPX2,EPYave,EPYerr,EPY1,EPY2,
	Etemp,Estep,Eostep,Chi2orig},
     FFS["cal"];
     TopFile=OpenWrite["./output/FitResult.top"];
     Pos1=LINE["Position","QF5X"];
     Pos2=LINE["Position","BH5X.1"];
     Xmin = LINE["s",Pos1];
     Xmax = LINE["s",Pos2];

    If[(DispDataX[1]>2),
	Chi2orig=Chi2;
	EXXave=Twiss["PEX","IEX"];
	EPXave=Twiss["PEPX","IEX"];
	Eostep=0.1;$FORM="F15.6";

	EXX1=EXXave;
	FFS["IEX EX "//EXX1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EXX1;EXX1=Etemp+Eostep;
	   FFS["IEX EX "//EXX1];FFS["CAL"];];
	EXX1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EXX1;EXX1=Etemp+Estep;
	   FFS["IEX EX "//EXX1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EXX2=EXXave;
	FFS["IEX EX "//EXX2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EXX2;EXX2=Etemp-Eostep;
	   FFS["IEX EX "//EXX2];FFS["CAL"];];
	EXX2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EXX2;EXX2=Etemp-Estep;
	   FFS["IEX EX "//EXX2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EX "//EXXave];FFS["CAL"];
	EXXerr=(EXX1-EXX2)/2.0;

	EPX1=EPXave;
	FFS["IEX EPX "//EPX1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPX1;EPX1=Etemp+Eostep;
	   FFS["IEX EPX "//EPX1];FFS["CAL"];];
	EPX1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPX1;EPX1=Etemp+Estep;
	   FFS["IEX EPX "//EPX1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EPX2=EPXave;
	FFS["IEX EPX "//EPX2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPX2;EPX2=Etemp-Eostep;
	   FFS["IEX EPX "//EPX2];FFS["CAL"];];
	EPX2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPX2;EPX2=Etemp-Estep;
	   FFS["IEX EPX "//EPX2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EPX "//EPXave];FFS["CAL"];
	EPXerr=(EPX1-EPX2)/2.0;

        MW0Xxave=-Twiss["PEX","MW0X"]*1000.;
        MW1Xxave=-Twiss["PEX","MW1X"]*1000.;
        MW2Xxave=-Twiss["PEX","MW2X"]*1000.;
        MW3Xxave=-Twiss["PEX","MW3X"]*1000.;
        MW4Xxave=-Twiss["PEX","MW4X"]*1000.;
	MW0Xxerr=Sqrt[(TransferMatrix["IEX","MW0X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW0X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW1Xxerr=Sqrt[(TransferMatrix["IEX","MW1X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW1X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW2Xxerr=Sqrt[(TransferMatrix["IEX","MW2X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW2X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW3Xxerr=Sqrt[(TransferMatrix["IEX","MW3X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW3X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW4Xxerr=Sqrt[(TransferMatrix["IEX","MW4X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW4X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];];


    If[(DispDataY[1]>2),
	Chi2orig=Chi2;
	EYYave=Twiss["PEY","IEX"];
	EPYave=Twiss["PEPY","IEX"];
	Eostep=0.1;$FORM="F15.6";

	EYY1=EYYave;
	FFS["IEX EY "//EYY1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EYY1;EYY1=Etemp+Eostep;
	   FFS["IEX EY "//EYY1];FFS["CAL"];];
	EYY1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EYY1;EYY1=Etemp+Estep;
	   FFS["IEX EY "//EYY1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EYY2=EYYave;
	FFS["IEX EY "//EYY2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EYY2;EYY2=Etemp-Eostep;
	   FFS["IEX EY "//EYY2];FFS["CAL"];];
	EYY2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EYY2;EYY2=Etemp-Estep;
	   FFS["IEX EY "//EYY2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EY "//EYYave];FFS["CAL"];
	EYYerr=(EYY1-EYY2)/2.0;

	EPY1=EPYave;
	FFS["IEX EPY "//EPY1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPY1;EPY1=Etemp+Eostep;
	   FFS["IEX EPY "//EPY1];FFS["CAL"];];
	EPY1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPY1;EPY1=Etemp+Estep;
	   FFS["IEX EPY "//EPY1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EPY2=EPYave;
	FFS["IEX EPY "//EPY2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPY2;EPY2=Etemp-Eostep;
	   FFS["IEX EPY "//EPY2];FFS["CAL"];];
	EPY2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPY2;EPY2=Etemp-Estep;
	   FFS["IEX EPY "//EPY2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EPY "//EPYave];FFS["CAL"];
	EPYerr=(EPY1-EPY2)/2.0;

        MW0Xyave=-Twiss["PEY","MW0X"]*1000.;
        MW1Xyave=-Twiss["PEY","MW1X"]*1000.;
        MW2Xyave=-Twiss["PEY","MW2X"]*1000.;
        MW3Xyave=-Twiss["PEY","MW3X"]*1000.;
        MW4Xyave=-Twiss["PEY","MW4X"]*1000.;
	MW0Xyerr=Sqrt[(TransferMatrix["IEX","MW0X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW0X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW1Xyerr=Sqrt[(TransferMatrix["IEX","MW1X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW1X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW2Xyerr=Sqrt[(TransferMatrix["IEX","MW2X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW2X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW3Xyerr=Sqrt[(TransferMatrix["IEX","MW3X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW3X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW4Xyerr=Sqrt[(TransferMatrix["IEX","MW4X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW4X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];];

     MaxEtaX = -Twiss["PEX",Pos1]*1000.;
     MinEtaX = -Twiss["PEX",Pos1]*1000.;
     MaxEtaY = -Twiss["PEY",Pos1]*1000.;
     MinEtaY = -Twiss["PEY",Pos1]*1000.;
     Do[TmpEtaX = -Twiss["PEX",Loop]*1000.;
        TmpEtaY = -Twiss["PEY",Loop]*1000.;
	TmpXMax = Max[MaxEtaX,TmpEtaX];
	TmpXMin = Min[MinEtaX,TmpEtaX];
	TmpYMax = Max[MaxEtaY,TmpEtaY];
	TmpYMin = Min[MinEtaY,TmpEtaY];
	MaxEtaX = TmpXMax;
	MinEtaX = TmpXMin;
	MaxEtaY = TmpYMax;
	MinEtaY = TmpYMin,{Loop,Pos1,Pos2}];
     Do[TmpXMax = Max[MaxEtaX,DispDataX[2,Loop,2]+DispDataX[2,Loop,3]];
	TmpXMin = Min[MinEtaX,DispDataX[2,Loop,2]-DispDataX[2,Loop,3]];
	MaxEtaX = TmpXMax;
	MinEtaX = TmpXMin,{Loop,DispDataX[1]}];
     Do[TmpYMax = Max[MaxEtaY,DispDataY[2,Loop,2]+DispDataY[2,Loop,3]];
	TmpYMin = Min[MinEtaY,DispDataY[2,Loop,2]-DispDataY[2,Loop,3]];
	MaxEtaY = TmpYMax;
	MinEtaY = TmpYMin,{Loop,DispDataY[1]}];
     If[MaxEtaX>0,YHmax=1.2*MaxEtaX,YHmax=0.8*MaxEtaX];
     If[MinEtaX<0,YHmin=1.2*MinEtaX,YHmin=0.8*MinEtaX];
     If[MaxEtaY>0,YVmax=1.2*MaxEtaY,YVmax=0.8*MaxEtaY];
     If[MinEtaY<0,YVmin=1.2*MinEtaY,YVmin=0.8*MinEtaY];
     If[DispDataX[1]==0,YHmax=+100;YHmin=-100];
     If[DispDataY[1]==0,YVmax=+100;YVmin=-100];
     $FORM="F15.8";
     Write[TopFile," SET WINDOW X FROM 5.50 TO 12.70"];
     Write[TopFile," SET WINDOW Y FROM 5.80 TO  9.30"];
     Write[TopFile," SET LIMITS X FROM ", Xmin," TO", Xmax];
     Write[TopFile," SET LIMITS Y FROM ",YHmin," TO",YHmax];
     Write[TopFile," SET LABELS SIZE  1.30"];
     Write[TopFile," SET LABELS ALL    OFF"];
     Write[TopFile," SET LABELS BOTTOM ON "];
     Write[TopFile," SET LABELS LEFT   ON "];
     Write[TopFile," SET TICKS  SIZE  0.05"];
     Write[TopFile," SET TICKS  ALL    OFF"];
     Write[TopFile," SET TICKS  BOTTOM ON "];
     Write[TopFile," SET TICKS  LEFT   ON "];
     Write[TopFile," SET TITLE  SIZE  2.00"];
     Write[TopFile," TITLE 7.3 9.6 'Horizontal Dispersion'"];
     Write[TopFile," SET TITLE  SIZE  1.70"];
     Write[TopFile," TITLE X LINES=0.7 SPACES 8 's [m]'"];
     Write[TopFile," TITLE Y LINES=0.7 SPACES 8 'eta_x [mm]'"];
     If[(DispDataX[1]>2),
	$FORM="F10.3";
	Write[TopFile," SET TITLE  SIZE  1.50"];
	Write[TopFile," TITLE 1.8 9.0 'Fitted X Dispersion'"];
	Write[TopFile," TITLE 0.5 8.5 'MW0X :"//
			MW0Xxave//" +/-"//MW0Xxerr//" mm'"];
	Write[TopFile," TITLE 0.5 8.0 'MW1X :"//
			MW1Xxave//" +/-"//MW1Xxerr//" mm'"];
	Write[TopFile," TITLE 0.5 7.5 'MW2X :"//
			MW2Xxave//" +/-"//MW2Xxerr//" mm'"];
	Write[TopFile," TITLE 0.5 7.0 'MW3X :"//
			MW3Xxave//" +/-"//MW3Xxerr//" mm'"];
	Write[TopFile," TITLE 0.5 6.5 'MW4X :"//
			MW4Xxave//" +/-"//MW4Xxerr//" mm'"];];
     $FORM="F15.8";
     If[DispDataX[1]==0,
	Write[TopFile,LINE["s",Pos1],0.0];
	Write[TopFile,LINE["s",Pos2],0.0];
	Write[TopFile," JOIN 1"],
	Do[Xval=LINE["s",Loop];Yval=-Twiss["PEX",Loop]*1000.; 
	   Write[TopFile,Xval,Yval],{Loop,Pos1,Pos2}];
	Write[TopFile," JOIN 1"];
	Write[TopFile," SET SYMBOL 2O"];
	Write[TopFile," SET ORDER  X Y DY"];
	Scan[Write[TopFile,LINE["S",#[1]],#[2],#[3]]&,DispDataX[2]];
	Write[TopFile," PLOT"]];
     $FORM="F15.8";
     Write[TopFile," SET WINDOW X FROM  5.50  TO  12.70"];
     Write[TopFile," SET WINDOW Y FROM  0.80  TO   4.30"];
     Write[TopFile," SET LIMITS X FROM ", Xmin," TO", Xmax];
     Write[TopFile," SET LIMITS Y FROM ",YVmin," TO",YVmax];
     Write[TopFile," SET LABELS SIZE  1.30"];
     Write[TopFile," SET LABELS ALL    OFF"];
     Write[TopFile," SET LABELS BOTTOM ON "];
     Write[TopFile," SET LABELS LEFT   ON "];
     Write[TopFile," SET TICKS  SIZE  0.05"];
     Write[TopFile," SET TICKS  ALL    OFF"];
     Write[TopFile," SET TICKS  BOTTOM ON "];
     Write[TopFile," SET TICKS  LEFT   ON "];
     Write[TopFile," SET TITLE  SIZE  2.00"];
     Write[TopFile," TITLE 7.5 4.6 'Vertical Dispersion'"];
     Write[TopFile," SET TITLE  SIZE  1.70"];
     Write[TopFile," TITLE X LINES=0.7 SPACES 8 's [m]'"];
     Write[TopFile," TITLE Y LINES=0.7 SPACES 8 'eta_y [mm]'"];
     If[(DispDataY[1]>2),
	$FORM="F10.3";
	Write[TopFile," SET TITLE  SIZE  1.50"];
	Write[TopFile," TITLE 1.8 4.0 'Fitted Y Dispersion'"];
	Write[TopFile," TITLE 0.5 3.5 'MW0X :"//
			MW0Xyave//" +/-"//MW0Xyerr//" mm'"];
	Write[TopFile," TITLE 0.5 3.0 'MW1X :"//
			MW1Xyave//" +/-"//MW1Xyerr//" mm'"];
	Write[TopFile," TITLE 0.5 2.5 'MW2X :"//
			MW2Xyave//" +/-"//MW2Xyerr//" mm'"];
	Write[TopFile," TITLE 0.5 2.0 'MW3X :"//
			MW3Xyave//" +/-"//MW3Xyerr//" mm'"];
	Write[TopFile," TITLE 0.5 1.5 'MW4X :"//
			MW4Xyave//" +/-"//MW4Xyerr//" mm'"];];
     $FORM="F15.8";
     If[DispDataY[1]==0,
	Write[TopFile,LINE["s",Pos1],0.0];
	Write[TopFile,LINE["s",Pos2],0.0];
	Write[TopFile," JOIN 1"],
	Do[Xval=LINE["s",Loop];Yval=-Twiss["PEY",Loop]*1000.; 
	   Write[TopFile,Xval,Yval],{Loop,Pos1,Pos2}];
	Write[TopFile," JOIN 1"];
	Write[TopFile," SET SYMBOL 2O"];
	Write[TopFile," SET ORDER  X Y DY"];
	Scan[Write[TopFile,LINE["S",#[1]],#[2],#[3]]&,DispDataY[2]];
	Write[TopFile," PLOT"]];
     Close[TopFile];System["tdr -v X FitResult.top"];];


 ShowFitResultsG:=Module[
     {Pos1,Pos2,Xmin,Xmax,TmpEtaX,TmpEtaY,TmpXMax,TmpXMin,TmpYMax,TmpYMin,
	MaxEtaX,MinEtaX,MaxEtaY,MinEtaY,MonEtaX,MonEtaY,MonPos,
	YHmax,YHmax,YHmin,YHmin,YVmax,YVmax,YVmin,YVmin,Loop,
	MW0Xxave,MW1Xxave,MW2Xxave,MW3Xxave,MW4Xxave,
	MW0Xxerr,MW1Xxerr,MW2Xxerr,MW3Xxerr,MW4Xxerr,
	MW0Xyave,MW1Xyave,MW2Xyave,MW3Xyave,MW4Xyave,
	MW0Xyerr,MW1Xyerr,MW2Xyerr,MW3Xyerr,MW4Xyerr,
	EXXave,EXXerr,EXX1,EXX2,EYYave,EYYerr,EYY1,EYY2,
	EPXave,EPXerr,EPX1,EPX2,EPYave,EPYerr,EPY1,EPY2,
	Etemp,Estep,Eostep,Chi2orig},
     FFS["cal"];

     file="./output/plotdata_extdispersion";

  Print[file];

     Pos1=LINE["Position","QF5X"];
     Pos2=LINE["Position","BH5X.1"];
     Xmin = LINE["s",Pos1];
     Xmax = LINE["s",Pos2];

    If[(DispDataX[1]>2),
	Chi2orig=Chi2;
	EXXave=Twiss["PEX","IEX"];
	EPXave=Twiss["PEPX","IEX"];
	Eostep=0.1;$FORM="F15.6";

	EXX1=EXXave;
	FFS["IEX EX "//EXX1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EXX1;EXX1=Etemp+Eostep;
	   FFS["IEX EX "//EXX1];FFS["CAL"];];
	EXX1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EXX1;EXX1=Etemp+Estep;
	   FFS["IEX EX "//EXX1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EXX2=EXXave;
	FFS["IEX EX "//EXX2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EXX2;EXX2=Etemp-Eostep;
	   FFS["IEX EX "//EXX2];FFS["CAL"];];
	EXX2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EXX2;EXX2=Etemp-Estep;
	   FFS["IEX EX "//EXX2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EX "//EXXave];FFS["CAL"];
	EXXerr=(EXX1-EXX2)/2.0;

	EPX1=EPXave;
	FFS["IEX EPX "//EPX1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPX1;EPX1=Etemp+Eostep;
	   FFS["IEX EPX "//EPX1];FFS["CAL"];];
	EPX1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPX1;EPX1=Etemp+Estep;
	   FFS["IEX EPX "//EPX1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EPX2=EPXave;
	FFS["IEX EPX "//EPX2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPX2;EPX2=Etemp-Eostep;
	   FFS["IEX EPX "//EPX2];FFS["CAL"];];
	EPX2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPX2;EPX2=Etemp-Estep;
	   FFS["IEX EPX "//EPX2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EPX "//EPXave];FFS["CAL"];
	EPXerr=(EPX1-EPX2)/2.0;

        MW0Xxave=-Twiss["PEX","MW0X"]*1000.;
        MW1Xxave=-Twiss["PEX","MW1X"]*1000.;
        MW2Xxave=-Twiss["PEX","MW2X"]*1000.;
        MW3Xxave=-Twiss["PEX","MW3X"]*1000.;
        MW4Xxave=-Twiss["PEX","MW4X"]*1000.;
	MW0Xxerr=Sqrt[(TransferMatrix["IEX","MW0X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW0X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW1Xxerr=Sqrt[(TransferMatrix["IEX","MW1X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW1X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW2Xxerr=Sqrt[(TransferMatrix["IEX","MW2X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW2X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW3Xxerr=Sqrt[(TransferMatrix["IEX","MW3X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW3X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];
	MW4Xxerr=Sqrt[(TransferMatrix["IEX","MW4X"][1,1]*EXXerr)^2
		     +(TransferMatrix["IEX","MW4X"][1,2]*EPXerr)^2]
		*1000.*Sqrt[Chi2orig/Nfree];];


    If[(DispDataY[1]>2),
	Chi2orig=Chi2;
	EYYave=Twiss["PEY","IEX"];
	EPYave=Twiss["PEPY","IEX"];
	Eostep=0.1;$FORM="F15.6";

	EYY1=EYYave;
	FFS["IEX EY "//EYY1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EYY1;EYY1=Etemp+Eostep;
	   FFS["IEX EY "//EYY1];FFS["CAL"];];
	EYY1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EYY1;EYY1=Etemp+Estep;
	   FFS["IEX EY "//EYY1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EYY2=EYYave;
	FFS["IEX EY "//EYY2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EYY2;EYY2=Etemp-Eostep;
	   FFS["IEX EY "//EYY2];FFS["CAL"];];
	EYY2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EYY2;EYY2=Etemp-Estep;
	   FFS["IEX EY "//EYY2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EY "//EYYave];FFS["CAL"];
	EYYerr=(EYY1-EYY2)/2.0;

	EPY1=EPYave;
	FFS["IEX EPY "//EPY1];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPY1;EPY1=Etemp+Eostep;
	   FFS["IEX EPY "//EPY1];FFS["CAL"];];
	EPY1=Etemp+Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPY1;EPY1=Etemp+Estep;
	   FFS["IEX EPY "//EPY1];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	EPY2=EPYave;
	FFS["IEX EPY "//EPY2];FFS["CAL"];
	While[(Chi2-Chi2orig)<1.0,
	   Etemp=EPY2;EPY2=Etemp-Eostep;
	   FFS["IEX EPY "//EPY2];FFS["CAL"];];
	EPY2=Etemp-Eostep;
	Estep=-0.5*Eostep;
	Do[Etemp=EPY2;EPY2=Etemp-Estep;
	   FFS["IEX EPY "//EPY2];FFS["CAL"];
	   If[(Chi2-Chi2orig)>1.0,
	      Estep=-0.5^(Loop+1)*Eostep,
	      Estep=+0.5^(Loop+1)*Eostep],{Loop,14}];
	FFS["IEX EPY "//EPYave];FFS["CAL"];
	EPYerr=(EPY1-EPY2)/2.0;

        MW0Xyave=-Twiss["PEY","MW0X"]*1000.;
        MW1Xyave=-Twiss["PEY","MW1X"]*1000.;
        MW2Xyave=-Twiss["PEY","MW2X"]*1000.;
        MW3Xyave=-Twiss["PEY","MW3X"]*1000.;
        MW4Xyave=-Twiss["PEY","MW4X"]*1000.;
	MW0Xyerr=Sqrt[(TransferMatrix["IEX","MW0X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW0X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW1Xyerr=Sqrt[(TransferMatrix["IEX","MW1X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW1X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW2Xyerr=Sqrt[(TransferMatrix["IEX","MW2X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW2X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW3Xyerr=Sqrt[(TransferMatrix["IEX","MW3X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW3X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];
	MW4Xyerr=Sqrt[(TransferMatrix["IEX","MW4X"][3,3]*EYYerr)^2
		     +(TransferMatrix["IEX","MW4X"][3,4]*EPYerr)^2]
		 *1000.*Sqrt[Chi2orig/Nfree];];

     MaxEtaX = -Twiss["PEX",Pos1]*1000.;
     MinEtaX = -Twiss["PEX",Pos1]*1000.;
     MaxEtaY = -Twiss["PEY",Pos1]*1000.;
     MinEtaY = -Twiss["PEY",Pos1]*1000.;
     Do[TmpEtaX = -Twiss["PEX",Loop]*1000.;
        TmpEtaY = -Twiss["PEY",Loop]*1000.;
	TmpXMax = Max[MaxEtaX,TmpEtaX];
	TmpXMin = Min[MinEtaX,TmpEtaX];
	TmpYMax = Max[MaxEtaY,TmpEtaY];
	TmpYMin = Min[MinEtaY,TmpEtaY];
	MaxEtaX = TmpXMax;
	MinEtaX = TmpXMin;
	MaxEtaY = TmpYMax;
	MinEtaY = TmpYMin,{Loop,Pos1,Pos2}];
     Do[TmpXMax = Max[MaxEtaX,DispDataX[2,Loop,2]+DispDataX[2,Loop,3]];
	TmpXMin = Min[MinEtaX,DispDataX[2,Loop,2]-DispDataX[2,Loop,3]];
	MaxEtaX = TmpXMax;
	MinEtaX = TmpXMin,{Loop,DispDataX[1]}];
     Do[TmpYMax = Max[MaxEtaY,DispDataY[2,Loop,2]+DispDataY[2,Loop,3]];
	TmpYMin = Min[MinEtaY,DispDataY[2,Loop,2]-DispDataY[2,Loop,3]];
	MaxEtaY = TmpYMax;
	MinEtaY = TmpYMin,{Loop,DispDataY[1]}];
     If[MaxEtaX>0,YHmax=1.2*MaxEtaX,YHmax=0.8*MaxEtaX];
     If[MinEtaX<0,YHmin=1.2*MinEtaX,YHmin=0.8*MinEtaX];
     If[MaxEtaY>0,YVmax=1.2*MaxEtaY,YVmax=0.8*MaxEtaY];
     If[MinEtaY<0,YVmin=1.2*MinEtaY,YVmin=0.8*MinEtaY];
     If[DispDataX[1]==0,YHmax=+100;YHmin=-100];
     If[DispDataY[1]==0,YVmax=+100;YVmin=-100];

     $FORM="7.3";
     frame1={{0.423,0.977},{.58,.93}};
     range1={{Xmin,Xmax},{YHmin,YHmax}};
     text1={{"Horizontal Dispersion",0.55,0.96,0},{"s (m)",0.4,0.53,0},{"eta_x (mm)",0.36,0.71,90}};

     If[(DispDataX[1]>2),
	$FORM="6.2";
        text1=Join[text1,{
               {"Fitted X Dispersion",0.1,0.9,0},
               {"MW0X:"//MW0Xxave//"+/-"//MW0Xxerr//" mm",0.0385,0.85,0},
               {"MW1X:"//MW1Xxave//"+/-"//MW1Xxerr//" mm",0.0385,0.80,0},
               {"MW2X:"//MW2Xxave//"+/-"//MW2Xxerr//" mm",0.0385,0.75,0},
               {"MW3X:"//MW3Xxave//"+/-"//MW3Xxerr//" mm",0.0385,0.70,0},
               {"MW4X:"//MW4Xxave//"+/-"//MW4Xxerr//" mm",0.0385,0.65,0}}];
     ];

     $FORM="F15.8";
     If[DispDataX[1]==0,
        data1={{1,0,{LINE["s",Pos1],LINE["s",Pos2]},{0,0}}};
     ,
        data1={{1,0,Map[(LINE["S",#])&,Table[Loop,{Loop,Pos1,Pos2}]],Map[(-Twiss["PEX",#]*1000.)&,Table[Loop,{Loop,Pos1,Pos2}]]},
               {0,2,Map[LINE["S",#[1]]&,DispDataX[2]],Map[#[2]&,DispDataX[2]],Map[#[3]&,DispDataX[2]]}};
     ];

     $FORM="7.3";
     frame2={{0.423,0.977},{.08,.43}};
     range2={{Xmin,Xmax},{YVmin,YVmax}};
     text2={{"Vertical Dispersion",0.55,0.46,0},{"s (m)",0.4,0.03,0},{"eta_y (mm)",0.36,0.21,90}};

     If[(DispDataY[1]>2),
	$FORM="6.2";
        text2=Join[text2,{
               {"Fitted Y Dispersion",0.1,0.40,0},
               {"MW0X:"//MW0Xyave//"+/-"//MW0Xyerr//" mm",0.0385,0.35,0},
               {"MW1X:"//MW1Xyave//"+/-"//MW1Xyerr//" mm",0.0385,0.30,0},
               {"MW2X:"//MW2Xyave//"+/-"//MW2Xyerr//" mm",0.0385,0.25,0},
               {"MW3X:"//MW3Xyave//"+/-"//MW3Xyerr//" mm",0.0385,0.20,0},
               {"MW4X:"//MW4Xyave//"+/-"//MW4Xyerr//" mm",0.0385,0.15,0}}];
     ];

     $FORM="15.8";
     If[DispDataY[1]==0,
        data2={{1,0,{LINE["s",Pos1],LINE["s",Pos2]},{0,0}}};
     ,
        data2={{1,0,Map[(LINE["S",#])&,Table[Loop,{Loop,Pos1,Pos2}]],Map[(-Twiss["PEY",#]*1000.)&,Table[Loop,{Loop,Pos1,Pos2}]]},
               {0,2,Map[LINE["S",#[1]]&,DispDataY[2]],Map[#[2]&,DispDataY[2]],Map[#[3]&,DispDataY[2]]}};
     ];

     DrawGraph[{{data1,range1,frame1,text1},{data2,range2,frame2,text2}},file];
   ];

