! 20010205 change  FitDispersionAtSr. add SR 2nd port 

  GoDispersionBump[n_:1]:=Module[{
    bumplist=ATFBump/.Option/.ATFBump->{},
    k0list=ATFSteer/.ATFData[n]/.ATFSteer->{}},
    k0list=Select[k0list,(StringMatchQ[#[1],"Z*R"])&];
    Print[" bump"];
    name=Name/.bumplist/.Name->"";
    name=TruncateStringBoth[name];
    If[name=="", Print[" ? no bump data ? "];FFS["abort"]];
    Print[name];
    k0name=Map[#[1]&,k0list];
    bump1=LINE["Position",name];
    If[bump1>0, ,  
     Print[" ? no such name ? "];FFS["abort"], 
     Print[" ? no such name ? "];FFS["abort"]]; 
    bump2=LINE["Length"];
    ipos=LINE["Position",name];
    FFS["CAL"];
    etax0=Twiss["EX",ipos];etapx0=Twiss["EPX",ipos];
    etax2=Twiss["EX",bump2];etapx2=Twiss["EPX",bump2];
!    orbitlist=OrbitBump/.bumplist;
    orbitlist=DispersionBump/.bumplist;

    NSteerX=NSteerX/.bumplist;
    NSteerY=NSteerY/.bumplist;

    dxb=orbitlist[1];dpxb=orbitlist[2];
    dyb=orbitlist[3];dpyb=orbitlist[4];
    bumpx={dxb,dpxb,0,0};
    If[dxb=="Null", bumpx={dpxb,0,0}];
    If[dpxb=="Null", bumpx={dxb,0,0}];
    bumpy={dyb,dpyb,0,0};
    If[dyb=="Null", bumpy={dpyb,0,0}];
    If[dpyb=="Null", bumpy={dyb,0,0}];

    Print[name];
    xcorr={};ycorr={};
    nstx=0; ipo=ipos; ipp=0;
    While[nstx<NSteerX/2 && ipp==0,
     ipo=ipo+1;
     sif=StringMatchQ[LINE["NAME",ipo],"ZH*"];
     ssif=MemberQ[k0name,LINE["NAME",ipo]];
     If[sif && ssif, nstx=nstx+1; ipx=ipo;
      xcorr=Append[xcorr,ipo]];
     If[ipo==LINE["Length"], ipp=1];
     Print[ipo," ",ipos];
    ];

    Print[name];
    ipo=ipos; ipp=0;
    While[nstx<NSteerX && ipp==0,
     ipo=ipo-1; 
     sif=StringMatchQ[LINE["NAME",ipo],"ZH*"];
     ssif=MemberQ[k0name,LINE["NAME",ipo]];
    If[sif && ssif, nstx=nstx+1; 
      xcorr=Append[xcorr,ipo]];
     If[ipo==1, ipp=1];
    ];

    ipo=ipx; ipp=0;
    While[nstx<NSteerX && ipp==0,
     ipo=ipo+1;
     sif=StringMatchQ[LINE["NAME",ipo],"ZH*"];
     ssif=MemberQ[k0name,LINE["NAME",ipo]];
     If[sif && ssif, nstx=nstx+1; ipx=ipo;
      xcorr=Append[xcorr,ipo]];
     If[ipo==LINE["Length"], ipp=1];
    ];

    Print[name];
    nsty=0; ipo=ipos; ipp=0;
    While[nsty<NSteerY/2 && ipp==0,
     ipo=ipo+1;
     sif=StringMatchQ[LINE["NAME",ipo],"ZV*"];
     ssif=MemberQ[k0name,LINE["NAME",ipo]];
     If[sif && ssif, nsty=nsty+1; ipy=ipo;
      ycorr=Append[ycorr,ipo]];
     If[ipo==LINE["Length"], ipp=1];
    ];
    ipo=ipos; ipp=0;
    While[nsty<NSteerY && ipp==0,
     ipo=ipo-1; 
     sif=StringMatchQ[LINE["NAME",ipo],"ZV*"];
     ssif=MemberQ[k0name,LINE["NAME",ipo]];
    If[sif && ssif, nsty=nsty+1; 
      ycorr=Append[ycorr,ipo]];
     If[ipo==1, ipp=1];
    ];
    ipo=ipy; ipp=0;
    While[nsty<NSteerY && ipp==0,
     ipo=ipo+1;
     sif=StringMatchQ[LINE["NAME",ipo],"ZV*"];
     ssif=MemberQ[k0name,LINE["NAME",ipo]];
     If[sif && ssif, nsty=nsty+1; ipy=ipo;
      ycorr=Append[ycorr,ipo]];
     If[ipo==LINE["Length"], ipp=1];
    ];
    Print[name];
    bump3=Max[Join[xcorr,ycorr]]+1;
    etax3=Twiss["EX",bump3];etapx3=Twiss["EPX",bump3];

  If[NSteerX==0, ,
    Print[xcorr];
    Print[bumpx];
    Map[FFS[" free "//LINE["NAME",#]]&,xcorr]; 
    FFS[" fit "//LINE["NAME",bump2]//" dx 0 dpx 0"]; 
    FFS[" fit "//LINE["NAME",bump2]//" dy 0 dpy 0"]; 
!    FFS[" fit "//LINE["NAME",bump2]//" ex 0 epx 0"]; 
    FFS[" fit "//LINE["NAME",bump2]//" ex "//etax2//" epx "//etapx2]; 
    FFS[" fit "//LINE["NAME",bump2]//" ey 0 epy 0"]; 

    FFS[" fit "//LINE["NAME",bump3]//" dx 0 dpx 0"]; 
    FFS[" fit "//LINE["NAME",bump3]//" dy 0 dpy 0"]; 
!    FFS[" fit "//LINE["NAME",bump3]//" ex 0 epx 0"]; 
    FFS[" fit "//LINE["NAME",bump3]//" ex "//etax3//" epx "//etapx3]; 
    FFS[" fit "//LINE["NAME",bump3]//" ey 0 epy 0"]; 
    FFS[" fit "//LINE["NAME",bump3+1]//" ey 0 epy 0"]; 
    If[dpxb=="Null", ,
      FFS[" fit "//name//" epx "//dpxb+etapx0],
      FFS[" fit "//name//" epx "//dpxb+etapx0]];
    If[dxb=="Null", ,
      FFS[" fit "//name//" ex "//dxb+etax0],
      FFS[" fit "//name//" ex "//dxb+etax0]];
    FFS[" show",6];
    FFS["var",6];
    If[dxb=="Null" && dpxb=="Null", ,FFS[" go "], FFS[" go "]];
  ];

    Print[name];
  If[NSteerY==0, ,
    Print[ycorr];
    Print[bumpy];
    Map[FFS[" free "//LINE["NAME",#]]&,ycorr]; 
    FFS[" fit "//LINE["NAME",bump2]//" dx 0 dpx 0"]; 
    FFS[" fit "//LINE["NAME",bump2]//" dy 0 dpy 0"]; 
!    FFS[" fit "//LINE["NAME",bump2]//" ex 0 epx 0"]; 
    FFS[" fit "//LINE["NAME",bump2]//" ex "//etax2//" epx "//etapx2]; 
    FFS[" fit "//LINE["NAME",bump2]//" ey 0 epy 0"]; 
    FFS[" fit "//LINE["NAME",bump3]//" dx 0 dpx 0"]; 
    FFS[" fit "//LINE["NAME",bump3]//" dy 0 dpy 0"]; 
!    FFS[" fit "//LINE["NAME",bump3]//" ex 0 epx 0"]; 
    FFS[" fit "//LINE["NAME",bump3]//" ex "//etax3//" epx "//etapx3]; 
    FFS[" fit "//LINE["NAME",bump3]//" ey 0 epy 0"];
    FFS[" fit "//LINE["NAME",bump3+1]//" ey 0 epy 0"]; 

    If[dpyb=="Null", ,
      FFS[" fit "//name//" epy "//dpyb],
      FFS[" fit "//name//" epy "//dpyb]];
    If[dyb=="Null", ,
      FFS[" fit "//name//" ey "//dyb],
      FFS[" fit "//name//" ey "//dyb]];
    FFS[" show",6];
    FFS["var",6];
    If[dyb=="Null" && dpyb=="Null", ,FFS[" go "], FFS[" go "]];
  ];

    Print[name];
     FFS["CALC"];
     dxylist={{name,(dxb+etax0)*1.e0,dyb*1.e0}};
     ADrawRingOrbit[{}];
     DrawDispersion[dxylist];
     orbitdata=ATFRingOrbit;
     dispdata=ATFRingDispersion;
    Print["residual ",MatchingResidual];
!     Print["k0list",k0list];

    Scan[Check[LINE["K0",#[1]]=LINE["K0",#[1]]+#[2],]&,k0list];

 ];


  FitDispersionAtSr:=Module[{
    bpmlist=ATFBPM/.ATFData[1]/.ATFBPM->{}},

    fchange=FrequencyChange/.(EtaCorrect/.Option)/.(FrequencyChange ->5e3);
    momentumcomp=MomentumCompaction/.(EtaCorrect/.Option)/.(MomentumCompaction ->2e-3);
    de=-fchange/714e6/momentumcomp;
!    bpmlist=Select[bpmlist,(#[4]>chop)&];
    bpmlist=Select[bpmlist,MemberQ[bpmaroundsr,#[1]]&];
    eta=Map[{#[[1]],#[[2]]/de,#[[3]]/de,#[[4]]}&,bpmlist];
   If[Length[eta]=>2,
    smax=Max[Map[LINE["S",#[1]]&,eta]];
    smin=Min[Map[LINE["S",#[1]]&,eta]];
!    outsteer=Select[LINE["Name","Z*R"],(LINE["S",#]>smax || LINE["S",#]<smin )&];
    outsteer=Select[LINE["Name","Z*R"],(LINE["S",#]<smin )&];
    Scan[(LINE["K0",#]=0)&,outsteer];
    fitfunctionx:=Map[(Twiss["EX",LINE["Position",#[[1]]]+0.5]-#[[2]])&,eta];
    fitfunctiony:=Map[(Twiss["EY",LINE["Position",#[[1]]]+0.5]-#[[3]])&,eta];
    FitFunction:=Join[fitfunctionx,fitfunctiony];
    VARY K0 Z*;
    Scan[FFS["FREE "//#]&,outsteer];
    FFS["GO"];GO;GO;
    possr=LINE["Position","BH1R.27"]+0.27;
    possr2=LINE["Position","BH1R.27"]+0.9;

    Write[outfile,"etax at SR ",Twiss["EX",possr]," m"];
    Write[outfile,"etay at SR ",Twiss["EX",possr]," m"];
    Write[outfile,"etax at SR2 ",Twiss["EX",possr2]," m"];
    Write[outfile,"etay at SR2 ",Twiss["EX",possr2]," m"];
    Scan[Write[outfile,"eta at "//#[1]//" ",#[2]," ",#[3]]&,eta];

    pmax=Max[Map[LINE["Position",#[1]]&,eta]];
    pmin=Min[Map[LINE["Position",#[1]]&,eta]];
    p1=pmin-2;
    p2=pmax+1;
  
    pplot=Flatten[Table[{i,i+0.1,i+0.2,i+0.3,i+0.4,i+0.5,i+0.6,i+0.7,i+0.8,i+0.9},{i,p1,p2}]];

    x1=Map[LINE["S",#]&,pplot];
    y11=Map[Twiss["EX",#]&,pplot];
    y21=Map[Twiss["EY",#]&,pplot];

    x2=Map[(LINE["S",LINE["Position",#[[1]]]+0.5])&,eta];
    y12=Map[(#[2])&,eta];
    y22=Map[(#[3])&,eta];

    dataplot={{1,0,x1,y11},{0,1,x2,y12},{2,0,x1,y21},{0,2,x2,y22},
     {2,0,{LINE["S",possr],LINE["S",possr]},{0,50}},{2,0,{LINE["S",possr2],LINE["S",possr2]},{0,50}}};

    range={{LINE["S",p1],LINE["S",p2]},{0,0.15}};
    frame={{0.2,0.85},{0.15,0.9}};

    $FORM="F7.3";
    text={{"etax at SR"//Twiss["EX",possr]//"m", 0.23,0.85,0},
          {"etay at SR"//Twiss["EY",possr]//"m", 0.23,0.80,0},
          {"etax at SR2"//Twiss["EX",possr2]//"m", 0.5,0.85,0},
          {"etay at SR2"//Twiss["EY",possr2]//"m", 0.5,0.80,0}};

    $FORM="F9.5";
    f="./output/plotdata_sr_etafit";
    DrawGraph[{{dataplot,range,frame,text}},f];

  ,
    Print["Can not fit. Less than 2 bpm data from "//bpmaroundsr];
  ];
  ];

