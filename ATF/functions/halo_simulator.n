!======================================================
! Functions for tail/halo particle generation
! by Renjun Yang (created on 3 May, 2020)
! ** require BesselI() function, which is wrong for some SAD version (2017-2020)
!
! Requires,
!   MINCOUP, PBUNCH,
!   Get["func.n"];
!   path$lib (or self-define when define Touschek Class[]);
!
! Usage:
! ** Ver. emittance could be controlled with MINCOUP, others couldn't be adjusted afteward
! ** Class@Initialize[] after resetting instance variables
! ** BGS/TP are global variables: --> BGS=BeamGasScattering[] and TP=TouschekPiwinski[] are madantory !!!
!
! add BGS class    2021.01.28
!
! To add,
! 1) LossMonitor --> LOSSMAP (not urgent, for the moment)
!======================================================

! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part1 **
! BGS Scattering
! 
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

BeamGasScattering=Class[{}, {normProb$Elastic, normBin$Elastic, normProb$Inelastic, normBin$Inelastic, nElasticBGSOneTurn$, nInelasticBGSOneTurn$}, {vacuum$=2e-7, e$, ncomp$, cir$, nbins$=10000, nmin$=20, nmax$=10000, dpmin$=3e-3, dpmax$=0.02},
  Constructor[]:= Module[{},
    Print["\n  ...Start BGS Class[] initialization.\n"];
    e$ = Emittance[OneTurnInformation->True, ExpandElementValues->False];
    {ncomp$, cir$} = LINE[{"POSITION", "S"}, "$$$"];
    Initialize[];
    ShowParameters[];    
    Print["  .........BGS Class[] initialization done!\n"];
    ];

  ShowParameters[]:=(Print["\n  Parameters for BGS Class[]:\n",
    "  EMITX = ", EMITX, "\t EMITY = ", EMITY+EMITX*MINCOUP, "\t EMITZ = ", EMITZ, "\n",
    "  SIGZ = ", SIGZ, "\t SIGE = ", SIGE, "\n",
    "  MINCOUP = ", MINCOUP, "\t PBUNCH = ", PBUNCH, "\n",
    "  Elastic BGS setting (", nbins$ ," bins):\n",
    "  Theta_min: ", nmin$, " [thetam]\t Theta_max: ", nmax$, " [thetam]\n",
    "  Number of event per turn: ", nElasticBGSOneTurn$, "\n",
    "  Inelastic BGS setting (", nbins$ ," bins):\n",
    "  Dp_min: ", dpmin$, "\tDp_max: ", dpmax$, "\n",
    "  Number of event per turn: ", nInelasticBGSOneTurn$, "\n\n"]);
    
  (* presure, gas type should be global variable inside Class[] *)
  dsigma$ElasticCO[theta_Real]:= theta/(theta^2+thetam1^2)^2;
  ! kk = Es/E
  dsigma$InelasticCO[kk_Real] := Module[{z01=6,z02=8,Fz1,Fz2,sigmaie1,sigmaie2,sigmaie},
    Fz1 = z01^2*Log[183/z01^(1/3)]+z01*Log[1194/z01^(2/3)]; 
    Fz2 = z02^2*Log[183/z02^(1/3)]+z02*Log[1194/z02^(2/3)];
    sigmaie1 = (4/3/kk-4/3+kk)*Fz1+1/9*(1/kk-1)*(z01^2+z01);
    sigmaie2 = (4/3/kk-4/3+kk)*Fz2+1/9*(1/kk-1)*(z02^2+z02);
    sigmaie = sigmaie1+sigmaie2
    ];

  ! creat bins for BGS perturbation
  ! {areamkBG1, xx1mkBG1} = CreateBin[5, 10000, Type->"Elastic"] or {areamkBG2CO, xx1mkBG2CO} = CreateBin[1e-5, 0.04, Type->"Inelastic"]
  CreateBin[phys_String] := Module[{mode=0, gamma, alpha0 = 7.293525664e-3, zz1 = Sqrt[50], thetma1, hlim, llim, xx, xx1, xx2, xbin, areaSwap, area0, i, j},
    mode = Which[phys=="Elastic", 1, phys=="Inelastic", 2];
    If[~(mode), Print["  ??? Enter correct scattering type: Elastic or Inelastic.\n"]];
  
    gamma = MOMENTUM/0.511e6; 
    thetam1=zz1^(1./3.)*alpha0/gamma;
  
    {llim, hlim} = If[mode<2, {nmin$, nmax$}*thetam1, {dpmin$, dpmax$}];  
    xx = Table[llim+i*(hlim-llim)/nbins$,{i,1,nbins$-1}];
    xx1 = Sort[Append[xx,llim]]; 
    xx2 = Sort[Append[xx,hlim]]; 
    xbin = xx2-xx1;            
    areaSwap = If[mode<2, Table[dsigma$ElasticCO[xx1[i]]*xbin[i],{i,1, nbins$}], Table[dsigma$InelasticCO[xx1[i]]*xbin[i],{i,1,nbins$}]];  
    area0 = Table[Apply[Plus,Table[areaSwap[i],{i,1,j}]],{j,1,nbins$}];
    {area0/area0[nbins$], xx1}
    ];

  ! num of elastics BGS events per turn
  ! nminthetam/nmaxthetam -> min/max num of min. scattering angle
  ! Nb -> number of particles per bunch
  ElasticOneTurn$[___]:=
    Module[{alpha0 = 7.293525664e-3, c0 = 299792458, zz1 = Sqrt[50], kb = 1.38e-23, re = 2.818e-15, T = 300, gamma = MOMENTUM/0.511e6, pho, thetam1, theta0, thetaMax, sigma2},
      pho = vacuum$/kb/T; 
      thetam1=zz1^(1./3.)*alpha0/gamma;
      theta0 = nmin$ * thetam1;
      thetaMax = nmax$ * thetam1;
      sigma2=4*Pi*(zz1^2)*(re^2)/(gamma^2)*(1/(thetam1^2+theta0^2)-1/(thetaMax^2+thetam1^2));
      2*cir$*pho*(PBUNCH)*sigma2
      ];
    
  ! inelastic BGS (Bresstruhlung && extraction) events in 1 turn
  ! kk1/kk2 -> range of momentum deviation
  ! Ee=MOMENTUM, Nb -> num of particles per bunch (PBUNCH)
  InelasticOneTurn$[dpmin_Real, dpmax_Real]:=
    Module[{z01=6, z02=8, alpha0 = 7.293525664e-3, c0 = 299792458, kb = 1.38e-23, re = 2.818e-15, T = 300,  zz1 = Sqrt[50], gamma = MOMENTUM/0.511e6, pho, fz1, fz1, fz2, sigmaie1, sigmaie2, sigmaie},
      pho = vacuum$/kb/T; ! volumn gas density    
      fz1 = z01^2*Log[183/z01^(1/3)]+z01*Log[1194/z01^(2/3)];
      fz2 = z02^2*Log[183/z02^(1/3)]+z02*Log[1194/z02^(2/3)];
      sigmaie1 = 4*alpha0*re^2*(fz1*(4/3*Log[dpmax/dpmin]-5/6)+1/9*(z01^2+z01)*(Log[dpmax/dpmin]-1));
      sigmaie2 = 4*alpha0*re^2*(fz2*(4/3*Log[dpmax/dpmin]-5/6)+1/9*(z02^2+z02)*(Log[dpmax/dpmin]-1));
      sigmaie = sigmaie1+sigmaie2;

      cir$*pho*PBUNCH*sigmaie];
      
  SetAttributes[{ProbHistElastic, ProbHistInelastic, nElasticOneTurn, nInelasticOneTurn}, Constant];
  
  With[{def={OneTurnInformation->False}},    
    Initialize[opt___]:=Module[{op, r={}},
      op = Override[opt, def];
      
      {normProb$Elastic, normBin$Elastic} = CreateBin["Elastic"];
      nElasticBGSOneTurn$ = ElasticOneTurn$[nmin$, nmax$];
      
      {normProb$Inelastic, normBin$Inelastic} = CreateBin["Inelastic"];
      nInelasticBGSOneTurn$ = InelasticOneTurn$[dpmin$, dpmax$];
      
      If[OneTurnInformation/.op, r=Join[r, {ProbHistElastic->{normProb$Elastic, normBin$Elastic}, ProbHistInelastic->{normProb$Inelastic, normBin$Inelastic}, nElasticOneTurn->nElasticBGSOneTurn$, nInelasticOneTurn->nInelasticBGSOneTurn$}]]];  
    ]; 
      
  ! Global varibles: normProb$, normBin$
  ! nmin, dmin should be determined wrt. equilibrium beam parameters
  With[{def={Elastic->False, Inelastic->False}},
    GenKickBGS[num_Real, opt___] := Module[{op, out={}, hist, xbin, proi, i, j, kick},
      op = Override[opt, def];
      {hist, xbin} = Which[Elastic/.op,  {normProb$Elastic, normBin$Elastic}, Inelastic/.op, {normProb$Inelastic, normBin$Inelastic}];
      proi = (Min[##]+(Max[##]-Min[##])*Random[num])&@@hist;
      For[i=1, i<=num, i++, Do[If[proi[i]<hist[j], kick=xbin[j-1];AppendTo[out, kick];Break[]],{j,1, INF}];];
      If[~(Length@out), Print[" ??? GenKickBGS error.\n"]];
      out];
    
    ! phys = "Elastic"/"Inelastic"
    ! 2J>n*EMITX2, ... Anomalous equilibrium emittance
    !?? UnProtect[BGSParticles];
    BGSParticles[np_Real, nmin_Real, phys__String]:=Module[{np0=Floor[np], Jmin, dr1, s, pos, i, j=0, par, par2, J, pars={}},
      Jmin = nmin*{EMITX, EMITY+MINCOUP*EMITX, EMITZ};
      While[j<np0,
        dr1 = Which[phys=="Elastic",  GenKickBGS[1, Elastic->True][1], phys=="Inelastic", GenKickBGS[1, Inelastic->True][1], True, Return[$Failed]];      
        s = cir$*Random[];
        pos = If[s<LINE["S", Floor[ncomp$/2]],
                Catch[Do[If[LINE["S", i]>s, Throw[i-1]], {i, Floor[ncomp$/2]}]],
                Catch[Do[If[LINE["S", i]>s, Throw[i-1]], {i, Floor[ncomp$/2], ncomp$}]]];
        par = GenGaussBeam[pos, 1, 3, {EMITX, EMITY+MINCOUP*EMITX, EMITZ}];
        If[phys=="Elastic",
          psi = 2*PI*Random[];
          par = MapAt[(#+dr1*Cos[psi])&, par, {2, 2}];
          par = MapAt[(#+dr1*Sin[psi])&, par, {2, 4}]];
        If[phys=="Inelastic", par = MapAt[(#-dr1*Sin[2*PI*Random[]])&, par, {2, 6}]];
        J = CalEmit[par, Method->"None"][2,1];
        J = 2*Flatten@J;
        If[(J[1]>Jmin[1])+(J[2]>Jmin[2])+(J[3]>Jmin[3]),
          par2 = TrackParticles[par, "$$$"];
          If[par2[2,7,1],
            pars = If[~(Length[pars]), par2, AppendBeam[pars, par2]];
            j++]
          ];
        ];
      pars];
   
    ! set default as w/o boundary
    BGSParticles[np_Real, phys__]:=BGSParticles[np, 0, phys];
    BGSParticles[np_Real]:=BGSParticles[np, 0, "Elastic"];
    !??? Protect[BGSParticles];
    ];  
  ];

! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part2 **
! Touschek Scattering
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
TouschekPiwinski=Class[{}, {posTous$, tousDpSquareLib$, pTousDpLib$, pTousDpCDFLib$, pTousOneTurn$, pTousSum$, nTousOneTurn$, pTousAccuNorm$}, {e$, ncomp$, cir$, elements$="*", dpmin$=2e-3, dpmax$=2e-2, nbins$=2000, path$py=path$lib, dsmin$=0.1, pwd$=GetEnv["PWD"]//"/", ScatteringRates$=""},
  (* 
  dpmin0$: to get reasonable scattering probability versus dp;
  dsmin$: min. distance between adjacent scattering points (default as 0.1 m)
  *)
  Constructor[] := Module[{},
    Print["\n  ...Start Touschek Class[] initialization.\n"];   
    e$ = Emittance[OneTurnInformation->True, ExpandElementValues->False];
    {ncomp$, cir$} = LINE[{"POSITION", "S"}, "$$$"];
    Initialize[];
    ShowParameters[];
    Print["  .........Touschek Class[] initialization done!\n"];    
    ];

  ShowParameters[]:=(Print["\n  Parameters for TouschekPiwinski Class[]:\n",
    "  EMITX = ", EMITX, "\t EMITY = ", EMITY+MINCOUP*EMITX, "\t EMITS = ", EMITZ, "\n",
    "  SIGZ = ", SIGZ, "\t SIGE = ", SIGE, "\n",
    "  MINCOUP = ", MINCOUP, "\t PBUNCH = ", PBUNCH, "\n",
    "  Dp_min: ", dpmin$, "\tDp_max: ", dpmax$, "\n",
    "  Number of event per turn: ", nTousOneTurn$, "\n\n"]);

  With[{def={OneTurnInformation->False}},
    ! wrt. A. Piwinski, 1998 paper Eq. (42)
    ! fast assessement of integration part F(B1, B2, tau)
    func42[t_Real, tm_Real, B1_Real, B2_Real] := ((2 + 1/t)^2*(t/tm/(1 + t) - 1) + 1 - Sqrt[(1 + t)*tm/t] - (2/t + 1/2/t^2)*Log[t/tm/(1 + t)])*Exp[-B1*t]*BesselI[0, B2*t]*Sqrt[t/(1 + t)];

    ! tw0: {alfx, betx, alfy, bety, etax, etapx, etay, etapy}
    ! sigs, sigp: could be inferred from emits (to be checked, 2020.05.03
    ! dpmin/dpmx: min/max scattering angle (X, kappa in Piwinski's formulas)
    ! output: scatter rate, {tau_i, F_i, para}, para={tau_min, tau_max, dtau, B1, B2}
    ScatRateOnePoint[tw0_List, opt___]:=Module[{op, Ee=0.511e6, Ek, re=2.818e-15, gamma, beta, c0=2.9979e8, alfx, betx, alfy, bety, etax, etapx, etay, etapy, {emitx, emity, emits} = {EMITX, EMITY+MINCOUP*EMITX, EMITZ}, sigp=SIGE, sigxb, sigx, etax2, sigyb, sigy, etay2, sigx2, sigy2, sigh, B1, B2, tmax, tmin, tmin0, Fdt={}, intg=0, dt, tt, pp, args, itgr, Fint, pScat=0, Tous, para, taumax$=0.01, fn},
      op = Override[opt, def];
      Ek = MOMENTUM-Ee;
      gamma = MOMENTUM/Ee;
      beta = Sqrt[1-1/gamma^2];
      {alfx, betx, alfy, bety, etax, etapx, etay, etapy} = tw0;

      sigxb = Sqrt[emitx*betx]; ! betatron size
      sigx = Sqrt[emitx*betx + etax^2*sigp^2];
      etax2 = alfx*etax + betx*etapx;
 
      sigyb = Sqrt[emity*bety];
      sigy = Sqrt[emity*bety + etay^2*sigp^2];
      etay2 = alfy*etay + bety*etapy;

      sigx2 = Sqrt[emitx*betx + sigp^2*(etax^2 + etax2^2)]; ! sigmaxx
      sigy2 = Sqrt[emity*bety + sigp^2*(etay^2 + etay2^2)];

      sigh = 1/Sqrt[1/(sigp^2*sigxb^2*sigyb^2) * (sigx2^2*sigyb^2+sigy2^2*sigxb^2-sigxb^2*sigyb^2)];
      B1 = betx^2/(2*beta^2*gamma^2*sigxb^2)*(1-sigh^2*etax2^2/sigxb^2) + bety^2/(2*beta^2 *gamma^2 *sigyb^2)*(1-sigh^2*etay2^2/sigyb^2);
      B2 = Sqrt[B1^2 - betx^2*bety^2*sigh^2/(beta^4*gamma^4*sigxb^4*sigyb^4*sigp^2)*(sigx^2 *sigy^2 - sigp^4 *etax^2 *etay^2)];
      tmax = (beta*dpmax$)^2;
      tmin = (beta*dpmin$)^2;
      dt = (tmax-tmin)/nbins$;
      
      para = {tmin, tmax, dt, B1, B2};
      tt = Table[i, {i, tmin, tmax, dt}];
      fn = ".tmp_"//FromDate[];      
      args = "python3 "//path$py//"Integrate_Eq41.py -tm_min "//tmin//" -tm_max "//tmax//" -ntm "//nbins$//" -B1 "//B1//" -B2 "//B2//" > "//fn;
      System[args];
      If[FileQ[fn],
        itgr = Import[fn];
        System["rm "//fn]];
      If[Length[Flatten@itgr]>0,
        pp = itgr[;;,1];
        If[#<nbins$, Print[#]]&/@{Length@pp};        
        intg = pp[1]-pp[-1];
        tt = Drop[tt,-1];
        pp = Drop[pp,-1]-Drop[pp,1];
        Fdt={tt, pp};
      
        Fint = Sqrt[Pi*(B1^2 -B2^2)]*tmin*intg;
        ! local Scatter rate
        pScat = re^2*c0*PBUNCH^2/(8*Pi*gamma^2*SIGZ*Sqrt[sigx^2*sigy^2-sigp^4*etax^2*etay^2]*tmin)*Fint; ! scatter rate (dN/dt)
        ,
        Print@args;
        Fdt = {{0}, {0}}];               
      {pScat, Fdt, para}]; 

    ! ScatterRate, (/s); TousLifeTime, (s)
    SetAttributes[{ScatterRate, TousLifeTime, ScatterRateOneTurn, ScatterRateCDFOneTurnNorm, ScatterRateDpOneTurn, ScatterRateDpCDFOneTurnNorm, DpSquareOneTurn}, Constant];

    ! Rcdf: CDF of scattering rate vs. dp
    ! pTous, scatter rate; nTous, number of scatterred particles (not pairs!!!)
    Initialize[opt___]:=Module[{op, r={}, i, c0=2.9979e8, Ee=0.511e6, gamma, seq0, beta, s0, pv={1}, s00=0, elem, nobs, s1, ds, twall, rates={}, pTousAllObs={}, tau, F, RcdfAll={}, Rcdfi, fi, pTousAccu={}, lifetime, fw, datas, now="", fn},
      op = Override[opt, def];
      Check[
        gamma = MOMENTUM/Ee;
        beta = Sqrt[1-1/gamma^2];
        {seq0, s0} = LINE[{"POSITION", "S"}, elements$];
        
        Scan[If[(s0[#]-s00>dsmin$)&&(s0[#]<cir$), AppendTo[pv, #]; s00=s0[#]]&, Range[Length@s0-1]];
        {seq0, s0} = {seq0[pv], s0[pv]};        
        posTous$ = seq0;
        nobs = Length@s0;
        s1 = Append[s0[Range[nobs-1]+1], cir$];
        ds = s1-s0;
        twall = Thread[Twiss[{"AX", "BX", "AY", "BY", "PEX", "PEPX", "PEY", "PEPY"}, seq0]];
        
        ! scat. rate at every  point
        If[StringLength[ScatteringRates$],
          Check[datas = Get[ScatteringRates$]; Print["  ...'ScatterRates' loaded."], Print["??? 'ScatteringRates' is not correctly defined. Stop."]; FFS["END"]];
          rates = "Rates"/.datas;
          ,
          Scan[(AppendTo[rates, ScatRateOnePoint[twall[#]]];)&, Range[nobs]];
          now=FromDate[];
          fn = pwd$//"tousRates_"//now//".data";
          fw = OpenWrite[fn];
          Write[fw, {"Rates"->rates}];
          Close[fw];
          Print["!!! Save 'ScatteringRates' to "//fn];
          ];
          
        pTousAllObs = rates[;;,1];
        {tau, F} = Thread[rates[;;,2]];
        
        Table[
          fi= F[i];
          If[RealQ[fi[-1]],
            Rcdfi = Plus@@(fi[Range[#]])&/@Range[Length@fi];
            Rcdfi /= Plus@@fi;
            AppendTo[RcdfAll, Rcdfi];
            ,
            Print["??? Invalid value at the "//i//"-th point."]];
          ,{i, Length@F}];
          
        tousDpSquareLib$ = tau/(beta^2)/(1+tau); ! delta^2        
        pTousDpLib$=F;  
        pTousDpCDFLib$ = RcdfAll; ! R vs. dp/tau, normalized
        ! scattering rate for one turn
        pTousOneTurn$ = pTousAllObs*ds/c0; ! unit, /s
        Scan[(AppendTo[pTousAccu, Plus@@((pTousAllObs*ds/cir$)[Range[#]])])&, Range[nobs]];
        pTousSum$ = pTousAccu[-1]; ! sum over one turn
        nTousOneTurn$ = pTousSum$*cir$/c0;
        pTousAccuNorm$ = pTousAccu/pTousSum$; ! nomalized, max=1
        lifetime = 1/pTousSum$*PBUNCH;
        
        r = {ScatterRate->pTousSum$, TousLifeTime->lifetime};
        If[OneTurnInformation/.op, r=Join[r, {nScatterOneTurn->nTousOneTurn$, ScatterRateOneTurn->pTousOneTurn$, ScatterRateCDFOneTurnNorm->pTousAccuNorm$, ScatterRateDpOneTurn->pTousDpLib$, ScatterRateDpCDFOneTurnNorm->pTousDpCDFLib$, DpSquareOneTurn->tousDpSquareLib$}]];
        r, {}]];
     ]; ! With

  ! CDF, cumulative distribution function (discrete)
  ! general function
  RandomOverCDF[CDF_List]:=Module[{h, npos, left=0, mid, right, i, ndiv=2},
    h = Random[];
    npos = Length@CDF;
    mid=Round[npos/2];  
    right=npos;
    Table[If[npos/2^ndiv>20, ndiv++], {20}];
    Scan[(If[h<CDF[mid], right=mid; mid= left+Round[npos/(2^#)], left=mid; mid=left+Round[npos/(2^#)]])&,Range[ndiv]+1];
    pos = Catch[Do[If[h<=CDF[i], Throw[i]], {i, left, right}]];
    pos];

  GetScatLocation[CDF_List, np_:1]:=Module[{pos={}},
    Table[AppendTo[pos, RandomOverCDF[CDF]],{np}];
    pos];
  GetScatLocation[np_:1]:=GetScatLocation[pTousAccuNorm$, np];

  GenKickTous[pos_List, opt___]:=Module[{op, posi, deltap={}, sig, Ee=0.511e6, gamma, beta},
    gamma = MOMENTUM/Ee;
    beta = Sqrt[1-1/gamma^2];
    If[~(Length@pTousDpCDFLib$), Print[" ??? Initialize Touschek functions.\n"]; FFS["ABORT;"]];
    op = Override[opt, def];
    Scan[(
      posi = RandomOverCDF[pTousDpCDFLib$[pos[#]]];
      AppendTo[deltap, Sqrt[tousDpSquareLib$[pos[#], posi]]/beta])&, Range[Length@pos]];     
    deltap];

  With[{def={InitialEmitCut->{0,0,0}}},
  TouschekParticles[np_:1, opt___]:=Module[{op, np0=Floor[np], seq, posi, dpabs, par0, sign, par1, pars={}},
    op = Override[opt, def];
    If[np0,
      seq = GetScatLocation[np0]; ! sequence in the Lib$ arrays
      dpabs = GenKickTous[seq];
      Scan[(posi = posTous$[seq[#]];
        par0 = GenGaussBeam[posi, 1, 3, {EMITX, EMITY+MINCOUP*EMITX, EMITZ}];
        sign = If[Random[]<0.5, -1, 1];
        par0[2,6] += (sign*dpabs[#]);
        par1 = TrackParticles2[par0, "$$$"]; 
        If[par1[2,7,1], pars = If[~(Length[pars]), par1, AppendBeam[pars, par1]]];)&, Range[np0]];
        If[(Length@pars)&&(Plus@@[InitialEmitCut/.op]), pars = PureParticles[pars, InitialEmitCut/.op, "MIN"]];
      ];
    pars];
    ];
  ]; ! Class

! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part3 **
! beam-beam, other collective effect
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =


! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part4 **
! tail/halo particle genration
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
HaloSimulator = Class[{}, {}, {emitCut$={0, 0, 0}},
  Constructor[] := Module[{},
    If[~(Length@BGS), Print@" ??? BeamGasScattering Class has to be initialized as BGS.\n"; Return[$Failed]];
    If[~(Length@TP), Print@" ??? TouschekPiwinski Class has to be initialized as TP.\n"; Return[$Failed]];
    ];
   
  With[{def={ElasticBGS->False, InelasticBGS->False, BeamBeam->False, Touschek->False, FileHead->"turn=", Targz->False,SaveParticles->False, SaveJ->False, SaveLastTurns->0, CleanOut->False, LossMonitor->False, Method->"SAD", OneTurnPerturbation->True, Damping->True, Excitation->True, Aperture->False, RFbucket->False, InputFileName->"", InputBeam->False, InitialEmitCut->{0,0,0}}},
    ! SaveLastTurns -> number, 0 or 10, 100
    ! FileHead: the head of output file name
    ! SaveParticles: enable particle export, no file saved if SaveParticles->False
    ! SaveJ: export (J, phi) 
    ! SaveLastTurns: number, export particles in the last "n" turns
    ! CleanOut: rm the previous exported file, affects only file saved before the LastTurns
    ! LossMonitor: write particle loss locations (to be added, 2020.05.13)
    ! Method: Tracking method, "SAD" or "Linear"
    ! OneturnPerturbation, Damping, Excitation are options for TrackParticles2[] function
    GenTrackParticles[nturn0_Real, nmax_Real, nstep_Real, opt___]:=Module[{op, fhead, fn, b0={}, b1={}, n1=0, n2=0, n3=0, bn1 = {}, bn2={}, bnew3={}, ii, emi, act, fw1, fw2, fm1="a.aa", fm2="a.aa"},
    (* generate halo particle turn-by-turn, and accumulated at the entrance *)
      op = Override[opt, def];
      fhead = FileHead/.op;
      If[SaveParticles/.op, 
        If[nturn0>1,
           If[StringLength[InputFileName/.op]&&~(Length[InputBeam/.op]<2),
              fn = InputFileName/.op,
              fn = "./beam_"//fhead//ToString[nturn0]//".dat"];
           b0 = If[Length[InputBeam/.op]==2, InputBeam/.op, ImportBeam[fn]],
           System["rm -f ./beam_"//fhead//"*"];]];           
      Table[        
         If[ElasticBGS/.op,
           n1 = (n1-Floor@n1+BGS@nElasticBGSOneTurn$);
           bn1 = BGS@BGSParticles[n1, "Elastic"]];
         If[InelasticBGS/.op,
           n2 = (n2-Floor@n2+BGS@nInelasticBGSOneTurn$);
           bn2 = BGS@BGSParticles[n2, "Inelastic"]];
         If[Touschek/.op,
           n3 = (n3-Floor[n3]+TP@nTousOneTurn$);
           bn3 = TP@TouschekParticles[n3, op]];
         ! Space charge and other collective effects ??
         If[~(n1+n2+n3), Print@" ??? No physics process is stated!"; Return[$Failed]];
         If[Length@b0,
            b1=If[~(FractionPart[(ii-nturn0)/nstep])&&Length[Aperture/.op],
              TrackParticles2[b0,'$$$', 1, op],
              TrackParticles2[b0,'$$$', 1, Override[{Aperture->False}, op]]]];

         Scan[If[Length[#]&&((ElasticBGS/.op)||(Inelastic/.op)||(Touschek/.op)), b1=If[~(Length@b1), #, AppendBeam[b1, #]]]&, {bn1, bn2, bn3}];         
         If[Length@b1, b0 = {1, b1[2]}];

         If[(Length@b1)&&(~(FractionPart[(ii-nturn0)/nstep])||ii>=(nmax-SaveLastTurns/.op)),
            b0 = PureParticles[b0, emitCut$, "MIN", op]; ! remove lost and core particles
            If[(Length@b0)&&SaveParticles/.op,
              fw1 = "./beam_"//fhead//ToString[ii]//".dat";
              fw2 = "./beam_"//fhead//ToString[ii]//"_J.dat";
              ExportBeam[fw1, b0];
              If[Targz/.op,
                System["tar -zcf "//fw1//".gz "//fw1];
                System["rm -f "//fw1]];              
              If[SaveJ/.op,
                {emi, act} = CalEmit[b0, Method->"Static"];
                Export[fw2, Thread@(Join[act[1], act[2]])];
                If[Targz/.op,
                  System["tar -zcf "//fw2//".gz "//fw2];
                  System["rm -f "//fw2]];
                ];
              
              ! clear previous out
              If[ii<=(nmax-SaveLastTurns/.op)&&CleanOut/.op,
                System["rm -f "//fm1];
                If[SaveJ/.op, System["rm -f "//fm2]];                
                fm1=fw1; fm2=fw2;];
              If[LossMonitor/.op, Export["./loss_"//fhead//ToString[ii]//".dat", Null];]]];
        ,{ii, nturn0, nmax}];
       b0];

    ! seeds, list of seeds
    ! head1, heads: output file heads
    MergeParallelOut[head1_, head2_, seeds_, opt___]:=Module[{fn, b0, b1={}},
      Scan[(
        fn = "./beam_"//head1//ToString[#]//head2//".dat";
        If[FileQ[fn], 
          b0=ImportBeam[fn];
          If[~(Length@b1), b1=b0, b1=AppendBeam[b1, b0]],
          Print["!!! file not exist.\n"//fn];])&, seeds];
      If[FileQ[fn], ExportBeam["./beam"//head2//".dat", b1]];
      ];    
    ];
  ];

Protect[BeamGasScattering, TouschekPiwinski, HaloSimulator]