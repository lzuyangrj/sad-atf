!==============================================================
! Library for elastic and inelastic beam-gas scattering
! Implanted from Renjun's script for ATF-DR
! 2019.04.16
!==============================================================

RandomInteger[min_,max_] := Module[{num, out},
  num = (max-min)*Random[]+min;
  out=If[num<Floor[num]+0.5, Floor[num],Ceiling[num]]
  ];

dsigmaCO[theta_]:= theta/(theta^2+thetam1^2)^2;
! kk = Es/E
dsigmaieCO[kk_] := Module[{z01=6,z02=8,Fz1,Fz2,sigmaie1,sigmaie2,sigmaie},
  Fz1 = z01^2*Log[183/z01^(1/3)]+z01*Log[1194/z01^(2/3)];
  Fz2 = z02^2*Log[183/z02^(1/3)]+z02*Log[1194/z02^(2/3)];
  sigmaie1 = (4/3/kk-4/3+kk)*Fz1+1/9*(1/kk-1)*(z01^2+z01);
  sigmaie2 = (4/3/kk-4/3+kk)*Fz2+1/9*(1/kk-1)*(z02^2+z02);
  sigmaie = sigmaie1+sigmaie2
  ];

! creat bins for BGS perturbation
! {areamkBG1, xx1mkBG1} = CreateBinBGS[5, 10000, Type->"Elastic"] or {areamkBG2CO, xx1mkBG2CO} = CreateBinBGS[1e-5, 0.04, Type->"Inelastic"]
CreateBinBGS[nmin_:5, nmax_:1e4, type_] := Module[{mode=0, alpha0 = 7.293525664e-3, zz1 = Sqrt[50], thetma1, hlim, llim, nbins=10000, xx, xx1, xx2, xbin, areaSwap, area0, i, j},
  mode = If[type=="Elastic", 1, If[type=="Inelastic", 2]];
  If[~(mode), Print["\tEnter correct scattering type: Elastic or Inelastic.\n"]];
  
  gamma = MOMENTUM/0.511e6; 
  thetam1=zz1^(1./3.)*alpha0/gamma;
  
  {llim, hlim} = If[mode<2, {nmin, nmax}*thetam1, {nmin, nmax}];  
  xx = Table[llim+i*(hlim-llim)/nbins,{i,1,nbins-1}];
  xx1 = Sort[Append[xx,llim]]; 
  xx2 = Sort[Append[xx,hlim]]; 
  xbin = xx2-xx1;            
  areaSwap = If[mode<2, Table[dsigmaCO[xx1[i]]*xbin[i],{i,1, nbins}], Table[dsigmaieCO[xx1[i]]*xbin[i],{i,1,nbins}]];  
  area0 = Table[Apply[Plus,Table[areaSwap[i],{i,1,j}]],{j,1,nbins}];
  {area0/area0[nbins], xx1}
  ];

! Global varibles: NormalizedProbability$, NormalizedBin$
! nmin, dmin should be determined wrt. equilibrium beam parameters
With[{def={Elastic->False, Inelastic->False, Nmin->20, Nmax->10000, Dpmin->SIGE/2, Dpmax->0.04}},
  GenKickBGS[num_, opt___] := Module[{op, out={}, hist, xbin, proi, i, j, kick},
    op = Override[opt, def];
    If[Elastic/.op,
      If[Length@NormalizedBin$1<2, {NormalizedProbability$1, NormalizedBin$1} = CreateBinBGS[Nmin/.op, Nmax/.op, "Elastic"]];
      {hist, xbin} = {NormalizedProbability$1, NormalizedBin$1};
      ];
    
    If[Inelastic/.op,
      If[Length@NormalizedBin$2<2, {NormalizedProbability$2, NormalizedBin$2} = CreateBinBGS[Dpmin/.op, Dpmax/.op, "Inelastic"]];
      {hist, xbin} = {NormalizedProbability$2, NormalizedBin$2};    
      ];

    proi = (Min[##]+(Max[##]-Min[##])*Random[num])&@@hist;
    For[i=1, i<=num, i++, Do[If[proi[i]<hist[j], kick=xbin[j-1];AppendTo[out, kick];Break[]],{j,1,1e4}];];
    out];
    
  ! phys = "Elastic"/"Inelastic"
  ! 2J>n*EMITX2, ... Anomalous equilibrium emittance
  BGSParticles[np_, nmin_, phys__]:=Module[{Jmin, dr1, {ncomp, cir} = LINE[{"POSITION", "S"}, "$$$"], s, pos, i, j=0, par, par2, J, pars={}},
    Jmin = nmin*{EMITX2, EMITY2, EMITZ2};
    While[j<np,
      If[phys=="Elastic",
        dr1 = GenKickBGS[1, Elastic->True][1],
        If[phys=="Inelastic",
          dr1 = GenKickBGS[1, Inelastic->True]][1]
        ];      
      s = cir*Random[];
      pos = If[s<LINE["S", Floor[ncomp/2]],
              Catch[Do[If[LINE["S", i]>s, Throw[i-1]], {i, Floor[ncomp/2]}]],
              Catch[Do[If[LINE["S", i]>s, Throw[i-1]], {i, Floor[ncomp/2], ncomp}]]];
      par = GenGaussBeam[pos, 1, 3, {EMITX2, EMITY2, EMITZ2}];
      If[phys=="Elastic",
        psi = 2*PI*Random[];
        par = MapAt[(#+dr1*Cos[psi])&, par, {2, 1}];
        par = MapAt[(#+dr1*Sin[psi])&, par, {2, 3}]];
      If[phys=="Inelastic", par = MapAt[(#+dr1*Sin[2*PI*Random[]])&, par, {2, 6}]];
      J = CalEmit[par, Method->"None"][2,1];
      J = 2*Flatten@J;
      If[(J[1]>Jmin[1])+(J[2]>Jmin[2])+(J[3]>Jmin[3]),
        par2 = TrackParticles[par, "$$$"];
        If[par2[2,7,1],
          pars = If[~(Length[pars]), par2, AppendBeam[pars, par2]];
          j++]
        ];
      ];
    pars];
   
  ! set default as w/o boundary
  BGSParticles[np_, phys__]:=BGSParticles[np, 0, phys];
  ];
  
! num of elastics BGS events per turn
! nminthetam/nmaxthetam -> min/max num of min. scattering angle
! Nb -> number of particles per bunch
NElasticBGSOneTurn[nmin_, nmax_, Nb_, pp_]:=
  Module[{alpha0, c0, zz1, kb, re, T, gamma, pho, cir, thetam1, theta0, thetaMax, sigma2},
    alpha0 = 7.293525664e-3;
    c0 = 299792458;
    zz1 = Sqrt[50]; 
    kb = 1.38e-23; 
    re = 2.818e-15; 
    T = 300;  !24oc
    gamma = MOMENTUM/0.511e6; 
    pho = pp/kb/T; 
    cir = LINE["S", "$$$"]; ! circumference of ring

    thetam1=zz1^(1./3.)*alpha0/gamma;
    theta0 = nmin * thetam1;
    thetaMax = nmax * thetam1;
    sigma2=4*Pi*(zz1^2)*(re^2)/(gamma^2)*(1/(thetam1^2+theta0^2)-1/(thetaMax^2+thetam1^2));
    2*cir*pho*Nb*sigma2
    ];
    
! inelastic BGS (Bresstruhlung && extraction) events in 1 turn
! kk1/kk2 -> range of momentum deviation
! Ee=MOMENTUM, Nb -> num of particles per bunch (PBUNCH)
NInelasticBGSOneTurn[kk1_, kk2_, Nb_, pp_]:=
  Module[{z01=6, z02=8, alpha0 = 7.293525664e-3, c0 = 299792458, kb = 1.38e-23, re = 2.818e-15, T = 300, zz1, gamma, pho, cir, Emin, Emax, fz1, fz1, fz2, sigmaie1, sigmaie2, sigmaie},
    zz1 = Sqrt[50]; ! CO
    gamma = MOMENTUM/0.511e6; 
    pho = pp/kb/T; ! volumn gas density    
    cir = LINE["S", "$$$"]; ! circumference of ring
  
    Emin = kk1*Ee;
    Emax = kk2*Ee;

    fz1 = z01^2*Log[183/z01^(1/3)]+z01*Log[1194/z01^(2/3)];
    fz2 = z02^2*Log[183/z02^(1/3)]+z02*Log[1194/z02^(2/3)];
    sigmaie1 = 4*alpha0*re^2*(fz1*(4/3*Log[Emax/Emin]-5/6)+1/9*(z01^2+z01)*(Log[Emax/Emin]-1));
    sigmaie2 = 4*alpha0*re^2*(fz2*(4/3*Log[Emax/Emin]-5/6)+1/9*(z02^2+z02)*(Log[Emax/Emin]-1));
    sigmaie = sigmaie1+sigmaie2;

    cir*pho*Nb*sigmaie
    ]; 

! estimate mean(beta) of the beam line
AverageBeta[] := Module[{sumbx=0, sumby=0, pos},
  pos = LINE["POSITION", "*"];
  Do[ds = pos[i+1]-pos[i];
     {bx, by} = Twiss[{"BX", "BY"}, pos[i]];
     sumbx += ds*bx;
     sumby += ds*by,
     {i, 1, Length[posEles]-1}];
    
  Print["\tmeanbx = ", meanbx = sumbx/LINE["S", "$$$"]];
  Print["\tmeanby = ", meanby = sumby/LINE["S", "$$$"]];
  {meanbx, meanby}];

! assess BGS lifetime, analytically
! Aperture element is named as "AP.*"
CalBGSlifetime[pres_:Pa] := Module[{alpha0 = 7.293525664e-3, c0 = 299792458, zz1 = Sqrt[50], kb = 1.38e-23, re = 2.818e-15, T = 300, Ee, gamma, rho, thetaI, thetac, ApertDis, thetaMax, sigma1, dEmin, dEmin2, z01, z02, fz1, fz2, sigmaie1, sigmaie2, sigmaie, tau1, tau2},  
  e = Emittance[OneTurnInformation->True, ExpandElementValues->False];
  gamma =MOMENTUM/0.511e6;
  rho = pres/kb/T; 

  ! Elastics x
  thetaI = {};
  Scan[AppendTo[thetaI, {LINE["AX",#]/Sqrt[Twiss["BX", #]*meanbx], LINE["AY",#]/Sqrt[Twiss["BY", #]*meanby]}]&, LINE["NAME", "AP*"]];
  thetac = Min[thetaI]; ! minimum deflection angle
  
  ! Export Aperture information
  ! ApertDis = Transpose[LINE[{"S", "AX", "AY"}, "AP.*"]];
  ! Export["Apert_noColl.dat", ApertDis];

  thetaMax = 0.5; ! 500 mrad
  sigma1 = 4*Pi*(zz1^2)*(re^2)/(gamma^2)*(1/thetac^2-1/thetaMax^2);
  
  ! Inelastic
  dEmin = {};
  Scan[AppendTo[dEmin, Abs[LINE["AX", #]/Twiss["EX", #]]]&, LINE["NAME", "AP*"]];
  dEmin = Min[dEmin];
  
  dEmin2 = Min@{BucketHeight/.e, dEmin};  
  
  z01 = 6;
  z02 = 8;
  dEmax = 0.5; 
  fz1 = z01^2*Log[183/z01^(1/3)]+z01*Log[1194/z01^(2/3)];
  fz2 = z02^2*Log[183/z02^(1/3)]+z02*Log[1194/z02^(2/3)];
  sigmaie1 = 4*alpha0*re^2*(fz1*(4/3*Log[dEmax/dEmin2]-5/6)+1/9*(z01^2+z01)*(Log[dEmax/dEmin2]-1));
  sigmaie2 = 4*alpha0*re^2*(fz2*(4/3*Log[dEmax/dEmin2]-5/6)+1/9*(z02^2+z02)*(Log[dEmax/dEmin2]-1));
  sigmaie = sigmaie1+sigmaie2;

  tau1 = (2*rho*sigma1*c0)^-1;
  tau2 = (rho*sigmaie*c0)^-1; 
  Print["\tElastic BGS lifetime = ", tau1/60, " [min]\n"];
  Print["\tInelas. BGS lifetime = ", tau2/60, " [min]\n"];  
  Print["\tTotal   BGS lifetime = ", 1/60*(1/tau1 + 1/tau2)^-1, " [min]\n"];
  ];


