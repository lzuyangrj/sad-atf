#!/usr/bin/env python3
import os
import sys
from subprocess import Popen, PIPE

# fp, parent file; fw, new son file name
def run(value, fp):
    source = os.path.basename(fp)
    fw = source + '.' + str(value)
    replace(fp, value, fw)
    process = Popen(['/afs/cern.ch/user/y/yozhao/oldsad/bin/gs', fw], stdout=PIPE, stderr=PIPE,
            universal_newlines=True)
    #process = Popen(['/eos/experiment/fcc/ee/accelerator/SAD/oldsad/bin/gs', input], stdout=PIPE, stderr=PIPE,
    #        universal_newlines=True)
    stdout, stderr = process.communicate()
    if stderr:
        print(stdout)
        print(stderr)
    else:
        print(stdout)

def replace(fp, varible, fw):
    with open(fp,'r') as fr:
    	tex = fr.read()
    with open(fw, "w") as fw:
        fw.write(tex.replace('NEWSEED', str(varible)))
    
if __name__ == '__main__':

    args = sys.argv
    narg = len(args[1:])
    if narg == 0 or narg == 1:
        print("The input file is missing!")
        sys.exit(1)
    elif narg == 2:
        varible = args[1]
        fn = args[2]
        run(varible, fn)
    else:
        sys.exit(1)
