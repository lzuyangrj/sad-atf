import os,sys
import numpy as np
import argparse

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
parser.add_argument("nturn", type=int, help="number of turns")
parser.add_argument("nseed", type=int, help="number of seeds")
args = parser.parse_args()

nseed = args.nseed
nturn = args.nturn
for s in range(nseed):
    dat0=np.loadtxt("beam_seed="+str(s+1)+"_turn="+str(args.nturn)+".dat")
    x, px, y, py, z, dp, flag = np.transpose(dat0)

    if s==0:
        mode = 'w'
    else:
        mode = 'a'
        
    fw=open('beam_turn='+str(args.nturn)+'.dat', mode)        
    for i in range(len(x)):
        fw.write('%12.6e\t%12.6e\t%12.6e\t%12.6e\t%12.6e\t%12.6e\t%d\n'%(x[i], px[i], y[i], py[i], z[i], dp[i], flag[i]))        
    fw.close()
        
