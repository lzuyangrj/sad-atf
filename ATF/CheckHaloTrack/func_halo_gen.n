!======================================================
! Functions for tail/halo particle generation
! by Renjun Yang (created on 3 May, 2020)
! require BesselI() function, which is wrong for some SAD version (2017-2020)
!======================================================

! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part1 **
! BGS Scattering
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =


! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part2 **
! Touschek Scattering
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
TouschekPiwinski=Class[{}, {}, {},
  With[{def={OneTurnInformation->False, Elements->"*"}},
    ! wrt. A. Piwinski, 1998 paper Eq. (42)
    ! fast assessement of integration part F(B1, B2, tau)
    FsigmaInt[t_, tm_, B1_, B2_]:=Module[{},
      ((2+1/t)^2*(t/tm/(1+t)-1)+1-Sqrt[(1+t)*tm/t]-(2/t+1/2/t^2)*Log[t/tm/(1+t)])*Exp[-B1*t]*BesselI[0, B2*t]*Sqrt[t/(1+t)]];
     
    ! tw0: {alfx, betx, alfy, bety, etax, etapx, etay, etapy}
    ! sigs, sigp: could be inferred from emits (to be checked, 2020.05.03
    ! dpmin/dpmx: min/max scattering angle (X, kappa in Piwinski's formulas)
    ! output: scatter rate, {tau_i, F_i, para}, para={tau_min, tau_max, dtau, B1, B2}
    ScatRateOnePoint[tw0_, emit0_, sigs_, sigp_, Ne_:PBUNCH, dpmin_, dpmax_, opt___]:=Module[{op, Ee=0.511e6, E0=MOMENTUM, Ek, re=2.818e-15, gamma, beta, c0=2.9979e8, alfx, betx, alfy, bety, etax, etapx, etay, etapy, emitx, emity, emits, sigxb, sigx, etax2, sigyb, sigy, etay2, sigx2, sigy2, sigh, B1, B2, tmax, tmin, Fdt={}, intg=0, nbin=2000, dt, tt, F, pScat, Tous, para},
      op = Override[opt, def];
      Ek = E0-Ee;
      gamma = E0/Ee;
      beta = Sqrt[1-1/gamma^2];
      {alfx, betx, alfy, bety, etax, etapx, etay, etapy} = tw0;
      {emitx, emity, emits} = emit0;
      sigxb = Sqrt[emitx*betx]; ! betatron size
      sigx = Sqrt[emitx*betx + etax^2*sigp^2];
      etax2 = alfx*etax + betx*etapx;
 
      sigyb = Sqrt[emity*bety];
      sigy = Sqrt[emity*bety + etay^2*sigp^2];
      etay2 = alfy*etay + bety*etapy;

      sigx2 = Sqrt[emitx*betx + sigp^2*(etax^2 + etax2^2)]; ! sigmaxx
      sigy2 = Sqrt[emity*bety + sigp^2*(etay^2 + etay2^2)];

      sigh = 1/Sqrt[1/(sigp^2*sigxb^2*sigyb^2) * (sigx2^2*sigyb^2+sigy2^2*sigxb^2-sigxb^2*sigyb^2)];
      B1 = betx^2/(2*beta^2*gamma^2*sigxb^2)*(1-sigh^2*etax2^2/sigxb^2) + bety^2/(2*beta^2 *gamma^2 *sigyb^2)*(1-sigh^2*etay2^2/sigyb^2);
      B2 = Sqrt[B1^2 - betx^2*bety^2*sigh^2/(beta^4*gamma^4*sigxb^4*sigyb^4*sigp^2)*(sigx^2 *sigy^2 - sigp^4 *etax^2 *etay^2)];
      !tmax = (beta*dpmax)^2;
      tmax = (beta*dpmax)^2/(1-(beta*dpmax)^2);
      tmin = (beta*dpmin)^2/(1-(beta*dpmin)^2);
      dt = (tmax-tmin)/nbin;
      para = {tmin, tmax, dt, B1, B2};
      Scan[(tt = FsigmaInt[tmin+#*dt, tmin, B1, B2]; ! -> tmax = 2%, for instance    
          If[tt>1e-15, AppendTo[Fdt, {tmin+(#-1)*dt, tt}]; intg+=tt*dt];)&, Range[nbin]]; ! left side
      
      F = Sqrt[Pi*(B1^2 -B2^2)]*tmin*intg;
      ! local Scatter rate
      pScat = re^2*c0*Ne^2/(8*Pi*gamma^2*sigs*Sqrt[sigx^2*sigy^2-sigp^4*etax^2*etay^2]*tmin)*F; ! scatter rate (dN/dt)
      {pScat, Thread@Fdt, para}]; 

    ! ScatterRate, (/s); TousLifeTime, (s)
    SetAttributes[{ScatterRate, TousLifeTime, ScatterRateOneTurn, ScatterRateCDFOneTurnNorm, ScatterRateDpOneTurn, ScatterRateDpCDFOneTurnNorm, DpSquareOneTurn}, Constant];

    ! Rcdf: CDF of scattering rate vs. dp
    ! pTous, scatter rate; nTous, number of scatterred particles (not pairs!!!)
    Init[emit0_, sigs_, sigp_, Ne_, dpmin_, dpmax_, opt___]:=Module[{op, r={}, i, Ee=0.511e6, E0=MOMENTUM, c0=2.9979e8, gamma, beta, seq0, s0, elem, nobs, cir0, s1, ds, twall, resAllObs={}, pTousAllObs={}, tau, Fdt, RcdfAll={}, Rcdfi, pTousAccu={}, lifetime},
      op = Override[opt, def];
      elem = Elements/.op;
      Check[
        gamma = E0/Ee;
        beta = Sqrt[1-1/gamma^2];
        {seq0, s0} = LINE[{"POSITION", "S"}, elem];
        posTous$ = seq0;
        nobs = Length@s0;
        cir0 = LINE["S", "$$$"];
        s1 = Append[s0[Range[nobs-1]+1], cir0];
        ds = s1-s0;
        twall = Thread[Twiss[{"AX", "BX", "AY", "BY", "PEX", "PEPX", "PEY", "PEPY"}, seq0]];

        ! scat. rate at every  point
        Scan[(AppendTo[resAllObs, ScatRateOnePoint[twall[#], emit0, sigs, sigp, Ne, dpmin, dpmax]])&,Range[nobs]];
        pTousAllObs = resAllObs[;;,1];
        {tau, Fdt} = Thread[resAllObs[;;,2]];
        Table[
          Rcdfi={};
          Scan[(AppendTo[Rcdfi, Plus@@(Fdt[i, Range[#]])])&,Range[Length@(Fdt[i])]];
          Rcdfi /= Plus@@(Fdt[i]);
          AppendTo[RcdfAll, Rcdfi],{i, Length@Fdt}];
        TousDpSquareLib$ = tau/(beta^2)/(1+tau); ! delta^2
        pTousDpLib$=Fdt;  
        pTousDpCDFLib$ = RcdfAll; ! R vs. dp/tau, normalized

        ! scattering rate for one turn
        pTousOneTurn$ = pTousAllObs*ds/c0; ! unit, /s
        Scan[(AppendTo[pTousAccu, Plus@@((pTousAllObs*ds/cir0)[Range[#]])])&, Range[nobs]];
        pTousSum$ = pTousAccu[-1]; ! sum over one turn
        nTousOneTurn$ = pTousSum$*cir0/c0;
        pTousAccuNorm$ = pTousAccu/pTousSum$; ! nomalized, max=1
        lifetime = 1/pTousSum$*Ne;
        r = {ScatterRate->pTousSum$, TousLifeTime->lifetime};
        If[OneTurnInformation/.op, r=Join[r, {nScatterOneTurn->nTousOneTurn$, ScatterRateOneTurn->pTousOneTurn$, ScatterRateCDFOneTurnNorm->pTousAccuNorm$, ScatterRateDpOneTurn->pTousDpLib$, ScatterRateDpCDFOneTurnNorm->pTousDpCDFLib$, DpSquareOneTurn->TousDpSquareLib$}]];
        r, {}]];
      
    TouschekLifeTime[dpmin_, dpmax_, opt___]:=Module[{op},
    (* default arguments -> emittance para.*)
      op = Override[opt, def];
      FFS["INTRA; EMIT;"];
      InitTouschek$[{EMITX, EMITY+MINCOUP*EMITX, EMITZ}, SIGZ, SIGE, PBUNCH, dpmin, dpmax, op]];

    TouschekLifeTime[opt___]:=TouschekLifeTime[1e-2, 0.5, opt];
  ]; ! With

  ! CDF, cumulative distribution function (discrete)
  ! general function
  RandomOverCDF[CDF_]:=Module[{h, npos, left=0, mid, right, i, ndiv=2},
    h = Random[];
    npos = Length@CDF;
    mid=Round[npos/2];  
    right=npos;
    Table[If[npos/2^ndiv>20, ndiv++], {20}];
    Scan[(If[h<CDF[mid], right=mid; mid= left+Round[npos/(2^#)], left=mid; mid=left+Round[npos/(2^#)]])&,Range[ndiv]+1];
    pos = Catch[Do[If[h<=CDF[i], Throw[i]], {i, left, right}]];
    pos];

  GetScatLocation[CDF_, np_:1]:=Module[{pos={}},
    Table[AppendTo[pos, RandomOverCDF[CDF]],{np}];
    pos];
  GetScatLocation[np_:1]:=GetScatLocation[pTousAccuNorm$, np];

  GenKickTous[pos_, opt___]:=Module[{op, posi, deltap={}, sig},
    (*needs InitTouscheck$ *)
    If[~(Length@pTousDpCDFLib$), Print["!!!\tInitialize Touschek functions.\n"]; FFS["ABORT;"]];
    op = Override[opt, def];
    Scan[(
      posi = RandomOverCDF[pTousDpCDFLib$[pos[#]]];
      AppendTo[deltap, Sqrt[TousDpSquareLib$[pos[#], posi]]])&, Range[Length@pos]];
    deltap];

  touschekparticles[np_:1, opt___]:=Module[{op, np0=Floor[np], Jmin, seq, posi, dpabs, par0, sign, par1, pars={}},
    op = Override[opt, def];
    !Jmin = nmin*{EMITX2, EMITY2, EMITZ2};    
    seq = GetScatLocation[np0]; ! sequence in the Lib$ arrays
    dpabs = GenKickTous[seq];
    Scan[(posi = posTous$[seq[#]];
      par0 = GenGaussBeam[posi, 1, 3, {EMITX, EMITX*MINCOUP+EMITY, EMITZ}];
      sign = If[Random[]<0.5, -1, 1];
      par0[2,6] += (sign*dpabs[#]);
      par1 = TrackParticles2[par0, "$$$"]; 
      If[par1[2,7,1], pars = If[~(Length[pars]), par1, AppendBeam[pars, par1]]];)&, Range[np0]];
    pars];
  ]; ! Class
  
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
! ** part4 **
! tail/halo particle genration
! = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  With[{def={ElasticBGS->True, InelasticBGS->False, BeamBeam->False, TouschekS->False, FileHead->"turn=", SaveParticles->False, SaveJ->False, SaveLastTurns->0, CleanOut->False, LossMonitor->False, Method->"SAD", OneTurnPerturbation->True, Damping->True, Excitation->True, Aperture->False, RFbucket->False, InputFileName->""}},
    ! SaveLastTurns -> number, 0 or 10, 100
    ! FileHead: the head of output file name
    ! SaveParticles: enable particle export, no file saved if SaveParticles->False
    ! SaveJ: export (J, phi) 
    ! SaveLastTurns: number, export particles in the last "n" turns
    ! CleanOut: rm the previous exported file, affects only file saved before the LastTurns
    ! LossMonitor: write particle loss locations (to be added, 2020.05.13)
    ! Method: Tracking method, "SAD" or "Linear"
    ! OneturnPerturbation, Damping, Excitation are options for TrackParticles2[] function
    GenTrackHaloParticles[nturn0_, nmax_, nstep_, opt___]:=Module[{op, TP, fhead, fn, b0={}, b1={}, n0=0, n1=0, n2=0, n3=0, bnew0 = {}, bnew1 = {}, bnew2={}, bnew3={}, ii, emi, act, fw1, fw2, fm1="a.aa", fm2="a.aa"},
    (* generate halo particle turn-by-turn, and accumulated at the entrance *)
      op = Override[opt, def];
      TP = TouschekPiwinski[];
      fhead = FileHead/.op;
      If[~(RealQ@nTousOneTurn$), Print["!!!\tInitialize Touschek generator.\n\tIn GenHaloParticles[]... \n"]];

      If[SaveParticles/.op, 
        If[nturn0>1,
           If[StringLength[InputFileName/.op],
              fn = InputFileName/.op,
              fn = "./beam_"//fhead//ToString[nturn0]//".dat"; Print["\tImported beam file: ",fn]];
           b0 = ImportBeam[fn],
           System["rm ./beam_"//fhead//"*"];]];
    
      Table[
         !n0 =
         !n1 =
         !n2 =        
         n3 = (n3-Floor[n3]+nTousOneTurn$);
         !If[ElasticBGS/.op, bnew0 = BGSParticles[n0, "Elastic"]];
         !If[InelasticBGS/.op, bnew1 = BGSParticles[n1, "Inelastic"]];       
         !If[BeamBeam/.op, bnew2 = BeamBeamParticles[n2, __]];
         If[TouschekS/.op, bnew3 = TP@TouschekParticles[n3];];
         If[Length@b0,
            b1=If[~(FractionPart[(ii-nturn0)/nstep])&&Length[Aperture/.op],
              TrackParticles2[b0,'$$$', op],
              TrackParticles2[b0,'$$$', Override[{Aperture->False}, op]]]];

         Scan[If[Length[#], b1=If[~(Length@b1), #, AppendBeam[b1, #]]]&, {bnew1, bnew2, bnew3}];
         b0 = b1;

         If[(Length@b1)&&(~(FractionPart[(ii-nturn0)/nstep])||ii>=(nmax-SaveLastTurns/.op)),
            b0 = PureParticles[b0, {0, 0, 9*EMITZ}, "MIN", op]; ! remove lost and core particles
            If[(Length@b0)&&SaveParticles/.op,
              fw1 = "./beam_"//fhead//ToString[ii]//".dat";
              fw2 = "./beam_"//fhead//ToString[ii]//"_J.dat";
              ExportBeam[fw1, b0];
              If[SaveJ/.op,
                {emi, act} = CalEmit[b1, Method->"Static"];
                Export[fw2, Thread@(Join[act[1], act[2]])];];
              
              ! clear previous out
              If[ii<=(nmax-SaveLastTurns/.op)&&CleanOut/.op,
                System["rm -f "//fm1];
                If[SaveJ/.op, System["rm -f "//fm2]];                
                fm1=fw1; fm2=fw2;];
              If[LossMonitor/.op, Export["./loss_"//fhead//ToString[ii]//".dat", Null];]]];        
        ,{ii, nturn0, nmax}];
      b0];

    ! seeds, list of seeds
    ! head1, heads: output file heads
    MergeParallelOut[head1_, head2_, seeds_, opt___]:=Module[{fn, b0, b1={}},
      Scan[(
        fn = "./beam_"//head1//ToString[#]//head2//".dat";
        If[FileQ[fn], 
          b0=ImportBeam[fn];
          If[~(Length@b1), b1=b0, b1=AppendBeam[b1, b0]],
          Print["!!!\tfile not exist.\n"//fn];])&, seeds];
      If[FileQ[fn], ExportBeam["./beam"//head2//".dat", b1]];
      ];    
    ]; ! With
