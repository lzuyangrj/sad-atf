# plot optic of ATF2

reset

set terminal postscript enhanced eps colour font 'Helvetica,24' #set the terminal to output eps file
set datafile commentschars "#"
set key font ",24"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "betx_lhcb1.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol b}_x [m]"

# set grid off
#set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'twiss_madx_lhcb1.dat' u 2:4 pt -1 ps 1.5 lc rgb 'red' lw 3 lt 1 w lp t "MADX" ,\
     'twiss_lhcb1.dat' u 2:4 pt -1 ps 1.5 lc rgb 'blue' lw 3 lt 2 w lp t "SAD k64" ,\

#########################################
reset

set terminal postscript enhanced eps colour font 'Helvetica,22' #set the terminal to output eps file
set datafile commentschars "#"
set key font ",22"
set key spacing 1.5
set key left top #set position of the titles of points of lines

set output "DX_lhcb1.eps"
set xlabel "{/Italic s} [m]"
set ylabel "{/Symbol h}_x [m]"

# set grid off
#set xrange[0:90]
# set format x "%.0e"
# set yrange[0:35]

plot  'twiss_madx_lhcb1.dat' u 2:8 pt -1 ps 1.5 lc rgb 'red' lw 3 lt 1 w lp t "MADX" ,\
     'twiss_lhcb1.dat' u 2:8 pt -1 ps 1.5 lc rgb 'blue' lw 3 lt 2 w lp t "SAD k64" ,\

